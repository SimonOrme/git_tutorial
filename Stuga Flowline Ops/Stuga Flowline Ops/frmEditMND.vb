﻿Public Class frmEditMND

    Public sMNDName As String = ""

#Region " Form - Load() "

    Private Sub frmEditMND_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim sFileName As String = prfOpdesignLoc + sMNDName + ".MND"

        If Not System.IO.File.Exists(sFileName) Then
            MsgBox(sFileName + " does not exist.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        FileOpen(1, sFileName, OpenMode.Input, OpenAccess.Read)
        Do While Not EOF(1)
            Dim s As String = LineInput(1)
            Me.rtMND.Text += s + vbCrLf

        Loop
        FileClose(1)

    End Sub

#End Region

#Region " Button - Save and Exit "

    Private Sub btnCloseStdOps_Click(sender As System.Object, e As System.EventArgs) Handles btnCloseStdOps.Click

        Dim sFileName As String = prfOpdesignLoc + sMNDName + ".MND"

        FileOpen(1, sFileName, OpenMode.Output)
        For Each s As String In Me.rtMND.Lines
            Print(1, s + vbCrLf)
        Next

        FileClose(1)

        Beep()

        Me.Close()

    End Sub

#End Region

End Class