﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.lblProfileMul = New System.Windows.Forms.Label()
        Me.txtProfileMul = New System.Windows.Forms.TextBox()
        Me.lblStdOpsMul = New System.Windows.Forms.Label()
        Me.txtStdOpsMul = New System.Windows.Forms.TextBox()
        Me.lblZStdOpDefMul = New System.Windows.Forms.Label()
        Me.lblProfileSaw = New System.Windows.Forms.Label()
        Me.lblOpdesign = New System.Windows.Forms.Label()
        Me.txtZStdOpDefMul = New System.Windows.Forms.TextBox()
        Me.txtProfileSaw = New System.Windows.Forms.TextBox()
        Me.txtOpdesign = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lvlDXF = New System.Windows.Forms.Label()
        Me.txtDXF = New System.Windows.Forms.TextBox()
        Me.chkNeuron = New System.Windows.Forms.CheckBox()
        Me.lblOpTypes = New System.Windows.Forms.Label()
        Me.txtOpTypes = New System.Windows.Forms.TextBox()
        Me.txtBatches = New System.Windows.Forms.TextBox()
        Me.lblBatches = New System.Windows.Forms.Label()
        Me.txtSystems = New System.Windows.Forms.TextBox()
        Me.lblSystemLoc = New System.Windows.Forms.Label()
        Me.txtHardwareMul = New System.Windows.Forms.TextBox()
        Me.lblHardwareMul = New System.Windows.Forms.Label()
        Me.txtPrepsMul = New System.Windows.Forms.TextBox()
        Me.lblPrepsMul = New System.Windows.Forms.Label()
        Me.grpMuls = New System.Windows.Forms.GroupBox()
        Me.grpSawFiles = New System.Windows.Forms.GroupBox()
        Me.grpFolders = New System.Windows.Forms.GroupBox()
        Me.lblDiagnose = New System.Windows.Forms.Label()
        Me.txtDiagnose = New System.Windows.Forms.TextBox()
        Me.butImport = New System.Windows.Forms.Button()
        Me.btnCreatePassword = New System.Windows.Forms.Button()
        Me.grpMuls.SuspendLayout()
        Me.grpSawFiles.SuspendLayout()
        Me.grpFolders.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblProfileMul
        '
        Me.lblProfileMul.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblProfileMul.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileMul.ForeColor = System.Drawing.Color.White
        Me.lblProfileMul.Location = New System.Drawing.Point(11, 19)
        Me.lblProfileMul.Name = "lblProfileMul"
        Me.lblProfileMul.Size = New System.Drawing.Size(232, 26)
        Me.lblProfileMul.TabIndex = 0
        Me.lblProfileMul.Text = "PROFILE.MUL Location:"
        Me.lblProfileMul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProfileMul
        '
        Me.txtProfileMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfileMul.Location = New System.Drawing.Point(249, 19)
        Me.txtProfileMul.Name = "txtProfileMul"
        Me.txtProfileMul.Size = New System.Drawing.Size(417, 26)
        Me.txtProfileMul.TabIndex = 1
        '
        'lblStdOpsMul
        '
        Me.lblStdOpsMul.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblStdOpsMul.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdOpsMul.ForeColor = System.Drawing.Color.White
        Me.lblStdOpsMul.Location = New System.Drawing.Point(11, 49)
        Me.lblStdOpsMul.Name = "lblStdOpsMul"
        Me.lblStdOpsMul.Size = New System.Drawing.Size(232, 26)
        Me.lblStdOpsMul.TabIndex = 2
        Me.lblStdOpsMul.Text = "STDOPS.MUL Location:"
        Me.lblStdOpsMul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStdOpsMul
        '
        Me.txtStdOpsMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStdOpsMul.Location = New System.Drawing.Point(249, 49)
        Me.txtStdOpsMul.Name = "txtStdOpsMul"
        Me.txtStdOpsMul.Size = New System.Drawing.Size(417, 26)
        Me.txtStdOpsMul.TabIndex = 3
        '
        'lblZStdOpDefMul
        '
        Me.lblZStdOpDefMul.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblZStdOpDefMul.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZStdOpDefMul.ForeColor = System.Drawing.Color.White
        Me.lblZStdOpDefMul.Location = New System.Drawing.Point(11, 79)
        Me.lblZStdOpDefMul.Name = "lblZStdOpDefMul"
        Me.lblZStdOpDefMul.Size = New System.Drawing.Size(232, 26)
        Me.lblZStdOpDefMul.TabIndex = 4
        Me.lblZStdOpDefMul.Text = "ZSTDOPDEF.MUL Location:"
        Me.lblZStdOpDefMul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProfileSaw
        '
        Me.lblProfileSaw.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblProfileSaw.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileSaw.ForeColor = System.Drawing.Color.White
        Me.lblProfileSaw.Location = New System.Drawing.Point(11, 19)
        Me.lblProfileSaw.Name = "lblProfileSaw"
        Me.lblProfileSaw.Size = New System.Drawing.Size(232, 26)
        Me.lblProfileSaw.TabIndex = 0
        Me.lblProfileSaw.Text = "PROFILE.SAW Location:"
        Me.lblProfileSaw.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOpdesign
        '
        Me.lblOpdesign.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblOpdesign.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpdesign.ForeColor = System.Drawing.Color.White
        Me.lblOpdesign.Location = New System.Drawing.Point(11, 16)
        Me.lblOpdesign.Name = "lblOpdesign"
        Me.lblOpdesign.Size = New System.Drawing.Size(232, 26)
        Me.lblOpdesign.TabIndex = 0
        Me.lblOpdesign.Text = "OPDESIGN Location:"
        Me.lblOpdesign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtZStdOpDefMul
        '
        Me.txtZStdOpDefMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZStdOpDefMul.Location = New System.Drawing.Point(249, 79)
        Me.txtZStdOpDefMul.Name = "txtZStdOpDefMul"
        Me.txtZStdOpDefMul.Size = New System.Drawing.Size(417, 26)
        Me.txtZStdOpDefMul.TabIndex = 5
        '
        'txtProfileSaw
        '
        Me.txtProfileSaw.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfileSaw.Location = New System.Drawing.Point(249, 19)
        Me.txtProfileSaw.Name = "txtProfileSaw"
        Me.txtProfileSaw.Size = New System.Drawing.Size(417, 26)
        Me.txtProfileSaw.TabIndex = 1
        '
        'txtOpdesign
        '
        Me.txtOpdesign.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpdesign.Location = New System.Drawing.Point(249, 16)
        Me.txtOpdesign.Name = "txtOpdesign"
        Me.txtOpdesign.Size = New System.Drawing.Size(417, 26)
        Me.txtOpdesign.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(561, 464)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(117, 33)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'lvlDXF
        '
        Me.lvlDXF.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lvlDXF.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvlDXF.ForeColor = System.Drawing.Color.White
        Me.lvlDXF.Location = New System.Drawing.Point(11, 76)
        Me.lvlDXF.Name = "lvlDXF"
        Me.lvlDXF.Size = New System.Drawing.Size(232, 26)
        Me.lvlDXF.TabIndex = 4
        Me.lvlDXF.Text = "DXF Location:"
        Me.lvlDXF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDXF
        '
        Me.txtDXF.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDXF.Location = New System.Drawing.Point(249, 76)
        Me.txtDXF.Name = "txtDXF"
        Me.txtDXF.Size = New System.Drawing.Size(417, 26)
        Me.txtDXF.TabIndex = 5
        '
        'chkNeuron
        '
        Me.chkNeuron.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNeuron.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNeuron.ForeColor = System.Drawing.Color.White
        Me.chkNeuron.Location = New System.Drawing.Point(26, 471)
        Me.chkNeuron.Name = "chkNeuron"
        Me.chkNeuron.Size = New System.Drawing.Size(249, 24)
        Me.chkNeuron.TabIndex = 3
        Me.chkNeuron.Text = "Use Neuron"
        Me.chkNeuron.UseVisualStyleBackColor = True
        '
        'lblOpTypes
        '
        Me.lblOpTypes.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblOpTypes.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpTypes.ForeColor = System.Drawing.Color.White
        Me.lblOpTypes.Location = New System.Drawing.Point(11, 169)
        Me.lblOpTypes.Name = "lblOpTypes"
        Me.lblOpTypes.Size = New System.Drawing.Size(232, 26)
        Me.lblOpTypes.TabIndex = 10
        Me.lblOpTypes.Text = "OPTYPES.MUL Location:"
        Me.lblOpTypes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpTypes
        '
        Me.txtOpTypes.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpTypes.Location = New System.Drawing.Point(249, 169)
        Me.txtOpTypes.Name = "txtOpTypes"
        Me.txtOpTypes.Size = New System.Drawing.Size(417, 26)
        Me.txtOpTypes.TabIndex = 11
        '
        'txtBatches
        '
        Me.txtBatches.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatches.Location = New System.Drawing.Point(249, 46)
        Me.txtBatches.Name = "txtBatches"
        Me.txtBatches.Size = New System.Drawing.Size(417, 26)
        Me.txtBatches.TabIndex = 3
        '
        'lblBatches
        '
        Me.lblBatches.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblBatches.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatches.ForeColor = System.Drawing.Color.White
        Me.lblBatches.Location = New System.Drawing.Point(11, 46)
        Me.lblBatches.Name = "lblBatches"
        Me.lblBatches.Size = New System.Drawing.Size(232, 26)
        Me.lblBatches.TabIndex = 2
        Me.lblBatches.Text = "Batch Archive Location:"
        Me.lblBatches.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSystems
        '
        Me.txtSystems.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSystems.Location = New System.Drawing.Point(249, 199)
        Me.txtSystems.Name = "txtSystems"
        Me.txtSystems.Size = New System.Drawing.Size(417, 26)
        Me.txtSystems.TabIndex = 13
        '
        'lblSystemLoc
        '
        Me.lblSystemLoc.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblSystemLoc.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSystemLoc.ForeColor = System.Drawing.Color.White
        Me.lblSystemLoc.Location = New System.Drawing.Point(11, 199)
        Me.lblSystemLoc.Name = "lblSystemLoc"
        Me.lblSystemLoc.Size = New System.Drawing.Size(232, 26)
        Me.lblSystemLoc.TabIndex = 12
        Me.lblSystemLoc.Text = "SYSTEMS.MUL Location:"
        Me.lblSystemLoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHardwareMul
        '
        Me.txtHardwareMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHardwareMul.Location = New System.Drawing.Point(249, 109)
        Me.txtHardwareMul.Name = "txtHardwareMul"
        Me.txtHardwareMul.Size = New System.Drawing.Size(417, 26)
        Me.txtHardwareMul.TabIndex = 7
        '
        'lblHardwareMul
        '
        Me.lblHardwareMul.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblHardwareMul.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHardwareMul.ForeColor = System.Drawing.Color.White
        Me.lblHardwareMul.Location = New System.Drawing.Point(11, 109)
        Me.lblHardwareMul.Name = "lblHardwareMul"
        Me.lblHardwareMul.Size = New System.Drawing.Size(232, 26)
        Me.lblHardwareMul.TabIndex = 6
        Me.lblHardwareMul.Text = "HARDWARE.MUL Location:"
        Me.lblHardwareMul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPrepsMul
        '
        Me.txtPrepsMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrepsMul.Location = New System.Drawing.Point(249, 139)
        Me.txtPrepsMul.Name = "txtPrepsMul"
        Me.txtPrepsMul.Size = New System.Drawing.Size(417, 26)
        Me.txtPrepsMul.TabIndex = 9
        '
        'lblPrepsMul
        '
        Me.lblPrepsMul.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblPrepsMul.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrepsMul.ForeColor = System.Drawing.Color.White
        Me.lblPrepsMul.Location = New System.Drawing.Point(11, 139)
        Me.lblPrepsMul.Name = "lblPrepsMul"
        Me.lblPrepsMul.Size = New System.Drawing.Size(232, 26)
        Me.lblPrepsMul.TabIndex = 8
        Me.lblPrepsMul.Text = "PREPS.MUL Location:"
        Me.lblPrepsMul.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpMuls
        '
        Me.grpMuls.Controls.Add(Me.txtProfileMul)
        Me.grpMuls.Controls.Add(Me.txtPrepsMul)
        Me.grpMuls.Controls.Add(Me.lblProfileMul)
        Me.grpMuls.Controls.Add(Me.lblPrepsMul)
        Me.grpMuls.Controls.Add(Me.lblStdOpsMul)
        Me.grpMuls.Controls.Add(Me.txtHardwareMul)
        Me.grpMuls.Controls.Add(Me.txtStdOpsMul)
        Me.grpMuls.Controls.Add(Me.lblHardwareMul)
        Me.grpMuls.Controls.Add(Me.lblZStdOpDefMul)
        Me.grpMuls.Controls.Add(Me.txtSystems)
        Me.grpMuls.Controls.Add(Me.txtZStdOpDefMul)
        Me.grpMuls.Controls.Add(Me.lblSystemLoc)
        Me.grpMuls.Controls.Add(Me.lblOpTypes)
        Me.grpMuls.Controls.Add(Me.txtOpTypes)
        Me.grpMuls.ForeColor = System.Drawing.Color.White
        Me.grpMuls.Location = New System.Drawing.Point(12, 12)
        Me.grpMuls.Name = "grpMuls"
        Me.grpMuls.Size = New System.Drawing.Size(675, 234)
        Me.grpMuls.TabIndex = 0
        Me.grpMuls.TabStop = False
        Me.grpMuls.Text = "MUL Files"
        '
        'grpSawFiles
        '
        Me.grpSawFiles.Controls.Add(Me.txtProfileSaw)
        Me.grpSawFiles.Controls.Add(Me.lblProfileSaw)
        Me.grpSawFiles.ForeColor = System.Drawing.Color.White
        Me.grpSawFiles.Location = New System.Drawing.Point(12, 252)
        Me.grpSawFiles.Name = "grpSawFiles"
        Me.grpSawFiles.Size = New System.Drawing.Size(675, 55)
        Me.grpSawFiles.TabIndex = 1
        Me.grpSawFiles.TabStop = False
        Me.grpSawFiles.Text = "Saw Files"
        '
        'grpFolders
        '
        Me.grpFolders.Controls.Add(Me.lblDiagnose)
        Me.grpFolders.Controls.Add(Me.txtDiagnose)
        Me.grpFolders.Controls.Add(Me.lblOpdesign)
        Me.grpFolders.Controls.Add(Me.txtOpdesign)
        Me.grpFolders.Controls.Add(Me.lvlDXF)
        Me.grpFolders.Controls.Add(Me.lblBatches)
        Me.grpFolders.Controls.Add(Me.txtDXF)
        Me.grpFolders.Controls.Add(Me.txtBatches)
        Me.grpFolders.ForeColor = System.Drawing.Color.White
        Me.grpFolders.Location = New System.Drawing.Point(12, 313)
        Me.grpFolders.Name = "grpFolders"
        Me.grpFolders.Size = New System.Drawing.Size(675, 145)
        Me.grpFolders.TabIndex = 2
        Me.grpFolders.TabStop = False
        Me.grpFolders.Text = "Folders"
        '
        'lblDiagnose
        '
        Me.lblDiagnose.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblDiagnose.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiagnose.ForeColor = System.Drawing.Color.White
        Me.lblDiagnose.Location = New System.Drawing.Point(11, 106)
        Me.lblDiagnose.Name = "lblDiagnose"
        Me.lblDiagnose.Size = New System.Drawing.Size(232, 26)
        Me.lblDiagnose.TabIndex = 6
        Me.lblDiagnose.Text = "Diagnose Location:"
        Me.lblDiagnose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDiagnose
        '
        Me.txtDiagnose.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiagnose.Location = New System.Drawing.Point(249, 106)
        Me.txtDiagnose.Name = "txtDiagnose"
        Me.txtDiagnose.Size = New System.Drawing.Size(417, 26)
        Me.txtDiagnose.TabIndex = 7
        '
        'butImport
        '
        Me.butImport.BackColor = System.Drawing.Color.White
        Me.butImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butImport.Location = New System.Drawing.Point(312, 464)
        Me.butImport.Name = "butImport"
        Me.butImport.Size = New System.Drawing.Size(231, 33)
        Me.butImport.TabIndex = 5
        Me.butImport.Text = "Import Legacy Data"
        Me.butImport.UseVisualStyleBackColor = False
        '
        'btnCreatePassword
        '
        Me.btnCreatePassword.BackColor = System.Drawing.Color.White
        Me.btnCreatePassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreatePassword.Location = New System.Drawing.Point(312, 508)
        Me.btnCreatePassword.Name = "btnCreatePassword"
        Me.btnCreatePassword.Size = New System.Drawing.Size(231, 33)
        Me.btnCreatePassword.TabIndex = 6
        Me.btnCreatePassword.Text = "Change Password"
        Me.btnCreatePassword.UseVisualStyleBackColor = False
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Navy
        Me.ClientSize = New System.Drawing.Size(699, 553)
        Me.Controls.Add(Me.btnCreatePassword)
        Me.Controls.Add(Me.butImport)
        Me.Controls.Add(Me.grpFolders)
        Me.Controls.Add(Me.grpSawFiles)
        Me.Controls.Add(Me.grpMuls)
        Me.Controls.Add(Me.chkNeuron)
        Me.Controls.Add(Me.btnSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSettings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Settings"
        Me.grpMuls.ResumeLayout(False)
        Me.grpMuls.PerformLayout()
        Me.grpSawFiles.ResumeLayout(False)
        Me.grpSawFiles.PerformLayout()
        Me.grpFolders.ResumeLayout(False)
        Me.grpFolders.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblProfileMul As System.Windows.Forms.Label
    Friend WithEvents txtProfileMul As System.Windows.Forms.TextBox
    Friend WithEvents lblStdOpsMul As System.Windows.Forms.Label
    Friend WithEvents txtStdOpsMul As System.Windows.Forms.TextBox
    Friend WithEvents lblZStdOpDefMul As System.Windows.Forms.Label
    Friend WithEvents lblProfileSaw As System.Windows.Forms.Label
    Friend WithEvents lblOpdesign As System.Windows.Forms.Label
    Friend WithEvents txtZStdOpDefMul As System.Windows.Forms.TextBox
    Friend WithEvents txtProfileSaw As System.Windows.Forms.TextBox
    Friend WithEvents txtOpdesign As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lvlDXF As System.Windows.Forms.Label
    Friend WithEvents txtDXF As System.Windows.Forms.TextBox
    Friend WithEvents chkNeuron As System.Windows.Forms.CheckBox
    Friend WithEvents lblOpTypes As System.Windows.Forms.Label
    Friend WithEvents txtOpTypes As System.Windows.Forms.TextBox
    Friend WithEvents txtBatches As System.Windows.Forms.TextBox
    Friend WithEvents lblBatches As System.Windows.Forms.Label
    Friend WithEvents txtSystems As System.Windows.Forms.TextBox
    Friend WithEvents lblSystemLoc As System.Windows.Forms.Label
    Friend WithEvents txtHardwareMul As System.Windows.Forms.TextBox
    Friend WithEvents lblHardwareMul As System.Windows.Forms.Label
    Friend WithEvents txtPrepsMul As System.Windows.Forms.TextBox
    Friend WithEvents lblPrepsMul As System.Windows.Forms.Label
    Friend WithEvents grpMuls As System.Windows.Forms.GroupBox
    Friend WithEvents grpSawFiles As System.Windows.Forms.GroupBox
    Friend WithEvents grpFolders As System.Windows.Forms.GroupBox
    Friend WithEvents butImport As System.Windows.Forms.Button
    Friend WithEvents lblDiagnose As System.Windows.Forms.Label
    Friend WithEvents txtDiagnose As System.Windows.Forms.TextBox
    Friend WithEvents btnCreatePassword As System.Windows.Forms.Button
End Class
