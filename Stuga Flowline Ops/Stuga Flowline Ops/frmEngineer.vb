﻿Imports System.Data.SqlClient
Imports System.IO

Public Class frmEngineer

#Region " Declare Variables "

    Private binitialising = True

    Private PosCalc As New clsPosCalc
    Private ParseMND As New clsParseMND

    Dim MouseX As Short
    Dim MouseY As Short
    Dim DragMode As Byte

    Dim bCreating As Boolean = False
    Private RedrawOK As Boolean = True 'Stops Numeric Edit Boxes from redrawing when setting their values

#End Region

#Region " Form - Load "

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'Set Zoom
        nebZoom.Value = iScale

        'Setup Poscalc Class
        InitialisePosCalc()

        'MsgBox(prfSelectedProfileSuite)

        BindProfileGrid(dgvProfile, True, "")
        BindZStdOpDefGrid(dgvStdOps)
        BindSystemsGrid(dgvSuites)
        BindHardwareGrid(dgvHardware)
        BindOpTypesGrid(dgvOpTypes)
        FillOperationsGrid(dgvOps, "", False, "", False, "", False, True)
        dgvOps.AllowUserToAddRows = False

        FillSystems(cmbProfSuite)
        FillProfileTypes(cmbType)

        'MsgBox(prfSelectedProfileSuite)

        cmbProfSuite.SelectedValue = prfSelectedProfileSuite
        cmbType.SelectedValue = prfSelectedProfileType
        chkShowReplicates.Checked = prfShowReplicates

        binitialising = False

    End Sub

#End Region

    'Profile Tab
#Region " Button - Edit Profile "

    Private Sub btnEditProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditProfile.Click

        Dim sCellValue As String
        Dim iSelectedRows As Integer = dgvProfile.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfile.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            MsgBox("Please select a profile.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            Dim sr As DataGridViewRow = dgvProfile.SelectedRows(0)
            sCellValue = sr.Cells.Item(0).Value

        Else

            'Cell Selected
            Dim sc As DataGridViewCell = dgvProfile.SelectedCells(0)
            Dim ri As Integer = sc.RowIndex
            sCellValue = dgvProfile.Rows(ri).Cells.Item(0).Value

        End If

        SaveProfileMUL()

        Dim frmProfile As New frmProfile
        frmProfile.txtProfileCode.Text = sCellValue
        frmProfile.Show()

        Me.Close()

    End Sub

#End Region
#Region " Button - Save Profile "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        SaveProfileMUL()

        MsgBox("Profiles Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo Profile "

    Private Sub btnUndoProfiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoProfiles.Click

        'Get Profile Data from Profile.Mul
        tblProfile = GetProfileTable()
        dsProfile.Tables.Clear()
        dsProfile.Tables.Add(tblProfile)

        Dim sFilter As String = ""
        If cmbProfSuite.SelectedValue = "ALL" And cmbType.SelectedValue.ToString = 0 Then
            sFilter = ""
        ElseIf cmbProfSuite.SelectedValue = "ALL" Then
            sFilter = "ProfileTypeID = " + cmbType.SelectedValue.ToString
        ElseIf cmbType.SelectedValue = 0 Then
            sFilter = "System = '" + cmbProfSuite.SelectedValue + "'"
        Else
            sFilter = "ProfileTypeID = " + cmbType.SelectedValue.ToString + " AND System = '" + cmbProfSuite.SelectedValue + "'"
        End If

        BindProfileGrid(dgvProfile, True, sFilter)

    End Sub

#End Region
#Region " Button - Exit Profile "

    Private Sub btnCloseProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseProfile.Click

        SaveProfileMUL()
        Me.Close()

    End Sub

#End Region
#Region " Event - dgvProfile_DataError "

    Private Sub dgvProfile_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvProfile.DataError

        'Find Row an Column Which Caused the error
        Dim iColumn As Integer = e.ColumnIndex
        Dim iRow As Integer = e.RowIndex

        Select Case iColumn

            Case 2          'if Flip Column is at fault
                If dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "0" Then dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "N"
                If dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "1" Then dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "Y"

            Case 10         'If Systems Column was at Fault

                'Get the Cell Value for the profile suite
                Dim sSuite As String = dgvProfile.Rows(iRow).Cells.Item(iColumn).Value

                'Ask the user for a description
                Dim sDesc As String = ""
                Do While Len(sDesc) = 0
                    sDesc = InputBox("Please Enter the Suite description for " + sSuite + "." + vbCrLf + "This will then be added to SYSTEMS.MUL", sSuite & " does not exist in the Systems table.", "")
                Loop

                'Set the system to use this Suite
                Dim bUse As Boolean = True

                'add new Suite to the Systems Table
                Dim rRow As DataRow
                rRow = tblSystems.NewRow
                rRow.Item("Suite") = sSuite
                rRow.Item("Description") = sDesc
                rRow.Item("Used") = bUse
                tblSystems.Rows.Add(rRow)

                'Update Systems.Mul
                SaveTblSystems()

            Case Else

                'Display the error message
                MsgBox(e.Exception.Message)

        End Select

    End Sub

#End Region
#Region " Event - cmbSuite_SelectedValueChanged "

    Private Sub cmbSuite_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProfSuite.SelectedValueChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
            If Not binitialising Then prfSelectedProfileSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
        Catch ex As Exception

        End Try
        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region " Event - cmbType_SelectedValueChanged "

    Private Sub cmbType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType.SelectedValueChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
            If Not binitialising Then prfSelectedProfileType = cmbType.SelectedValue
        Catch ex As Exception

        End Try
        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region " Event - chkShowReplicates_CheckedChanged() "

    Private Sub chkShowReplicates_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowReplicates.CheckedChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
        Catch ex As Exception

        End Try

        If Not binitialising Then prfShowReplicates = chkShowReplicates.Checked

        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region " Sub - FilterProfiles() "

    Sub FilterProfiles(ByVal sSuite As String, ByVal iType As Integer)

        Dim sFilter As String = ""

        If sSuite = "" Or iType = -1 Then
            Exit Sub
        End If

        If sSuite = "ALL" And iType = 0 Then
            sFilter = ""
        ElseIf sSuite = "ALL" Then
            sFilter = "ProfileTypeID = " + iType.ToString
        ElseIf iType = 0 Then
            sFilter = "System = '" + sSuite + "'"
        Else
            sFilter = "ProfileTypeID = " + iType.ToString + " AND System = '" + sSuite + "'"
        End If
        If Not chkShowReplicates.Checked And Len(sFilter) > 0 Then sFilter += " AND"
        If Not chkShowReplicates.Checked Then sFilter += " (ReplicateID = '' OR ReplicateID IS NULL)"

        BindProfileGrid(dgvProfile, True, sFilter)

        Console.WriteLine("*" + sSuite + "*" + iType.ToString + "*" + sFilter + "*")

    End Sub

#End Region
#Region " Context Menu - Copy (Profile) "

    Private Sub CopyToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCopyProfile.Click

        'Check user has selected rows
        If dgvProfile.SelectedRows.Count = 0 Then
            MsgBox("Please Select the Row(s) to be Copied.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Initialise String with header line
        Dim s As String = "CodeName	ZSection	Flip	Width	GripPosition	StdLength	ReverseLoad	ZRebate	Height	EuroHeight	System	ProfileName	ReplicateID	ProfileTypeID	CrossSection	NeuronStr" + vbCrLf

        'Build Cells into Clipboard String
        For Each dr As DataGridViewRow In dgvProfile.SelectedRows
            For Each cCell As DataGridViewCell In dr.Cells
                s += cCell.Value.ToString + vbTab
            Next
            s = Mid(s, 1, Len(s) - 1) + vbCrLf
        Next

        'Post string to clipboard
        Clipboard.SetText(s)

        'MsgBox(s)

    End Sub

#End Region
#Region " Context Menu - Paste (Profile) "

    Private Sub PasteToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem1.Click

        'Get Text from the Clipboard
        Dim sClipboard As String = Clipboard.GetText()

        'Check the Text is in a valid format
        If Mid(sClipboard, 1, 8) <> "CodeName" Then
            MsgBox("No valid text exists", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Display Clipboard Text
        'MsgBox(sClipboard)

        'Split Clipboard into separate operations
        Dim sLines() As String = sClipboard.Split(vbCrLf)

        'Import the clipboard line for each operation separately ("" Means keep the profile same as the copied line)
        For Each sLine As String In sLines
            ImportProfileClipboardLine(sLine)
        Next

    End Sub

#End Region
#Region " Context Menu - Opening Event "

    Private Sub ctmProfile_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ctmProfile.Opening

        If dgvProfile.SelectedRows.Count = 0 Then
            ctmProfile.Items("tsmiCopyProfile").Enabled = False
        Else
            ctmProfile.Items("tsmiCopyProfile").Enabled = True
        End If

    End Sub

#End Region

    'Standard Ops Tab
#Region " Button - Edit StdOps "

    Private Sub btnEditStdop_Click(sender As System.Object, e As System.EventArgs) Handles btnEditStdop.Click

        Dim sCellValue As String
        Dim iSelectedRows As Integer = dgvStdOps.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvStdOps.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            MsgBox("Please select an standard op.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            Dim sr As DataGridViewRow = dgvStdOps.SelectedRows(0)
            sCellValue = sr.Cells.Item(0).Value

        Else

            'Cell Selected
            Dim sc As DataGridViewCell = dgvStdOps.SelectedCells(0)
            Dim ri As Integer = sc.RowIndex
            sCellValue = dgvStdOps.Rows(ri).Cells.Item(0).Value

        End If

        Dim frmMND As New frmEditMND
        frmMND.sMNDName = sCellValue
        frmMND.Show()

    End Sub

#End Region
#Region " Button - Save StdOps "

    Private Sub btnSaveStdOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveStdOps.Click

        SaveZStdOpDefMul()

        MsgBox("StdOpDef Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo StdOps "

    Private Sub btnCancelStdOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelStdOps.Click

        'Get Op Definition Data from ZStdOpDef.Mul
        tblStdOpDef = GetStdOpDefTable()
        dsStdOpDef.Tables.Clear()
        dsStdOpDef.Tables.Add(tblStdOpDef)

        BindZStdOpDefGrid(dgvStdOps)

    End Sub

#End Region
#Region " Button - Exit StdOps "

    Private Sub btnCloseStdOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseStdOps.Click

        SaveStdOpsMUL()
        Me.Close()

    End Sub

#End Region
#Region " Event - dgvStdOps_DataError() "

    Private Sub dgvStdOps_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStdOps.DataError

        'Find Row and Column Which Caused the error
        Dim iColumn As Integer = e.ColumnIndex
        Dim iRow As Integer = e.RowIndex

        Select Case iColumn

            Case 2          'if Std Op Type Column is at fault (which it will be)

                dgvStdOps.Rows(iRow).Cells.Item(iColumn).Value = 0

                'Update Systems.Mul
                SaveZStdOpDefMul()

            Case Else

                'Display the error message
                MsgBox(e.Exception.Message)

        End Select

    End Sub

#End Region

    'Profile Suites Tab
#Region " Button - Save Suites "

    Private Sub btnSaveSuites_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSuites.Click

        SaveTblSystems()
        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
        Catch ex As Exception

        End Try
        FilterProfiles(sSuite, iType)

        MsgBox("Suites Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo Suites "

    Private Sub btnUndoSuites_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoSuites.Click

        'Get System Data from Database
        tblSystems = GetTblSystems()
        dsSystems.Tables.Clear()
        dsSystems.Tables.Add(tblSystems)

        BindSystemsGrid(dgvSuites)

    End Sub

#End Region
#Region " Button - Exit Suites "

    Private Sub btnCloseSuites_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseSuites.Click

        SaveTblSystems()
        Me.Close()

    End Sub

#End Region

    'Hardware Design Tab
#Region " Button - Edit Hardware "

    Private Sub btnEditHardware_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditHardware.Click

        Dim sCode As String
        Dim sDesc As String
        Dim iSelectedRows As Integer = dgvHardware.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvHardware.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            MsgBox("Please select Hardware.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            Dim sr As DataGridViewRow = dgvHardware.SelectedRows(0)
            sCode = sr.Cells.Item(0).Value
            sDesc = sr.Cells.Item(1).Value

        Else

            'Cell Selected
            Dim sc As DataGridViewCell = dgvHardware.SelectedCells(0)
            Dim ri As Integer = sc.RowIndex
            sCode = dgvHardware.Rows(ri).Cells.Item(0).Value
            sDesc = dgvHardware.Rows(ri).Cells.Item(1).Value

        End If

        SaveHardware()

        Dim frmPreps As New frmPreps
        frmPreps.HardwareCode = sCode
        frmPreps.txtHardwareCode.Text = sCode
        frmPreps.txtName.Text = sDesc
        frmPreps.Show()

    End Sub

#End Region
#Region " Button - SaveHardware "

    Private Sub btnSaveHardware_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveHardware.Click

        SaveHardware()
        SavePreps()

        MsgBox("Hardware Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - UndoHardware "

    Private Sub btnUndoHardware_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoHardware.Click

        'Get Hardware
        tblHardware = CreateTblHardware()
        dsHardware.Tables.Clear()
        dsHardware.Tables.Add(tblHardware)

        'Get Preps
        tblPreps = GetPrepsTable()
        dsPreps.Tables.Clear()
        dsPreps.Tables.Add(tblPreps)

        BindHardwareGrid(dgvHardware)

    End Sub

#End Region
#Region " Button - ExitHardware "

    Private Sub btnCloseHardware_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseHardware.Click

        SaveHardware()
        SavePreps()
        Me.Close()

    End Sub

#End Region

    'Op Types Tab
#Region " Button - SaveOpTypes "

    Private Sub btnSaveOpTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveOpTypes.Click

        SaveTblOpTypes()

        MsgBox("OpTypes Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - UndoOpTypes "

    Private Sub btnUndoOpTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoOpTypes.Click

        'Get System Data from Database
        tblOpTypes = GetOpTypesTable()
        dsOpTypes.Tables.Clear()
        dsOpTypes.Tables.Add(tblOpTypes)

        BindOpTypesGrid(dgvOpTypes)

    End Sub

#End Region
#Region " Button - ExitOpTypes "

    Private Sub btnCloseOpTypes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseOpTypes.Click

        SaveTblOpTypes()

        Me.Close()

    End Sub

#End Region

    'Operations Tab
#Region " Button - Speedy Ops "

    Private Sub btnSpeedy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpeedy.Click

        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        Dim tblStdOpsCopy As DataTable = tblStdOps.Copy
        Dim dNewY As Double = 0
        Dim dNewZ As Double = 0

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sStdOpCode As String = rRow.Item("StdOpCode")

            If sStdOpCode.ToUpper = "DRAIN" And chkDrain.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") + (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") - (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "DRAINS"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If

            If sStdOpCode.ToUpper = "DRAINEQ" And chkDrainEQ.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") + (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") - (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "DRAINEQS"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
            If sStdOpCode.ToUpper = "SP3" And chkSP3.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") + (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") - (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "SP3S"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
            If sStdOpCode.ToUpper = "SP5" And chkSP5.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") + (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") - (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "SP5S"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
        Next

        tblStdOps = tblStdOpsCopy

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Remove Slow Ops "

    Private Sub btnRemoveSlow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSlow.Click

        'Check User is Sure
        Dim mbrReply As MsgBoxResult = MsgBox("This will remove all op types ticked in the Speedy Ops box." & vbCrLf & "Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        'Initialise Variables
        Dim sDeletionRows As String = ""
        Dim rcRows As DataRowCollection = tblStdOps.Rows
        Dim iCount As Integer = rcRows.Count
        Dim iloop As Integer = 0

        'find which rows are to be deleted
        For iloop = 0 To iCount - 1
            Dim sStdOpCode As String = rcRows(iloop).Item("StdOpCode")
            If sStdOpCode.ToUpper = "DRAIN" And chkDrain.Checked Then
                sDeletionRows += iloop.ToString + ","
            End If
            If sStdOpCode.ToUpper = "SP3" And chkSP3.Checked Then
                sDeletionRows += iloop.ToString + ","
            End If
            If sStdOpCode.ToUpper = "SP5" And chkSP5.Checked Then
                sDeletionRows += iloop.ToString + ","
            End If
            If sStdOpCode.ToUpper = "DRAINEQ" And chkDrainEQ.Checked Then
                sDeletionRows += iloop.ToString + ","
            End If
        Next

        'If deletion string is blank exit routine
        If Len(sDeletionRows) = 0 Then
            MsgBox("No Operations to Delete", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Delete Rows
        Dim iDeletions As Integer = 0
        Dim sRows() As String = sDeletionRows.Split(",")
        For Each sRow As String In sRows
            If chNull(sRow) <> "" Then
                'Try
                'MsgBox(rcRows(CInt(sRow) - iDeletions).Item("StdOpCode"))
                'Catch ex As Exception
                'MsgBox("OOPS")
                'End Try
                rcRows(CInt(sRow) - iDeletions).Delete()
            End If
            iDeletions += 1
        Next
        tblStdOps.AcceptChanges()

        'Save Changes
        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - UnSpeedy Ops "

    Private Sub btnUnSpeedy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnSpeedy.Click

        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        Dim tblStdOpsCopy As DataTable = tblStdOps.Copy
        Dim dNewY As Double = 0
        Dim dNewZ As Double = 0

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sStdOpCode As String = rRow.Item("StdOpCode")

            If sStdOpCode.ToUpper = "DRAINS" And chkDrain.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") - (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") + (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "DRAIN"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If

            If sStdOpCode.ToUpper = "DRAINEQS" And chkDrainEQ.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") - (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") + (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "DRAINEQ"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
            If sStdOpCode.ToUpper = "SP3S" And chkSP3.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") - (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") + (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "SP3"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
            If sStdOpCode.ToUpper = "SP5S" And chkSP5.Checked Then

                'Find New Positions
                dNewY = rRow.Item("OpVariable1") - (rRow.Item("OpVariable4") * Math.Sin(rRow.Item("OpVariable3") / 180 * 3.14159265358979))
                dNewZ = rRow.Item("OpVariable2") + (rRow.Item("OpVariable4") * Math.Cos(rRow.Item("OpVariable3") / 180 * 3.14159265358979))

                'Add New Row
                Dim rNewRow As DataRow = tblStdOpsCopy.NewRow
                rNewRow.Item("ProfileCode") = rRow.Item("ProfileCode")
                rNewRow.Item("OpToolCode") = rRow.Item("OpToolCode")
                rNewRow.Item("StdOpCode") = "SP5"
                rNewRow.Item("OpDescription") = rRow.Item("OpDescription")
                rNewRow.Item("Use") = True
                rNewRow.Item("OpVariable1") = CInt(dNewY * 10) / 10
                rNewRow.Item("OpVariable2") = CInt(dNewZ * 10) / 10
                rNewRow.Item("OpVariable3") = rRow.Item("OpVariable3")
                rNewRow.Item("OpVariable4") = rRow.Item("OpVariable4")
                rNewRow.Item("OpVariable5") = rRow.Item("OpVariable5")
                rNewRow.Item("OpVariable6") = rRow.Item("OpVariable6")
                rNewRow.Item("OpVariable7") = rRow.Item("OpVariable7")
                rNewRow.Item("OpVariable8") = rRow.Item("OpVariable8")
                tblStdOpsCopy.Rows.Add(rNewRow)

            End If
        Next

        tblStdOps = tblStdOpsCopy

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)


    End Sub

#End Region
#Region " Button - Reset Notches "

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        'Check if user is sure
        Dim mbrReply As MsgBoxResult
        mbrReply = MsgBox("this will reset all V/Y depths to default Values" + vbCrLf + "e.g. VF320 will be set to 32mm" + vbCrLf + "Ok to Continue?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        'Dim tblStdOpsCopy As DataTable = tblStdOps.Copy
        Dim sVToolCode As String = ""
        Dim dNewDepth As Double = 0

        For Each rRow As DataRow In tblStdOps.Rows

            sVToolCode = rRow.Item("OpToolCode")

            'Reset V Notches
            If Mid(sVToolCode.ToUpper, 1, 2) = "VF" Or Mid(sVToolCode.ToUpper, 1, 2) = "VR" Then

                'Get Depth
                dNewDepth = CDbl(Mid(sVToolCode, 3)) / 10

                'Set New Depth
                rRow.Item("OpVariable1") = dNewDepth

            End If

            'Reset Front Y Notches
            If Mid(sVToolCode.ToUpper, 1, 3) = "YFL" Or Mid(sVToolCode.ToUpper, 1, 3) = "YFR" Then

                'Get Depth
                dNewDepth = CDbl(Mid(sVToolCode, 4)) / 10

                'Set New Depth
                rRow.Item("OpVariable1") = dNewDepth

            End If

            'Reset Rear Y Notches
            If Mid(sVToolCode.ToUpper, 1, 3) = "YRL" Or Mid(sVToolCode.ToUpper, 1, 3) = "YRR" Then

                'Get Depth
                dNewDepth = CDbl(Mid(sVToolCode, 4)) / 10

                'Set New Depth
                rRow.Item("OpVariable1") = dNewDepth

            End If

        Next

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Update Column "

    Private Sub btnUpdateCol_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateCol.Click

        'Declare Variables
        Dim iCol As Integer = 0

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvOps.SelectedRows.Count
        If iSelectedRows = 0 Then
            MsgBox("No Row(s) selected", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Check if Sure
        Dim mbrReply As MsgBoxResult = MsgBox("Are you sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        'Get the Colum Number for the selected column
        Select Case Mid(Me.cmbColumn.Text, 1, 5)
            Case "Varia"
                iCol = CInt(Mid(Me.cmbColumn.Text, Len(Me.cmbColumn.Text))) + 5
            Case "Tool "
                iCol = 2
            Case "Descr"
                iCol = 5
            Case Else
                MsgBox("Unknown Column", MsgBoxStyle.Exclamation)
                Exit Sub
        End Select

        'Iterate through the Selected Ops and replace the value for the selected rows
        For Each dgvr As DataGridViewRow In dgvOps.SelectedRows
            dgvr.Cells(iCol).Value = Me.txtNewValue.Text
        Next

        'Update the Grid

    End Sub

#End Region
#Region " Button - Show Options "

    Private Sub btnShow_Click(sender As System.Object, e As System.EventArgs) Handles btnShow.Click

        If btnShow.Text = "Hide Options" Then
            btnShow.Text = "Show Options"
        Else
            btnShow.Text = "Hide Options"
        End If

        Panel16.Visible = Not Panel16.Visible

    End Sub

#End Region
#Region " Button - Hide Preview "

    Private Sub btnHidePreview_Click(sender As System.Object, e As System.EventArgs) Handles btnHidePreview.Click

        If btnHidePreview.Text = "Hide Preview" Then
            btnHidePreview.Text = "Show Preview"
        Else
            btnHidePreview.Text = "Hide Preview"
        End If

        Panel13.Visible = Not Panel13.Visible
        Splitter2.Visible = Not Splitter2.Visible

    End Sub

#End Region
#Region " Button - SaveOps "

    Private Sub btnSaveOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveOps.Click

        SaveStdOpsMUL()

        MsgBox("Ops Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - UndoOps "

    Private Sub btnUndoOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoOps.Click

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Button - ExitOps "

    Private Sub btnExitOps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExitOps.Click

        SaveStdOpsMUL()

        Me.Close()

    End Sub

#End Region
#Region " Event - txtToolCode_TextChanged "

    Private Sub txtToolCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToolCode.TextChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - txtStdOp_TextChanged "

    Private Sub txtStdOp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdOp.TextChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - txtProfileCode_TextChanged "

    Private Sub txtProfileCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProfileCode.TextChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - rbProfEq_CheckedChanged "

    Private Sub rbProfEQ_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbProfEQ.CheckedChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - rbStdOpEq_CheckedChanged "

    Private Sub rbStdOpEQ_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbStdOpEQ.CheckedChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - rbToolEQ_CheckedChanged "

    Private Sub rbToolEQ_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbToolEQ.CheckedChanged

        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

    End Sub

#End Region
#Region " Event - dgvOps_SelectionChanged "

    Private Sub dgvOps_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles dgvOps.Paint

        UpdateOpCount(dgvOps)

    End Sub

    Private Sub dgvOps_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOps.SelectionChanged


        'Declare Variables
        Dim sString As String = ""
        Dim V1 As Double = 0
        Dim V2 As Double = 0
        Dim V3 As Double = 0
        Dim V4 As Double = 0
        Dim V5 As Double = 0
        Dim V6 As Double = 0
        Dim V7 As Double = 0
        Dim V8 As Double = 0

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvOps.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvOps.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvOps.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvOps.SelectedCells(0).RowIndex
            sr = dgvOps.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get MND File and Variables
        Try
            sString = sr.Cells(3).Value
            V1 = sr.Cells(6).Value
            V2 = sr.Cells(7).Value
            V3 = sr.Cells(8).Value
            V4 = sr.Cells(9).Value
            V5 = sr.Cells(10).Value
            V6 = sr.Cells(11).Value
            V7 = sr.Cells(12).Value
            V8 = sr.Cells(13).Value
        Catch ex As Exception
            Exit Sub
        End Try

        'Get Profile Width
        ProfileWidth = GetProfileWidth(sr.Cells(1).Value)
        ProfileHeight = GetProfileHeight(sr.Cells(1).Value)
        'Fill Headers
        'Dim rRow As DataRow() = tblStdOpDef.Select("StdOpCode = '" + sString + "'")   SELECT COMMAND REQUIRES .NET 3.5 (XP and Later Only)
        Dim rRow As DataRow
        For Each temprow As DataRow In tblStdOpDef.Rows
            If temprow.Item(0).ToString = sString Then
                rRow = temprow
                dgvOps.Columns(6).HeaderText = rRow.Item(3).ToString
                dgvOps.Columns(7).HeaderText = rRow.Item(4).ToString
                dgvOps.Columns(8).HeaderText = rRow.Item(5).ToString
                dgvOps.Columns(9).HeaderText = rRow.Item(6).ToString
                dgvOps.Columns(10).HeaderText = rRow.Item(7).ToString
                dgvOps.Columns(11).HeaderText = rRow.Item(8).ToString
                dgvOps.Columns(12).HeaderText = rRow.Item(9).ToString
                dgvOps.Columns(13).HeaderText = rRow.Item(10).ToString
            End If
        Next

        Redraw()

        UpdateOpCount(dgvOps)

    End Sub



#End Region
#Region " Event - dgvOps_DataError() "

    Private Sub dgvOps_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOps.DataError

        ImportMissingOp(sender, e, dgvOps)

    End Sub

#End Region
#Region " Context Menu - Opening "

    Private Sub ctmOps_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ctmOps.Opening

        If dgvOps.SelectedRows.Count = 0 Then
            ctmOps.Items("CopyToolStripMenuItem").Enabled = False
        Else
            ctmOps.Items("CopyToolStripMenuItem").Enabled = True
        End If

    End Sub

#End Region
#Region " Context Menu - Copy (Operations) "

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click

        'Check user has selected rows
        If dgvOps.SelectedRows.Count = 0 Then
            MsgBox("Please Select the Row(s) to be Copied.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Initialise String with header line
        Dim s As String = "OpID	ProfileCode	OpToolCode	StdOpCode	Use	OpDescription	OpVariable1	OpVariable2	OpVariable3	OpVariable4	OpVariable5	OpVariable6	OpVariable7	OpVariable8" + vbCrLf

        'Build Cells into Clipboard String
        For Each dr As DataGridViewRow In dgvOps.SelectedRows
            For Each cCell As DataGridViewCell In dr.Cells
                s += cCell.Value.ToString + vbTab
            Next
            s = Mid(s, 1, Len(s) - 1) + vbCrLf
        Next

        'Post string to clipboard
        Clipboard.SetText(s)

        'MsgBox(s)

    End Sub

#End Region
#Region " Context Menu - Paste "

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click

        'Get Text from the Clipboard
        Dim sClipboard As String = Clipboard.GetText()

        'Check the Text is in a valid format
        If Mid(sClipboard, 1, 4) <> "OpID" Then
            MsgBox("No valid text exists", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Display Clipboard Text
        'MsgBox(sClipboard)

        'Split Clipboard into separate operations
        Dim sLines() As String = sClipboard.Split(vbCrLf)

        'Import the clipboard line for each operation separately ("" Means keep the profile same as the copied line)
        For Each sLine As String In sLines
            ImportClipboardLine(sLine, "")
        Next

        UpdateOpCount(dgvOps)

    End Sub

#End Region

#Region " Sub - Redraw() "

    Sub Redraw()

        'Declare Variables
        Dim sString As String = ""
        Dim sProfile As String = ""
        Dim V1 As Double = 0
        Dim V2 As Double = 0
        Dim V3 As Double = 0
        Dim V4 As Double = 0
        Dim V5 As Double = 0
        Dim V6 As Double = 0
        Dim V7 As Double = 0
        Dim V8 As Double = 0

        If bCreating Then Exit Sub

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvOps.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvOps.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvOps.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvOps.SelectedCells(0).RowIndex
            sr = dgvOps.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile
        sProfile = sr.Cells(1).Value

        'Get MND File and Variables
        Try
            sString = sr.Cells(3).Value
            V1 = sr.Cells(6).Value
            V2 = sr.Cells(7).Value
            V3 = sr.Cells(8).Value
            V4 = sr.Cells(9).Value
            V5 = sr.Cells(10).Value
            V6 = sr.Cells(11).Value
            V7 = sr.Cells(12).Value
            V8 = sr.Cells(13).Value
        Catch ex As Exception
            Exit Sub
        End Try

        'Fill Headers
        'Dim rRow As DataRow() = tblStdOpDef.Select("StdOpCode = '" + sString + "'")   SELECT COMMAND REQUIRES .NET 3.5 (XP and Later Only)
        Dim rRow As DataRow
        For Each temprow As DataRow In tblStdOpDef.Rows
            If temprow.Item(0).ToString = sString Then
                rRow = temprow
                dgvOps.Columns(6).HeaderText = rRow.Item(3).ToString
                dgvOps.Columns(7).HeaderText = rRow.Item(4).ToString
                dgvOps.Columns(8).HeaderText = rRow.Item(5).ToString
                dgvOps.Columns(9).HeaderText = rRow.Item(6).ToString
                dgvOps.Columns(10).HeaderText = rRow.Item(7).ToString
                dgvOps.Columns(11).HeaderText = rRow.Item(8).ToString
                dgvOps.Columns(12).HeaderText = rRow.Item(9).ToString
                dgvOps.Columns(13).HeaderText = rRow.Item(10).ToString
            End If
        Next

        'Parse MND File
        RedrawOK = False
        Dim sFile As String = prfOpdesignLoc + sString + ".MND"
        ParseMND.ParseMNDFile(sFile, V1, V2, V3, V4, V5, V6, V7, V8, Me.rb1, Me.nebYP, Me.nebZP, Me.nebANG, Me.nebPL, Me.nebToolDia)
        RedrawOK = True

        'Draw Operation
        With PosCalc
            .YP = nebYP.Value
            .ZP = nebZP.Value
            .Angle = nebANG.Value
            .Plunge = nebPL.Value
            .ToolDiameter = nebToolDia.Value
            .Scale = nebZoom.Value
            .bShowGrip = False
            .PushGripperHeight = prfPushGripperHeight
            .PushGripperWidth = prfPushGripperWidth
            .LoadDXF(modGlobal.prfDXFLoc & sProfile & ".dxf")
            .CreateDrawing()
        End With

    End Sub

#End Region
#Region " Sub - InitialisePosCalc() "

    Sub InitialisePosCalc()

        With PosCalc

            'Link Controls
            .pbPosCalc = pbPosCalc

            'Set Scale
            .Scale = iScale

            'Set Centre Point
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2

        End With

    End Sub

#End Region
#Region " Redraw Routines "

    Private Sub btnDraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDraw.Click
        Redraw()
    End Sub

    Private Sub rb1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb1.CheckedChanged
        Redraw()
    End Sub

    Private Sub nebZoom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nebZoom.ValueChanged
        Redraw()
    End Sub

    Private Sub pbPosCalc_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbPosCalc.Resize

        InitialisePosCalc()
        Redraw()

    End Sub

#End Region
#Region " Event - pbPosCalc_MouseMove() "

    Private Sub pbPosCalc_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles pbPosCalc.MouseMove

        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        Dim s As Double
        Dim c As Double
        Dim a1 As Double

        If Button = 1 Then

            If DragMode = 1 Then 'Rotate

                'Calculate angle
                If (MouseY - Y) = 0 Then
                    If MouseX - X > 0 Then PosCalc.Angle = 90 Else PosCalc.Angle = 90 = 270
                Else
                    PosCalc.Angle = 180 * System.Math.Atan((MouseX - X) / (MouseY - Y)) / PosCalc.pi
                    If (MouseY - Y) < 0 Then PosCalc.Angle = PosCalc.Angle + 180
                    If PosCalc.Angle < 0 Then PosCalc.Angle = PosCalc.Angle + 360
                End If

                'Generate Sin / Cos of Angle
                a1 = -PosCalc.Angle * 3.14159265358979 / 180
                c = System.Math.Sin(a1)
                s = System.Math.Cos(a1)

                'Check Min/Max
                Dim MinMaxY As Integer = (MouseX - (PosCalc.shtRingDia * c) - PosCalc.XOffset) / PosCalc.Scale
                If MinMaxY < nebYP.Minimum Then MinMaxY = nebYP.Minimum
                If MinMaxY > nebYP.Maximum Then MinMaxY = nebYP.Maximum

                Dim MinMaxZ As Integer = (-MouseY - (PosCalc.shtRingDia * s) + PosCalc.YOffset) / PosCalc.Scale
                If MinMaxZ < nebZP.Minimum Then MinMaxZ = nebZP.Minimum
                If MinMaxZ > nebZP.Maximum Then MinMaxZ = nebZP.Maximum

                'Recalc ring pos
                PosCalc.YP = MinMaxY
                PosCalc.ZP = MinMaxZ

                'Draw Ring
                RedrawOK = False
                nebYP.Value = PosCalc.YP
                nebZP.Value = PosCalc.ZP
                nebANG.Value = PosCalc.Angle
                RedrawOK = True
                PosCalc.CreateDrawing()

            Else ' Drag
                Dim MinMaxY As Integer = (X - MouseX) / PosCalc.Scale
                If MinMaxY < nebYP.Minimum Then MinMaxY = nebYP.Minimum
                If MinMaxY > nebYP.Maximum Then MinMaxY = nebYP.Maximum
                PosCalc.YP = MinMaxY

                Dim MinMaxZ As Integer = (-(Y - MouseY)) / PosCalc.Scale
                If MinMaxZ < nebZP.Minimum Then MinMaxZ = nebZP.Minimum
                If MinMaxZ > nebZP.Maximum Then MinMaxZ = nebZP.Maximum
                PosCalc.ZP = MinMaxZ

                RedrawOK = False
                nebYP.Value = PosCalc.YP
                nebZP.Value = PosCalc.ZP
                nebANG.Value = PosCalc.Angle
                RedrawOK = True
                PosCalc.CreateDrawing()
            End If

        Else
            a1 = -PosCalc.Angle * 3.14159265358979 / 180
            c = System.Math.Sin(a1)
            s = System.Math.Cos(a1)

            Dim iMX As Integer = PosCalc.tx + ((prfTipToCollet + 15) * c * PosCalc.Scale)
            Dim iMY As Integer = PosCalc.ty - ((prfTipToCollet + 15) * s * PosCalc.Scale)
            Dim iXoff As Integer = X - iMX
            Dim iYoff As Integer = Y - iMY
            If iXoff > -32 And iXoff < 32 And iYoff > -32 And iYoff < 32 Then
                DragMode = 1
                MouseX = PosCalc.tx
                MouseY = PosCalc.ty
            Else
                DragMode = 0
                MouseX = X - (PosCalc.YP * PosCalc.Scale)
                MouseY = Y + (PosCalc.ZP * PosCalc.Scale)
            End If

        End If

    End Sub

#End Region

    'Misc
#Region " Button - Create Profile.saw "

    Private Sub btnCreateProfileSaw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateProfileSaw.Click

        CreateProfileSaw()

    End Sub

#End Region
#Region " Button - Create AHAdjust.Mul "

    Private Sub btnCreateAHAdjustMul_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateAHAdjustMul.Click

        CreateAHAdjust()

    End Sub

#End Region

#Region " Button - Adjust Z Blocks "

    Private Sub btnZBlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnZBlock.Click

        Dim frmzblock As New frmZBlock
        frmzblock.frmParent = Me
        frmzblock.Show()

    End Sub

#End Region
#Region " Button - Gripper Setup "

    Private Sub btnGripperSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGripperSetup.Click

        Dim frmGripperSetup As New frmGripper
        frmGripperSetup.frmParent = Me

        frmGripperSetup.Show()

    End Sub

#End Region

#Region " Button - Enable V Notches "

    Private Sub btnVON_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVON.Click

        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sToolCode As String = rRow.Item("OpToolCode")

            If Mid(sToolCode.ToUpper, 1, 2) = "VF" Then rRow.Item("Use") = True
            If Mid(sToolCode.ToUpper, 1, 2) = "VR" Then rRow.Item("Use") = True


        Next

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Diable V Notches "

    Private Sub btnVOFF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVOFF.Click
        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sToolCode As String = rRow.Item("OpToolCode")

            If Mid(sToolCode.ToUpper, 1, 2) = "VF" Then rRow.Item("Use") = False
            If Mid(sToolCode.ToUpper, 1, 2) = "VR" Then rRow.Item("Use") = False


        Next

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Enable Y Notches "

    Private Sub btnYON_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYON.Click

        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sToolCode As String = rRow.Item("OpToolCode")

            If Mid(sToolCode.ToUpper, 1, 3) = "YFL" Then rRow.Item("Use") = True
            If Mid(sToolCode.ToUpper, 1, 3) = "YFR" Then rRow.Item("Use") = True
            If Mid(sToolCode.ToUpper, 1, 3) = "YRL" Then rRow.Item("Use") = True
            If Mid(sToolCode.ToUpper, 1, 3) = "YRR" Then rRow.Item("Use") = True


        Next

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Disable Y Notches "

    Private Sub btnYOFF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYOFF.Click

        Dim mbrReply As MsgBoxResult = MsgBox("Are you Sure?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        For Each rRow As DataRow In tblStdOps.Rows

            Dim sToolCode As String = rRow.Item("OpToolCode")

            If Mid(sToolCode.ToUpper, 1, 3) = "YFL" Then rRow.Item("Use") = False
            If Mid(sToolCode.ToUpper, 1, 3) = "YFR" Then rRow.Item("Use") = False
            If Mid(sToolCode.ToUpper, 1, 3) = "YRL" Then rRow.Item("Use") = False
            If Mid(sToolCode.ToUpper, 1, 3) = "YRR" Then rRow.Item("Use") = False


        Next

        SaveStdOpsMUL()

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Fill Ops Grid
        FillOperationsGrid(dgvOps, txtProfileCode.Text, rbProfEQ.Checked, txtToolCode.Text, rbToolEQ.Checked, txtStdOp.Text, rbStdOpEQ.Checked, True)
        dgvOps.AllowUserToAddRows = False

        'Let User Know all is complete
        MsgBox("Done", MsgBoxStyle.Information)

    End Sub

#End Region


End Class
