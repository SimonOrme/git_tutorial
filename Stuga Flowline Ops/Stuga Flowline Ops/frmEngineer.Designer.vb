﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEngineer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEngineer))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkShowReplicates = New System.Windows.Forms.CheckBox()
        Me.cmbType = New System.Windows.Forms.ComboBox()
        Me.lblType = New System.Windows.Forms.Label()
        Me.cmbProfSuite = New System.Windows.Forms.ComboBox()
        Me.lblSuite = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btnUndoProfiles = New System.Windows.Forms.Button()
        Me.btnCloseProfile = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnEditProfile = New System.Windows.Forms.Button()
        Me.dgvProfile = New System.Windows.Forms.DataGridView()
        Me.ctmProfile = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiCopyProfile = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tcEngineer = New System.Windows.Forms.TabControl()
        Me.tpProfile = New System.Windows.Forms.TabPage()
        Me.tpStandardOps = New System.Windows.Forms.TabPage()
        Me.dgvStdOps = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnEditStdop = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnCancelStdOps = New System.Windows.Forms.Button()
        Me.btnSaveStdOps = New System.Windows.Forms.Button()
        Me.btnCloseStdOps = New System.Windows.Forms.Button()
        Me.tpSuites = New System.Windows.Forms.TabPage()
        Me.dgvSuites = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.btnUndoSuites = New System.Windows.Forms.Button()
        Me.btnSaveSuites = New System.Windows.Forms.Button()
        Me.btnCloseSuites = New System.Windows.Forms.Button()
        Me.tpHardware = New System.Windows.Forms.TabPage()
        Me.dgvHardware = New System.Windows.Forms.DataGridView()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btnEditHardware = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.btnUndoHardware = New System.Windows.Forms.Button()
        Me.btnCloseHardware = New System.Windows.Forms.Button()
        Me.btnSaveHardware = New System.Windows.Forms.Button()
        Me.tpOpTypes = New System.Windows.Forms.TabPage()
        Me.dgvOpTypes = New System.Windows.Forms.DataGridView()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.btnUndoOpTypes = New System.Windows.Forms.Button()
        Me.btnSaveOpTypes = New System.Windows.Forms.Button()
        Me.btnCloseOpTypes = New System.Windows.Forms.Button()
        Me.tpOperations = New System.Windows.Forms.TabPage()
        Me.dgvOps = New System.Windows.Forms.DataGridView()
        Me.ctmOps = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.tcOperationOptions = New System.Windows.Forms.TabControl()
        Me.tpSpeedy = New System.Windows.Forms.TabPage()
        Me.btnRemoveSlow = New System.Windows.Forms.Button()
        Me.btnUnSpeedy = New System.Windows.Forms.Button()
        Me.btnSpeedy = New System.Windows.Forms.Button()
        Me.grpSpeedy = New System.Windows.Forms.GroupBox()
        Me.chkSP3 = New System.Windows.Forms.CheckBox()
        Me.chkDrainEQ = New System.Windows.Forms.CheckBox()
        Me.chkSP5 = New System.Windows.Forms.CheckBox()
        Me.chkDrain = New System.Windows.Forms.CheckBox()
        Me.tpNotching = New System.Windows.Forms.TabPage()
        Me.grpNotching = New System.Windows.Forms.GroupBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.tpOperationMisc = New System.Windows.Forms.TabPage()
        Me.grpColumnValues = New System.Windows.Forms.GroupBox()
        Me.btnUpdateCol = New System.Windows.Forms.Button()
        Me.txtNewValue = New System.Windows.Forms.TextBox()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.lblValue = New System.Windows.Forms.Label()
        Me.lblColumn = New System.Windows.Forms.Label()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.pbPosCalc = New System.Windows.Forms.PictureBox()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.nebPL = New System.Windows.Forms.NumericUpDown()
        Me.nebToolDia = New System.Windows.Forms.NumericUpDown()
        Me.nebZoom = New System.Windows.Forms.NumericUpDown()
        Me.gbTool = New System.Windows.Forms.GroupBox()
        Me.rb2 = New System.Windows.Forms.RadioButton()
        Me.rb1 = New System.Windows.Forms.RadioButton()
        Me.lbltool = New System.Windows.Forms.Label()
        Me.btnDraw = New System.Windows.Forms.Button()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.lblPL = New System.Windows.Forms.Label()
        Me.lblANG = New System.Windows.Forms.Label()
        Me.lblZP = New System.Windows.Forms.Label()
        Me.nebANG = New System.Windows.Forms.NumericUpDown()
        Me.nebZP = New System.Windows.Forms.NumericUpDown()
        Me.lblYP = New System.Windows.Forms.Label()
        Me.nebYP = New System.Windows.Forms.NumericUpDown()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.grpStdOpEQ = New System.Windows.Forms.GroupBox()
        Me.rbStdOpContains = New System.Windows.Forms.RadioButton()
        Me.rbStdOpEQ = New System.Windows.Forms.RadioButton()
        Me.grpToolEQ = New System.Windows.Forms.GroupBox()
        Me.rbToolContains = New System.Windows.Forms.RadioButton()
        Me.rbToolEQ = New System.Windows.Forms.RadioButton()
        Me.grpProfEQ = New System.Windows.Forms.GroupBox()
        Me.rbProfContains = New System.Windows.Forms.RadioButton()
        Me.rbProfEQ = New System.Windows.Forms.RadioButton()
        Me.txtProfileCode = New System.Windows.Forms.TextBox()
        Me.lblProfileCode = New System.Windows.Forms.Label()
        Me.txtStdOp = New System.Windows.Forms.TextBox()
        Me.txtToolCode = New System.Windows.Forms.TextBox()
        Me.lblStdOp = New System.Windows.Forms.Label()
        Me.lblToolCode = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.btnHidePreview = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.btnUndoOps = New System.Windows.Forms.Button()
        Me.btnSaveOps = New System.Windows.Forms.Button()
        Me.btnExitOps = New System.Windows.Forms.Button()
        Me.tpMisc = New System.Windows.Forms.TabPage()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.grpNotch = New System.Windows.Forms.GroupBox()
        Me.btnYOFF = New System.Windows.Forms.Button()
        Me.btnVOFF = New System.Windows.Forms.Button()
        Me.btnVON = New System.Windows.Forms.Button()
        Me.btnYON = New System.Windows.Forms.Button()
        Me.grpSetup = New System.Windows.Forms.GroupBox()
        Me.btnGripperSetup = New System.Windows.Forms.Button()
        Me.btnZBlock = New System.Windows.Forms.Button()
        Me.grpFiles = New System.Windows.Forms.GroupBox()
        Me.btnCreateAHAdjustMul = New System.Windows.Forms.Button()
        Me.btnCreateProfileSaw = New System.Windows.Forms.Button()
        Me.lblRows = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvProfile, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctmProfile.SuspendLayout()
        Me.tcEngineer.SuspendLayout()
        Me.tpProfile.SuspendLayout()
        Me.tpStandardOps.SuspendLayout()
        CType(Me.dgvStdOps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.tpSuites.SuspendLayout()
        CType(Me.dgvSuites, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.tpHardware.SuspendLayout()
        CType(Me.dgvHardware, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.tpOpTypes.SuspendLayout()
        CType(Me.dgvOpTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.tpOperations.SuspendLayout()
        CType(Me.dgvOps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctmOps.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.tcOperationOptions.SuspendLayout()
        Me.tpSpeedy.SuspendLayout()
        Me.grpSpeedy.SuspendLayout()
        Me.tpNotching.SuspendLayout()
        Me.grpNotching.SuspendLayout()
        Me.tpOperationMisc.SuspendLayout()
        Me.grpColumnValues.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel14.SuspendLayout()
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebToolDia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTool.SuspendLayout()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        Me.grpStdOpEQ.SuspendLayout()
        Me.grpToolEQ.SuspendLayout()
        Me.grpProfEQ.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.tpMisc.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.grpNotch.SuspendLayout()
        Me.grpSetup.SuspendLayout()
        Me.grpFiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.chkShowReplicates)
        Me.Panel1.Controls.Add(Me.cmbType)
        Me.Panel1.Controls.Add(Me.lblType)
        Me.Panel1.Controls.Add(Me.cmbProfSuite)
        Me.Panel1.Controls.Add(Me.lblSuite)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.btnEditProfile)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(3, 503)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(990, 119)
        Me.Panel1.TabIndex = 3
        '
        'chkShowReplicates
        '
        Me.chkShowReplicates.CheckAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkShowReplicates.Checked = True
        Me.chkShowReplicates.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowReplicates.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReplicates.ForeColor = System.Drawing.Color.White
        Me.chkShowReplicates.Location = New System.Drawing.Point(455, 62)
        Me.chkShowReplicates.Name = "chkShowReplicates"
        Me.chkShowReplicates.Size = New System.Drawing.Size(81, 52)
        Me.chkShowReplicates.TabIndex = 9
        Me.chkShowReplicates.Text = "Show Replicates"
        Me.chkShowReplicates.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.chkShowReplicates.UseVisualStyleBackColor = True
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(312, 7)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(137, 24)
        Me.cmbType.TabIndex = 7
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.ForeColor = System.Drawing.Color.White
        Me.lblType.Location = New System.Drawing.Point(268, 10)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(38, 16)
        Me.lblType.TabIndex = 6
        Me.lblType.Text = "Type"
        '
        'cmbProfSuite
        '
        Me.cmbProfSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProfSuite.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProfSuite.FormattingEnabled = True
        Me.cmbProfSuite.Location = New System.Drawing.Point(52, 7)
        Me.cmbProfSuite.Name = "cmbProfSuite"
        Me.cmbProfSuite.Size = New System.Drawing.Size(210, 24)
        Me.cmbProfSuite.TabIndex = 5
        '
        'lblSuite
        '
        Me.lblSuite.AutoSize = True
        Me.lblSuite.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuite.ForeColor = System.Drawing.Color.White
        Me.lblSuite.Location = New System.Drawing.Point(5, 10)
        Me.lblSuite.Name = "lblSuite"
        Me.lblSuite.Size = New System.Drawing.Size(41, 16)
        Me.lblSuite.TabIndex = 4
        Me.lblSuite.Text = "Suite"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnUndoProfiles)
        Me.Panel4.Controls.Add(Me.btnCloseProfile)
        Me.Panel4.Controls.Add(Me.btnSave)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel4.Location = New System.Drawing.Point(547, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(443, 119)
        Me.Panel4.TabIndex = 2
        '
        'btnUndoProfiles
        '
        Me.btnUndoProfiles.BackColor = System.Drawing.Color.White
        Me.btnUndoProfiles.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoProfiles.Image = CType(resources.GetObject("btnUndoProfiles.Image"), System.Drawing.Image)
        Me.btnUndoProfiles.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoProfiles.Location = New System.Drawing.Point(152, 37)
        Me.btnUndoProfiles.Name = "btnUndoProfiles"
        Me.btnUndoProfiles.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoProfiles.TabIndex = 3
        Me.btnUndoProfiles.Text = "Cancel Change"
        Me.btnUndoProfiles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoProfiles.UseVisualStyleBackColor = False
        '
        'btnCloseProfile
        '
        Me.btnCloseProfile.BackColor = System.Drawing.Color.White
        Me.btnCloseProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseProfile.Image = CType(resources.GetObject("btnCloseProfile.Image"), System.Drawing.Image)
        Me.btnCloseProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseProfile.Location = New System.Drawing.Point(298, 37)
        Me.btnCloseProfile.Name = "btnCloseProfile"
        Me.btnCloseProfile.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseProfile.TabIndex = 2
        Me.btnCloseProfile.Text = "Save and Exit"
        Me.btnCloseProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseProfile.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(6, 37)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 77)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save Profile"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnEditProfile
        '
        Me.btnEditProfile.BackColor = System.Drawing.Color.White
        Me.btnEditProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditProfile.Image = CType(resources.GetObject("btnEditProfile.Image"), System.Drawing.Image)
        Me.btnEditProfile.Location = New System.Drawing.Point(5, 37)
        Me.btnEditProfile.Name = "btnEditProfile"
        Me.btnEditProfile.Size = New System.Drawing.Size(129, 77)
        Me.btnEditProfile.TabIndex = 0
        Me.btnEditProfile.Text = "Edit Profile"
        Me.btnEditProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditProfile.UseVisualStyleBackColor = False
        '
        'dgvProfile
        '
        Me.dgvProfile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProfile.ContextMenuStrip = Me.ctmProfile
        Me.dgvProfile.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProfile.Location = New System.Drawing.Point(3, 3)
        Me.dgvProfile.Name = "dgvProfile"
        Me.dgvProfile.Size = New System.Drawing.Size(990, 500)
        Me.dgvProfile.TabIndex = 4
        '
        'ctmProfile
        '
        Me.ctmProfile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiCopyProfile, Me.PasteToolStripMenuItem1})
        Me.ctmProfile.Name = "ctmProfile"
        Me.ctmProfile.Size = New System.Drawing.Size(103, 48)
        '
        'tsmiCopyProfile
        '
        Me.tsmiCopyProfile.Image = CType(resources.GetObject("tsmiCopyProfile.Image"), System.Drawing.Image)
        Me.tsmiCopyProfile.ImageTransparentColor = System.Drawing.Color.Silver
        Me.tsmiCopyProfile.Name = "tsmiCopyProfile"
        Me.tsmiCopyProfile.Size = New System.Drawing.Size(102, 22)
        Me.tsmiCopyProfile.Text = "Copy"
        '
        'PasteToolStripMenuItem1
        '
        Me.PasteToolStripMenuItem1.Image = CType(resources.GetObject("PasteToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Silver
        Me.PasteToolStripMenuItem1.Name = "PasteToolStripMenuItem1"
        Me.PasteToolStripMenuItem1.Size = New System.Drawing.Size(102, 22)
        Me.PasteToolStripMenuItem1.Text = "Paste"
        '
        'tcEngineer
        '
        Me.tcEngineer.Controls.Add(Me.tpProfile)
        Me.tcEngineer.Controls.Add(Me.tpStandardOps)
        Me.tcEngineer.Controls.Add(Me.tpSuites)
        Me.tcEngineer.Controls.Add(Me.tpHardware)
        Me.tcEngineer.Controls.Add(Me.tpOpTypes)
        Me.tcEngineer.Controls.Add(Me.tpOperations)
        Me.tcEngineer.Controls.Add(Me.tpMisc)
        Me.tcEngineer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcEngineer.Font = New System.Drawing.Font("Arial", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcEngineer.Location = New System.Drawing.Point(0, 0)
        Me.tcEngineer.Name = "tcEngineer"
        Me.tcEngineer.SelectedIndex = 0
        Me.tcEngineer.Size = New System.Drawing.Size(1004, 662)
        Me.tcEngineer.TabIndex = 5
        '
        'tpProfile
        '
        Me.tpProfile.Controls.Add(Me.dgvProfile)
        Me.tpProfile.Controls.Add(Me.Panel1)
        Me.tpProfile.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tpProfile.Location = New System.Drawing.Point(4, 33)
        Me.tpProfile.Name = "tpProfile"
        Me.tpProfile.Padding = New System.Windows.Forms.Padding(3)
        Me.tpProfile.Size = New System.Drawing.Size(996, 625)
        Me.tpProfile.TabIndex = 0
        Me.tpProfile.Text = "Profile"
        Me.tpProfile.UseVisualStyleBackColor = True
        '
        'tpStandardOps
        '
        Me.tpStandardOps.Controls.Add(Me.dgvStdOps)
        Me.tpStandardOps.Controls.Add(Me.Panel2)
        Me.tpStandardOps.Location = New System.Drawing.Point(4, 33)
        Me.tpStandardOps.Name = "tpStandardOps"
        Me.tpStandardOps.Padding = New System.Windows.Forms.Padding(3)
        Me.tpStandardOps.Size = New System.Drawing.Size(996, 625)
        Me.tpStandardOps.TabIndex = 1
        Me.tpStandardOps.Text = "Standard Ops"
        Me.tpStandardOps.UseVisualStyleBackColor = True
        '
        'dgvStdOps
        '
        Me.dgvStdOps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStdOps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStdOps.Location = New System.Drawing.Point(3, 3)
        Me.dgvStdOps.Name = "dgvStdOps"
        Me.dgvStdOps.Size = New System.Drawing.Size(990, 532)
        Me.dgvStdOps.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Navy
        Me.Panel2.Controls.Add(Me.btnEditStdop)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(3, 535)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(990, 87)
        Me.Panel2.TabIndex = 0
        '
        'btnEditStdop
        '
        Me.btnEditStdop.BackColor = System.Drawing.Color.White
        Me.btnEditStdop.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditStdop.Image = CType(resources.GetObject("btnEditStdop.Image"), System.Drawing.Image)
        Me.btnEditStdop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditStdop.Location = New System.Drawing.Point(5, 6)
        Me.btnEditStdop.Name = "btnEditStdop"
        Me.btnEditStdop.Size = New System.Drawing.Size(140, 77)
        Me.btnEditStdop.TabIndex = 4
        Me.btnEditStdop.Text = "Edit StdOp"
        Me.btnEditStdop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditStdop.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.btnCancelStdOps)
        Me.Panel5.Controls.Add(Me.btnSaveStdOps)
        Me.Panel5.Controls.Add(Me.btnCloseStdOps)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(547, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(443, 87)
        Me.Panel5.TabIndex = 3
        '
        'btnCancelStdOps
        '
        Me.btnCancelStdOps.BackColor = System.Drawing.Color.White
        Me.btnCancelStdOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelStdOps.Image = CType(resources.GetObject("btnCancelStdOps.Image"), System.Drawing.Image)
        Me.btnCancelStdOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelStdOps.Location = New System.Drawing.Point(149, 5)
        Me.btnCancelStdOps.Name = "btnCancelStdOps"
        Me.btnCancelStdOps.Size = New System.Drawing.Size(140, 77)
        Me.btnCancelStdOps.TabIndex = 3
        Me.btnCancelStdOps.Text = "Cancel Change"
        Me.btnCancelStdOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancelStdOps.UseVisualStyleBackColor = False
        '
        'btnSaveStdOps
        '
        Me.btnSaveStdOps.BackColor = System.Drawing.Color.White
        Me.btnSaveStdOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveStdOps.Image = CType(resources.GetObject("btnSaveStdOps.Image"), System.Drawing.Image)
        Me.btnSaveStdOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveStdOps.Location = New System.Drawing.Point(3, 5)
        Me.btnSaveStdOps.Name = "btnSaveStdOps"
        Me.btnSaveStdOps.Size = New System.Drawing.Size(140, 77)
        Me.btnSaveStdOps.TabIndex = 2
        Me.btnSaveStdOps.Text = "Save StdOps"
        Me.btnSaveStdOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveStdOps.UseVisualStyleBackColor = False
        '
        'btnCloseStdOps
        '
        Me.btnCloseStdOps.BackColor = System.Drawing.Color.White
        Me.btnCloseStdOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseStdOps.Image = CType(resources.GetObject("btnCloseStdOps.Image"), System.Drawing.Image)
        Me.btnCloseStdOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseStdOps.Location = New System.Drawing.Point(295, 5)
        Me.btnCloseStdOps.Name = "btnCloseStdOps"
        Me.btnCloseStdOps.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseStdOps.TabIndex = 2
        Me.btnCloseStdOps.Text = "Save and Exit"
        Me.btnCloseStdOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseStdOps.UseVisualStyleBackColor = False
        '
        'tpSuites
        '
        Me.tpSuites.Controls.Add(Me.dgvSuites)
        Me.tpSuites.Controls.Add(Me.Panel3)
        Me.tpSuites.Location = New System.Drawing.Point(4, 33)
        Me.tpSuites.Name = "tpSuites"
        Me.tpSuites.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSuites.Size = New System.Drawing.Size(996, 625)
        Me.tpSuites.TabIndex = 2
        Me.tpSuites.Text = "Profile Suites"
        Me.tpSuites.UseVisualStyleBackColor = True
        '
        'dgvSuites
        '
        Me.dgvSuites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSuites.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSuites.Location = New System.Drawing.Point(3, 3)
        Me.dgvSuites.Name = "dgvSuites"
        Me.dgvSuites.Size = New System.Drawing.Size(990, 532)
        Me.dgvSuites.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Navy
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 535)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(990, 87)
        Me.Panel3.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.btnUndoSuites)
        Me.Panel6.Controls.Add(Me.btnSaveSuites)
        Me.Panel6.Controls.Add(Me.btnCloseSuites)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel6.Location = New System.Drawing.Point(547, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(443, 87)
        Me.Panel6.TabIndex = 4
        '
        'btnUndoSuites
        '
        Me.btnUndoSuites.BackColor = System.Drawing.Color.White
        Me.btnUndoSuites.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoSuites.Image = CType(resources.GetObject("btnUndoSuites.Image"), System.Drawing.Image)
        Me.btnUndoSuites.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoSuites.Location = New System.Drawing.Point(149, 5)
        Me.btnUndoSuites.Name = "btnUndoSuites"
        Me.btnUndoSuites.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoSuites.TabIndex = 3
        Me.btnUndoSuites.Text = "Cancel Change"
        Me.btnUndoSuites.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoSuites.UseVisualStyleBackColor = False
        '
        'btnSaveSuites
        '
        Me.btnSaveSuites.BackColor = System.Drawing.Color.White
        Me.btnSaveSuites.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSuites.Image = CType(resources.GetObject("btnSaveSuites.Image"), System.Drawing.Image)
        Me.btnSaveSuites.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveSuites.Location = New System.Drawing.Point(3, 5)
        Me.btnSaveSuites.Name = "btnSaveSuites"
        Me.btnSaveSuites.Size = New System.Drawing.Size(140, 77)
        Me.btnSaveSuites.TabIndex = 2
        Me.btnSaveSuites.Text = "Save Suites"
        Me.btnSaveSuites.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveSuites.UseVisualStyleBackColor = False
        '
        'btnCloseSuites
        '
        Me.btnCloseSuites.BackColor = System.Drawing.Color.White
        Me.btnCloseSuites.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseSuites.Image = CType(resources.GetObject("btnCloseSuites.Image"), System.Drawing.Image)
        Me.btnCloseSuites.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseSuites.Location = New System.Drawing.Point(295, 5)
        Me.btnCloseSuites.Name = "btnCloseSuites"
        Me.btnCloseSuites.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseSuites.TabIndex = 2
        Me.btnCloseSuites.Text = "Save and Exit"
        Me.btnCloseSuites.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseSuites.UseVisualStyleBackColor = False
        '
        'tpHardware
        '
        Me.tpHardware.Controls.Add(Me.dgvHardware)
        Me.tpHardware.Controls.Add(Me.Panel7)
        Me.tpHardware.Location = New System.Drawing.Point(4, 33)
        Me.tpHardware.Name = "tpHardware"
        Me.tpHardware.Padding = New System.Windows.Forms.Padding(3)
        Me.tpHardware.Size = New System.Drawing.Size(996, 625)
        Me.tpHardware.TabIndex = 3
        Me.tpHardware.Text = "Hardware Design"
        Me.tpHardware.UseVisualStyleBackColor = True
        '
        'dgvHardware
        '
        Me.dgvHardware.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHardware.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHardware.Location = New System.Drawing.Point(3, 3)
        Me.dgvHardware.Name = "dgvHardware"
        Me.dgvHardware.Size = New System.Drawing.Size(990, 532)
        Me.dgvHardware.TabIndex = 0
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Navy
        Me.Panel7.Controls.Add(Me.btnEditHardware)
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel7.Location = New System.Drawing.Point(3, 535)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(990, 87)
        Me.Panel7.TabIndex = 1
        '
        'btnEditHardware
        '
        Me.btnEditHardware.BackColor = System.Drawing.Color.White
        Me.btnEditHardware.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditHardware.Image = CType(resources.GetObject("btnEditHardware.Image"), System.Drawing.Image)
        Me.btnEditHardware.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditHardware.Location = New System.Drawing.Point(5, 5)
        Me.btnEditHardware.Name = "btnEditHardware"
        Me.btnEditHardware.Size = New System.Drawing.Size(138, 77)
        Me.btnEditHardware.TabIndex = 4
        Me.btnEditHardware.Text = "Edit Hardware"
        Me.btnEditHardware.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditHardware.UseVisualStyleBackColor = False
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.btnUndoHardware)
        Me.Panel8.Controls.Add(Me.btnCloseHardware)
        Me.Panel8.Controls.Add(Me.btnSaveHardware)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel8.Location = New System.Drawing.Point(547, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(443, 87)
        Me.Panel8.TabIndex = 3
        '
        'btnUndoHardware
        '
        Me.btnUndoHardware.BackColor = System.Drawing.Color.White
        Me.btnUndoHardware.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoHardware.Image = CType(resources.GetObject("btnUndoHardware.Image"), System.Drawing.Image)
        Me.btnUndoHardware.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoHardware.Location = New System.Drawing.Point(149, 5)
        Me.btnUndoHardware.Name = "btnUndoHardware"
        Me.btnUndoHardware.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoHardware.TabIndex = 3
        Me.btnUndoHardware.Text = "Cancel Change"
        Me.btnUndoHardware.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoHardware.UseVisualStyleBackColor = False
        '
        'btnCloseHardware
        '
        Me.btnCloseHardware.BackColor = System.Drawing.Color.White
        Me.btnCloseHardware.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseHardware.Image = CType(resources.GetObject("btnCloseHardware.Image"), System.Drawing.Image)
        Me.btnCloseHardware.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseHardware.Location = New System.Drawing.Point(295, 5)
        Me.btnCloseHardware.Name = "btnCloseHardware"
        Me.btnCloseHardware.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseHardware.TabIndex = 2
        Me.btnCloseHardware.Text = "Save and Exit"
        Me.btnCloseHardware.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseHardware.UseVisualStyleBackColor = False
        '
        'btnSaveHardware
        '
        Me.btnSaveHardware.BackColor = System.Drawing.Color.White
        Me.btnSaveHardware.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveHardware.Image = CType(resources.GetObject("btnSaveHardware.Image"), System.Drawing.Image)
        Me.btnSaveHardware.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveHardware.Location = New System.Drawing.Point(3, 5)
        Me.btnSaveHardware.Name = "btnSaveHardware"
        Me.btnSaveHardware.Size = New System.Drawing.Size(140, 77)
        Me.btnSaveHardware.TabIndex = 1
        Me.btnSaveHardware.Text = "Save Hardware"
        Me.btnSaveHardware.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveHardware.UseVisualStyleBackColor = False
        '
        'tpOpTypes
        '
        Me.tpOpTypes.Controls.Add(Me.dgvOpTypes)
        Me.tpOpTypes.Controls.Add(Me.Panel9)
        Me.tpOpTypes.Location = New System.Drawing.Point(4, 33)
        Me.tpOpTypes.Name = "tpOpTypes"
        Me.tpOpTypes.Padding = New System.Windows.Forms.Padding(3)
        Me.tpOpTypes.Size = New System.Drawing.Size(996, 625)
        Me.tpOpTypes.TabIndex = 4
        Me.tpOpTypes.Text = "Op Types"
        Me.tpOpTypes.UseVisualStyleBackColor = True
        '
        'dgvOpTypes
        '
        Me.dgvOpTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOpTypes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOpTypes.Location = New System.Drawing.Point(3, 3)
        Me.dgvOpTypes.Name = "dgvOpTypes"
        Me.dgvOpTypes.Size = New System.Drawing.Size(990, 532)
        Me.dgvOpTypes.TabIndex = 2
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Navy
        Me.Panel9.Controls.Add(Me.Panel10)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel9.Location = New System.Drawing.Point(3, 535)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(990, 87)
        Me.Panel9.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.btnUndoOpTypes)
        Me.Panel10.Controls.Add(Me.btnSaveOpTypes)
        Me.Panel10.Controls.Add(Me.btnCloseOpTypes)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel10.Location = New System.Drawing.Point(547, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(443, 87)
        Me.Panel10.TabIndex = 4
        '
        'btnUndoOpTypes
        '
        Me.btnUndoOpTypes.BackColor = System.Drawing.Color.White
        Me.btnUndoOpTypes.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoOpTypes.Image = CType(resources.GetObject("btnUndoOpTypes.Image"), System.Drawing.Image)
        Me.btnUndoOpTypes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoOpTypes.Location = New System.Drawing.Point(149, 5)
        Me.btnUndoOpTypes.Name = "btnUndoOpTypes"
        Me.btnUndoOpTypes.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoOpTypes.TabIndex = 3
        Me.btnUndoOpTypes.Text = "Cancel Change"
        Me.btnUndoOpTypes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoOpTypes.UseVisualStyleBackColor = False
        '
        'btnSaveOpTypes
        '
        Me.btnSaveOpTypes.BackColor = System.Drawing.Color.White
        Me.btnSaveOpTypes.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveOpTypes.Image = CType(resources.GetObject("btnSaveOpTypes.Image"), System.Drawing.Image)
        Me.btnSaveOpTypes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveOpTypes.Location = New System.Drawing.Point(3, 5)
        Me.btnSaveOpTypes.Name = "btnSaveOpTypes"
        Me.btnSaveOpTypes.Size = New System.Drawing.Size(140, 77)
        Me.btnSaveOpTypes.TabIndex = 2
        Me.btnSaveOpTypes.Text = "Save Op Types "
        Me.btnSaveOpTypes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveOpTypes.UseVisualStyleBackColor = False
        '
        'btnCloseOpTypes
        '
        Me.btnCloseOpTypes.BackColor = System.Drawing.Color.White
        Me.btnCloseOpTypes.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseOpTypes.Image = CType(resources.GetObject("btnCloseOpTypes.Image"), System.Drawing.Image)
        Me.btnCloseOpTypes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseOpTypes.Location = New System.Drawing.Point(295, 5)
        Me.btnCloseOpTypes.Name = "btnCloseOpTypes"
        Me.btnCloseOpTypes.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseOpTypes.TabIndex = 2
        Me.btnCloseOpTypes.Text = "Save and Exit"
        Me.btnCloseOpTypes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseOpTypes.UseVisualStyleBackColor = False
        '
        'tpOperations
        '
        Me.tpOperations.Controls.Add(Me.dgvOps)
        Me.tpOperations.Controls.Add(Me.Panel16)
        Me.tpOperations.Controls.Add(Me.Splitter2)
        Me.tpOperations.Controls.Add(Me.Panel13)
        Me.tpOperations.Controls.Add(Me.Panel11)
        Me.tpOperations.Location = New System.Drawing.Point(4, 33)
        Me.tpOperations.Name = "tpOperations"
        Me.tpOperations.Padding = New System.Windows.Forms.Padding(3)
        Me.tpOperations.Size = New System.Drawing.Size(996, 625)
        Me.tpOperations.TabIndex = 5
        Me.tpOperations.Text = "Operations"
        Me.tpOperations.UseVisualStyleBackColor = True
        '
        'dgvOps
        '
        Me.dgvOps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOps.ContextMenuStrip = Me.ctmOps
        Me.dgvOps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOps.Location = New System.Drawing.Point(3, 3)
        Me.dgvOps.Name = "dgvOps"
        Me.dgvOps.Size = New System.Drawing.Size(501, 245)
        Me.dgvOps.TabIndex = 3
        '
        'ctmOps
        '
        Me.ctmOps.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem})
        Me.ctmOps.Name = "ctmOps"
        Me.ctmOps.Size = New System.Drawing.Size(103, 48)
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(102, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(102, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.tcOperationOptions)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel16.Location = New System.Drawing.Point(3, 248)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(501, 204)
        Me.Panel16.TabIndex = 8
        Me.Panel16.Visible = False
        '
        'tcOperationOptions
        '
        Me.tcOperationOptions.Controls.Add(Me.tpSpeedy)
        Me.tcOperationOptions.Controls.Add(Me.tpNotching)
        Me.tcOperationOptions.Controls.Add(Me.tpOperationMisc)
        Me.tcOperationOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcOperationOptions.Location = New System.Drawing.Point(0, 0)
        Me.tcOperationOptions.Name = "tcOperationOptions"
        Me.tcOperationOptions.SelectedIndex = 0
        Me.tcOperationOptions.Size = New System.Drawing.Size(501, 204)
        Me.tcOperationOptions.TabIndex = 0
        '
        'tpSpeedy
        '
        Me.tpSpeedy.BackColor = System.Drawing.Color.Navy
        Me.tpSpeedy.Controls.Add(Me.btnRemoveSlow)
        Me.tpSpeedy.Controls.Add(Me.btnUnSpeedy)
        Me.tpSpeedy.Controls.Add(Me.btnSpeedy)
        Me.tpSpeedy.Controls.Add(Me.grpSpeedy)
        Me.tpSpeedy.Location = New System.Drawing.Point(4, 33)
        Me.tpSpeedy.Name = "tpSpeedy"
        Me.tpSpeedy.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSpeedy.Size = New System.Drawing.Size(493, 167)
        Me.tpSpeedy.TabIndex = 0
        Me.tpSpeedy.Text = "Speedy Ops"
        '
        'btnRemoveSlow
        '
        Me.btnRemoveSlow.BackColor = System.Drawing.Color.White
        Me.btnRemoveSlow.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveSlow.Image = CType(resources.GetObject("btnRemoveSlow.Image"), System.Drawing.Image)
        Me.btnRemoveSlow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemoveSlow.Location = New System.Drawing.Point(317, 78)
        Me.btnRemoveSlow.Name = "btnRemoveSlow"
        Me.btnRemoveSlow.Size = New System.Drawing.Size(140, 77)
        Me.btnRemoveSlow.TabIndex = 9
        Me.btnRemoveSlow.Text = "Remove Slow Ops"
        Me.btnRemoveSlow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRemoveSlow.UseVisualStyleBackColor = False
        '
        'btnUnSpeedy
        '
        Me.btnUnSpeedy.BackColor = System.Drawing.Color.White
        Me.btnUnSpeedy.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnSpeedy.Image = CType(resources.GetObject("btnUnSpeedy.Image"), System.Drawing.Image)
        Me.btnUnSpeedy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUnSpeedy.Location = New System.Drawing.Point(25, 78)
        Me.btnUnSpeedy.Name = "btnUnSpeedy"
        Me.btnUnSpeedy.Size = New System.Drawing.Size(140, 77)
        Me.btnUnSpeedy.TabIndex = 11
        Me.btnUnSpeedy.Text = "Un Speedy"
        Me.btnUnSpeedy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUnSpeedy.UseVisualStyleBackColor = False
        '
        'btnSpeedy
        '
        Me.btnSpeedy.BackColor = System.Drawing.Color.White
        Me.btnSpeedy.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpeedy.Image = CType(resources.GetObject("btnSpeedy.Image"), System.Drawing.Image)
        Me.btnSpeedy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSpeedy.Location = New System.Drawing.Point(171, 78)
        Me.btnSpeedy.Name = "btnSpeedy"
        Me.btnSpeedy.Size = New System.Drawing.Size(140, 77)
        Me.btnSpeedy.TabIndex = 4
        Me.btnSpeedy.Text = "Speedy Ops"
        Me.btnSpeedy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSpeedy.UseVisualStyleBackColor = False
        '
        'grpSpeedy
        '
        Me.grpSpeedy.Controls.Add(Me.chkSP3)
        Me.grpSpeedy.Controls.Add(Me.chkDrainEQ)
        Me.grpSpeedy.Controls.Add(Me.chkSP5)
        Me.grpSpeedy.Controls.Add(Me.chkDrain)
        Me.grpSpeedy.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpSpeedy.ForeColor = System.Drawing.Color.White
        Me.grpSpeedy.Location = New System.Drawing.Point(81, 21)
        Me.grpSpeedy.Name = "grpSpeedy"
        Me.grpSpeedy.Size = New System.Drawing.Size(320, 50)
        Me.grpSpeedy.TabIndex = 10
        Me.grpSpeedy.TabStop = False
        Me.grpSpeedy.Text = "Speedy Ops"
        '
        'chkSP3
        '
        Me.chkSP3.AutoSize = True
        Me.chkSP3.ForeColor = System.Drawing.Color.White
        Me.chkSP3.Location = New System.Drawing.Point(183, 22)
        Me.chkSP3.Name = "chkSP3"
        Me.chkSP3.Size = New System.Drawing.Size(58, 22)
        Me.chkSP3.TabIndex = 5
        Me.chkSP3.Text = "SP3"
        Me.chkSP3.UseVisualStyleBackColor = True
        '
        'chkDrainEQ
        '
        Me.chkDrainEQ.AutoSize = True
        Me.chkDrainEQ.ForeColor = System.Drawing.Color.White
        Me.chkDrainEQ.Location = New System.Drawing.Point(86, 22)
        Me.chkDrainEQ.Name = "chkDrainEQ"
        Me.chkDrainEQ.Size = New System.Drawing.Size(91, 22)
        Me.chkDrainEQ.TabIndex = 8
        Me.chkDrainEQ.Text = "DrainEQ"
        Me.chkDrainEQ.UseVisualStyleBackColor = True
        '
        'chkSP5
        '
        Me.chkSP5.AutoSize = True
        Me.chkSP5.ForeColor = System.Drawing.Color.White
        Me.chkSP5.Location = New System.Drawing.Point(258, 22)
        Me.chkSP5.Name = "chkSP5"
        Me.chkSP5.Size = New System.Drawing.Size(58, 22)
        Me.chkSP5.TabIndex = 6
        Me.chkSP5.Text = "SP5"
        Me.chkSP5.UseVisualStyleBackColor = True
        '
        'chkDrain
        '
        Me.chkDrain.AutoSize = True
        Me.chkDrain.Checked = True
        Me.chkDrain.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDrain.ForeColor = System.Drawing.Color.White
        Me.chkDrain.Location = New System.Drawing.Point(17, 22)
        Me.chkDrain.Name = "chkDrain"
        Me.chkDrain.Size = New System.Drawing.Size(68, 22)
        Me.chkDrain.TabIndex = 7
        Me.chkDrain.Text = "Drain"
        Me.chkDrain.UseVisualStyleBackColor = True
        '
        'tpNotching
        '
        Me.tpNotching.BackColor = System.Drawing.Color.Navy
        Me.tpNotching.Controls.Add(Me.grpNotching)
        Me.tpNotching.Location = New System.Drawing.Point(4, 33)
        Me.tpNotching.Name = "tpNotching"
        Me.tpNotching.Padding = New System.Windows.Forms.Padding(3)
        Me.tpNotching.Size = New System.Drawing.Size(493, 167)
        Me.tpNotching.TabIndex = 2
        Me.tpNotching.Text = "Notching"
        '
        'grpNotching
        '
        Me.grpNotching.Controls.Add(Me.btnReset)
        Me.grpNotching.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpNotching.ForeColor = System.Drawing.Color.White
        Me.grpNotching.Location = New System.Drawing.Point(20, 11)
        Me.grpNotching.Name = "grpNotching"
        Me.grpNotching.Size = New System.Drawing.Size(145, 50)
        Me.grpNotching.TabIndex = 17
        Me.grpNotching.TabStop = False
        Me.grpNotching.Text = "Notching"
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(6, 21)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(133, 23)
        Me.btnReset.TabIndex = 0
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'tpOperationMisc
        '
        Me.tpOperationMisc.BackColor = System.Drawing.Color.Navy
        Me.tpOperationMisc.Controls.Add(Me.grpColumnValues)
        Me.tpOperationMisc.Location = New System.Drawing.Point(4, 33)
        Me.tpOperationMisc.Name = "tpOperationMisc"
        Me.tpOperationMisc.Padding = New System.Windows.Forms.Padding(3)
        Me.tpOperationMisc.Size = New System.Drawing.Size(493, 167)
        Me.tpOperationMisc.TabIndex = 1
        Me.tpOperationMisc.Text = "Misc"
        '
        'grpColumnValues
        '
        Me.grpColumnValues.Controls.Add(Me.btnUpdateCol)
        Me.grpColumnValues.Controls.Add(Me.txtNewValue)
        Me.grpColumnValues.Controls.Add(Me.cmbColumn)
        Me.grpColumnValues.Controls.Add(Me.lblValue)
        Me.grpColumnValues.Controls.Add(Me.lblColumn)
        Me.grpColumnValues.ForeColor = System.Drawing.Color.White
        Me.grpColumnValues.Location = New System.Drawing.Point(20, 6)
        Me.grpColumnValues.Name = "grpColumnValues"
        Me.grpColumnValues.Size = New System.Drawing.Size(467, 145)
        Me.grpColumnValues.TabIndex = 18
        Me.grpColumnValues.TabStop = False
        Me.grpColumnValues.Text = "Change Column Values"
        '
        'btnUpdateCol
        '
        Me.btnUpdateCol.BackColor = System.Drawing.Color.White
        Me.btnUpdateCol.ForeColor = System.Drawing.Color.Black
        Me.btnUpdateCol.Location = New System.Drawing.Point(99, 94)
        Me.btnUpdateCol.Name = "btnUpdateCol"
        Me.btnUpdateCol.Size = New System.Drawing.Size(211, 36)
        Me.btnUpdateCol.TabIndex = 4
        Me.btnUpdateCol.Text = "Update Column"
        Me.btnUpdateCol.UseVisualStyleBackColor = False
        '
        'txtNewValue
        '
        Me.txtNewValue.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewValue.Location = New System.Drawing.Point(99, 62)
        Me.txtNewValue.Name = "txtNewValue"
        Me.txtNewValue.Size = New System.Drawing.Size(349, 26)
        Me.txtNewValue.TabIndex = 3
        '
        'cmbColumn
        '
        Me.cmbColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumn.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbColumn.FormattingEnabled = True
        Me.cmbColumn.Items.AddRange(New Object() {"Tool Code", "Description", "Variable 1", "Variable 2", "Variable 3", "Variable 4", "Variable 5", "Variable 6", "Variable 7", "Variable 8"})
        Me.cmbColumn.Location = New System.Drawing.Point(99, 28)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(144, 26)
        Me.cmbColumn.TabIndex = 2
        '
        'lblValue
        '
        Me.lblValue.AutoSize = True
        Me.lblValue.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.Location = New System.Drawing.Point(12, 65)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(55, 18)
        Me.lblValue.TabIndex = 1
        Me.lblValue.Text = "Value:"
        '
        'lblColumn
        '
        Me.lblColumn.AutoSize = True
        Me.lblColumn.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumn.Location = New System.Drawing.Point(12, 31)
        Me.lblColumn.Name = "lblColumn"
        Me.lblColumn.Size = New System.Drawing.Size(73, 18)
        Me.lblColumn.TabIndex = 0
        Me.lblColumn.Text = "Column:"
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter2.Location = New System.Drawing.Point(504, 3)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(10, 449)
        Me.Splitter2.TabIndex = 7
        Me.Splitter2.TabStop = False
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.pbPosCalc)
        Me.Panel13.Controls.Add(Me.Splitter1)
        Me.Panel13.Controls.Add(Me.Panel14)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel13.Location = New System.Drawing.Point(514, 3)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(479, 449)
        Me.Panel13.TabIndex = 6
        '
        'pbPosCalc
        '
        Me.pbPosCalc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPosCalc.Location = New System.Drawing.Point(0, 0)
        Me.pbPosCalc.Name = "pbPosCalc"
        Me.pbPosCalc.Size = New System.Drawing.Size(479, 339)
        Me.pbPosCalc.TabIndex = 5
        Me.pbPosCalc.TabStop = False
        '
        'Splitter1
        '
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter1.Location = New System.Drawing.Point(0, 339)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(479, 10)
        Me.Splitter1.TabIndex = 7
        Me.Splitter1.TabStop = False
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.Navy
        Me.Panel14.Controls.Add(Me.nebPL)
        Me.Panel14.Controls.Add(Me.nebToolDia)
        Me.Panel14.Controls.Add(Me.nebZoom)
        Me.Panel14.Controls.Add(Me.gbTool)
        Me.Panel14.Controls.Add(Me.lbltool)
        Me.Panel14.Controls.Add(Me.btnDraw)
        Me.Panel14.Controls.Add(Me.lblZoom)
        Me.Panel14.Controls.Add(Me.lblPL)
        Me.Panel14.Controls.Add(Me.lblANG)
        Me.Panel14.Controls.Add(Me.lblZP)
        Me.Panel14.Controls.Add(Me.nebANG)
        Me.Panel14.Controls.Add(Me.nebZP)
        Me.Panel14.Controls.Add(Me.lblYP)
        Me.Panel14.Controls.Add(Me.nebYP)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel14.Location = New System.Drawing.Point(0, 349)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(479, 100)
        Me.Panel14.TabIndex = 6
        '
        'nebPL
        '
        Me.nebPL.DecimalPlaces = 1
        Me.nebPL.Enabled = False
        Me.nebPL.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebPL.Location = New System.Drawing.Point(366, 5)
        Me.nebPL.Margin = New System.Windows.Forms.Padding(4)
        Me.nebPL.Maximum = New Decimal(New Integer() {150, 0, 0, 0})
        Me.nebPL.Name = "nebPL"
        Me.nebPL.Size = New System.Drawing.Size(65, 25)
        Me.nebPL.TabIndex = 21
        Me.nebPL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nebToolDia
        '
        Me.nebToolDia.DecimalPlaces = 1
        Me.nebToolDia.Enabled = False
        Me.nebToolDia.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebToolDia.Location = New System.Drawing.Point(366, 40)
        Me.nebToolDia.Margin = New System.Windows.Forms.Padding(4)
        Me.nebToolDia.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nebToolDia.Name = "nebToolDia"
        Me.nebToolDia.Size = New System.Drawing.Size(65, 25)
        Me.nebToolDia.TabIndex = 23
        Me.nebToolDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nebToolDia.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nebZoom
        '
        Me.nebZoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebZoom.Location = New System.Drawing.Point(60, 10)
        Me.nebZoom.Margin = New System.Windows.Forms.Padding(4)
        Me.nebZoom.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebZoom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebZoom.Name = "nebZoom"
        Me.nebZoom.Size = New System.Drawing.Size(45, 25)
        Me.nebZoom.TabIndex = 25
        Me.nebZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nebZoom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'gbTool
        '
        Me.gbTool.Controls.Add(Me.rb2)
        Me.gbTool.Controls.Add(Me.rb1)
        Me.gbTool.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTool.ForeColor = System.Drawing.Color.White
        Me.gbTool.Location = New System.Drawing.Point(113, 10)
        Me.gbTool.Margin = New System.Windows.Forms.Padding(4)
        Me.gbTool.Name = "gbTool"
        Me.gbTool.Padding = New System.Windows.Forms.Padding(4)
        Me.gbTool.Size = New System.Drawing.Size(80, 83)
        Me.gbTool.TabIndex = 27
        Me.gbTool.TabStop = False
        Me.gbTool.Text = "Position"
        '
        'rb2
        '
        Me.rb2.AutoSize = True
        Me.rb2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb2.ForeColor = System.Drawing.Color.White
        Me.rb2.Location = New System.Drawing.Point(38, 48)
        Me.rb2.Margin = New System.Windows.Forms.Padding(4)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(34, 22)
        Me.rb2.TabIndex = 1
        Me.rb2.Text = "2"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb1
        '
        Me.rb1.AutoSize = True
        Me.rb1.Checked = True
        Me.rb1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb1.ForeColor = System.Drawing.Color.White
        Me.rb1.Location = New System.Drawing.Point(8, 18)
        Me.rb1.Margin = New System.Windows.Forms.Padding(4)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(34, 22)
        Me.rb1.TabIndex = 0
        Me.rb1.TabStop = True
        Me.rb1.Text = "1"
        Me.rb1.UseVisualStyleBackColor = True
        '
        'lbltool
        '
        Me.lbltool.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltool.ForeColor = System.Drawing.Color.White
        Me.lbltool.Location = New System.Drawing.Point(321, 38)
        Me.lbltool.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbltool.Name = "lbltool"
        Me.lbltool.Size = New System.Drawing.Size(49, 26)
        Me.lbltool.TabIndex = 22
        Me.lbltool.Text = "Tool"
        Me.lbltool.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDraw
        '
        Me.btnDraw.BackColor = System.Drawing.Color.White
        Me.btnDraw.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDraw.Location = New System.Drawing.Point(4, 38)
        Me.btnDraw.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDraw.Name = "btnDraw"
        Me.btnDraw.Size = New System.Drawing.Size(101, 55)
        Me.btnDraw.TabIndex = 26
        Me.btnDraw.Text = "Draw"
        Me.btnDraw.UseVisualStyleBackColor = False
        '
        'lblZoom
        '
        Me.lblZoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZoom.ForeColor = System.Drawing.Color.White
        Me.lblZoom.Location = New System.Drawing.Point(4, 8)
        Me.lblZoom.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(48, 26)
        Me.lblZoom.TabIndex = 24
        Me.lblZoom.Text = "Zoom"
        Me.lblZoom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPL
        '
        Me.lblPL.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPL.ForeColor = System.Drawing.Color.White
        Me.lblPL.Location = New System.Drawing.Point(321, 7)
        Me.lblPL.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPL.Name = "lblPL"
        Me.lblPL.Size = New System.Drawing.Size(49, 26)
        Me.lblPL.TabIndex = 20
        Me.lblPL.Text = "PL"
        Me.lblPL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblANG
        '
        Me.lblANG.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblANG.ForeColor = System.Drawing.Color.White
        Me.lblANG.Location = New System.Drawing.Point(201, 69)
        Me.lblANG.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblANG.Name = "lblANG"
        Me.lblANG.Size = New System.Drawing.Size(37, 26)
        Me.lblANG.TabIndex = 18
        Me.lblANG.Text = "Ang"
        Me.lblANG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblZP
        '
        Me.lblZP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZP.ForeColor = System.Drawing.Color.White
        Me.lblZP.Location = New System.Drawing.Point(201, 38)
        Me.lblZP.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblZP.Name = "lblZP"
        Me.lblZP.Size = New System.Drawing.Size(37, 26)
        Me.lblZP.TabIndex = 16
        Me.lblZP.Text = "Zp"
        Me.lblZP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nebANG
        '
        Me.nebANG.DecimalPlaces = 1
        Me.nebANG.Enabled = False
        Me.nebANG.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebANG.Location = New System.Drawing.Point(246, 71)
        Me.nebANG.Margin = New System.Windows.Forms.Padding(4)
        Me.nebANG.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.nebANG.Name = "nebANG"
        Me.nebANG.Size = New System.Drawing.Size(65, 25)
        Me.nebANG.TabIndex = 19
        Me.nebANG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nebZP
        '
        Me.nebZP.DecimalPlaces = 1
        Me.nebZP.Enabled = False
        Me.nebZP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebZP.Location = New System.Drawing.Point(246, 40)
        Me.nebZP.Margin = New System.Windows.Forms.Padding(4)
        Me.nebZP.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nebZP.Minimum = New Decimal(New Integer() {120, 0, 0, -2147483648})
        Me.nebZP.Name = "nebZP"
        Me.nebZP.Size = New System.Drawing.Size(65, 25)
        Me.nebZP.TabIndex = 17
        Me.nebZP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblYP
        '
        Me.lblYP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYP.ForeColor = System.Drawing.Color.White
        Me.lblYP.Location = New System.Drawing.Point(201, 5)
        Me.lblYP.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblYP.Name = "lblYP"
        Me.lblYP.Size = New System.Drawing.Size(37, 26)
        Me.lblYP.TabIndex = 14
        Me.lblYP.Text = "Yp"
        Me.lblYP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nebYP
        '
        Me.nebYP.DecimalPlaces = 1
        Me.nebYP.Enabled = False
        Me.nebYP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebYP.Location = New System.Drawing.Point(246, 7)
        Me.nebYP.Margin = New System.Windows.Forms.Padding(4)
        Me.nebYP.Maximum = New Decimal(New Integer() {125, 0, 0, 0})
        Me.nebYP.Minimum = New Decimal(New Integer() {125, 0, 0, -2147483648})
        Me.nebYP.Name = "nebYP"
        Me.nebYP.Size = New System.Drawing.Size(65, 25)
        Me.nebYP.TabIndex = 15
        Me.nebYP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Navy
        Me.Panel11.Controls.Add(Me.lblRows)
        Me.Panel11.Controls.Add(Me.grpStdOpEQ)
        Me.Panel11.Controls.Add(Me.grpToolEQ)
        Me.Panel11.Controls.Add(Me.grpProfEQ)
        Me.Panel11.Controls.Add(Me.txtProfileCode)
        Me.Panel11.Controls.Add(Me.lblProfileCode)
        Me.Panel11.Controls.Add(Me.txtStdOp)
        Me.Panel11.Controls.Add(Me.txtToolCode)
        Me.Panel11.Controls.Add(Me.lblStdOp)
        Me.Panel11.Controls.Add(Me.lblToolCode)
        Me.Panel11.Controls.Add(Me.Panel12)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel11.Location = New System.Drawing.Point(3, 452)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(990, 170)
        Me.Panel11.TabIndex = 2
        '
        'grpStdOpEQ
        '
        Me.grpStdOpEQ.Controls.Add(Me.rbStdOpContains)
        Me.grpStdOpEQ.Controls.Add(Me.rbStdOpEQ)
        Me.grpStdOpEQ.Location = New System.Drawing.Point(295, 85)
        Me.grpStdOpEQ.Name = "grpStdOpEQ"
        Me.grpStdOpEQ.Size = New System.Drawing.Size(202, 43)
        Me.grpStdOpEQ.TabIndex = 16
        Me.grpStdOpEQ.TabStop = False
        '
        'rbStdOpContains
        '
        Me.rbStdOpContains.AutoSize = True
        Me.rbStdOpContains.Checked = True
        Me.rbStdOpContains.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbStdOpContains.ForeColor = System.Drawing.Color.White
        Me.rbStdOpContains.Location = New System.Drawing.Point(81, 16)
        Me.rbStdOpContains.Name = "rbStdOpContains"
        Me.rbStdOpContains.Size = New System.Drawing.Size(95, 22)
        Me.rbStdOpContains.TabIndex = 1
        Me.rbStdOpContains.TabStop = True
        Me.rbStdOpContains.Text = "Contains"
        Me.rbStdOpContains.UseVisualStyleBackColor = True
        '
        'rbStdOpEQ
        '
        Me.rbStdOpEQ.AutoSize = True
        Me.rbStdOpEQ.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbStdOpEQ.ForeColor = System.Drawing.Color.White
        Me.rbStdOpEQ.Location = New System.Drawing.Point(6, 16)
        Me.rbStdOpEQ.Name = "rbStdOpEQ"
        Me.rbStdOpEQ.Size = New System.Drawing.Size(69, 22)
        Me.rbStdOpEQ.TabIndex = 0
        Me.rbStdOpEQ.TabStop = True
        Me.rbStdOpEQ.Text = "Equal"
        Me.rbStdOpEQ.UseVisualStyleBackColor = True
        '
        'grpToolEQ
        '
        Me.grpToolEQ.Controls.Add(Me.rbToolContains)
        Me.grpToolEQ.Controls.Add(Me.rbToolEQ)
        Me.grpToolEQ.Location = New System.Drawing.Point(295, 41)
        Me.grpToolEQ.Name = "grpToolEQ"
        Me.grpToolEQ.Size = New System.Drawing.Size(202, 43)
        Me.grpToolEQ.TabIndex = 15
        Me.grpToolEQ.TabStop = False
        '
        'rbToolContains
        '
        Me.rbToolContains.AutoSize = True
        Me.rbToolContains.Checked = True
        Me.rbToolContains.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbToolContains.ForeColor = System.Drawing.Color.White
        Me.rbToolContains.Location = New System.Drawing.Point(81, 16)
        Me.rbToolContains.Name = "rbToolContains"
        Me.rbToolContains.Size = New System.Drawing.Size(95, 22)
        Me.rbToolContains.TabIndex = 1
        Me.rbToolContains.TabStop = True
        Me.rbToolContains.Text = "Contains"
        Me.rbToolContains.UseVisualStyleBackColor = True
        '
        'rbToolEQ
        '
        Me.rbToolEQ.AutoSize = True
        Me.rbToolEQ.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbToolEQ.ForeColor = System.Drawing.Color.White
        Me.rbToolEQ.Location = New System.Drawing.Point(6, 16)
        Me.rbToolEQ.Name = "rbToolEQ"
        Me.rbToolEQ.Size = New System.Drawing.Size(69, 22)
        Me.rbToolEQ.TabIndex = 0
        Me.rbToolEQ.TabStop = True
        Me.rbToolEQ.Text = "Equal"
        Me.rbToolEQ.UseVisualStyleBackColor = True
        '
        'grpProfEQ
        '
        Me.grpProfEQ.Controls.Add(Me.rbProfContains)
        Me.grpProfEQ.Controls.Add(Me.rbProfEQ)
        Me.grpProfEQ.Location = New System.Drawing.Point(296, 0)
        Me.grpProfEQ.Name = "grpProfEQ"
        Me.grpProfEQ.Size = New System.Drawing.Size(202, 40)
        Me.grpProfEQ.TabIndex = 14
        Me.grpProfEQ.TabStop = False
        '
        'rbProfContains
        '
        Me.rbProfContains.AutoSize = True
        Me.rbProfContains.Checked = True
        Me.rbProfContains.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbProfContains.ForeColor = System.Drawing.Color.White
        Me.rbProfContains.Location = New System.Drawing.Point(81, 16)
        Me.rbProfContains.Name = "rbProfContains"
        Me.rbProfContains.Size = New System.Drawing.Size(95, 22)
        Me.rbProfContains.TabIndex = 1
        Me.rbProfContains.TabStop = True
        Me.rbProfContains.Text = "Contains"
        Me.rbProfContains.UseVisualStyleBackColor = True
        '
        'rbProfEQ
        '
        Me.rbProfEQ.AutoSize = True
        Me.rbProfEQ.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbProfEQ.ForeColor = System.Drawing.Color.White
        Me.rbProfEQ.Location = New System.Drawing.Point(6, 16)
        Me.rbProfEQ.Name = "rbProfEQ"
        Me.rbProfEQ.Size = New System.Drawing.Size(69, 22)
        Me.rbProfEQ.TabIndex = 0
        Me.rbProfEQ.TabStop = True
        Me.rbProfEQ.Text = "Equal"
        Me.rbProfEQ.UseVisualStyleBackColor = True
        '
        'txtProfileCode
        '
        Me.txtProfileCode.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfileCode.Location = New System.Drawing.Point(133, 14)
        Me.txtProfileCode.Name = "txtProfileCode"
        Me.txtProfileCode.Size = New System.Drawing.Size(154, 26)
        Me.txtProfileCode.TabIndex = 10
        '
        'lblProfileCode
        '
        Me.lblProfileCode.AutoSize = True
        Me.lblProfileCode.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileCode.ForeColor = System.Drawing.Color.White
        Me.lblProfileCode.Location = New System.Drawing.Point(21, 18)
        Me.lblProfileCode.Name = "lblProfileCode"
        Me.lblProfileCode.Size = New System.Drawing.Size(106, 18)
        Me.lblProfileCode.TabIndex = 9
        Me.lblProfileCode.Text = "Profile Code:"
        '
        'txtStdOp
        '
        Me.txtStdOp.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStdOp.Location = New System.Drawing.Point(133, 100)
        Me.txtStdOp.Name = "txtStdOp"
        Me.txtStdOp.Size = New System.Drawing.Size(154, 26)
        Me.txtStdOp.TabIndex = 8
        '
        'txtToolCode
        '
        Me.txtToolCode.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToolCode.Location = New System.Drawing.Point(133, 58)
        Me.txtToolCode.Name = "txtToolCode"
        Me.txtToolCode.Size = New System.Drawing.Size(154, 26)
        Me.txtToolCode.TabIndex = 7
        '
        'lblStdOp
        '
        Me.lblStdOp.AutoSize = True
        Me.lblStdOp.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdOp.ForeColor = System.Drawing.Color.White
        Me.lblStdOp.Location = New System.Drawing.Point(21, 103)
        Me.lblStdOp.Name = "lblStdOp"
        Me.lblStdOp.Size = New System.Drawing.Size(103, 18)
        Me.lblStdOp.TabIndex = 6
        Me.lblStdOp.Text = "StdOp Code:"
        '
        'lblToolCode
        '
        Me.lblToolCode.AutoSize = True
        Me.lblToolCode.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToolCode.ForeColor = System.Drawing.Color.White
        Me.lblToolCode.Location = New System.Drawing.Point(21, 61)
        Me.lblToolCode.Name = "lblToolCode"
        Me.lblToolCode.Size = New System.Drawing.Size(90, 18)
        Me.lblToolCode.TabIndex = 5
        Me.lblToolCode.Text = "Tool Code:"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.btnHidePreview)
        Me.Panel12.Controls.Add(Me.btnShow)
        Me.Panel12.Controls.Add(Me.btnUndoOps)
        Me.Panel12.Controls.Add(Me.btnSaveOps)
        Me.Panel12.Controls.Add(Me.btnExitOps)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel12.Location = New System.Drawing.Point(511, 0)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(479, 170)
        Me.Panel12.TabIndex = 4
        '
        'btnHidePreview
        '
        Me.btnHidePreview.BackColor = System.Drawing.Color.White
        Me.btnHidePreview.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHidePreview.ForeColor = System.Drawing.Color.Black
        Me.btnHidePreview.Image = CType(resources.GetObject("btnHidePreview.Image"), System.Drawing.Image)
        Me.btnHidePreview.Location = New System.Drawing.Point(188, 3)
        Me.btnHidePreview.Name = "btnHidePreview"
        Me.btnHidePreview.Size = New System.Drawing.Size(140, 76)
        Me.btnHidePreview.TabIndex = 5
        Me.btnHidePreview.Text = "Hide Preview"
        Me.btnHidePreview.UseVisualStyleBackColor = False
        '
        'btnShow
        '
        Me.btnShow.BackColor = System.Drawing.Color.White
        Me.btnShow.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShow.Image = CType(resources.GetObject("btnShow.Image"), System.Drawing.Image)
        Me.btnShow.Location = New System.Drawing.Point(42, 3)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(140, 77)
        Me.btnShow.TabIndex = 4
        Me.btnShow.Text = "Show Options"
        Me.btnShow.UseVisualStyleBackColor = False
        '
        'btnUndoOps
        '
        Me.btnUndoOps.BackColor = System.Drawing.Color.White
        Me.btnUndoOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoOps.Image = CType(resources.GetObject("btnUndoOps.Image"), System.Drawing.Image)
        Me.btnUndoOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoOps.Location = New System.Drawing.Point(188, 85)
        Me.btnUndoOps.Name = "btnUndoOps"
        Me.btnUndoOps.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoOps.TabIndex = 3
        Me.btnUndoOps.Text = "Cancel Change"
        Me.btnUndoOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoOps.UseVisualStyleBackColor = False
        '
        'btnSaveOps
        '
        Me.btnSaveOps.BackColor = System.Drawing.Color.White
        Me.btnSaveOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveOps.Image = CType(resources.GetObject("btnSaveOps.Image"), System.Drawing.Image)
        Me.btnSaveOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveOps.Location = New System.Drawing.Point(42, 85)
        Me.btnSaveOps.Name = "btnSaveOps"
        Me.btnSaveOps.Size = New System.Drawing.Size(140, 77)
        Me.btnSaveOps.TabIndex = 2
        Me.btnSaveOps.Text = "Save Ops"
        Me.btnSaveOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveOps.UseVisualStyleBackColor = False
        '
        'btnExitOps
        '
        Me.btnExitOps.BackColor = System.Drawing.Color.White
        Me.btnExitOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExitOps.Image = CType(resources.GetObject("btnExitOps.Image"), System.Drawing.Image)
        Me.btnExitOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExitOps.Location = New System.Drawing.Point(334, 85)
        Me.btnExitOps.Name = "btnExitOps"
        Me.btnExitOps.Size = New System.Drawing.Size(140, 77)
        Me.btnExitOps.TabIndex = 2
        Me.btnExitOps.Text = "Save and Exit"
        Me.btnExitOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnExitOps.UseVisualStyleBackColor = False
        '
        'tpMisc
        '
        Me.tpMisc.Controls.Add(Me.Panel15)
        Me.tpMisc.Location = New System.Drawing.Point(4, 33)
        Me.tpMisc.Name = "tpMisc"
        Me.tpMisc.Padding = New System.Windows.Forms.Padding(3)
        Me.tpMisc.Size = New System.Drawing.Size(996, 625)
        Me.tpMisc.TabIndex = 6
        Me.tpMisc.Text = "Misc"
        Me.tpMisc.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Navy
        Me.Panel15.Controls.Add(Me.grpNotch)
        Me.Panel15.Controls.Add(Me.grpSetup)
        Me.Panel15.Controls.Add(Me.grpFiles)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel15.Location = New System.Drawing.Point(3, 3)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(990, 619)
        Me.Panel15.TabIndex = 0
        '
        'grpNotch
        '
        Me.grpNotch.Controls.Add(Me.btnYOFF)
        Me.grpNotch.Controls.Add(Me.btnVOFF)
        Me.grpNotch.Controls.Add(Me.btnVON)
        Me.grpNotch.Controls.Add(Me.btnYON)
        Me.grpNotch.ForeColor = System.Drawing.Color.White
        Me.grpNotch.Location = New System.Drawing.Point(408, 16)
        Me.grpNotch.Name = "grpNotch"
        Me.grpNotch.Size = New System.Drawing.Size(348, 208)
        Me.grpNotch.TabIndex = 13
        Me.grpNotch.TabStop = False
        Me.grpNotch.Text = "Noching"
        '
        'btnYOFF
        '
        Me.btnYOFF.BackColor = System.Drawing.Color.White
        Me.btnYOFF.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYOFF.ForeColor = System.Drawing.Color.Black
        Me.btnYOFF.Location = New System.Drawing.Point(176, 114)
        Me.btnYOFF.Name = "btnYOFF"
        Me.btnYOFF.Size = New System.Drawing.Size(152, 72)
        Me.btnYOFF.TabIndex = 13
        Me.btnYOFF.Text = "Y Off"
        Me.btnYOFF.UseVisualStyleBackColor = False
        '
        'btnVOFF
        '
        Me.btnVOFF.BackColor = System.Drawing.Color.White
        Me.btnVOFF.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVOFF.ForeColor = System.Drawing.Color.Black
        Me.btnVOFF.Location = New System.Drawing.Point(176, 36)
        Me.btnVOFF.Name = "btnVOFF"
        Me.btnVOFF.Size = New System.Drawing.Size(152, 72)
        Me.btnVOFF.TabIndex = 12
        Me.btnVOFF.Text = "V Off"
        Me.btnVOFF.UseVisualStyleBackColor = False
        '
        'btnVON
        '
        Me.btnVON.BackColor = System.Drawing.Color.White
        Me.btnVON.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVON.ForeColor = System.Drawing.Color.Black
        Me.btnVON.Location = New System.Drawing.Point(18, 36)
        Me.btnVON.Name = "btnVON"
        Me.btnVON.Size = New System.Drawing.Size(152, 72)
        Me.btnVON.TabIndex = 11
        Me.btnVON.Text = "V On"
        Me.btnVON.UseVisualStyleBackColor = False
        '
        'btnYON
        '
        Me.btnYON.BackColor = System.Drawing.Color.White
        Me.btnYON.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYON.ForeColor = System.Drawing.Color.Black
        Me.btnYON.Location = New System.Drawing.Point(18, 114)
        Me.btnYON.Name = "btnYON"
        Me.btnYON.Size = New System.Drawing.Size(152, 72)
        Me.btnYON.TabIndex = 10
        Me.btnYON.Text = "Y On"
        Me.btnYON.UseVisualStyleBackColor = False
        '
        'grpSetup
        '
        Me.grpSetup.Controls.Add(Me.btnGripperSetup)
        Me.grpSetup.Controls.Add(Me.btnZBlock)
        Me.grpSetup.ForeColor = System.Drawing.Color.White
        Me.grpSetup.Location = New System.Drawing.Point(211, 16)
        Me.grpSetup.Name = "grpSetup"
        Me.grpSetup.Size = New System.Drawing.Size(191, 208)
        Me.grpSetup.TabIndex = 12
        Me.grpSetup.TabStop = False
        Me.grpSetup.Text = "Setup"
        '
        'btnGripperSetup
        '
        Me.btnGripperSetup.BackColor = System.Drawing.Color.White
        Me.btnGripperSetup.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGripperSetup.ForeColor = System.Drawing.Color.Black
        Me.btnGripperSetup.Image = CType(resources.GetObject("btnGripperSetup.Image"), System.Drawing.Image)
        Me.btnGripperSetup.Location = New System.Drawing.Point(18, 31)
        Me.btnGripperSetup.Name = "btnGripperSetup"
        Me.btnGripperSetup.Size = New System.Drawing.Size(152, 77)
        Me.btnGripperSetup.TabIndex = 8
        Me.btnGripperSetup.Text = "Edit Gripper"
        Me.btnGripperSetup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGripperSetup.UseVisualStyleBackColor = False
        '
        'btnZBlock
        '
        Me.btnZBlock.BackColor = System.Drawing.Color.White
        Me.btnZBlock.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnZBlock.ForeColor = System.Drawing.Color.Black
        Me.btnZBlock.Location = New System.Drawing.Point(18, 114)
        Me.btnZBlock.Name = "btnZBlock"
        Me.btnZBlock.Size = New System.Drawing.Size(152, 72)
        Me.btnZBlock.TabIndex = 10
        Me.btnZBlock.Text = "ZBlock"
        Me.btnZBlock.UseVisualStyleBackColor = False
        '
        'grpFiles
        '
        Me.grpFiles.Controls.Add(Me.btnCreateAHAdjustMul)
        Me.grpFiles.Controls.Add(Me.btnCreateProfileSaw)
        Me.grpFiles.ForeColor = System.Drawing.Color.White
        Me.grpFiles.Location = New System.Drawing.Point(14, 16)
        Me.grpFiles.Name = "grpFiles"
        Me.grpFiles.Size = New System.Drawing.Size(191, 208)
        Me.grpFiles.TabIndex = 11
        Me.grpFiles.TabStop = False
        Me.grpFiles.Text = "Files"
        '
        'btnCreateAHAdjustMul
        '
        Me.btnCreateAHAdjustMul.BackColor = System.Drawing.Color.White
        Me.btnCreateAHAdjustMul.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreateAHAdjustMul.ForeColor = System.Drawing.Color.Black
        Me.btnCreateAHAdjustMul.Location = New System.Drawing.Point(18, 114)
        Me.btnCreateAHAdjustMul.Name = "btnCreateAHAdjustMul"
        Me.btnCreateAHAdjustMul.Size = New System.Drawing.Size(152, 72)
        Me.btnCreateAHAdjustMul.TabIndex = 11
        Me.btnCreateAHAdjustMul.Text = "Create AHAdjust.Mul"
        Me.btnCreateAHAdjustMul.UseVisualStyleBackColor = False
        '
        'btnCreateProfileSaw
        '
        Me.btnCreateProfileSaw.BackColor = System.Drawing.Color.White
        Me.btnCreateProfileSaw.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreateProfileSaw.ForeColor = System.Drawing.Color.Black
        Me.btnCreateProfileSaw.Image = CType(resources.GetObject("btnCreateProfileSaw.Image"), System.Drawing.Image)
        Me.btnCreateProfileSaw.Location = New System.Drawing.Point(19, 31)
        Me.btnCreateProfileSaw.Name = "btnCreateProfileSaw"
        Me.btnCreateProfileSaw.Size = New System.Drawing.Size(151, 77)
        Me.btnCreateProfileSaw.TabIndex = 3
        Me.btnCreateProfileSaw.Text = "Create Profile.Saw"
        Me.btnCreateProfileSaw.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCreateProfileSaw.UseVisualStyleBackColor = False
        '
        'lblRows
        '
        Me.lblRows.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRows.ForeColor = System.Drawing.Color.PowderBlue
        Me.lblRows.Location = New System.Drawing.Point(21, 144)
        Me.lblRows.Name = "lblRows"
        Me.lblRows.Size = New System.Drawing.Size(225, 18)
        Me.lblRows.TabIndex = 17
        Me.lblRows.Text = "Operations:"
        '
        'frmEngineer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 662)
        Me.Controls.Add(Me.tcEngineer)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEngineer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stuga Flowline Ops"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.dgvProfile, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctmProfile.ResumeLayout(False)
        Me.tcEngineer.ResumeLayout(False)
        Me.tpProfile.ResumeLayout(False)
        Me.tpStandardOps.ResumeLayout(False)
        CType(Me.dgvStdOps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.tpSuites.ResumeLayout(False)
        CType(Me.dgvSuites, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.tpHardware.ResumeLayout(False)
        CType(Me.dgvHardware, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.tpOpTypes.ResumeLayout(False)
        CType(Me.dgvOpTypes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.tpOperations.ResumeLayout(False)
        CType(Me.dgvOps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctmOps.ResumeLayout(False)
        Me.Panel16.ResumeLayout(False)
        Me.tcOperationOptions.ResumeLayout(False)
        Me.tpSpeedy.ResumeLayout(False)
        Me.grpSpeedy.ResumeLayout(False)
        Me.grpSpeedy.PerformLayout()
        Me.tpNotching.ResumeLayout(False)
        Me.grpNotching.ResumeLayout(False)
        Me.tpOperationMisc.ResumeLayout(False)
        Me.grpColumnValues.ResumeLayout(False)
        Me.grpColumnValues.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel14.ResumeLayout(False)
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebToolDia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTool.ResumeLayout(False)
        Me.gbTool.PerformLayout()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.grpStdOpEQ.ResumeLayout(False)
        Me.grpStdOpEQ.PerformLayout()
        Me.grpToolEQ.ResumeLayout(False)
        Me.grpToolEQ.PerformLayout()
        Me.grpProfEQ.ResumeLayout(False)
        Me.grpProfEQ.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.tpMisc.ResumeLayout(False)
        Me.Panel15.ResumeLayout(False)
        Me.grpNotch.ResumeLayout(False)
        Me.grpSetup.ResumeLayout(False)
        Me.grpFiles.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnEditProfile As System.Windows.Forms.Button
    Friend WithEvents dgvProfile As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents tcEngineer As System.Windows.Forms.TabControl
    Friend WithEvents tpProfile As System.Windows.Forms.TabPage
    Friend WithEvents tpStandardOps As System.Windows.Forms.TabPage
    Friend WithEvents dgvStdOps As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnSaveStdOps As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoProfiles As System.Windows.Forms.Button
    Friend WithEvents btnCloseProfile As System.Windows.Forms.Button
    Friend WithEvents tpSuites As System.Windows.Forms.TabPage
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnCancelStdOps As System.Windows.Forms.Button
    Friend WithEvents btnCloseStdOps As System.Windows.Forms.Button
    Friend WithEvents dgvSuites As System.Windows.Forms.DataGridView
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoSuites As System.Windows.Forms.Button
    Friend WithEvents btnSaveSuites As System.Windows.Forms.Button
    Friend WithEvents btnCloseSuites As System.Windows.Forms.Button
    Friend WithEvents tpHardware As System.Windows.Forms.TabPage
    Friend WithEvents dgvHardware As System.Windows.Forms.DataGridView
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoHardware As System.Windows.Forms.Button
    Friend WithEvents btnCloseHardware As System.Windows.Forms.Button
    Friend WithEvents btnSaveHardware As System.Windows.Forms.Button
    Friend WithEvents btnEditHardware As System.Windows.Forms.Button
    Friend WithEvents tpOpTypes As System.Windows.Forms.TabPage
    Friend WithEvents dgvOpTypes As System.Windows.Forms.DataGridView
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoOpTypes As System.Windows.Forms.Button
    Friend WithEvents btnSaveOpTypes As System.Windows.Forms.Button
    Friend WithEvents btnCloseOpTypes As System.Windows.Forms.Button
    Friend WithEvents tpOperations As System.Windows.Forms.TabPage
    Friend WithEvents dgvOps As System.Windows.Forms.DataGridView
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoOps As System.Windows.Forms.Button
    Friend WithEvents btnSaveOps As System.Windows.Forms.Button
    Friend WithEvents btnExitOps As System.Windows.Forms.Button
    Friend WithEvents lblStdOp As System.Windows.Forms.Label
    Friend WithEvents lblToolCode As System.Windows.Forms.Label
    Friend WithEvents txtStdOp As System.Windows.Forms.TextBox
    Friend WithEvents txtToolCode As System.Windows.Forms.TextBox
    Friend WithEvents lblProfileCode As System.Windows.Forms.Label
    Friend WithEvents txtProfileCode As System.Windows.Forms.TextBox
    Friend WithEvents grpProfEQ As System.Windows.Forms.GroupBox
    Friend WithEvents rbProfContains As System.Windows.Forms.RadioButton
    Friend WithEvents rbProfEQ As System.Windows.Forms.RadioButton
    Friend WithEvents grpToolEQ As System.Windows.Forms.GroupBox
    Friend WithEvents rbToolContains As System.Windows.Forms.RadioButton
    Friend WithEvents rbToolEQ As System.Windows.Forms.RadioButton
    Friend WithEvents grpStdOpEQ As System.Windows.Forms.GroupBox
    Friend WithEvents rbStdOpContains As System.Windows.Forms.RadioButton
    Friend WithEvents rbStdOpEQ As System.Windows.Forms.RadioButton
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents cmbProfSuite As System.Windows.Forms.ComboBox
    Friend WithEvents lblSuite As System.Windows.Forms.Label
    Friend WithEvents ctmOps As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnRemoveSlow As System.Windows.Forms.Button
    Friend WithEvents chkDrainEQ As System.Windows.Forms.CheckBox
    Friend WithEvents chkDrain As System.Windows.Forms.CheckBox
    Friend WithEvents chkSP5 As System.Windows.Forms.CheckBox
    Friend WithEvents chkSP3 As System.Windows.Forms.CheckBox
    Friend WithEvents btnSpeedy As System.Windows.Forms.Button
    Friend WithEvents grpSpeedy As System.Windows.Forms.GroupBox
    Friend WithEvents chkShowReplicates As System.Windows.Forms.CheckBox
    Friend WithEvents ctmProfile As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmiCopyProfile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pbPosCalc As System.Windows.Forms.PictureBox
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents nebPL As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebToolDia As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebZoom As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbTool As System.Windows.Forms.GroupBox
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents lbltool As System.Windows.Forms.Label
    Friend WithEvents btnDraw As System.Windows.Forms.Button
    Friend WithEvents lblZoom As System.Windows.Forms.Label
    Friend WithEvents lblPL As System.Windows.Forms.Label
    Friend WithEvents lblANG As System.Windows.Forms.Label
    Friend WithEvents lblZP As System.Windows.Forms.Label
    Friend WithEvents nebANG As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebZP As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblYP As System.Windows.Forms.Label
    Friend WithEvents nebYP As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnUnSpeedy As System.Windows.Forms.Button
    Friend WithEvents grpNotching As System.Windows.Forms.GroupBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents tpMisc As System.Windows.Forms.TabPage
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents grpNotch As System.Windows.Forms.GroupBox
    Friend WithEvents btnYOFF As System.Windows.Forms.Button
    Friend WithEvents btnVOFF As System.Windows.Forms.Button
    Friend WithEvents btnVON As System.Windows.Forms.Button
    Friend WithEvents btnYON As System.Windows.Forms.Button
    Friend WithEvents grpSetup As System.Windows.Forms.GroupBox
    Friend WithEvents btnGripperSetup As System.Windows.Forms.Button
    Friend WithEvents btnZBlock As System.Windows.Forms.Button
    Friend WithEvents grpFiles As System.Windows.Forms.GroupBox
    Friend WithEvents btnCreateAHAdjustMul As System.Windows.Forms.Button
    Friend WithEvents btnCreateProfileSaw As System.Windows.Forms.Button
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents tcOperationOptions As System.Windows.Forms.TabControl
    Friend WithEvents tpSpeedy As System.Windows.Forms.TabPage
    Friend WithEvents tpOperationMisc As System.Windows.Forms.TabPage
    Friend WithEvents grpColumnValues As System.Windows.Forms.GroupBox
    Friend WithEvents btnUpdateCol As System.Windows.Forms.Button
    Friend WithEvents txtNewValue As System.Windows.Forms.TextBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents lblColumn As System.Windows.Forms.Label
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents btnEditStdop As System.Windows.Forms.Button
    Friend WithEvents tpNotching As System.Windows.Forms.TabPage
    Friend WithEvents btnHidePreview As System.Windows.Forms.Button
    Friend WithEvents lblRows As System.Windows.Forms.Label

End Class
