﻿Imports System.IO

Public Class clsParseMND

#Region " Variable Declaration "

    'Position 1 Links
    Public YP1 As String
    Public ZP1 As String
    Public ANG1 As String
    Public PL1 As String
    Public TL1 As String
    Public WID1 As String
    Public HEI1 As String

    'Position 2 Links
    Public YP2 As String
    Public ZP2 As String
    Public ANG2 As String
    Public PL2 As String
    Public TL2 As String
    Public WID2 As String
    Public HEI2 As String

    'Position 1 Variables
    Public dY1 As Double
    Public dZ1 As Double
    Public dAng1 As Double
    Public dPl1 As Double
    Public dTl1 As Double

    'Position 2 variables
    Public dY2 As Double
    Public dZ2 As Double
    Public dAng2 As Double
    Public dPl2 As Double
    Public dTl2 As Double

    'Box Variables
    Public dBoxWidth1 As Double
    Public dBoxHeight1 As Double
    Public dBoxWidth2 As Double
    Public dBoxHeight2 As Double

    'Box Links
    Public BW1 As String
    Public BH1 As String
    Public BW2 As String
    Public BH2 As String

    Public Description1 As String
    Public Description2 As String

    'Misc Variables
    Public sMisc1Desc As String
    Public dMisc1Pos As Double
    Public sMisc1Var As String
    Public sMisc2Desc As String
    Public dMisc2Pos As Double
    Public sMisc2Var As String
    Public sMisc3Desc As String
    Public dMisc3Pos As Double
    Public sMisc3Var As String
    Public sMisc4Desc As String
    Public dMisc4Pos As Double
    Public sMisc4Var As String

#End Region

#Region " Sub - ResetVariables() "

    Sub ResetVariables()

        'Position 1 Variables
        dY1 = 0
        dZ1 = 0
        dAng1 = 0
        dPl1 = 0
        dTl1 = 5

        'Position 2 variables
        dY2 = 0
        dZ2 = 0
        dAng2 = 0
        dPl2 = 0
        dTl2 = 5

        'Position 1 Links
        YP1 = ""
        ZP1 = ""
        ANG1 = ""
        PL1 = ""
        TL1 = ""
        WID1 = ""
        HEI1 = ""

        'Position 2 Links
        YP2 = ""
        ZP2 = ""
        ANG2 = ""
        PL2 = ""
        TL2 = ""
        WID2 = ""
        HEI2 = ""

        'Box Variables
        dBoxWidth1 = 0
        dBoxHeight1 = 0
        dBoxWidth2 = 0
        dBoxHeight2 = 0

        'Box Links
        BW1 = ""
        BH1 = ""
        BW2 = ""
        BH2 = ""

        'Position Descriptions
        Description1 = ""
        Description2 = ""

        'Misc Variables
        sMisc1Desc = ""
        dMisc1Pos = 0
        sMisc1Var = ""

        sMisc2Desc = ""
        dMisc2Pos = 0
        sMisc2Var = ""

        sMisc3Desc = ""
        dMisc3Pos = 0
        sMisc3Var = ""

        sMisc4Desc = ""
        dMisc4Pos = 0
        sMisc4Var = ""

    End Sub

#End Region

#Region " Sub - ParseMNDFile() "

    Public Sub ParseMNDFile(ByVal sFile As String, ByVal V1 As Double, ByVal V2 As Double, ByVal V3 As Double, ByVal V4 As Double, ByVal V5 As Double, ByVal V6 As Double, ByVal V7 As Double, ByVal V8 As Double, ByVal rb1 As RadioButton, ByVal nebYP As NumericUpDown, ByVal nebZP As NumericUpDown, ByVal nebANG As NumericUpDown, ByVal nebPL As NumericUpDown, ByVal nebToolDia As NumericUpDown)

        'Declare Misc Variables
        Dim iPos As Integer

        'Reset Linked Variables
        ResetVariables()

        If Not File.Exists(sFile) Then
            MsgBox("File: " + sFile + " does not exist.", MsgBoxStyle.Exclamation)
            Return
        End If

        'Open MND file
        Try
            FileOpen(2, sFile, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)
        Catch ex As Exception
            MsgBox("Error Opening File:" + sFile + vbCrLf + ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try

        Do While Not EOF(2)
            Dim sLine As String = LineInput(2)

            'Get YP1
            iPos = InStr(sLine, ";YP1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dY1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                YP1 = TagParser(sVar)
            End If

            'Get YP2
            iPos = InStr(sLine, ";YP2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dY2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                YP2 = TagParser(sVar)
            End If

            'Get ZP1
            iPos = InStr(sLine, ";ZP1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dZ1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                ZP1 = TagParser(sVar)
            End If

            'Get ZP2
            iPos = InStr(sLine, ";ZP2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dZ2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                ZP2 = TagParser(sVar)
            End If

            'Get ANG1
            iPos = InStr(sLine, ";ANG1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 7)
                dAng1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                ANG1 = TagParser(sVar)
            End If

            'Get ANG2
            iPos = InStr(sLine, ";ANG2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 7)
                dAng2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                ANG2 = TagParser(sVar)
            End If

            'Get PL1
            iPos = InStr(sLine, ";PL1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dPl1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                PL1 = TagParser(sVar)
            End If

            'Get PL2
            iPos = InStr(sLine, ";PL2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dPl2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                PL2 = TagParser(sVar)
            End If

            'Get TL1
            iPos = InStr(sLine, ";TL1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dTl1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
            End If

            'Get TL2
            iPos = InStr(sLine, ";TL2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dTl2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
            End If

            'Get BW1
            iPos = InStr(sLine, ";BW1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dBoxWidth1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                BW1 = TagParser(sVar)
            End If

            'Get BW2
            iPos = InStr(sLine, ";BW2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dBoxWidth2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                BW2 = TagParser(sVar)
            End If

            'Get BH1
            iPos = InStr(sLine, ";BH1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dBoxHeight1 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                BH1 = TagParser(sVar)
            End If

            'Get BH2
            iPos = InStr(sLine, ";BH2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 6)
                dBoxHeight2 = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                BH2 = TagParser(sVar)
            End If

            'Get DESC1
            iPos = InStr(sLine, ";DESC1=", CompareMethod.Text)
            If iPos <> 0 Then Description1 = Mid(sLine, 8)

            'Get DESC2
            iPos = InStr(sLine, ";DESC2=", CompareMethod.Text)
            If iPos <> 0 Then Description2 = Mid(sLine, 8)

            'Get Misc1Desc
            iPos = InStr(sLine, ";MISC1DESC=", CompareMethod.Text)
            If iPos <> 0 Then sMisc1Desc = Mid(sLine, 12)

            'Get Misc1
            iPos = InStr(sLine, ";MISC1=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 8)
                dMisc1Pos = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                sMisc1Var = TagParser(sVar)
            End If

            'Get MISC2DESC
            iPos = InStr(sLine, ";MISC2DESC=", CompareMethod.Text)
            If iPos <> 0 Then sMisc2Desc = Mid(sLine, 12)

            'Get Misc2
            iPos = InStr(sLine, ";MISC2=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 8)
                dMisc2Pos = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                sMisc2Var = TagParser(sVar)
            End If

            'Get MISC3DESC
            iPos = InStr(sLine, ";MISC3DESC=", CompareMethod.Text)
            If iPos <> 0 Then sMisc3Desc = Mid(sLine, 12)

            'Get Misc3
            iPos = InStr(sLine, ";MISC3=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 8)
                dMisc3Pos = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                sMisc3Var = TagParser(sVar)
             End If

            'Get MISC4DESC
            iPos = InStr(sLine, ";MISC4DESC=", CompareMethod.Text)
            If iPos <> 0 Then sMisc4Desc = Mid(sLine, 12)

            'Get Misc4
            iPos = InStr(sLine, ";MISC4=", CompareMethod.Text)
            If iPos <> 0 Then
                Dim sVar As String = Mid(sLine, 8)
                dMisc4Pos = ValueParser(sVar, V1, V2, V3, V4, V5, V6, V7, V8)
                sMisc4Var = TagParser(sVar)
            End If

        Loop

        'Close MND File
        FileClose(2)

        'Display Positions
        If dY1 > nebYP.Maximum Then dY1 = nebYP.Maximum
        If dY1 < nebYP.Minimum Then dY1 = nebYP.Minimum
        If dY2 > nebYP.Maximum Then dY2 = nebYP.Maximum
        If dY2 < nebYP.Minimum Then dY2 = nebYP.Minimum

        If dZ1 > nebZP.Maximum Then dZ1 = nebZP.Maximum
        If dZ1 < nebZP.Minimum Then dZ1 = nebZP.Minimum
        If dZ2 > nebZP.Maximum Then dZ2 = nebZP.Maximum
        If dZ2 < nebZP.Minimum Then dZ2 = nebZP.Minimum

        If rb1.Checked Then
            nebYP.Value = dY1
            nebZP.Value = dZ1
            nebANG.Value = dAng1
            nebPL.Value = dPl1
            nebToolDia.Value = dTl1
        Else
            nebYP.Value = dY2
            nebZP.Value = dZ2
            nebANG.Value = dAng2
            nebPL.Value = dPl2
            nebToolDia.Value = dTl2
        End If

    End Sub

#End Region

#Region " Function - ValueParser() "

    Function ValueParser(ByVal sExpression As String, ByVal V1 As Double, ByVal V2 As Double, ByVal V3 As Double, ByVal V4 As Double, ByVal V5 As Double, ByVal V6 As Double, ByVal V7 As Double, ByVal V8 As Double) As Double

        Dim sUpperExp As String = sExpression.ToUpper
        Dim sExp1 As String = ""
        Dim sExp2 As String = ""
        Dim sOperator As String = ""
        Dim dExp1 As Double = 0
        Dim dExp2 As Double = 0

        Dim iPlus As Integer = InStr(sUpperExp, "+", CompareMethod.Text)
        Dim iMinus As Integer = InStr(sUpperExp, "-", CompareMethod.Text)
        Dim iMultiply As Integer = InStr(sUpperExp, "X", CompareMethod.Text)
        Dim iDivide As Integer = InStr(sUpperExp, "/", CompareMethod.Text)

        If iPlus > 0 Then
            sExp1 = Mid(sUpperExp, 1, iPlus - 1)
            sExp2 = Mid(sUpperExp, iPlus + 1)
            sOperator = "+"
        End If
        If iMinus > 1 Then
            sExp1 = Mid(sUpperExp, 1, iMinus - 1)
            sExp2 = Mid(sUpperExp, iMinus + 1)
            sOperator = "-"
        End If
        If iMultiply > 0 Then
            sExp1 = Mid(sUpperExp, 1, iMultiply - 1)
            sExp2 = Mid(sUpperExp, iMultiply + 1)
            sOperator = "X"
        End If
        If iDivide > 0 Then
            sExp1 = Mid(sUpperExp, 1, iDivide - 1)
            sExp2 = Mid(sUpperExp, iDivide + 1)
            sOperator = "/"
        End If
        If iPlus = 0 And iMinus < 2 And iMultiply = 0 And iDivide = 0 Then sExp1 = sUpperExp

        Select Case sExp1
            Case ""
                dExp1 = 0
            Case "V1"
                dExp1 = V1
            Case "V2"
                dExp1 = V2
            Case "V3"
                dExp1 = V3
            Case "V4"
                dExp1 = V4
            Case "V5"
                dExp1 = V5
            Case "V6"
                dExp1 = V6
            Case "V7"
                dExp1 = V7
            Case "V8"
                dExp1 = V8
            Case "VF"
                dExp1 = prfVFrontDatum + V1 - ProfileWidth
            Case "VR"
                dExp1 = prfVRearDatum - V1
            Case "YF"
                dExp1 = prfVFrontDatum + V1 - ProfileWidth
            Case "YR"
                dExp1 = prfVRearDatum - V1
            Case "HH"
                dExp1 = ProfileHeight / 2
            Case "PH"
                dExp1 = ProfileHeight
            Case "PW"
                dExp1 = ProfileWidth
            Case Else
                dExp1 = CDbl(sExp1)
        End Select

        Select Case sExp2
            Case ""
                dExp2 = 0
            Case "V1"
                dExp2 = V1
            Case "V2"
                dExp2 = V2
            Case "V3"
                dExp2 = V3
            Case "V4"
                dExp2 = V4
            Case "V5"
                dExp2 = V5
            Case "V6"
                dExp2 = V6
            Case "V7"
                dExp2 = V7
            Case "V8"
                dExp2 = V8
            Case "VF"
                dExp2 = prfVFrontDatum + V1 - ProfileWidth
            Case "VR"
                dExp2 = prfVRearDatum - V1
            Case "YF"
                dExp2 = prfVFrontDatum + V1 - ProfileWidth
            Case "YR"
                dExp2 = prfVRearDatum - V1
            Case "HH"
                dExp2 = ProfileHeight / 2
            Case "PH"
                dExp2 = ProfileHeight
            Case "PW"
                dExp2 = ProfileWidth
            Case Else
                dExp2 = CDbl(sExp2)
        End Select

        If sOperator = "+" Then Return dExp1 + dExp2
        If sOperator = "-" Then Return dExp1 - dExp2
        If sOperator = "X" Then Return dExp1 * dExp2
        If sOperator = "/" Then Return dExp1 / dExp2

        Return dExp1

    End Function

#End Region
#Region " Function - TagParser() "

    Function TagParser(ByVal sExp As String) As String

        Select Case Mid(sExp, 1, 2)
            Case "V1"
                Return "V1"
            Case "V2"
                Return "V2"
            Case "V3"
                Return "V3"
            Case "V4"
                Return "V4"
            Case "V5"
                Return "V5"
            Case "V6"
                Return "V6"
            Case "V7"
                Return "V7"
            Case "V8"
                Return "V8"
            Case "VF"
                Return ""
            Case "VR"
                Return ""
            Case "YF"
                Return ""
            Case "YR"
                Return ""
            Case Else
                Return ""
        End Select

    End Function

#End Region

End Class
