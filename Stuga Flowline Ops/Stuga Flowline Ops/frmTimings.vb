﻿Imports System.IO

Public Class frmTimings

#Region " Form - Load "

    Private Sub frmTimings_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FillMonthCombo()

    End Sub

#End Region

#Region " Button - GetData "

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        ImportAll()
    End Sub

#End Region

#Region " Event - cmbMonth_TextChanged() "

    Private Sub cmbMonth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMonth.TextChanged

        cmbDay.Items.Clear()

        Dim di As New DirectoryInfo(prfDiagnoseLoc + cmbMonth.Text + "\")
        Dim fi As FileInfo() = di.GetFiles("*.pcs")
        For Each f As FileInfo In fi
            Dim s As String = f.Name
            Dim iLoc As Integer = InStr(s, ".")
            s = Mid(s, 1, iLoc - 1)
            cmbDay.Items.Add(s)
        Next

    End Sub

#End Region

#Region " Sub - ImportAll() "

    Sub ImportAll()

        If cmbMonth.Text = "" Or cmbDay.Text = "" Then
            Exit Sub
        End If

        'Get Path to Diagnose Folder
        Dim PcsPath As String = prfDiagnoseLoc + cmbMonth.Text + "\" + cmbDay.Text + ".PCS"
        FileOpen(1, PcsPath, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Dim sPieces As String = LineInput(1)
        FileClose(1)

        Dim tsOn As New TimeSpan(0, 0, 0)
        Dim tsIdle As New TimeSpan(0, 0, 0)
        Dim tsRunning As New TimeSpan(0, 0, 0)
        Dim tsWaitSaw As New TimeSpan(0, 0, 0)
        Dim tsAlarm As New TimeSpan(0, 0, 0)

        Dim dtOnStart As DateTime
        Dim dtIdleStart As DateTime
        Dim dtRunningStart As DateTime
        Dim dtWaitSawStart As DateTime
        Dim dtAlarm As DateTime

        Dim bRunning As Boolean = False
        Dim bIdle As Boolean = False
        Dim bWaitSaw As Boolean = False
        Dim bAlarm As Boolean = False

        Dim dtThisTime As DateTime

        Dim TSPath As String = prfDiagnoseLoc + cmbMonth.Text + "\TS" + cmbDay.Text + ".DGN"
        FileOpen(1, TSPath, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Do While Not EOF(1)

            'Get This Time
            Dim sLine As String = LineInput(1)
            Dim sTime As String = Mid(sLine, 1, InStr(sLine, ",", CompareMethod.Text) - 1)
            Dim iHour As Integer = CInt(Mid(sTime, 1, InStr(sTime, ":", CompareMethod.Text) - 1))
            sTime = Mid(sTime, InStr(sTime, ":", CompareMethod.Text) + 1)
            Dim iMinute As Integer = CInt(Mid(sTime, 1, InStr(sTime, ":", CompareMethod.Text) - 1))
            sTime = Mid(sTime, InStr(sTime, ":", CompareMethod.Text) + 1)
            Dim iSecond As Integer = CInt(sTime)
            dtThisTime = New DateTime(CInt(Mid(cmbMonth.Text, 4)), GetMonth(Mid(cmbMonth.Text, 1, 3)), CInt(cmbDay.Text), iHour, iMinute, iSecond)

            'On
            If InStr(sLine.ToUpper, "ON", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                If dtOnStart <> Nothing Then tsOn = tsOn.Add(dtThisTime.Subtract(dtOnStart))
                dtOnStart = dtThisTime

                bRunning = False
                bIdle = False
                bWaitSaw = False
                bAlarm = False

            End If

            'Off
            If InStr(sLine.ToUpper, "OFF", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                tsOn = tsOn.Add(dtThisTime.Subtract(dtOnStart))
                dtOnStart = Nothing

                bRunning = False
                bIdle = False
                bWaitSaw = False
                bAlarm = False

            End If

            'Running
            If InStr(sLine.ToUpper, "RUNNING", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                dtRunningStart = dtThisTime

                bRunning = True
                bIdle = False
                bWaitSaw = False
                bAlarm = False

            End If

            'Alarm
            If InStr(sLine.ToUpper, "ALARM", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                dtAlarm = dtThisTime

                bRunning = False
                bIdle = False
                bWaitSaw = False
                bAlarm = True

            End If

            'Idle
            If InStr(sLine.ToUpper, "IDLE", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                dtIdleStart = dtThisTime

                bRunning = False
                bIdle = True
                bWaitSaw = False
                bAlarm = False

            End If

            'WaitSaw
            If InStr(sLine.ToUpper, "WAITSAW", CompareMethod.Text) <> 0 Then

                If bRunning = True Then tsRunning = tsRunning.Add(dtThisTime.Subtract(dtRunningStart))
                If bIdle = True Then tsIdle = tsIdle.Add(dtThisTime.Subtract(dtIdleStart))
                If bWaitSaw = True Then tsWaitSaw = tsWaitSaw.Add(dtThisTime.Subtract(dtWaitSawStart))
                If bAlarm = True Then tsAlarm = tsAlarm.Add(dtThisTime.Subtract(dtAlarm))

                dtWaitSawStart = dtThisTime

                bRunning = False
                bIdle = False
                bWaitSaw = True
                bAlarm = False

            End If

        Loop

        If dtOnStart <> Nothing Then
            tsOn = tsOn.Add(dtThisTime.Subtract(dtOnStart))
        End If

        FileClose(1)

        FillGrid(sPieces, tsOn, tsRunning, tsWaitSaw, tsAlarm, tsIdle)

    End Sub

#End Region
#Region " Sub - FillMonthCombo() "

    Sub FillMonthCombo()

        cmbMonth.Items.Clear()
        For Each sDirs As String In Directory.GetDirectories(prfDiagnoseLoc)
            cmbMonth.Items.Add(Mid(sDirs, 13))
        Next

    End Sub

#End Region
#Region " Sub - FillGrid() "

    Sub FillGrid(ByVal sPieces As String, ByVal tsOn As TimeSpan, ByVal tsRunning As TimeSpan, ByVal tsWaitSaw As TimeSpan, ByVal tsAlarm As TimeSpan, ByVal tsIdle As TimeSpan)

        Dim sString As String = ""
        Dim dsUtilisation As New DataSet("dsUtilisation")
        Dim tblTable As New DataTable
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()
        tblTable.TableName = "tblUtilisation"

        'utID
        Dim utID As New DataColumn("utID")
        utID.DataType = GetType(Integer)
        tblTable.Columns.Add(utID)

        'utDescription
        Dim utDescription As New DataColumn("utDescription")
        utDescription.DataType = GetType(String)
        tblTable.Columns.Add(utDescription)

        'utValue
        Dim utValue As New DataColumn("utValue")
        utValue.DataType = GetType(String)
        tblTable.Columns.Add(utValue)

        Dim rRow As DataRow

        'Add Pieces Cut Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 1
        rRow.Item("utDescription") = "Pieces Cut"
        rRow.Item("utValue") = sPieces
        tblTable.Rows.Add(rRow)

        'Add On Time Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 2
        rRow.Item("utDescription") = "Machine ON Time"
        rRow.Item("utValue") = tsOn.Hours.ToString + "h " + tsOn.Minutes.ToString + "m " + tsOn.Seconds.ToString + "s."
        tblTable.Rows.Add(rRow)

        'Add Running Time Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 3
        rRow.Item("utDescription") = "Running Time"
        rRow.Item("utValue") = tsRunning.Hours.ToString + "h " + tsRunning.Minutes.ToString + "m " + tsRunning.Seconds.ToString + "s."
        tblTable.Rows.Add(rRow)

        'Add WaitSaw Time Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 4
        rRow.Item("utDescription") = "Time Waiting for Saw"
        rRow.Item("utValue") = tsWaitSaw.Hours.ToString + "h " + tsWaitSaw.Minutes.ToString + "m " + tsWaitSaw.Seconds.ToString + "s."
        tblTable.Rows.Add(rRow)

        'Add Alarm Time Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 5
        rRow.Item("utDescription") = "Alarm Time"
        rRow.Item("utValue") = tsAlarm.Hours.ToString + "h " + tsAlarm.Minutes.ToString + "m " + tsAlarm.Seconds.ToString + "s."
        tblTable.Rows.Add(rRow)

        'Add Idle Time Row to Table
        rRow = tblTable.NewRow
        rRow.Item("utID") = 6
        rRow.Item("utDescription") = "Idle Time"
        rRow.Item("utValue") = tsIdle.Hours.ToString + "h " + tsIdle.Minutes.ToString + "m " + tsIdle.Seconds.ToString + "s."
        tblTable.Rows.Add(rRow)

        dsUtilisation.Tables.Add(tblTable)

        'Create Binding Source
        Dim bs As New BindingSource(dsUtilisation, "tblUtilisation")

        'Grab Grid Data
        With dgvUtilisation
            .DataSource = bs
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .AllowUserToOrderColumns = False
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = False
            .ReadOnly = True
            .Columns.Clear()
            .Font = New Font("Arial", 10, FontStyle.Regular)
        End With

        'utID
        Dim colID As New DataGridViewTextBoxColumn
        With colID
            .Name = "ID"
            .HeaderText = "ID"
            .Width = 50
            .DataPropertyName = "utID"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        dgvUtilisation.Columns.Add(colID)
        dgvUtilisation.Columns("ID").Visible = False

        'utDescription
        Dim colDescription As New DataGridViewTextBoxColumn
        With colDescription
            .HeaderText = "Description"
            .Width = 210
            .DataPropertyName = "utDescription"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = True
        End With
        dgvUtilisation.Columns.Add(colDescription)

        'utValue
        Dim colValue As New DataGridViewTextBoxColumn
        With colValue
            .HeaderText = "Value"
            .Width = 110
            .DataPropertyName = "utValue"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = True
        End With
        dgvUtilisation.Columns.Add(colValue)

    End Sub

#End Region

#Region " Function - GetMonth() "

    Function GetMonth(ByVal sMonth As String) As Integer

        GetMonth = 0

        Select Case sMonth.ToUpper
            Case "JAN"
                GetMonth = 1
            Case "FEB"
                GetMonth = 2
            Case "MAR"
                GetMonth = 3
            Case "APR"
                GetMonth = 4
            Case "MAY"
                GetMonth = 5
            Case "JUN"
                GetMonth = 6
            Case "JUL"
                GetMonth = 7
            Case "AUG"
                GetMonth = 8
            Case "SEP"
                GetMonth = 9
            Case "OCT"
                GetMonth = 10
            Case "NOV"
                GetMonth = 11
            Case "DEC"
                GetMonth = 12

        End Select

    End Function

#End Region

End Class