﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditMachining
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditMachining))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblStdOp = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnUndo = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.nebYP = New System.Windows.Forms.NumericUpDown()
        Me.nebZP = New System.Windows.Forms.NumericUpDown()
        Me.nebANG = New System.Windows.Forms.NumericUpDown()
        Me.nebTL = New System.Windows.Forms.NumericUpDown()
        Me.nebPL = New System.Windows.Forms.NumericUpDown()
        Me.dgvMachining = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.lblAngPL = New System.Windows.Forms.Label()
        Me.nebZoom = New System.Windows.Forms.NumericUpDown()
        Me.grpHeight = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBHMinus = New System.Windows.Forms.Button()
        Me.btnBHPlus = New System.Windows.Forms.Button()
        Me.grpWidth = New System.Windows.Forms.GroupBox()
        Me.lblLDesc = New System.Windows.Forms.Label()
        Me.btnBWMinus = New System.Windows.Forms.Button()
        Me.btnBWPlus = New System.Windows.Forms.Button()
        Me.grpJog = New System.Windows.Forms.GroupBox()
        Me.btnPLMinus = New System.Windows.Forms.Button()
        Me.btnPLPlus = New System.Windows.Forms.Button()
        Me.btnAngPlus = New System.Windows.Forms.Button()
        Me.btnUp = New System.Windows.Forms.Button()
        Me.btnAngMinus = New System.Windows.Forms.Button()
        Me.btnLeft = New System.Windows.Forms.Button()
        Me.btnRight = New System.Windows.Forms.Button()
        Me.btnDown = New System.Windows.Forms.Button()
        Me.grpPos = New System.Windows.Forms.GroupBox()
        Me.rb2 = New System.Windows.Forms.RadioButton()
        Me.rb1 = New System.Windows.Forms.RadioButton()
        Me.pbPoscalc = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtToolCode = New System.Windows.Forms.TextBox()
        Me.txtProfile = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.tcMisc = New System.Windows.Forms.TabControl()
        Me.tpMisc = New System.Windows.Forms.TabPage()
        Me.grpMisc1 = New System.Windows.Forms.GroupBox()
        Me.btnMisc1Minus = New System.Windows.Forms.Button()
        Me.btnMisc1Plus = New System.Windows.Forms.Button()
        Me.grpNotch = New System.Windows.Forms.GroupBox()
        Me.btnNotchMinus = New System.Windows.Forms.Button()
        Me.btnNotchPlus = New System.Windows.Forms.Button()
        Me.grpMisc3 = New System.Windows.Forms.GroupBox()
        Me.btnMisc3Minus = New System.Windows.Forms.Button()
        Me.btnMisc3Plus = New System.Windows.Forms.Button()
        Me.grpMisc2 = New System.Windows.Forms.GroupBox()
        Me.btnMisc2Minus = New System.Windows.Forms.Button()
        Me.btnMisc2Plus = New System.Windows.Forms.Button()
        Me.grpMisc4 = New System.Windows.Forms.GroupBox()
        Me.btnMisc4Minus = New System.Windows.Forms.Button()
        Me.btnMisc4Plus = New System.Windows.Forms.Button()
        Me.tpSteps = New System.Windows.Forms.TabPage()
        Me.grpOps = New System.Windows.Forms.GroupBox()
        Me.nebCustom = New System.Windows.Forms.NumericUpDown()
        Me.rbOps10 = New System.Windows.Forms.RadioButton()
        Me.rbOps5 = New System.Windows.Forms.RadioButton()
        Me.rbOps1 = New System.Windows.Forms.RadioButton()
        Me.rbOpsCustom = New System.Windows.Forms.RadioButton()
        Me.grpNotches = New System.Windows.Forms.GroupBox()
        Me.rbNotch5 = New System.Windows.Forms.RadioButton()
        Me.rbNotch25 = New System.Windows.Forms.RadioButton()
        Me.rbNotch1 = New System.Windows.Forms.RadioButton()
        Me.tpOpts = New System.Windows.Forms.TabPage()
        Me.grpGrip = New System.Windows.Forms.GroupBox()
        Me.optPullGrip = New System.Windows.Forms.RadioButton()
        Me.optPushGrip = New System.Windows.Forms.RadioButton()
        Me.optNoGrip = New System.Windows.Forms.RadioButton()
        Me.chkRingClash = New System.Windows.Forms.CheckBox()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebTL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMachining, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpHeight.SuspendLayout()
        Me.grpWidth.SuspendLayout()
        Me.grpJog.SuspendLayout()
        Me.grpPos.SuspendLayout()
        CType(Me.pbPoscalc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.tcMisc.SuspendLayout()
        Me.tpMisc.SuspendLayout()
        Me.grpMisc1.SuspendLayout()
        Me.grpNotch.SuspendLayout()
        Me.grpMisc3.SuspendLayout()
        Me.grpMisc2.SuspendLayout()
        Me.grpMisc4.SuspendLayout()
        Me.tpSteps.SuspendLayout()
        Me.grpOps.SuspendLayout()
        CType(Me.nebCustom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpNotches.SuspendLayout()
        Me.tpOpts.SuspendLayout()
        Me.grpGrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.lblStdOp)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.nebYP)
        Me.Panel1.Controls.Add(Me.nebZP)
        Me.Panel1.Controls.Add(Me.nebANG)
        Me.Panel1.Controls.Add(Me.nebTL)
        Me.Panel1.Controls.Add(Me.nebPL)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 661)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(992, 55)
        Me.Panel1.TabIndex = 0
        '
        'lblStdOp
        '
        Me.lblStdOp.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdOp.ForeColor = System.Drawing.Color.White
        Me.lblStdOp.Location = New System.Drawing.Point(498, 6)
        Me.lblStdOp.Name = "lblStdOp"
        Me.lblStdOp.Size = New System.Drawing.Size(195, 20)
        Me.lblStdOp.TabIndex = 16
        Me.lblStdOp.Text = "StdOp: ?"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnUndo)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(754, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(238, 55)
        Me.Panel2.TabIndex = 0
        '
        'btnUndo
        '
        Me.btnUndo.BackColor = System.Drawing.Color.White
        Me.btnUndo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndo.Location = New System.Drawing.Point(3, 9)
        Me.btnUndo.Name = "btnUndo"
        Me.btnUndo.Size = New System.Drawing.Size(114, 34)
        Me.btnUndo.TabIndex = 12
        Me.btnUndo.Text = "Undo Changes"
        Me.btnUndo.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(118, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(114, 34)
        Me.btnSave.TabIndex = 11
        Me.btnSave.Text = "Save and Exit"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'nebYP
        '
        Me.nebYP.Location = New System.Drawing.Point(12, 6)
        Me.nebYP.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nebYP.Minimum = New Decimal(New Integer() {120, 0, 0, -2147483648})
        Me.nebYP.Name = "nebYP"
        Me.nebYP.Size = New System.Drawing.Size(55, 20)
        Me.nebYP.TabIndex = 0
        Me.nebYP.Visible = False
        '
        'nebZP
        '
        Me.nebZP.Location = New System.Drawing.Point(195, 6)
        Me.nebZP.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nebZP.Minimum = New Decimal(New Integer() {120, 0, 0, -2147483648})
        Me.nebZP.Name = "nebZP"
        Me.nebZP.Size = New System.Drawing.Size(55, 20)
        Me.nebZP.TabIndex = 1
        Me.nebZP.Visible = False
        '
        'nebANG
        '
        Me.nebANG.Location = New System.Drawing.Point(73, 6)
        Me.nebANG.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.nebANG.Name = "nebANG"
        Me.nebANG.Size = New System.Drawing.Size(55, 20)
        Me.nebANG.TabIndex = 2
        Me.nebANG.Visible = False
        '
        'nebTL
        '
        Me.nebTL.Location = New System.Drawing.Point(134, 6)
        Me.nebTL.Name = "nebTL"
        Me.nebTL.Size = New System.Drawing.Size(55, 20)
        Me.nebTL.TabIndex = 4
        Me.nebTL.Visible = False
        '
        'nebPL
        '
        Me.nebPL.Location = New System.Drawing.Point(256, 6)
        Me.nebPL.Maximum = New Decimal(New Integer() {150, 0, 0, 0})
        Me.nebPL.Name = "nebPL"
        Me.nebPL.Size = New System.Drawing.Size(55, 20)
        Me.nebPL.TabIndex = 3
        Me.nebPL.Visible = False
        '
        'dgvMachining
        '
        Me.dgvMachining.AllowUserToAddRows = False
        Me.dgvMachining.AllowUserToDeleteRows = False
        Me.dgvMachining.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMachining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMachining.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMachining.Location = New System.Drawing.Point(0, 40)
        Me.dgvMachining.Name = "dgvMachining"
        Me.dgvMachining.Size = New System.Drawing.Size(468, 353)
        Me.dgvMachining.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Navy
        Me.Panel3.Controls.Add(Me.lblZoom)
        Me.Panel3.Controls.Add(Me.lblAngPL)
        Me.Panel3.Controls.Add(Me.nebZoom)
        Me.Panel3.Controls.Add(Me.grpHeight)
        Me.Panel3.Controls.Add(Me.grpWidth)
        Me.Panel3.Controls.Add(Me.grpJog)
        Me.Panel3.Controls.Add(Me.grpPos)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(489, 414)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(493, 247)
        Me.Panel3.TabIndex = 10
        '
        'lblZoom
        '
        Me.lblZoom.AutoSize = True
        Me.lblZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblZoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZoom.ForeColor = System.Drawing.Color.White
        Me.lblZoom.Location = New System.Drawing.Point(19, 27)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(102, 22)
        Me.lblZoom.TabIndex = 21
        Me.lblZoom.Text = "Zoom Factor"
        '
        'lblAngPL
        '
        Me.lblAngPL.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAngPL.ForeColor = System.Drawing.Color.White
        Me.lblAngPL.Location = New System.Drawing.Point(134, 227)
        Me.lblAngPL.Name = "lblAngPL"
        Me.lblAngPL.Size = New System.Drawing.Size(195, 20)
        Me.lblAngPL.TabIndex = 15
        Me.lblAngPL.Text = "Ang: ? PL: ?"
        '
        'nebZoom
        '
        Me.nebZoom.BackColor = System.Drawing.Color.White
        Me.nebZoom.DecimalPlaces = 1
        Me.nebZoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebZoom.Increment = New Decimal(New Integer() {2, 0, 0, 65536})
        Me.nebZoom.Location = New System.Drawing.Point(137, 25)
        Me.nebZoom.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebZoom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebZoom.Name = "nebZoom"
        Me.nebZoom.Size = New System.Drawing.Size(110, 26)
        Me.nebZoom.TabIndex = 20
        Me.nebZoom.Value = New Decimal(New Integer() {12, 0, 0, 65536})
        '
        'grpHeight
        '
        Me.grpHeight.Controls.Add(Me.Label1)
        Me.grpHeight.Controls.Add(Me.btnBHMinus)
        Me.grpHeight.Controls.Add(Me.btnBHPlus)
        Me.grpHeight.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpHeight.ForeColor = System.Drawing.Color.White
        Me.grpHeight.Location = New System.Drawing.Point(343, 152)
        Me.grpHeight.Name = "grpHeight"
        Me.grpHeight.Size = New System.Drawing.Size(141, 84)
        Me.grpHeight.TabIndex = 3
        Me.grpHeight.TabStop = False
        Me.grpHeight.Text = "Slot Height:"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(11, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Around the Bar"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnBHMinus
        '
        Me.btnBHMinus.BackColor = System.Drawing.Color.White
        Me.btnBHMinus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBHMinus.ForeColor = System.Drawing.Color.Black
        Me.btnBHMinus.Location = New System.Drawing.Point(72, 21)
        Me.btnBHMinus.Name = "btnBHMinus"
        Me.btnBHMinus.Size = New System.Drawing.Size(60, 40)
        Me.btnBHMinus.TabIndex = 1
        Me.btnBHMinus.Text = "-"
        Me.btnBHMinus.UseVisualStyleBackColor = False
        '
        'btnBHPlus
        '
        Me.btnBHPlus.BackColor = System.Drawing.Color.White
        Me.btnBHPlus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBHPlus.ForeColor = System.Drawing.Color.Black
        Me.btnBHPlus.Location = New System.Drawing.Point(6, 21)
        Me.btnBHPlus.Name = "btnBHPlus"
        Me.btnBHPlus.Size = New System.Drawing.Size(60, 40)
        Me.btnBHPlus.TabIndex = 0
        Me.btnBHPlus.Text = "+"
        Me.btnBHPlus.UseVisualStyleBackColor = False
        '
        'grpWidth
        '
        Me.grpWidth.Controls.Add(Me.lblLDesc)
        Me.grpWidth.Controls.Add(Me.btnBWMinus)
        Me.grpWidth.Controls.Add(Me.btnBWPlus)
        Me.grpWidth.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpWidth.ForeColor = System.Drawing.Color.White
        Me.grpWidth.Location = New System.Drawing.Point(343, 72)
        Me.grpWidth.Name = "grpWidth"
        Me.grpWidth.Size = New System.Drawing.Size(141, 80)
        Me.grpWidth.TabIndex = 2
        Me.grpWidth.TabStop = False
        Me.grpWidth.Text = "Slot Length:"
        '
        'lblLDesc
        '
        Me.lblLDesc.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLDesc.ForeColor = System.Drawing.Color.White
        Me.lblLDesc.Location = New System.Drawing.Point(11, 58)
        Me.lblLDesc.Name = "lblLDesc"
        Me.lblLDesc.Size = New System.Drawing.Size(121, 16)
        Me.lblLDesc.TabIndex = 15
        Me.lblLDesc.Text = "Along the Bar"
        Me.lblLDesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnBWMinus
        '
        Me.btnBWMinus.BackColor = System.Drawing.Color.White
        Me.btnBWMinus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBWMinus.ForeColor = System.Drawing.Color.Black
        Me.btnBWMinus.Location = New System.Drawing.Point(72, 16)
        Me.btnBWMinus.Name = "btnBWMinus"
        Me.btnBWMinus.Size = New System.Drawing.Size(60, 40)
        Me.btnBWMinus.TabIndex = 1
        Me.btnBWMinus.Text = "-"
        Me.btnBWMinus.UseVisualStyleBackColor = False
        '
        'btnBWPlus
        '
        Me.btnBWPlus.BackColor = System.Drawing.Color.White
        Me.btnBWPlus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBWPlus.ForeColor = System.Drawing.Color.Black
        Me.btnBWPlus.Location = New System.Drawing.Point(6, 16)
        Me.btnBWPlus.Name = "btnBWPlus"
        Me.btnBWPlus.Size = New System.Drawing.Size(60, 40)
        Me.btnBWPlus.TabIndex = 0
        Me.btnBWPlus.Text = "+"
        Me.btnBWPlus.UseVisualStyleBackColor = False
        '
        'grpJog
        '
        Me.grpJog.Controls.Add(Me.btnPLMinus)
        Me.grpJog.Controls.Add(Me.btnPLPlus)
        Me.grpJog.Controls.Add(Me.btnAngPlus)
        Me.grpJog.Controls.Add(Me.btnUp)
        Me.grpJog.Controls.Add(Me.btnAngMinus)
        Me.grpJog.Controls.Add(Me.btnLeft)
        Me.grpJog.Controls.Add(Me.btnRight)
        Me.grpJog.Controls.Add(Me.btnDown)
        Me.grpJog.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpJog.ForeColor = System.Drawing.Color.White
        Me.grpJog.Location = New System.Drawing.Point(127, 72)
        Me.grpJog.Name = "grpJog"
        Me.grpJog.Size = New System.Drawing.Size(210, 164)
        Me.grpJog.TabIndex = 1
        Me.grpJog.TabStop = False
        Me.grpJog.Text = "Jog"
        '
        'btnPLMinus
        '
        Me.btnPLMinus.BackColor = System.Drawing.Color.LightGreen
        Me.btnPLMinus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPLMinus.Image = CType(resources.GetObject("btnPLMinus.Image"), System.Drawing.Image)
        Me.btnPLMinus.Location = New System.Drawing.Point(138, 112)
        Me.btnPLMinus.Name = "btnPLMinus"
        Me.btnPLMinus.Size = New System.Drawing.Size(60, 40)
        Me.btnPLMinus.TabIndex = 7
        Me.btnPLMinus.UseVisualStyleBackColor = False
        '
        'btnPLPlus
        '
        Me.btnPLPlus.BackColor = System.Drawing.Color.LightGreen
        Me.btnPLPlus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPLPlus.Image = CType(resources.GetObject("btnPLPlus.Image"), System.Drawing.Image)
        Me.btnPLPlus.Location = New System.Drawing.Point(6, 112)
        Me.btnPLPlus.Name = "btnPLPlus"
        Me.btnPLPlus.Size = New System.Drawing.Size(60, 40)
        Me.btnPLPlus.TabIndex = 5
        Me.btnPLPlus.UseVisualStyleBackColor = False
        '
        'btnAngPlus
        '
        Me.btnAngPlus.BackColor = System.Drawing.Color.LightGreen
        Me.btnAngPlus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAngPlus.Image = CType(resources.GetObject("btnAngPlus.Image"), System.Drawing.Image)
        Me.btnAngPlus.Location = New System.Drawing.Point(6, 20)
        Me.btnAngPlus.Name = "btnAngPlus"
        Me.btnAngPlus.Size = New System.Drawing.Size(60, 40)
        Me.btnAngPlus.TabIndex = 0
        Me.btnAngPlus.UseVisualStyleBackColor = False
        '
        'btnUp
        '
        Me.btnUp.BackColor = System.Drawing.Color.LightGreen
        Me.btnUp.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Location = New System.Drawing.Point(72, 20)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(60, 40)
        Me.btnUp.TabIndex = 1
        Me.btnUp.UseVisualStyleBackColor = False
        '
        'btnAngMinus
        '
        Me.btnAngMinus.BackColor = System.Drawing.Color.LightGreen
        Me.btnAngMinus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAngMinus.Image = CType(resources.GetObject("btnAngMinus.Image"), System.Drawing.Image)
        Me.btnAngMinus.Location = New System.Drawing.Point(138, 21)
        Me.btnAngMinus.Name = "btnAngMinus"
        Me.btnAngMinus.Size = New System.Drawing.Size(60, 40)
        Me.btnAngMinus.TabIndex = 2
        Me.btnAngMinus.UseVisualStyleBackColor = False
        '
        'btnLeft
        '
        Me.btnLeft.BackColor = System.Drawing.Color.LightGreen
        Me.btnLeft.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeft.Image = CType(resources.GetObject("btnLeft.Image"), System.Drawing.Image)
        Me.btnLeft.Location = New System.Drawing.Point(6, 66)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(60, 40)
        Me.btnLeft.TabIndex = 3
        Me.btnLeft.UseVisualStyleBackColor = False
        '
        'btnRight
        '
        Me.btnRight.BackColor = System.Drawing.Color.LightGreen
        Me.btnRight.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRight.Image = CType(resources.GetObject("btnRight.Image"), System.Drawing.Image)
        Me.btnRight.Location = New System.Drawing.Point(138, 66)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(60, 40)
        Me.btnRight.TabIndex = 4
        Me.btnRight.UseVisualStyleBackColor = False
        '
        'btnDown
        '
        Me.btnDown.BackColor = System.Drawing.Color.LightGreen
        Me.btnDown.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Location = New System.Drawing.Point(72, 112)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(60, 40)
        Me.btnDown.TabIndex = 6
        Me.btnDown.UseVisualStyleBackColor = False
        '
        'grpPos
        '
        Me.grpPos.Controls.Add(Me.rb2)
        Me.grpPos.Controls.Add(Me.rb1)
        Me.grpPos.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpPos.ForeColor = System.Drawing.Color.White
        Me.grpPos.Location = New System.Drawing.Point(6, 72)
        Me.grpPos.Name = "grpPos"
        Me.grpPos.Size = New System.Drawing.Size(115, 164)
        Me.grpPos.TabIndex = 0
        Me.grpPos.TabStop = False
        Me.grpPos.Text = "Position:"
        '
        'rb2
        '
        Me.rb2.ForeColor = System.Drawing.Color.White
        Me.rb2.Location = New System.Drawing.Point(6, 93)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(103, 57)
        Me.rb2.TabIndex = 1
        Me.rb2.Text = "Secondary Position"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb1
        '
        Me.rb1.Checked = True
        Me.rb1.ForeColor = System.Drawing.Color.White
        Me.rb1.Location = New System.Drawing.Point(6, 21)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(103, 59)
        Me.rb1.TabIndex = 0
        Me.rb1.TabStop = True
        Me.rb1.Text = "Primary Position"
        Me.rb1.UseVisualStyleBackColor = True
        '
        'pbPoscalc
        '
        Me.pbPoscalc.BackColor = System.Drawing.Color.White
        Me.pbPoscalc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbPoscalc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPoscalc.Location = New System.Drawing.Point(489, 11)
        Me.pbPoscalc.Name = "pbPoscalc"
        Me.pbPoscalc.Size = New System.Drawing.Size(493, 393)
        Me.pbPoscalc.TabIndex = 3
        Me.pbPoscalc.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Navy
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(13, 661)
        Me.Panel4.TabIndex = 4
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Navy
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(13, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(979, 11)
        Me.Panel5.TabIndex = 5
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Navy
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel6.Location = New System.Drawing.Point(982, 11)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(10, 650)
        Me.Panel6.TabIndex = 6
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.dgvMachining)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Controls.Add(Me.Panel9)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel8.Location = New System.Drawing.Point(13, 11)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(468, 650)
        Me.Panel8.TabIndex = 8
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Label3)
        Me.Panel10.Controls.Add(Me.txtToolCode)
        Me.Panel10.Controls.Add(Me.txtProfile)
        Me.Panel10.Controls.Add(Me.Label2)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel10.Location = New System.Drawing.Point(0, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(468, 40)
        Me.Panel10.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Navy
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 22)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Profile"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtToolCode
        '
        Me.txtToolCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToolCode.Location = New System.Drawing.Point(316, 4)
        Me.txtToolCode.Name = "txtToolCode"
        Me.txtToolCode.Size = New System.Drawing.Size(146, 22)
        Me.txtToolCode.TabIndex = 3
        '
        'txtProfile
        '
        Me.txtProfile.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfile.Location = New System.Drawing.Point(87, 3)
        Me.txtProfile.Name = "txtProfile"
        Me.txtProfile.Size = New System.Drawing.Size(146, 22)
        Me.txtProfile.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Navy
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(240, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 21)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tool Code"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Navy
        Me.Panel9.Controls.Add(Me.tcMisc)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel9.Location = New System.Drawing.Point(0, 393)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(468, 257)
        Me.Panel9.TabIndex = 2
        '
        'tcMisc
        '
        Me.tcMisc.Alignment = System.Windows.Forms.TabAlignment.Right
        Me.tcMisc.Controls.Add(Me.tpMisc)
        Me.tcMisc.Controls.Add(Me.tpSteps)
        Me.tcMisc.Controls.Add(Me.tpOpts)
        Me.tcMisc.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcMisc.Location = New System.Drawing.Point(9, 6)
        Me.tcMisc.Multiline = True
        Me.tcMisc.Name = "tcMisc"
        Me.tcMisc.SelectedIndex = 0
        Me.tcMisc.Size = New System.Drawing.Size(456, 245)
        Me.tcMisc.TabIndex = 5
        '
        'tpMisc
        '
        Me.tpMisc.BackColor = System.Drawing.Color.Navy
        Me.tpMisc.Controls.Add(Me.grpMisc1)
        Me.tpMisc.Controls.Add(Me.grpNotch)
        Me.tpMisc.Controls.Add(Me.grpMisc3)
        Me.tpMisc.Controls.Add(Me.grpMisc2)
        Me.tpMisc.Controls.Add(Me.grpMisc4)
        Me.tpMisc.ForeColor = System.Drawing.Color.Black
        Me.tpMisc.Location = New System.Drawing.Point(4, 4)
        Me.tpMisc.Name = "tpMisc"
        Me.tpMisc.Padding = New System.Windows.Forms.Padding(3)
        Me.tpMisc.Size = New System.Drawing.Size(428, 237)
        Me.tpMisc.TabIndex = 0
        Me.tpMisc.Text = "Misc"
        '
        'grpMisc1
        '
        Me.grpMisc1.Controls.Add(Me.btnMisc1Minus)
        Me.grpMisc1.Controls.Add(Me.btnMisc1Plus)
        Me.grpMisc1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMisc1.ForeColor = System.Drawing.Color.White
        Me.grpMisc1.Location = New System.Drawing.Point(12, 6)
        Me.grpMisc1.Name = "grpMisc1"
        Me.grpMisc1.Size = New System.Drawing.Size(174, 70)
        Me.grpMisc1.TabIndex = 0
        Me.grpMisc1.TabStop = False
        Me.grpMisc1.Text = "Misc1: 0"
        Me.grpMisc1.Visible = False
        '
        'btnMisc1Minus
        '
        Me.btnMisc1Minus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc1Minus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc1Minus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc1Minus.Location = New System.Drawing.Point(92, 20)
        Me.btnMisc1Minus.Name = "btnMisc1Minus"
        Me.btnMisc1Minus.Size = New System.Drawing.Size(76, 40)
        Me.btnMisc1Minus.TabIndex = 17
        Me.btnMisc1Minus.Tag = "V1"
        Me.btnMisc1Minus.Text = "-"
        Me.btnMisc1Minus.UseVisualStyleBackColor = False
        '
        'btnMisc1Plus
        '
        Me.btnMisc1Plus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc1Plus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc1Plus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc1Plus.Location = New System.Drawing.Point(6, 20)
        Me.btnMisc1Plus.Name = "btnMisc1Plus"
        Me.btnMisc1Plus.Size = New System.Drawing.Size(80, 40)
        Me.btnMisc1Plus.TabIndex = 16
        Me.btnMisc1Plus.Tag = "V1"
        Me.btnMisc1Plus.Text = "+"
        Me.btnMisc1Plus.UseVisualStyleBackColor = False
        '
        'grpNotch
        '
        Me.grpNotch.Controls.Add(Me.btnNotchMinus)
        Me.grpNotch.Controls.Add(Me.btnNotchPlus)
        Me.grpNotch.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpNotch.ForeColor = System.Drawing.Color.White
        Me.grpNotch.Location = New System.Drawing.Point(126, 158)
        Me.grpNotch.Name = "grpNotch"
        Me.grpNotch.Size = New System.Drawing.Size(174, 70)
        Me.grpNotch.TabIndex = 4
        Me.grpNotch.TabStop = False
        Me.grpNotch.Text = "Notch Depth: 0"
        Me.grpNotch.Visible = False
        '
        'btnNotchMinus
        '
        Me.btnNotchMinus.BackColor = System.Drawing.Color.LightGreen
        Me.btnNotchMinus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotchMinus.ForeColor = System.Drawing.Color.Black
        Me.btnNotchMinus.Location = New System.Drawing.Point(92, 20)
        Me.btnNotchMinus.Name = "btnNotchMinus"
        Me.btnNotchMinus.Size = New System.Drawing.Size(76, 40)
        Me.btnNotchMinus.TabIndex = 17
        Me.btnNotchMinus.Tag = "V1"
        Me.btnNotchMinus.Text = "-"
        Me.btnNotchMinus.UseVisualStyleBackColor = False
        '
        'btnNotchPlus
        '
        Me.btnNotchPlus.BackColor = System.Drawing.Color.LightGreen
        Me.btnNotchPlus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotchPlus.ForeColor = System.Drawing.Color.Black
        Me.btnNotchPlus.Location = New System.Drawing.Point(6, 20)
        Me.btnNotchPlus.Name = "btnNotchPlus"
        Me.btnNotchPlus.Size = New System.Drawing.Size(80, 40)
        Me.btnNotchPlus.TabIndex = 16
        Me.btnNotchPlus.Tag = "V1"
        Me.btnNotchPlus.Text = "+"
        Me.btnNotchPlus.UseVisualStyleBackColor = False
        '
        'grpMisc3
        '
        Me.grpMisc3.Controls.Add(Me.btnMisc3Minus)
        Me.grpMisc3.Controls.Add(Me.btnMisc3Plus)
        Me.grpMisc3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMisc3.ForeColor = System.Drawing.Color.White
        Me.grpMisc3.Location = New System.Drawing.Point(12, 82)
        Me.grpMisc3.Name = "grpMisc3"
        Me.grpMisc3.Size = New System.Drawing.Size(174, 70)
        Me.grpMisc3.TabIndex = 2
        Me.grpMisc3.TabStop = False
        Me.grpMisc3.Text = "Misc3: 0"
        Me.grpMisc3.Visible = False
        '
        'btnMisc3Minus
        '
        Me.btnMisc3Minus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc3Minus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc3Minus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc3Minus.Location = New System.Drawing.Point(92, 20)
        Me.btnMisc3Minus.Name = "btnMisc3Minus"
        Me.btnMisc3Minus.Size = New System.Drawing.Size(76, 40)
        Me.btnMisc3Minus.TabIndex = 17
        Me.btnMisc3Minus.Tag = "V1"
        Me.btnMisc3Minus.Text = "-"
        Me.btnMisc3Minus.UseVisualStyleBackColor = False
        '
        'btnMisc3Plus
        '
        Me.btnMisc3Plus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc3Plus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc3Plus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc3Plus.Location = New System.Drawing.Point(6, 20)
        Me.btnMisc3Plus.Name = "btnMisc3Plus"
        Me.btnMisc3Plus.Size = New System.Drawing.Size(80, 40)
        Me.btnMisc3Plus.TabIndex = 16
        Me.btnMisc3Plus.Tag = "V1"
        Me.btnMisc3Plus.Text = "+"
        Me.btnMisc3Plus.UseVisualStyleBackColor = False
        '
        'grpMisc2
        '
        Me.grpMisc2.Controls.Add(Me.btnMisc2Minus)
        Me.grpMisc2.Controls.Add(Me.btnMisc2Plus)
        Me.grpMisc2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMisc2.ForeColor = System.Drawing.Color.White
        Me.grpMisc2.Location = New System.Drawing.Point(240, 6)
        Me.grpMisc2.Name = "grpMisc2"
        Me.grpMisc2.Size = New System.Drawing.Size(174, 70)
        Me.grpMisc2.TabIndex = 1
        Me.grpMisc2.TabStop = False
        Me.grpMisc2.Text = "Misc2: 0"
        Me.grpMisc2.Visible = False
        '
        'btnMisc2Minus
        '
        Me.btnMisc2Minus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc2Minus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc2Minus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc2Minus.Location = New System.Drawing.Point(92, 20)
        Me.btnMisc2Minus.Name = "btnMisc2Minus"
        Me.btnMisc2Minus.Size = New System.Drawing.Size(76, 40)
        Me.btnMisc2Minus.TabIndex = 17
        Me.btnMisc2Minus.Tag = "V1"
        Me.btnMisc2Minus.Text = "-"
        Me.btnMisc2Minus.UseVisualStyleBackColor = False
        '
        'btnMisc2Plus
        '
        Me.btnMisc2Plus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc2Plus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc2Plus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc2Plus.Location = New System.Drawing.Point(6, 20)
        Me.btnMisc2Plus.Name = "btnMisc2Plus"
        Me.btnMisc2Plus.Size = New System.Drawing.Size(80, 40)
        Me.btnMisc2Plus.TabIndex = 16
        Me.btnMisc2Plus.Tag = "V1"
        Me.btnMisc2Plus.Text = "+"
        Me.btnMisc2Plus.UseVisualStyleBackColor = False
        '
        'grpMisc4
        '
        Me.grpMisc4.Controls.Add(Me.btnMisc4Minus)
        Me.grpMisc4.Controls.Add(Me.btnMisc4Plus)
        Me.grpMisc4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMisc4.ForeColor = System.Drawing.Color.White
        Me.grpMisc4.Location = New System.Drawing.Point(240, 82)
        Me.grpMisc4.Name = "grpMisc4"
        Me.grpMisc4.Size = New System.Drawing.Size(174, 70)
        Me.grpMisc4.TabIndex = 3
        Me.grpMisc4.TabStop = False
        Me.grpMisc4.Text = "Misc4: 0"
        Me.grpMisc4.Visible = False
        '
        'btnMisc4Minus
        '
        Me.btnMisc4Minus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc4Minus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc4Minus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc4Minus.Location = New System.Drawing.Point(92, 20)
        Me.btnMisc4Minus.Name = "btnMisc4Minus"
        Me.btnMisc4Minus.Size = New System.Drawing.Size(76, 40)
        Me.btnMisc4Minus.TabIndex = 17
        Me.btnMisc4Minus.Tag = "V1"
        Me.btnMisc4Minus.Text = "-"
        Me.btnMisc4Minus.UseVisualStyleBackColor = False
        '
        'btnMisc4Plus
        '
        Me.btnMisc4Plus.BackColor = System.Drawing.Color.LightGreen
        Me.btnMisc4Plus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMisc4Plus.ForeColor = System.Drawing.Color.Black
        Me.btnMisc4Plus.Location = New System.Drawing.Point(6, 20)
        Me.btnMisc4Plus.Name = "btnMisc4Plus"
        Me.btnMisc4Plus.Size = New System.Drawing.Size(80, 40)
        Me.btnMisc4Plus.TabIndex = 16
        Me.btnMisc4Plus.Tag = "V1"
        Me.btnMisc4Plus.Text = "+"
        Me.btnMisc4Plus.UseVisualStyleBackColor = False
        '
        'tpSteps
        '
        Me.tpSteps.BackColor = System.Drawing.Color.Navy
        Me.tpSteps.Controls.Add(Me.grpOps)
        Me.tpSteps.Controls.Add(Me.grpNotches)
        Me.tpSteps.ForeColor = System.Drawing.Color.Black
        Me.tpSteps.Location = New System.Drawing.Point(4, 4)
        Me.tpSteps.Name = "tpSteps"
        Me.tpSteps.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSteps.Size = New System.Drawing.Size(428, 237)
        Me.tpSteps.TabIndex = 1
        Me.tpSteps.Text = "Steps"
        '
        'grpOps
        '
        Me.grpOps.Controls.Add(Me.nebCustom)
        Me.grpOps.Controls.Add(Me.rbOps10)
        Me.grpOps.Controls.Add(Me.rbOps5)
        Me.grpOps.Controls.Add(Me.rbOps1)
        Me.grpOps.Controls.Add(Me.rbOpsCustom)
        Me.grpOps.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpOps.ForeColor = System.Drawing.Color.White
        Me.grpOps.Location = New System.Drawing.Point(12, 6)
        Me.grpOps.Name = "grpOps"
        Me.grpOps.Size = New System.Drawing.Size(211, 150)
        Me.grpOps.TabIndex = 6
        Me.grpOps.TabStop = False
        Me.grpOps.Text = "Operations"
        '
        'nebCustom
        '
        Me.nebCustom.DecimalPlaces = 1
        Me.nebCustom.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebCustom.Location = New System.Drawing.Point(99, 102)
        Me.nebCustom.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nebCustom.Minimum = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.nebCustom.Name = "nebCustom"
        Me.nebCustom.Size = New System.Drawing.Size(81, 32)
        Me.nebCustom.TabIndex = 3
        Me.nebCustom.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'rbOps10
        '
        Me.rbOps10.AutoSize = True
        Me.rbOps10.Location = New System.Drawing.Point(13, 82)
        Me.rbOps10.Name = "rbOps10"
        Me.rbOps10.Size = New System.Drawing.Size(57, 20)
        Me.rbOps10.TabIndex = 2
        Me.rbOps10.Text = "1mm"
        Me.rbOps10.UseVisualStyleBackColor = True
        '
        'rbOps5
        '
        Me.rbOps5.AutoSize = True
        Me.rbOps5.Checked = True
        Me.rbOps5.Location = New System.Drawing.Point(13, 56)
        Me.rbOps5.Name = "rbOps5"
        Me.rbOps5.Size = New System.Drawing.Size(68, 20)
        Me.rbOps5.TabIndex = 1
        Me.rbOps5.TabStop = True
        Me.rbOps5.Text = "0.5mm"
        Me.rbOps5.UseVisualStyleBackColor = True
        '
        'rbOps1
        '
        Me.rbOps1.AutoSize = True
        Me.rbOps1.Location = New System.Drawing.Point(13, 30)
        Me.rbOps1.Name = "rbOps1"
        Me.rbOps1.Size = New System.Drawing.Size(68, 20)
        Me.rbOps1.TabIndex = 0
        Me.rbOps1.Text = "0.1mm"
        Me.rbOps1.UseVisualStyleBackColor = True
        '
        'rbOpsCustom
        '
        Me.rbOpsCustom.AutoSize = True
        Me.rbOpsCustom.Location = New System.Drawing.Point(13, 108)
        Me.rbOpsCustom.Name = "rbOpsCustom"
        Me.rbOpsCustom.Size = New System.Drawing.Size(73, 20)
        Me.rbOpsCustom.TabIndex = 1
        Me.rbOpsCustom.Text = "Custom"
        Me.rbOpsCustom.UseVisualStyleBackColor = True
        '
        'grpNotches
        '
        Me.grpNotches.Controls.Add(Me.rbNotch5)
        Me.grpNotches.Controls.Add(Me.rbNotch25)
        Me.grpNotches.Controls.Add(Me.rbNotch1)
        Me.grpNotches.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpNotches.ForeColor = System.Drawing.Color.White
        Me.grpNotches.Location = New System.Drawing.Point(240, 6)
        Me.grpNotches.Name = "grpNotches"
        Me.grpNotches.Size = New System.Drawing.Size(174, 150)
        Me.grpNotches.TabIndex = 5
        Me.grpNotches.TabStop = False
        Me.grpNotches.Text = "Notches"
        '
        'rbNotch5
        '
        Me.rbNotch5.AutoSize = True
        Me.rbNotch5.Location = New System.Drawing.Point(17, 82)
        Me.rbNotch5.Name = "rbNotch5"
        Me.rbNotch5.Size = New System.Drawing.Size(68, 20)
        Me.rbNotch5.TabIndex = 4
        Me.rbNotch5.Text = "0.5mm"
        Me.rbNotch5.UseVisualStyleBackColor = True
        '
        'rbNotch25
        '
        Me.rbNotch25.AutoSize = True
        Me.rbNotch25.Checked = True
        Me.rbNotch25.Location = New System.Drawing.Point(17, 56)
        Me.rbNotch25.Name = "rbNotch25"
        Me.rbNotch25.Size = New System.Drawing.Size(75, 20)
        Me.rbNotch25.TabIndex = 3
        Me.rbNotch25.TabStop = True
        Me.rbNotch25.Text = "0.25mm"
        Me.rbNotch25.UseVisualStyleBackColor = True
        '
        'rbNotch1
        '
        Me.rbNotch1.AutoSize = True
        Me.rbNotch1.Location = New System.Drawing.Point(17, 31)
        Me.rbNotch1.Name = "rbNotch1"
        Me.rbNotch1.Size = New System.Drawing.Size(68, 20)
        Me.rbNotch1.TabIndex = 2
        Me.rbNotch1.Text = "0.1mm"
        Me.rbNotch1.UseVisualStyleBackColor = True
        '
        'tpOpts
        '
        Me.tpOpts.BackColor = System.Drawing.Color.Navy
        Me.tpOpts.Controls.Add(Me.grpGrip)
        Me.tpOpts.Controls.Add(Me.chkRingClash)
        Me.tpOpts.ForeColor = System.Drawing.Color.Black
        Me.tpOpts.Location = New System.Drawing.Point(4, 4)
        Me.tpOpts.Name = "tpOpts"
        Me.tpOpts.Padding = New System.Windows.Forms.Padding(3)
        Me.tpOpts.Size = New System.Drawing.Size(428, 237)
        Me.tpOpts.TabIndex = 2
        Me.tpOpts.Text = "Options"
        '
        'grpGrip
        '
        Me.grpGrip.Controls.Add(Me.optPullGrip)
        Me.grpGrip.Controls.Add(Me.optPushGrip)
        Me.grpGrip.Controls.Add(Me.optNoGrip)
        Me.grpGrip.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpGrip.ForeColor = System.Drawing.Color.White
        Me.grpGrip.Location = New System.Drawing.Point(6, 37)
        Me.grpGrip.Name = "grpGrip"
        Me.grpGrip.Size = New System.Drawing.Size(135, 119)
        Me.grpGrip.TabIndex = 10
        Me.grpGrip.TabStop = False
        Me.grpGrip.Text = "Gripper"
        '
        'optPullGrip
        '
        Me.optPullGrip.AutoSize = True
        Me.optPullGrip.Location = New System.Drawing.Point(15, 80)
        Me.optPullGrip.Name = "optPullGrip"
        Me.optPullGrip.Size = New System.Drawing.Size(51, 20)
        Me.optPullGrip.TabIndex = 2
        Me.optPullGrip.Text = "Pull"
        Me.optPullGrip.UseVisualStyleBackColor = True
        '
        'optPushGrip
        '
        Me.optPushGrip.AutoSize = True
        Me.optPushGrip.Location = New System.Drawing.Point(15, 54)
        Me.optPushGrip.Name = "optPushGrip"
        Me.optPushGrip.Size = New System.Drawing.Size(57, 20)
        Me.optPushGrip.TabIndex = 1
        Me.optPushGrip.Text = "Push"
        Me.optPushGrip.UseVisualStyleBackColor = True
        '
        'optNoGrip
        '
        Me.optNoGrip.AutoSize = True
        Me.optNoGrip.Checked = True
        Me.optNoGrip.Location = New System.Drawing.Point(15, 28)
        Me.optNoGrip.Name = "optNoGrip"
        Me.optNoGrip.Size = New System.Drawing.Size(59, 20)
        Me.optNoGrip.TabIndex = 0
        Me.optNoGrip.TabStop = True
        Me.optNoGrip.Text = "None"
        Me.optNoGrip.UseVisualStyleBackColor = True
        '
        'chkRingClash
        '
        Me.chkRingClash.AutoSize = True
        Me.chkRingClash.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRingClash.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRingClash.ForeColor = System.Drawing.Color.White
        Me.chkRingClash.Location = New System.Drawing.Point(6, 6)
        Me.chkRingClash.Name = "chkRingClash"
        Me.chkRingClash.Size = New System.Drawing.Size(135, 20)
        Me.chkRingClash.TabIndex = 5
        Me.chkRingClash.Text = "Show Ring Clash"
        Me.chkRingClash.UseVisualStyleBackColor = True
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(481, 11)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(8, 650)
        Me.Splitter1.TabIndex = 9
        Me.Splitter1.TabStop = False
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter2.Location = New System.Drawing.Point(489, 404)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(493, 10)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'frmEditMachining
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Navy
        Me.ClientSize = New System.Drawing.Size(992, 716)
        Me.Controls.Add(Me.pbPoscalc)
        Me.Controls.Add(Me.Splitter2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEditMachining"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Machining"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebTL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMachining, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpHeight.ResumeLayout(False)
        Me.grpWidth.ResumeLayout(False)
        Me.grpJog.ResumeLayout(False)
        Me.grpPos.ResumeLayout(False)
        CType(Me.pbPoscalc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.tcMisc.ResumeLayout(False)
        Me.tpMisc.ResumeLayout(False)
        Me.grpMisc1.ResumeLayout(False)
        Me.grpNotch.ResumeLayout(False)
        Me.grpMisc3.ResumeLayout(False)
        Me.grpMisc2.ResumeLayout(False)
        Me.grpMisc4.ResumeLayout(False)
        Me.tpSteps.ResumeLayout(False)
        Me.grpOps.ResumeLayout(False)
        Me.grpOps.PerformLayout()
        CType(Me.nebCustom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpNotches.ResumeLayout(False)
        Me.grpNotches.PerformLayout()
        Me.tpOpts.ResumeLayout(False)
        Me.tpOpts.PerformLayout()
        Me.grpGrip.ResumeLayout(False)
        Me.grpGrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents dgvMachining As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents pbPoscalc As System.Windows.Forms.PictureBox
    Friend WithEvents nebTL As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebPL As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebANG As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebZP As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebYP As System.Windows.Forms.NumericUpDown
    Friend WithEvents grpPos As System.Windows.Forms.GroupBox
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnAngMinus As System.Windows.Forms.Button
    Friend WithEvents btnAngPlus As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents btnRight As System.Windows.Forms.Button
    Friend WithEvents btnLeft As System.Windows.Forms.Button
    Friend WithEvents grpJog As System.Windows.Forms.GroupBox
    Friend WithEvents btnPLMinus As System.Windows.Forms.Button
    Friend WithEvents btnPLPlus As System.Windows.Forms.Button
    Friend WithEvents grpHeight As System.Windows.Forms.GroupBox
    Friend WithEvents grpWidth As System.Windows.Forms.GroupBox
    Friend WithEvents btnBHMinus As System.Windows.Forms.Button
    Friend WithEvents btnBHPlus As System.Windows.Forms.Button
    Friend WithEvents btnBWMinus As System.Windows.Forms.Button
    Friend WithEvents btnBWPlus As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents txtToolCode As System.Windows.Forms.TextBox
    Friend WithEvents txtProfile As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblLDesc As System.Windows.Forms.Label
    Friend WithEvents chkRingClash As System.Windows.Forms.CheckBox
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents lblAngPL As System.Windows.Forms.Label
    Friend WithEvents grpMisc4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMisc4Minus As System.Windows.Forms.Button
    Friend WithEvents btnMisc4Plus As System.Windows.Forms.Button
    Friend WithEvents grpMisc2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMisc2Minus As System.Windows.Forms.Button
    Friend WithEvents btnMisc2Plus As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grpMisc1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMisc1Minus As System.Windows.Forms.Button
    Friend WithEvents btnMisc1Plus As System.Windows.Forms.Button
    Friend WithEvents grpNotch As System.Windows.Forms.GroupBox
    Friend WithEvents btnNotchMinus As System.Windows.Forms.Button
    Friend WithEvents btnNotchPlus As System.Windows.Forms.Button
    Friend WithEvents grpMisc3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMisc3Minus As System.Windows.Forms.Button
    Friend WithEvents btnMisc3Plus As System.Windows.Forms.Button
    Friend WithEvents lblStdOp As System.Windows.Forms.Label
    Friend WithEvents tcMisc As System.Windows.Forms.TabControl
    Friend WithEvents tpMisc As System.Windows.Forms.TabPage
    Friend WithEvents tpSteps As System.Windows.Forms.TabPage
    Friend WithEvents grpOps As System.Windows.Forms.GroupBox
    Friend WithEvents nebCustom As System.Windows.Forms.NumericUpDown
    Friend WithEvents rbOps10 As System.Windows.Forms.RadioButton
    Friend WithEvents rbOps5 As System.Windows.Forms.RadioButton
    Friend WithEvents rbOps1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbOpsCustom As System.Windows.Forms.RadioButton
    Friend WithEvents grpNotches As System.Windows.Forms.GroupBox
    Friend WithEvents rbNotch5 As System.Windows.Forms.RadioButton
    Friend WithEvents rbNotch25 As System.Windows.Forms.RadioButton
    Friend WithEvents rbNotch1 As System.Windows.Forms.RadioButton
    Friend WithEvents tpOpts As System.Windows.Forms.TabPage
    Friend WithEvents grpGrip As System.Windows.Forms.GroupBox
    Friend WithEvents optPullGrip As System.Windows.Forms.RadioButton
    Friend WithEvents optPushGrip As System.Windows.Forms.RadioButton
    Friend WithEvents optNoGrip As System.Windows.Forms.RadioButton
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents btnUndo As System.Windows.Forms.Button
    Friend WithEvents nebZoom As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblZoom As System.Windows.Forms.Label
End Class
