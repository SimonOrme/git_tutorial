﻿Public Class clsPosCalc

#Region " Variable Declaration "

    Public pbPosCalc As System.Windows.Forms.PictureBox

    Private dblYP As Double = 0
    Private dblZP As Double = 0
    Private dblAng As Double = 0
    Private dblPlunge As Double = 0
    Private dblToolDiameter As Double = 5

    Private YMaximum As Double = 125
    Private YMinimum As Double = -125
    Private ZMaximum As Double = 120
    Private ZMinimum As Double = -120
    Private AngMaximum As Double = 0
    Private AngMinimum As Double = 359

    Private intScale As Integer = 3

    Public shtRingDia As Short = 75 * intScale 'Dia of unplunged tool - See also Scale Parameter
    Public shtOuterRingRad As Short = prfOuterRingRad * intScale ' Rad of Ring - See also Scale Parameter
    Public shtVCylinder As Short = prfVCylinder * intScale 'amount the V Blades retract from their Notching Pos
    Public shtBladeOffset As Short = prfVBladeOffset * intScale

    Private dblXOffset As Double = 200
    Private dblYoffset As Double = 175

    Public DangerY As Double = prfDangerY * intScale
    Public DangerZ As Double = prfDangerZ * intScale
    Public DangerRad As Short = prfDangerRad * intScale

    Const XDatum As Short = 0
    Const YDatum As Short = 0
    Const ToolLen As Short = 20 '20

    Public bShowPrep As Boolean = True
    Public bShowGrip As Boolean = False

    'Gripper
    Public dblGY As Double = 0
    Public dblGZ As Double = 0
    Public GripperType As Integer = 2 '1=Pull 2=Push

    Public PushGripperHeight As Double = 10
    Public PushGripperWidth As Double = 5
    Public PushGripperRotate As Boolean = False

    Public PullGripperHeight As Double = 10
    Public PullGripperZPos As Double = 20

    Public ShowGripperLimits As Boolean = False
    Public MinGY As Double = 0
    Public MaxGY As Double = 0
    Public MinGZ As Double = 0
    Public MaxGZ As Double = 0

    'Tool Positions
    Public tx As Double
    Public ty As Double
    Public tz As Double

    Public Structure GraphicEntity
        Dim X As Double
        Dim Y As Double
        Dim Rad As Double
        Dim X1 As Double
        Dim Y1 As Double
    End Structure

    Public ProfileGeo() As GraphicEntity

    Public Structure CoType
        Dim X1 As Double
        Dim Y1 As Double
        Dim X2 As Double
        Dim Y2 As Double
    End Structure
    Public co(6) As CoType

    Public pi As Double = 3.14159265358979

    Private working As Boolean = False

    Private iZBLock As Integer = 0
    Private sngBlock1 As Single = 20
    Private sngBlock2 As Single = 20
    Private sngBlock3 As Single = 20
    Private sngBlock4 As Single = 20
    Private bMk6Block As Boolean = False
    Private bMk6Wheel As Boolean = False
    Private dblProfileWidth As Double = 0
    Private dblProfileHeight As Double = 0
    Private iWheelY As Integer = 0
    Private bMk6Wheel3 As Boolean = False

#End Region

#Region " Properties "

    Property YP() As Integer
        Get
            Return dblYP
        End Get
        Set(ByVal value As Integer)
            dblYP = value
        End Set
    End Property

    Property ZP() As Integer
        Get
            Return dblZP
        End Get
        Set(ByVal value As Integer)
            dblZP = value
        End Set
    End Property

    Property Angle() As Integer
        Get
            Return dblAng
        End Get
        Set(ByVal value As Integer)
            dblAng = value
        End Set
    End Property

    Property Plunge() As Integer
        Get
            Return dblPlunge
        End Get
        Set(ByVal value As Integer)
            dblPlunge = value
        End Set
    End Property

    Property ToolDiameter() As Integer
        Get
            Return dblToolDiameter
        End Get
        Set(ByVal value As Integer)
            dblToolDiameter = value
            CreateTool()
        End Set
    End Property

    Property Scale() As Integer
        Get
            Return intScale
        End Get
        Set(ByVal value As Integer)
            intScale = value
            shtRingDia = 75 * intScale 'Dia of unplunged tool
            shtOuterRingRad = prfOuterRingRad * intScale
            DangerY = prfDangerY * intScale
            DangerZ = prfDangerZ * intScale
            DangerRad = prfDangerRad * intScale
            shtVCylinder = prfVCylinder * intScale
            shtBladeOffset = (prfVBladeOffset * intScale)
            CreateTool()
        End Set
    End Property

    Property XOffset() As Integer
        Get
            Return dblXOffset
        End Get
        Set(ByVal value As Integer)
            dblXOffset = value
        End Set
    End Property

    Property YOffset() As Integer
        Get
            Return dblYoffset
        End Get
        Set(ByVal value As Integer)
            dblYoffset = value
        End Set
    End Property

    Property ZBlock() As Integer
        Get
            Return iZBLock
        End Get
        Set(ByVal value As Integer)
            iZBLock = value
        End Set
    End Property

    Property Block1() As Integer
        Get
            Return sngBlock1
        End Get
        Set(ByVal value As Integer)
            sngBlock1 = value
        End Set
    End Property

    Property Block2() As Integer
        Get
            Return sngBlock2
        End Get
        Set(ByVal value As Integer)
            sngBlock2 = value
        End Set
    End Property

    Property Block3() As Integer
        Get
            Return sngBlock3
        End Get
        Set(ByVal value As Integer)
            sngBlock3 = value
        End Set
    End Property

    Property Block4() As Integer
        Get
            Return sngBlock4
        End Get
        Set(ByVal value As Integer)
            sngBlock4 = value
        End Set
    End Property

    Property Mk6Block() As Boolean
        Get
            Return bMk6Block
        End Get
        Set(ByVal value As Boolean)
            bMk6Block = value
        End Set
    End Property

    Property ProfileWidth() As Double
        Get
            Return dblProfileWidth
        End Get
        Set(ByVal value As Double)
            dblProfileWidth = value
        End Set
    End Property

    Property ProfileHeight() As Double
        Get
            Return dblProfileHeight
        End Get
        Set(ByVal value As Double)
            dblProfileHeight = value
        End Set
    End Property

    Property Mk6Wheel() As Boolean
        Get
            Return bMk6Wheel
        End Get
        Set(ByVal value As Boolean)
            bMk6Wheel = value
        End Set
    End Property

    Property WheelY() As Integer
        Get
            Return iWheelY
        End Get
        Set(ByVal value As Integer)
            iWheelY = value
        End Set
    End Property

    Property Mk6Wheel3() As Boolean
        Get
            Return bMk6Wheel3
        End Get
        Set(ByVal value As Boolean)
            bMk6Wheel3 = value
        End Set
    End Property

#End Region


#Region " Sub New - New Class Instance Created "

    Public Sub New()

        CreateTool()

    End Sub

#End Region

#Region " Sub - CreateTool() "

    Sub CreateTool()

        Dim i As Integer = 0
        Dim dia As Double
        Dim MaxRange As Double = 180

        dia = dblToolDiameter / 2

        i = 0
        co(i).X1 = -1 * dia
        co(i).Y1 = 0
        co(i).X2 = 1 * dia
        co(i).Y2 = 0

        i = 1
        co(i).X1 = 1 * dia
        co(i).Y1 = 0
        co(i).X2 = 1 * dia
        co(i).Y2 = prfTipToCollet

        i = 2
        co(i).X1 = -1 * dia
        co(i).Y1 = 0
        co(i).X2 = -1 * dia
        co(i).Y2 = prfTipToCollet

        i = 3
        co(i).X1 = -20
        co(i).Y1 = prfTipToCollet
        co(i).X2 = 20
        co(i).Y2 = prfTipToCollet

        i = 4
        co(i).X1 = -20
        co(i).Y1 = (prfTipToCollet + 30)
        co(i).X2 = 20
        co(i).Y2 = (prfTipToCollet + 30)

        i = 5
        co(i).X1 = -20
        co(i).Y1 = prfTipToCollet
        co(i).X2 = -20
        co(i).Y2 = (prfTipToCollet + 30)

        i = 6
        co(i).X1 = 20
        co(i).Y1 = prfTipToCollet
        co(i).X2 = 20
        co(i).Y2 = (prfTipToCollet + 30)

    End Sub

#End Region

#Region " Sub - LoadDXF() "

    Public Sub LoadDXF(ByVal Filename As String)
        Dim idx As Integer = 0
        Dim data As String = ""

        'Currently set to import only 1000 entites if more are required enter them here!
        ReDim ProfileGeo(1350)

        If Not System.IO.File.Exists(Filename) Then
            Exit Sub
        End If

        FileOpen(1, Filename, OpenMode.Input)

        'Find entities section
        Do Until data = "ENTITIES"
            data = LineInput(1)
        Loop

        'Find first Line or Arc Section
        Do Until data = "LINE" Or data = "ARC"
            data = LineInput(1)
        Loop

        Do Until EOF(1)
            Select Case data
                Case "LINE"
                    Do Until Trim(data) = "10"
                        data = LineInput(1)
                    Loop
                    data = LineInput(1)
                    ProfileGeo(idx).X = Val(data) + 50

                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).Y = Val(data) - 50

                    Do Until Trim(data) = "11"
                        data = LineInput(1)
                    Loop
                    data = LineInput(1)
                    ProfileGeo(idx).X1 = Val(data) + 50

                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).Y1 = Val(data) - 50

                    ProfileGeo(idx).Rad = 0
                    idx = idx + 1
                Case "ARC"
                    Do Until Trim(data) = "10"
                        data = LineInput(1)
                    Loop
                    data = LineInput(1)
                    ProfileGeo(idx).X = Val(data) + 50
                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).Y = Val(data) - 50
                    data = LineInput(1)
                    data = LineInput(1)
                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).Rad = Val(data)
                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).X1 = Val(data) * pi / 180 ' (360 - Val(Data)) * pi / 180
                    data = LineInput(1)
                    data = LineInput(1)
                    ProfileGeo(idx).Y1 = Val(data) * pi / 180 '(360 - Val(Data)) * pi / 180
                    idx = idx + 1
                Case Else
                    Do Until data = "LINE" Or data = "ARC" Or EOF(1)
                        data = LineInput(1)
                    Loop
            End Select
        Loop
        FileClose(1)

        ReDim Preserve ProfileGeo(idx - 1)

    End Sub

#End Region
#Region " Sub - DrawProfile() "

    Public Sub DrawProfile(ByRef Graphic() As GraphicEntity, ByRef Dest As Graphics)

        Dim i As Integer
        For i = 0 To UBound(Graphic)
            With Graphic(i)

                If .Rad = 0 Then
                    'Dest.Line((.X, -.Y) - (.X1, -.Y1))
                    Dim shX1 As Short = dblXOffset + .X * intScale
                    Dim shY1 As Short = dblYoffset - .Y * intScale
                    Dim shX2 As Short = dblXOffset + .X1 * intScale
                    Dim shY2 As Short = dblYoffset - .Y1 * intScale
                    Dest.DrawLine(Pens.Black, shX1, shY1, shX2, shY2)
                    'Dest.PSet(New Point[](.X1, -.Y1)
                Else
                    'Dest.Circle((.X, -.Y), .Rad, 0, .X1, .Y1) ' (2 * pi) - .X1, (2 * pi) - .Y1
                    'Dest.DrawString(shX2.ToString + "," + shY2.ToString, New Font("Arial", 8, FontStyle.Regular), Brushes.Navy, shX1, shY1)
                    Dim shX1 As Single = dblXOffset + .X * intScale
                    Dim shY1 As Single = dblYoffset - .Y * intScale
                    Dim shRad As Single = .Rad * intScale
                    Dim shX2 As Single = (.X1 * 180 / pi)
                    Dim shY2 As Single = (.Y1 * 180 / pi)
                    If shY2 < shX2 Then shY2 += 360
                    shX2 = 360 - shX2
                    shY2 = 360 - shY2
                    shY2 -= shX2
                    Dest.DrawArc(Pens.Black, shX1 - shRad, shY1 - shRad, shRad * 2, shRad * 2, shX2, shY2)
                End If

            End With

        Next i

    End Sub

#End Region

#Region " Sub - CreateDrawing() "

    Public Sub CreateDrawing()

        If IsNothing(ProfileGeo) Then Exit Sub

        'Declare Variables
        Dim Y2 As Short
        Dim X2 As Short
        Dim Y1 As Short
        Dim X1 As Short
        Dim i As Integer
        Dim s As Double
        Dim c As Double
        Dim a1 As Double

        If working Then Exit Sub
        working = True

        'Bring Angle within the 0-360 Range
        dblAng = (dblAng + 720) Mod 360

        'Check Y is Valid
        If dblYP > YMaximum Then dblYP = YMaximum
        If dblYP < YMinimum Then dblYP = YMinimum

        'Check Z is Valid
        If dblZP > ZMaximum Then dblZP = ZMaximum
        If dblZP < ZMinimum Then dblZP = ZMinimum

        'Generate Graphics Canvas
        Dim gPosCalc As System.Drawing.Graphics = pbPosCalc.CreateGraphics

        'Clear Screen
        gPosCalc.Clear(Color.White)

        'Draw Profile
        DrawProfile(ProfileGeo, gPosCalc)

        'Set Centre of Ring Y / Z Draw Position
        Dim rY As Short = dblXOffset + (dblYP * intScale)
        Dim rZ As Short = dblYoffset - (dblZP * intScale)

        'Set Clash Area
        Dim rClashY As Short = dblXOffset + DangerY
        Dim rClashZ As Short = dblYoffset - DangerZ

        'Set Datum Position
        Dim sX As Single = dblXOffset + XDatum + (50 * intScale)
        Dim sY As Single = dblYoffset + YDatum + (50 * intScale)

        'Draw 0 Point
        gPosCalc.DrawEllipse(Pens.Red, sX - (prfDatumSize / 2), sY - (prfDatumSize / 2), prfDatumSize, prfDatumSize)
        gPosCalc.DrawLine(Pens.Red, sX - prfDatumSize, sY, sX + prfDatumSize, sY)
        gPosCalc.DrawLine(Pens.Red, sX, sY - prfDatumSize, sX, sY + prfDatumSize)

        'Draw Gripper 1=Pull, 2= Push, 3=Mk6
        If bShowGrip Then
            If GripperType = 1 Then
                'Pull Type
                Dim rcX As Integer = sX - ((dblGY + 2) * intScale)
                Dim rcY As Integer = sY - ((PullGripperZPos + PullGripperHeight) * intScale)
                Dim rcWid As Integer = 5 * intScale
                Dim rcHei As Integer = (PullGripperZPos + PullGripperHeight) * intScale
                Dim rRect As New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Red, rRect)

                rcX = sX - ((dblGY + 2) * intScale)
                rcY = sY - ((PullGripperZPos + PullGripperHeight) * intScale)
                rcWid = 5 * intScale
                rcHei = PullGripperHeight * intScale
                rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Black, rRect)

                If ShowGripperLimits Then
                    rcX = sX - (MaxGY * intScale)
                    rcY = sY - (MaxGZ * intScale)
                    rcWid = (MaxGY - MinGY) * intScale
                    rcHei = (MaxGZ - MinGZ) * intScale
                    rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                    gPosCalc.DrawRectangle(Pens.Green, rRect)
                End If

            ElseIf GripperType = 2 Then

                'Calculate Gripper Position
                Dim GripDatumX As Integer = sX - (dblGY * intScale)
                Dim GripDatumY As Integer = sY - (dblGZ * intScale)

                If PushGripperRotate = True Then

                    'Push Type Rotated
                    Dim rcX As Integer = GripDatumX - ((PushGripperHeight / 2) * intScale)
                    Dim rcY As Integer = GripDatumY
                    Dim sdpT As New Point(GripDatumX + 1 * intScale, GripDatumY)
                    Dim sdpB As New Point(GripDatumX - 1 * intScale, GripDatumY)
                    Dim sdpL As New Point(GripDatumX, GripDatumY + 1 * intScale)
                    Dim sdpR As New Point(GripDatumX, GripDatumY - 1 * intScale)

                    'Draw Gripper Base
                    Dim rcWid As Integer = PushGripperWidth * intScale
                    Dim rcHei As Integer = PushGripperHeight * intScale
                    Dim rRect As New Rectangle(rcX, rcY, rcHei, rcWid)
                    gPosCalc.DrawRectangle(Pens.Red, rRect)

                    'Draw Gripper Point
                    gPosCalc.DrawLine(Pens.Black, sdpT, sdpB)
                    gPosCalc.DrawLine(Pens.Black, sdpL, sdpR)

                    'Draw Gripper Closer
                    rcX += ((PushGripperHeight - 6) / 2) * intScale
                    rcY -= 27 * intScale
                    rcWid = 6 * intScale
                    rcHei = 15 * intScale
                    rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                    gPosCalc.DrawRectangle(Pens.Red, rRect)

                    If ShowGripperLimits Then
                        rcX = sX - (MaxGY * intScale)
                        rcY = sY - (MaxGZ * intScale)
                        rcWid = (MaxGY - MinGY) * intScale
                        rcHei = (MaxGZ - MinGZ) * intScale
                        rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                        gPosCalc.DrawRectangle(Pens.Green, rRect)
                    End If

                Else

                    'Push Type Non Rotated
                    Dim rcX As Integer = GripDatumX
                    Dim rcY As Integer = GripDatumY - ((PushGripperHeight / 2) * intScale)
                    Dim sdpT As New Point(GripDatumX - 1 * intScale, GripDatumY)
                    Dim sdpB As New Point(GripDatumX + 1 * intScale, GripDatumY)
                    Dim sdpL As New Point(GripDatumX * intScale, GripDatumY + 1 * intScale)
                    Dim sdpR As New Point(GripDatumX * intScale, GripDatumY - 1 * intScale)

                    'Draw Gripper Base
                    Dim rcWid As Integer = PushGripperWidth * intScale
                    Dim rcHei As Integer = PushGripperHeight * intScale
                    Dim rRect As New Rectangle(rcX, rcY, rcWid, rcHei)
                    gPosCalc.DrawRectangle(Pens.Red, rRect)

                    'Draw Gripper Point
                    gPosCalc.DrawLine(Pens.Black, sdpT, sdpB)
                    gPosCalc.DrawLine(Pens.Black, sdpL, sdpR)

                    'Draw Gripper Closer
                    rcX -= 27 * intScale
                    rcY += ((PushGripperHeight - 6) / 2) * intScale
                    rcWid = 15 * intScale
                    rcHei = 6 * intScale
                    rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                    gPosCalc.DrawRectangle(Pens.Red, rRect)

                    If ShowGripperLimits Then
                        rcX = sX - (MaxGY * intScale)
                        rcY = sY - (MaxGZ * intScale)
                        rcWid = (MaxGY - MinGY) * intScale
                        rcHei = (MaxGZ - MinGZ) * intScale
                        rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                        gPosCalc.DrawRectangle(Pens.Green, rRect)
                    End If

                End If

            ElseIf GripperType = 3 Then   'Mk 6 Gripper section

                Dim GHeight As Integer = 90

                'Draw Rear Nose
                Dim dblGWid As Double = 10
                Dim GPos As Double = 15

                Dim rcX As Integer = sX - ((GPos + (dblGWid / 2)) * intScale)
                Dim rcY As Integer = sY - ((PullGripperZPos + GHeight) * intScale)
                Dim rcWid As Integer = dblGWid * intScale
                Dim rcHei As Integer = (PullGripperZPos + GHeight) * intScale
                Dim rRect As New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Red, rRect)

                rcX = sX - ((GPos + (dblGWid / 2)) * intScale)
                rcY = sY - ((PullGripperZPos + GHeight) * intScale)
                rcWid = dblGWid * intScale
                rcHei = PullGripperHeight * intScale
                rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Black, rRect)

                'Draw Front Nose
                GPos = 35

                rcX = sX - ((GPos + (dblGWid / 2)) * intScale)
                rcY = sY - ((PullGripperZPos + GHeight) * intScale)
                rcWid = dblGWid * intScale
                rcHei = (PullGripperZPos + GHeight) * intScale
                rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Red, rRect)

                rcX = sX - ((GPos + (dblGWid / 2)) * intScale)
                rcY = sY - ((PullGripperZPos + GHeight) * intScale)
                rcWid = dblGWid * intScale
                rcHei = PullGripperHeight * intScale
                rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Black, rRect)


            End If

        End If

        working = False

        'Draw Ring
        If bShowPrep Then
            If ToolDiameter <> 0 Then gPosCalc.DrawEllipse(Pens.LightGray, rY - shtRingDia, rZ - shtRingDia, shtRingDia * 2, shtRingDia * 2)
            gPosCalc.DrawLine(Pens.LightGray, rY - 10, rZ, rY + 10, rZ)
            gPosCalc.DrawLine(Pens.LightGray, rY, rZ - 10, rY, rZ + 10)

            If ToolDiameter <> 0 Then

                'Show Clash Area
                If ShowDangerRings Then

                    'Unplunged Ring
                    gPosCalc.DrawEllipse(Pens.Red, rClashY - DangerRad, rClashZ - DangerRad, DangerRad * 2, DangerRad * 2)

                    'Plunged Ring
                    gPosCalc.DrawEllipse(Pens.Red, rY - shtOuterRingRad, rZ - shtOuterRingRad, shtOuterRingRad * 2, shtOuterRingRad * 2)

                End If

                'Calculate Tip of Tool Position
                a1 = -Angle * 3.14159265358979 / 180
                c = System.Math.Sin(a1)
                s = System.Math.Cos(a1)
                tx = dblXOffset + (dblYP * intScale) + (shtRingDia * c)
                ty = dblYoffset - (dblZP * intScale) - (shtRingDia * s)
                'tssLabel4.Text = "tx=" + Format(tx, "0.0")
                'tssLabel5.Text = "ty=" + Format(ty, "0.0")

                'Draw Unplunged Centre Line
                X1 = tx
                Y1 = ty
                X2 = tx + ((ToolLen * c) * intScale)
                Y2 = ty - ((ToolLen * s) * intScale)
                gPosCalc.DrawLine(Pens.Black, X1, Y1, X2, Y2)

                'Draw Tool Before Plunge
                For i = 0 To 6
                    X1 = tx + (co(i).X1 * s + co(i).Y1 * c) * intScale
                    Y1 = ty + (co(i).X1 * c - co(i).Y1 * s) * intScale
                    X2 = tx + (co(i).X2 * s + co(i).Y2 * c) * intScale
                    Y2 = ty + (co(i).X2 * c - co(i).Y2 * s) * intScale
                    gPosCalc.DrawLine(Pens.Black, X1, Y1, X2, Y2)
                Next i

                'Draw Plunged Centre Line
                X1 = tx - (Plunge * c * intScale)
                Y1 = ty + (Plunge * s * intScale)
                X2 = tx - (Plunge * c * intScale) + ((ToolLen * c) * intScale)
                Y2 = ty + (Plunge * s * intScale) - ((ToolLen * s) * intScale)
                gPosCalc.DrawLine(Pens.Red, X1, Y1, X2, Y2)

                'Draw Plunged Tool
                For i = 0 To 6
                    X1 = tx - ((Plunge * c) * intScale) + ((co(i).X1 * s + co(i).Y1 * c) * intScale)
                    Y1 = ty + ((Plunge * s) * intScale) + ((co(i).X1 * c - co(i).Y1 * s) * intScale)
                    X2 = tx - ((Plunge * c) * intScale) + ((co(i).X2 * s + co(i).Y2 * c) * intScale)
                    Y2 = ty + ((Plunge * s) * intScale) + ((co(i).X2 * c - co(i).Y2 * s) * intScale)
                    gPosCalc.DrawLine(Pens.Blue, X1, Y1, X2, Y2)
                Next i

                'Draw Click Point
                X1 = tx + ((prfTipToCollet + 15) * c * intScale) - (8 * intScale)
                Y1 = ty - ((prfTipToCollet + 15) * s * intScale) - (8 * intScale)
                X2 = 8 * intScale * 2
                Y2 = 8 * intScale * 2
                gPosCalc.DrawEllipse(Pens.Black, X1, Y1, X2, Y2)

            Else

                'V Notch Coding
                'Front YPos to back of profile is 107.5-pw+depth on ZX3 / Microline
                'Rear Ypos base is -7.5-depth on ZX3 / Microline

                'Ring Clash Area
                If ShowDangerRings Then

                    'Draw Ring Plunged
                    gPosCalc.DrawEllipse(Pens.LightGray, rY - shtRingDia, rZ - shtRingDia, shtRingDia * 2, shtRingDia * 2)

                    'Draw Ring Unplunged
                    gPosCalc.DrawEllipse(Pens.LightGray, rY - shtOuterRingRad, rZ - shtOuterRingRad, shtOuterRingRad * 2, shtOuterRingRad * 2)

                End If

                shtBladeOffset = (prfVBladeOffset * intScale)

                'Draw V Notch Blade Top Centre Marking
                Dim iVFY As Short
                If Angle = 90 Then iVFY = rY - shtBladeOffset
                If Angle = 270 Then iVFY = rY + shtBladeOffset

                Dim iVFZ As Short = rZ + (0 * intScale)
                gPosCalc.DrawLine(Pens.Orange, iVFY - prfDatumSize, iVFZ, iVFY + prfDatumSize, iVFZ)
                gPosCalc.DrawLine(Pens.Orange, iVFY, iVFZ - prfDatumSize, iVFY, iVFZ + prfDatumSize)

                'Draw V Notch Blade Top
                Dim sBladeAt45Degrees As Double = System.Math.Sin(45 * 3.14159265358979 / 180) * prfVBladeDia
                Dim iBladeW As Short = (sBladeAt45Degrees / 2) * intScale
                Dim iBladeH As Short = (prfVBladeDia / 2) * intScale
                gPosCalc.DrawEllipse(Pens.Orange, iVFY - iBladeW, iVFZ - iBladeH, iBladeW * 2, iBladeH * 2)

                'Draw V Notch Blade Bottom Centre Marking
                gPosCalc.DrawLine(Pens.Orange, iVFY - prfDatumSize, sY, iVFY + prfDatumSize, sY)
                gPosCalc.DrawLine(Pens.Orange, iVFY, sY - prfDatumSize, iVFY, sY + prfDatumSize)

                'Draw V Notch Blade Bottom
                gPosCalc.DrawEllipse(Pens.Orange, iVFY - iBladeW, sY - iBladeH, iBladeW * 2, iBladeH * 2)

                'Draw Line of Notch
                gPosCalc.DrawLine(Pens.Orange, iVFY + iBladeW, iVFZ, iVFY + iBladeW, sY)
                gPosCalc.DrawLine(Pens.Orange, iVFY - iBladeW, iVFZ, iVFY - iBladeW, sY)

                'Ring Y Pos - V Notch Centre Blade + half Blade Width + 150 (Cylinder Stroke)
                Dim shtVSafe As Short = shtBladeOffset - iBladeW + shtVCylinder
                Dim intVSafeY As Integer = rY - shtVSafe
                Dim intVSafeZ As Integer = rZ - shtVSafe
                If ShowDangerRings Then gPosCalc.DrawEllipse(Pens.Red, intVSafeY, intVSafeZ, shtVSafe * 2, shtVSafe * 2)

            End If


        End If

        'Mark 6 Wheel Coding
        If Mk6Wheel Then

            Dim WHeight As Integer = CInt(dblProfileHeight) + 6
            Dim WWid As Double = 35
            Dim WPos As Integer = iWheelY

            'Draw Drop Area

            Dim rcX As Integer = sX - ((WPos + (WWid / 2)) * intScale)
            Dim rcY As Integer = sY - WHeight * intScale
            Dim rcWid As Integer = WWid * intScale
            Dim rcHei As Integer = WHeight * intScale
            Dim rRect As New Rectangle(rcX, rcY, rcWid, rcHei)
            gPosCalc.DrawRectangle(Pens.Red, rRect)

            'Draw Wheel Box
            rcX = sX - ((WPos + (WWid / 2)) * intScale)
            rcY = sY - ((WHeight + 11) * intScale)
            rcWid = WWid * intScale
            rcHei = 11 * intScale
            rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
            gPosCalc.DrawRectangle(Pens.Black, rRect)

            'Draw Extended Wheel Box
            rcX -= (15 * intScale)
            rcWid = 15 * intScale

            If bMk6Wheel3 Then
                rRect = New Rectangle(rcX, rcY, rcWid, rcHei)
                gPosCalc.DrawRectangle(Pens.Orange, rRect)
            End If

            'Draw Wheel Cables
            Dim rsXWheel1 As Integer = rcX + (4 * intScale)
            Dim rsXWheel2 As Integer = rcX + (19 * intScale)
            Dim rsXWheel3 As Integer = rcX + (34 * intScale)

            Dim rsYWheels As Integer = rcY + (rcHei / 2)

            If bMk6Wheel3 Then
                gPosCalc.DrawEllipse(Pens.Green, rsXWheel1, rsYWheels, 11 * intScale, 11 * intScale)
            End If
            gPosCalc.DrawEllipse(Pens.Green, rsXWheel2, rsYWheels, 11 * intScale, 11 * intScale)
            gPosCalc.DrawEllipse(Pens.Green, rsXWheel3, rsYWheels, 11 * intScale, 11 * intScale)

        End If

        'Draw Z Block
        'Set Centre of Ring Y / Z Draw Position
        Dim zbY As Short = dblXOffset + XDatum + (50 * intScale)
        Dim zbZ As Short = dblYoffset + YDatum + (50 * intScale)

        If bMk6Block = False Then

            Select Case iZBLock
                Case 0
                    'Do Nothing
                Case 1
                    'Z Block 1
                    gPosCalc.DrawLine(Pens.Red, zbY, zbZ, zbY, zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock1 * iScale), zbZ - (51 * iScale), zbY - (sngBlock1 * iScale), zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock1 * iScale), zbZ - (51 * iScale), zbY - (sngBlock1 * iScale) + (6 * iScale), zbZ - (51 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock1 * iScale) + (6 * iScale), zbZ - (51 * iScale), zbY - (sngBlock1 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (31 * iScale), zbY - (sngBlock1 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (70 * iScale), zbY - (sngBlock1 * iScale), zbZ - (70 * iScale))
                Case 2
                    'Z Block 2
                    gPosCalc.DrawLine(Pens.Red, zbY, zbZ, zbY, zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock2 * iScale), zbZ - (51 * iScale), zbY - (sngBlock2 * iScale), zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock2 * iScale), zbZ - (51 * iScale), zbY - (sngBlock2 * iScale) + (6 * iScale), zbZ - (51 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock2 * iScale) + (6 * iScale), zbZ - (51 * iScale), zbY - (sngBlock2 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (31 * iScale), zbY - (sngBlock2 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (70 * iScale), zbY - (sngBlock2 * iScale), zbZ - (70 * iScale))
                Case 3
                    'Z Block 3
                    gPosCalc.DrawLine(Pens.Red, zbY, zbZ, zbY, zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock3 * iScale), zbZ - (51 * iScale), zbY - (sngBlock3 * iScale), zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock3 * iScale), zbZ - (51 * iScale), zbY - (sngBlock3 * iScale) + (6 * iScale), zbZ - (51 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock3 * iScale) + (6 * iScale), zbZ - (51 * iScale), zbY - (sngBlock3 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (31 * iScale), zbY - (sngBlock3 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (70 * iScale), zbY - (sngBlock3 * iScale), zbZ - (70 * iScale))
                Case 4
                    'Z Block 4
                    gPosCalc.DrawLine(Pens.Red, zbY, zbZ, zbY, zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock4 * iScale), zbZ - (51 * iScale), zbY - (sngBlock4 * iScale), zbZ - (70 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock4 * iScale), zbZ - (51 * iScale), zbY - (sngBlock4 * iScale) + (6 * iScale), zbZ - (51 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY - (sngBlock4 * iScale) + (6 * iScale), zbZ - (51 * iScale), zbY - (sngBlock4 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (31 * iScale), zbY - (sngBlock4 * iScale) + (6 * iScale), zbZ - (31 * iScale))
                    gPosCalc.DrawLine(Pens.DarkGreen, zbY, zbZ - (70 * iScale), zbY - (sngBlock4 * iScale), zbZ - (70 * iScale))

            End Select

        Else

            'Mk 6 ZBlocks
            gPosCalc.DrawLine(Pens.Red, zbY - (CInt(dblProfileWidth) * iScale), zbZ, zbY - (CInt(dblProfileWidth) * iScale), zbZ - (70 * iScale))

            Dim iBlockBottom As Integer = 2
            Dim iBlockTop As Integer = 22
            Dim sngBlockDepth As Single = 0

            Select Case iZBLock
                Case 0
                    'Do Nothing
                Case 1
                    sngBlockDepth = sngBlock1
                Case 2
                    sngBlockDepth = sngBlock2
                Case 3
                    sngBlockDepth = sngBlock3
                Case 4
                    sngBlockDepth = sngBlock4
            End Select

            If iZBLock <> 0 Then
                gPosCalc.DrawLine(Pens.Red, zbY - (CInt(dblProfileWidth) * iScale), zbZ, zbY - (CInt(dblProfileWidth) * iScale) + (sngBlockDepth * iScale), zbZ)
                gPosCalc.DrawLine(Pens.DarkGreen, zbY - (CInt(dblProfileWidth) * iScale), zbZ - (iBlockBottom * iScale), zbY - (CInt(dblProfileWidth) * iScale) + (sngBlockDepth * iScale), zbZ - (iBlockBottom * iScale))
                gPosCalc.DrawLine(Pens.DarkGreen, zbY - (CInt(dblProfileWidth) * iScale) + (sngBlockDepth * iScale), zbZ - (iBlockBottom * iScale), zbY - (CInt(dblProfileWidth) * iScale) + (sngBlockDepth * iScale), zbZ - (iBlockTop * iScale))
                gPosCalc.DrawLine(Pens.DarkGreen, zbY - (CInt(dblProfileWidth) * iScale), zbZ - (iBlockTop * iScale), zbY - (CInt(dblProfileWidth) * iScale) + (sngBlockDepth * iScale), zbZ - (iBlockTop * iScale))
            End If

        End If

    End Sub

#End Region

End Class
