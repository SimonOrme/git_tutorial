﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMaster))
        Me.panTop = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblAccess = New System.Windows.Forms.Label()
        Me.txtAccess = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsMenu = New System.Windows.Forms.ToolStrip()
        Me.tsEditMachining = New System.Windows.Forms.ToolStripButton()
        Me.tsEditProfiles = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsPieceGen = New System.Windows.Forms.ToolStripButton()
        Me.tsTestGen = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsTimings = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsUnLock = New System.Windows.Forms.ToolStripButton()
        Me.tsLock = New System.Windows.Forms.ToolStripButton()
        Me.tsSettings = New System.Windows.Forms.ToolStripButton()
        Me.tsEngineer = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsBackup = New System.Windows.Forms.ToolStripButton()
        Me.tsExit = New System.Windows.Forms.ToolStripButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToWindowsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditMachiningToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditProfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnlockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministratorAccessToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PieceGeneratorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestGeneratorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.panTop.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.tsMenu.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panTop
        '
        Me.panTop.BackColor = System.Drawing.Color.Navy
        Me.panTop.Controls.Add(Me.Panel1)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panTop.Location = New System.Drawing.Point(0, 603)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(992, 41)
        Me.panTop.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblAccess)
        Me.Panel1.Controls.Add(Me.txtAccess)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel1.Location = New System.Drawing.Point(720, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(272, 41)
        Me.Panel1.TabIndex = 4
        '
        'lblAccess
        '
        Me.lblAccess.AutoSize = True
        Me.lblAccess.BackColor = System.Drawing.Color.Navy
        Me.lblAccess.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccess.ForeColor = System.Drawing.Color.White
        Me.lblAccess.Location = New System.Drawing.Point(3, 10)
        Me.lblAccess.Name = "lblAccess"
        Me.lblAccess.Size = New System.Drawing.Size(119, 19)
        Me.lblAccess.TabIndex = 0
        Me.lblAccess.Text = "Access Rights"
        '
        'txtAccess
        '
        Me.txtAccess.BackColor = System.Drawing.Color.Navy
        Me.txtAccess.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccess.ForeColor = System.Drawing.Color.White
        Me.txtAccess.Location = New System.Drawing.Point(128, 10)
        Me.txtAccess.Name = "txtAccess"
        Me.txtAccess.Size = New System.Drawing.Size(134, 19)
        Me.txtAccess.TabIndex = 1
        Me.txtAccess.Text = "User"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 644)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(992, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsMenu
        '
        Me.tsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsEditMachining, Me.tsEditProfiles, Me.ToolStripSeparator1, Me.tsPieceGen, Me.tsTestGen, Me.ToolStripSeparator4, Me.tsTimings, Me.ToolStripSeparator2, Me.tsUnLock, Me.tsLock, Me.tsSettings, Me.tsEngineer, Me.ToolStripSeparator3, Me.tsBackup, Me.tsExit})
        Me.tsMenu.Location = New System.Drawing.Point(0, 24)
        Me.tsMenu.Name = "tsMenu"
        Me.tsMenu.Size = New System.Drawing.Size(992, 86)
        Me.tsMenu.TabIndex = 3
        Me.tsMenu.Text = "ToolStrip1"
        '
        'tsEditMachining
        '
        Me.tsEditMachining.Image = CType(resources.GetObject("tsEditMachining.Image"), System.Drawing.Image)
        Me.tsEditMachining.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.tsEditMachining.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsEditMachining.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsEditMachining.Name = "tsEditMachining"
        Me.tsEditMachining.Size = New System.Drawing.Size(91, 83)
        Me.tsEditMachining.Text = "Edit Machining"
        Me.tsEditMachining.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.tsEditMachining.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsEditProfiles
        '
        Me.tsEditProfiles.Image = CType(resources.GetObject("tsEditProfiles.Image"), System.Drawing.Image)
        Me.tsEditProfiles.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsEditProfiles.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsEditProfiles.Name = "tsEditProfiles"
        Me.tsEditProfiles.Size = New System.Drawing.Size(73, 83)
        Me.tsEditProfiles.Text = "Edit Profiles"
        Me.tsEditProfiles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 86)
        '
        'tsPieceGen
        '
        Me.tsPieceGen.Image = CType(resources.GetObject("tsPieceGen.Image"), System.Drawing.Image)
        Me.tsPieceGen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsPieceGen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsPieceGen.Name = "tsPieceGen"
        Me.tsPieceGen.Size = New System.Drawing.Size(94, 83)
        Me.tsPieceGen.Text = "Piece Generator"
        Me.tsPieceGen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsTestGen
        '
        Me.tsTestGen.Image = CType(resources.GetObject("tsTestGen.Image"), System.Drawing.Image)
        Me.tsTestGen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsTestGen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsTestGen.Name = "tsTestGen"
        Me.tsTestGen.Size = New System.Drawing.Size(88, 83)
        Me.tsTestGen.Text = "Test Generator"
        Me.tsTestGen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 86)
        '
        'tsTimings
        '
        Me.tsTimings.Image = CType(resources.GetObject("tsTimings.Image"), System.Drawing.Image)
        Me.tsTimings.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsTimings.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsTimings.Name = "tsTimings"
        Me.tsTimings.Size = New System.Drawing.Size(98, 83)
        Me.tsTimings.Text = "Machine Useage"
        Me.tsTimings.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.tsTimings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 86)
        '
        'tsUnLock
        '
        Me.tsUnLock.Image = CType(resources.GetObject("tsUnLock.Image"), System.Drawing.Image)
        Me.tsUnLock.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsUnLock.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsUnLock.Name = "tsUnLock"
        Me.tsUnLock.Size = New System.Drawing.Size(68, 83)
        Me.tsUnLock.Text = "Unlock"
        Me.tsUnLock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsLock
        '
        Me.tsLock.Enabled = False
        Me.tsLock.Image = CType(resources.GetObject("tsLock.Image"), System.Drawing.Image)
        Me.tsLock.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsLock.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsLock.Name = "tsLock"
        Me.tsLock.Size = New System.Drawing.Size(68, 83)
        Me.tsLock.Text = "Lock"
        Me.tsLock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsSettings
        '
        Me.tsSettings.Enabled = False
        Me.tsSettings.Image = CType(resources.GetObject("tsSettings.Image"), System.Drawing.Image)
        Me.tsSettings.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsSettings.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsSettings.Name = "tsSettings"
        Me.tsSettings.Size = New System.Drawing.Size(68, 83)
        Me.tsSettings.Text = "Settings"
        Me.tsSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsEngineer
        '
        Me.tsEngineer.Enabled = False
        Me.tsEngineer.Image = CType(resources.GetObject("tsEngineer.Image"), System.Drawing.Image)
        Me.tsEngineer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsEngineer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsEngineer.Name = "tsEngineer"
        Me.tsEngineer.Size = New System.Drawing.Size(123, 83)
        Me.tsEngineer.Text = "Administrator Access"
        Me.tsEngineer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 86)
        '
        'tsBackup
        '
        Me.tsBackup.Image = CType(resources.GetObject("tsBackup.Image"), System.Drawing.Image)
        Me.tsBackup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsBackup.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsBackup.Name = "tsBackup"
        Me.tsBackup.Size = New System.Drawing.Size(68, 83)
        Me.tsBackup.Text = "Backup"
        Me.tsBackup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsExit
        '
        Me.tsExit.Image = CType(resources.GetObject("tsExit.Image"), System.Drawing.Image)
        Me.tsExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsExit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsExit.Name = "tsExit"
        Me.tsExit.Size = New System.Drawing.Size(98, 83)
        Me.tsExit.Text = "Exit To Windows"
        Me.tsExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = Nothing
        Me.PictureBox1.Location = New System.Drawing.Point(0, 110)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(992, 493)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.UsageToolStripMenuItem, Me.AdminToolStripMenuItem, Me.TestsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(992, 24)
        Me.MenuStrip1.TabIndex = 7
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BackupToolStripMenuItem, Me.ToolStripSeparator5, Me.ExitToWindowsToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'BackupToolStripMenuItem
        '
        Me.BackupToolStripMenuItem.Image = CType(resources.GetObject("BackupToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BackupToolStripMenuItem.Name = "BackupToolStripMenuItem"
        Me.BackupToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.BackupToolStripMenuItem.Text = "Backup"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(155, 6)
        '
        'ExitToWindowsToolStripMenuItem
        '
        Me.ExitToWindowsToolStripMenuItem.Image = CType(resources.GetObject("ExitToWindowsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ExitToWindowsToolStripMenuItem.Name = "ExitToWindowsToolStripMenuItem"
        Me.ExitToWindowsToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ExitToWindowsToolStripMenuItem.Text = "Exit to Windows"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditMachiningToolStripMenuItem, Me.EditProfilesToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "&Edit"
        '
        'EditMachiningToolStripMenuItem
        '
        Me.EditMachiningToolStripMenuItem.Image = CType(resources.GetObject("EditMachiningToolStripMenuItem.Image"), System.Drawing.Image)
        Me.EditMachiningToolStripMenuItem.Name = "EditMachiningToolStripMenuItem"
        Me.EditMachiningToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EditMachiningToolStripMenuItem.Text = "Edit Machining"
        '
        'EditProfilesToolStripMenuItem
        '
        Me.EditProfilesToolStripMenuItem.Image = CType(resources.GetObject("EditProfilesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.EditProfilesToolStripMenuItem.Name = "EditProfilesToolStripMenuItem"
        Me.EditProfilesToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EditProfilesToolStripMenuItem.Text = "Edit Profiles"
        '
        'UsageToolStripMenuItem
        '
        Me.UsageToolStripMenuItem.Name = "UsageToolStripMenuItem"
        Me.UsageToolStripMenuItem.Size = New System.Drawing.Size(100, 20)
        Me.UsageToolStripMenuItem.Text = "Machine &Usage"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UnlockToolStripMenuItem, Me.LockToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.AdministratorAccessToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "&Admin"
        '
        'UnlockToolStripMenuItem
        '
        Me.UnlockToolStripMenuItem.Image = CType(resources.GetObject("UnlockToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UnlockToolStripMenuItem.Name = "UnlockToolStripMenuItem"
        Me.UnlockToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.UnlockToolStripMenuItem.Text = "Unlock"
        '
        'LockToolStripMenuItem
        '
        Me.LockToolStripMenuItem.Enabled = False
        Me.LockToolStripMenuItem.Image = CType(resources.GetObject("LockToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LockToolStripMenuItem.Name = "LockToolStripMenuItem"
        Me.LockToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.LockToolStripMenuItem.Text = "Lock"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Enabled = False
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'AdministratorAccessToolStripMenuItem
        '
        Me.AdministratorAccessToolStripMenuItem.Enabled = False
        Me.AdministratorAccessToolStripMenuItem.Image = CType(resources.GetObject("AdministratorAccessToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AdministratorAccessToolStripMenuItem.Name = "AdministratorAccessToolStripMenuItem"
        Me.AdministratorAccessToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.AdministratorAccessToolStripMenuItem.Text = "Administrator Access"
        '
        'TestsToolStripMenuItem
        '
        Me.TestsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PieceGeneratorToolStripMenuItem, Me.TestGeneratorToolStripMenuItem})
        Me.TestsToolStripMenuItem.Name = "TestsToolStripMenuItem"
        Me.TestsToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.TestsToolStripMenuItem.Text = "&Tests"
        '
        'PieceGeneratorToolStripMenuItem
        '
        Me.PieceGeneratorToolStripMenuItem.Name = "PieceGeneratorToolStripMenuItem"
        Me.PieceGeneratorToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.PieceGeneratorToolStripMenuItem.Text = "Piece Generator"
        '
        'TestGeneratorToolStripMenuItem
        '
        Me.TestGeneratorToolStripMenuItem.Name = "TestGeneratorToolStripMenuItem"
        Me.TestGeneratorToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.TestGeneratorToolStripMenuItem.Text = "Test Generator"
        '
        'FrmMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(992, 666)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.panTop)
        Me.Controls.Add(Me.tsMenu)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FrmMaster"
        Me.Text = "Stuga CNC Programming"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panTop.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.tsMenu.ResumeLayout(False)
        Me.tsMenu.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panTop As System.Windows.Forms.Panel
    Friend WithEvents txtAccess As System.Windows.Forms.Label
    Friend WithEvents lblAccess As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents tsEditMachining As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsEditProfiles As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsPieceGen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsTestGen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsBackup As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsEngineer As System.Windows.Forms.ToolStripButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents tsExit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsSettings As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsUnLock As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsLock As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tsTimings As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToWindowsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditMachiningToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditProfilesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnlockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministratorAccessToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PieceGeneratorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestGeneratorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
