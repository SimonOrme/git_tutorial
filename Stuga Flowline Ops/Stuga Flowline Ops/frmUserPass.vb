﻿Public Class frmUserPass

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        'Get Old Password
        Dim sPass As String = prfPassword

        'If typed Old Password <> Correct Old Password then Bomb Out
        If Me.txtOldPassword.Text.ToUpper <> sPass.ToUpper Then
            MsgBox("Unauthorised Access", MsgBoxStyle.Information)
            Me.Close()
            Exit Sub
        End If

        If Me.txtPass1.Text <> Me.txtPass2.Text Then
            MsgBox("New Password 1 & 2 are different, please retry", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Set New Password
        prfPassword = txtPass1.Text

        'Write to Password File
        WritePWL()

        MsgBox("Password has been set.", MsgBoxStyle.Information)
        Me.Close()

    End Sub

    Private Sub btnNoPass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNoPass.Click

        'Get Old Password
        Dim sPass As String = prfPassword

        'If typed Old Password <> Correct Old Password then Bomb Out
        If Me.txtOldPassword.Text.ToUpper <> sPass.ToUpper Then
            MsgBox("Unauthorised Access", MsgBoxStyle.Information)
            Me.Close()
            Exit Sub
        End If

        'Set New Password
        prfPassword = "none"

        'Write to Password File
        WritePWL()

        MsgBox("Password has been set.", MsgBoxStyle.Information)
        Me.Close()

    End Sub

End Class