﻿Public Class frmZBlock

#Region " Variable Declaration "

    Private PosCalc As New clsPosCalc
    Public frmParent As Form
    Public frmParentUser As frmEditProfiles
    Public frmParentAdmin As frmEngineer

    Dim MouseX As Short
    Dim MouseY As Short
    Dim bInitialising As Boolean = True
    Dim bShowGripperLimits As Boolean = False

#End Region

#Region " Form - Load "

    Private Sub frmZBlock_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        bInitialising = False

        'Setup Poscalc Class
        InitialisePosCalc()

        'Get Profile Data
        GetData()

    End Sub

#End Region

#Region " Button - Jog Left "

    Private Sub btnLeft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeft.Click

        Dim sngBlock As Single = GetBlock()

        If chkMk6.Checked Then

            Select Case sngBlock
                Case 1
                    prfZBlock1 -= nebSteps.Value
                    rbBlock1.Text = "Z Block 1 (" + prfZBlock1.ToString + "mm)"
                Case 2
                    prfZBlock2 -= nebSteps.Value
                    rbBlock2.Text = "Z Block 2 (" + prfZBlock2.ToString + "mm)"
                Case 3
                    prfZBlock3 -= nebSteps.Value
                    rbBlock3.Text = "Z Block 3 (" + prfZBlock3.ToString + "mm)"
                Case 4
                    prfZBlock4 -= nebSteps.Value
                    rbBlock4.Text = "Z Block 4 (" + prfZBlock4.ToString + "mm)"
            End Select

        Else

            Select Case sngBlock
                Case 1
                    prfZBlock1 += nebSteps.Value
                    rbBlock1.Text = "Z Block 1 (" + prfZBlock1.ToString + "mm)"
                Case 2
                    prfZBlock2 += nebSteps.Value
                    rbBlock2.Text = "Z Block 2 (" + prfZBlock2.ToString + "mm)"
                Case 3
                    prfZBlock3 += nebSteps.Value
                    rbBlock3.Text = "Z Block 3 (" + prfZBlock3.ToString + "mm)"
                Case 4
                    prfZBlock4 += nebSteps.Value
                    rbBlock4.Text = "Z Block 4 (" + prfZBlock4.ToString + "mm)"
            End Select

        End If

        'CreateDrawMul()
        Redraw()

    End Sub

#End Region
#Region " Button - Jog Right "

    Private Sub btnRight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRight.Click

        Dim sngBlock As Single = GetBlock()

        If chkMk6.Checked Then

            Select Case sngBlock

                Case 1
                    prfZBlock1 += nebSteps.Value
                    rbBlock1.Text = "Z Block 1 (" + prfZBlock1.ToString + "mm)"
                Case 2
                    prfZBlock2 += nebSteps.Value
                    rbBlock2.Text = "Z Block 2 (" + prfZBlock2.ToString + "mm)"
                Case 3
                    prfZBlock3 += nebSteps.Value
                    rbBlock3.Text = "Z Block 3 (" + prfZBlock3.ToString + "mm)"
                Case 4
                    prfZBlock4 += nebSteps.Value
                    rbBlock4.Text = "Z Block 4 (" + prfZBlock4.ToString + "mm)"

            End Select

        Else

            Select Case sngBlock
                Case 1
                    prfZBlock1 -= nebSteps.Value
                    rbBlock1.Text = "Z Block 1 (" + prfZBlock1.ToString + "mm)"
                Case 2
                    prfZBlock2 -= nebSteps.Value
                    rbBlock2.Text = "Z Block 2 (" + prfZBlock2.ToString + "mm)"
                Case 3
                    prfZBlock3 -= nebSteps.Value
                    rbBlock3.Text = "Z Block 3 (" + prfZBlock3.ToString + "mm)"
                Case 4
                    prfZBlock4 -= nebSteps.Value
                    rbBlock4.Text = "Z Block 4 (" + prfZBlock4.ToString + "mm)"

            End Select

        End If

        'CreateDrawMul()
        Redraw()

    End Sub

#End Region
#Region " Button - Update Replicates "

    Private Sub btnUpdateReplicates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateReplicates.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetSelectedProfileRow()
        If sr Is Nothing Then Exit Sub

        'Get Values
        Dim sProfile As String = sr.Cells(0).Value
        Dim dZBlock As Double = sr.Cells(1).Value

        For Each r As DataRow In tblProfile.Rows
            If r.Item(12) = sProfile Then
                r.Item(1) = dZBlock
            End If
        Next

        MsgBox("Replicates Updated", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo "

    Private Sub btnUndoProfiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoProfiles.Click

        GetData()

    End Sub

#End Region
#Region " Button - Save "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        SaveProfileMUL()

        CreateDrawMul()

        MsgBox("Profiles Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Save and Exit "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        SaveProfileMUL()

        CreateDrawMul()

        'Update User Profiles List
        If frmParent.Name = "frmEditProfiles" Then
            frmParentUser = frmParent
            frmParentUser.dgvProfile.Refresh()
        End If

        'Update Admin Profiles List
        If frmParent.Name = "frmEngineer" Then
            frmParentAdmin = frmParent
            frmParentAdmin.dgvProfile.Refresh()
        End If

        Me.Close()

    End Sub

#End Region

#Region " Sub - GetData() "

    Sub GetData()

        BindProfileGrid(dgvProfiles, False, "ReplicateID = '' OR ReplicateID IS NULL")
        RemoveGridColumns()
        FillRadioDepths()

    End Sub

#End Region
#Region " Sub - InitialisePosCalc() "

    Sub InitialisePosCalc()

        With PosCalc

            'Link Controls
            .pbPosCalc = pbPosCalc

            'Set Scale
            .Scale = iScale

            'Set Centre Point
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2

            .ShowGripperLimits = False

        End With

    End Sub

#End Region
#Region " Sub - RemoveGridColumns() "

    Sub RemoveGridColumns()

        With dgvProfiles
            .Columns(0).Visible = True      ' "CodeName"
            .Columns(1).Visible = True      ' "ZSection"
            .Columns(2).Visible = False     ' "Flip"
            .Columns(3).Visible = False     ' "Width"
            .Columns(4).Visible = False     ' "GripPosition"
            .Columns(4).Width = 60
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Visible = False     ' "StdLength"
            .Columns(6).Visible = False     ' "ReverseLoad"
            .Columns(7).Visible = False     ' "ZRebate"
            .Columns(8).Visible = False     ' "Height"
            .Columns(9).Visible = False     ' "EuroHeight"
            .Columns(10).Visible = False    ' "System"
            .Columns(11).Visible = True     ' "ProfileName"
            .Columns(12).Visible = False    ' "ReplicateID"
            .Columns(13).Visible = False    ' "ProfileTypeID"
            .Columns(14).Visible = False    ' "NeuronStr"
            .Columns(15).Visible = False    ' "Centralise"
            .Columns(16).Visible = False    ' "OptTol"
            .Columns(17).Visible = False    ' "MinOffcut"
            .Columns(18).Visible = False    ' "GripY"
            .Columns(18).Width = 60
            .Columns(18).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(18).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(19).Visible = False    ' "GripZ"
            .Columns(19).Width = 60
            .Columns(19).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(19).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(20).Visible = False    ' "ID"
            .Columns(21).Visible = False    ' "CoreColour"
            .Columns(22).Visible = False    ' "SawCutStyle"
        End With

    End Sub

#End Region
#Region " Sub - Redraw() "

    Sub Redraw()

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value
        Dim dblProfilewidth As Double = CDbl(GetProfileWidth(sProfile))

        'Draw Operation
        With PosCalc
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2
            .LoadDXF(modGlobal.prfDXFLoc & sProfile & ".dxf")
            .YP = 0
            .ZP = 0
            .Angle = 0
            .Plunge = 0
            .ToolDiameter = 5
            .Scale = 3
            .bShowGrip = False
            .bShowPrep = False
            .ZBlock = GetBlock()
            .Block1 = prfZBlock1
            .Block2 = prfZBlock2
            .Block3 = prfZBlock3
            .Block4 = prfZBlock4
            .Mk6Block = chkMk6.Checked
            .ProfileWidth = dblProfilewidth
            .CreateDrawing()
        End With

    End Sub

#End Region
#Region " Sub - SelectZBlock() "

    Sub SelectZBlock()

        'Get Selected Row
        Dim sr As DataGridViewRow = GetSelectedProfileRow()
        If sr Is Nothing Then Exit Sub

        'Get Z Block
        Dim zBlock As Integer = sr.Cells(1).Value

        SetRadio(zBlock)

    End Sub

#End Region
#Region " Sub - FillReplicateList "

    Sub FillReplicateList()


        'Declare Variables

        Dim sr As DataGridViewRow = GetSelectedProfileRow()
        If sr Is Nothing Then Exit Sub

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value

        lstReplicates.Items.Clear()
        For Each r As DataRow In tblProfile.Rows
            Dim sTempProfile As String = r.Item(12).ToString
            If sTempProfile = sProfile Then lstReplicates.Items.Add(r.Item(0))
        Next

    End Sub

#End Region
#Region " Sub - SetRadio() "

    Sub SetRadio(ByVal iBlock As Integer)

        Select Case iBlock
            Case 0
                rbBlock0.Checked = True
                rbBlock1.Checked = False
                rbBlock2.Checked = False
                rbBlock3.Checked = False
                rbBlock4.Checked = False
            Case 1
                rbBlock0.Checked = False
                rbBlock1.Checked = True
                rbBlock2.Checked = False
                rbBlock3.Checked = False
                rbBlock4.Checked = False
            Case 2
                rbBlock0.Checked = False
                rbBlock1.Checked = False
                rbBlock2.Checked = True
                rbBlock3.Checked = False
                rbBlock4.Checked = False
            Case 3
                rbBlock0.Checked = False
                rbBlock1.Checked = False
                rbBlock2.Checked = False
                rbBlock3.Checked = True
                rbBlock4.Checked = False
            Case 4
                rbBlock0.Checked = False
                rbBlock1.Checked = False
                rbBlock2.Checked = False
                rbBlock3.Checked = False
                rbBlock4.Checked = True
        End Select

    End Sub

#End Region
#Region " Sub - FillRadioDepths() "

    Sub FillRadioDepths()

        rbBlock1.Text = "Z Block 1 (" + prfZBlock1.ToString + "mm)"
        rbBlock2.Text = "Z Block 2 (" + prfZBlock2.ToString + "mm)"
        rbBlock3.Text = "Z Block 3 (" + prfZBlock3.ToString + "mm)"
        rbBlock4.Text = "Z Block 4 (" + prfZBlock4.ToString + "mm)"

    End Sub

#End Region

#Region " Function - GetBlock() "

    Function GetBlock() As Integer

        GetBlock = 0

        If rbBlock0.Checked Then Return 0
        If rbBlock1.Checked Then Return 1
        If rbBlock2.Checked Then Return 2
        If rbBlock3.Checked Then Return 3
        If rbBlock4.Checked Then Return 4

    End Function

#End Region
#Region " Function - GetSelectedProfileRow() "

    Private Function GetSelectedProfileRow() As DataGridViewRow

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Return Nothing
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Return Nothing

        End If

        Return sr

    End Function

#End Region

#Region " Event - dgvProfiles_SelectionChanged() "

    Private Sub dgvProfiles_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProfiles.SelectionChanged

        FillReplicateList()
        SelectZBlock()
        Redraw()

    End Sub

#End Region
#Region " Event - rbBlock0_CheckedChanged() "

    Private Sub rbBlock0_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlock0.CheckedChanged

        If rbBlock0.Checked Then

            Dim sr As DataGridViewRow = GetSelectedProfileRow()
            If sr Is Nothing Then Exit Sub

            sr.Cells(1).Value = 0

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - rbBlock1_CheckedChanged() "

    Private Sub rbBlock1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlock1.CheckedChanged

        If rbBlock1.Checked Then

            Dim sr As DataGridViewRow = GetSelectedProfileRow()
            If sr Is Nothing Then Exit Sub

            sr.Cells(1).Value = 1

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - rbBlock2_CheckedChanged() "

    Private Sub rbBlock2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlock2.CheckedChanged

        If rbBlock2.Checked Then

            Dim sr As DataGridViewRow = GetSelectedProfileRow()
            If sr Is Nothing Then Exit Sub

            sr.Cells(1).Value = 2

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - rbBlock3_CheckedChanged() "

    Private Sub rbBlock3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlock3.CheckedChanged

        If rbBlock3.Checked Then

            Dim sr As DataGridViewRow = GetSelectedProfileRow()
            If sr Is Nothing Then Exit Sub

            sr.Cells(1).Value = 3

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - rbBlock4_CheckedChanged() "

    Private Sub rbBlock4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlock4.CheckedChanged

        If rbBlock4.Checked Then

            Dim sr As DataGridViewRow = GetSelectedProfileRow()
            If sr Is Nothing Then Exit Sub

            sr.Cells(1).Value = 4

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - chkMk6_CheckedChanged() "

    Private Sub chkMk6_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkMk6.CheckedChanged

        Redraw()

    End Sub

#End Region

End Class