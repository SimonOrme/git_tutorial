﻿Public Class frmSettings

    Private Sub frmSettings_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.txtProfileMul.Text = modGlobal.prfProfileMulLoc
        Me.txtStdOpsMul.Text = modGlobal.prfStdOpsMulLoc
        Me.txtZStdOpDefMul.Text = modGlobal.prfZStdOpDefMulLoc
        Me.txtOpTypes.Text = modGlobal.prfOpTypesLoc
        Me.txtSystems.Text = modGlobal.prfSystemsLoc
        Me.txtHardwareMul.Text = modGlobal.prfHardwareLoc
        Me.txtPrepsMul.Text = modGlobal.prfPrepsLoc

        Me.txtProfileSaw.Text = modGlobal.prfProfileSawLoc

        Me.txtOpdesign.Text = modGlobal.prfOpdesignLoc
        Me.txtBatches.Text = modGlobal.prfBatchLoc
        Me.txtDXF.Text = modGlobal.prfDXFLoc
        Me.txtDiagnose.Text = modGlobal.prfDiagnoseLoc

        Me.chkNeuron.Checked = modGlobal.prfUseNeuron

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        modGlobal.prfProfileMulLoc = Me.txtProfileMul.Text
        modGlobal.prfStdOpsMulLoc = Me.txtStdOpsMul.Text
        modGlobal.prfZStdOpDefMulLoc = Me.txtZStdOpDefMul.Text
        modGlobal.prfOpTypesLoc = Me.txtOpTypes.Text
        modGlobal.prfSystemsLoc = (Me.txtSystems.Text)
        modGlobal.prfHardwareLoc = (Me.txtHardwareMul.Text)
        modGlobal.prfPrepsLoc = (Me.txtPrepsMul.Text)

        modGlobal.prfProfileSawLoc = Me.txtProfileSaw.Text

        modGlobal.prfOpdesignLoc = Me.txtOpdesign.Text
        modGlobal.prfBatchLoc = Me.txtBatches.Text
        modGlobal.prfDXFLoc = Me.txtDXF.Text
        modGlobal.prfDiagnoseLoc = Me.txtDiagnose.Text

        modGlobal.prfUseNeuron = Me.chkNeuron.Checked

        WriteSettings()
        Me.Close()

    End Sub

    Private Sub butImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butImport.Click
        addChecksum()
        MsgBox("Data has been validated. Stuga Flowline Ops will now terminate. Data should import properly on restart")
        Application.Exit()
    End Sub

    Private Sub btnCreatePassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreatePassword.Click
        Dim frmpass As New frmUserPass
        frmpass.Show()
    End Sub
End Class