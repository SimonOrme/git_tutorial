﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditProfiles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditProfiles))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCreateProfileSaw = New System.Windows.Forms.Button()
        Me.btnSyncProfile = New System.Windows.Forms.Button()
        Me.chkShowReplicates = New System.Windows.Forms.CheckBox()
        Me.cmbType = New System.Windows.Forms.ComboBox()
        Me.lblType = New System.Windows.Forms.Label()
        Me.cmbProfSuite = New System.Windows.Forms.ComboBox()
        Me.lblSuite = New System.Windows.Forms.Label()
        Me.btnGripperSetup = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btnUndoProfile = New System.Windows.Forms.Button()
        Me.btnCloseProfile = New System.Windows.Forms.Button()
        Me.btnSaveProfile = New System.Windows.Forms.Button()
        Me.dgvProfile = New System.Windows.Forms.DataGridView()
        Me.btnZBlock = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvProfile, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.btnZBlock)
        Me.Panel1.Controls.Add(Me.btnCreateProfileSaw)
        Me.Panel1.Controls.Add(Me.btnSyncProfile)
        Me.Panel1.Controls.Add(Me.chkShowReplicates)
        Me.Panel1.Controls.Add(Me.btnGripperSetup)
        Me.Panel1.Controls.Add(Me.cmbType)
        Me.Panel1.Controls.Add(Me.lblType)
        Me.Panel1.Controls.Add(Me.cmbProfSuite)
        Me.Panel1.Controls.Add(Me.lblSuite)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 461)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1004, 123)
        Me.Panel1.TabIndex = 0
        '
        'btnCreateProfileSaw
        '
        Me.btnCreateProfileSaw.BackColor = System.Drawing.Color.White
        Me.btnCreateProfileSaw.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreateProfileSaw.Image = CType(resources.GetObject("btnCreateProfileSaw.Image"), System.Drawing.Image)
        Me.btnCreateProfileSaw.Location = New System.Drawing.Point(12, 35)
        Me.btnCreateProfileSaw.Name = "btnCreateProfileSaw"
        Me.btnCreateProfileSaw.Size = New System.Drawing.Size(151, 77)
        Me.btnCreateProfileSaw.TabIndex = 16
        Me.btnCreateProfileSaw.Text = "Create Profile.Saw"
        Me.btnCreateProfileSaw.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCreateProfileSaw.UseVisualStyleBackColor = False
        '
        'btnSyncProfile
        '
        Me.btnSyncProfile.BackColor = System.Drawing.Color.White
        Me.btnSyncProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSyncProfile.Image = CType(resources.GetObject("btnSyncProfile.Image"), System.Drawing.Image)
        Me.btnSyncProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSyncProfile.Location = New System.Drawing.Point(169, 35)
        Me.btnSyncProfile.Name = "btnSyncProfile"
        Me.btnSyncProfile.Size = New System.Drawing.Size(129, 77)
        Me.btnSyncProfile.TabIndex = 15
        Me.btnSyncProfile.Text = "Sync Z Blocks"
        Me.btnSyncProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSyncProfile.UseVisualStyleBackColor = False
        '
        'chkShowReplicates
        '
        Me.chkShowReplicates.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkShowReplicates.Checked = True
        Me.chkShowReplicates.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowReplicates.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReplicates.ForeColor = System.Drawing.Color.White
        Me.chkShowReplicates.Location = New System.Drawing.Point(484, 0)
        Me.chkShowReplicates.Name = "chkShowReplicates"
        Me.chkShowReplicates.Size = New System.Drawing.Size(108, 36)
        Me.chkShowReplicates.TabIndex = 14
        Me.chkShowReplicates.Text = "Show Replicates"
        Me.chkShowReplicates.UseVisualStyleBackColor = True
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(326, 5)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(152, 24)
        Me.cmbType.TabIndex = 13
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.ForeColor = System.Drawing.Color.White
        Me.lblType.Location = New System.Drawing.Point(282, 8)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(38, 16)
        Me.lblType.TabIndex = 12
        Me.lblType.Text = "Type"
        '
        'cmbProfSuite
        '
        Me.cmbProfSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProfSuite.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProfSuite.FormattingEnabled = True
        Me.cmbProfSuite.Location = New System.Drawing.Point(59, 5)
        Me.cmbProfSuite.Name = "cmbProfSuite"
        Me.cmbProfSuite.Size = New System.Drawing.Size(210, 24)
        Me.cmbProfSuite.TabIndex = 11
        '
        'lblSuite
        '
        Me.lblSuite.AutoSize = True
        Me.lblSuite.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuite.ForeColor = System.Drawing.Color.White
        Me.lblSuite.Location = New System.Drawing.Point(12, 8)
        Me.lblSuite.Name = "lblSuite"
        Me.lblSuite.Size = New System.Drawing.Size(41, 16)
        Me.lblSuite.TabIndex = 10
        Me.lblSuite.Text = "Suite"
        '
        'btnGripperSetup
        '
        Me.btnGripperSetup.BackColor = System.Drawing.Color.White
        Me.btnGripperSetup.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGripperSetup.Image = CType(resources.GetObject("btnGripperSetup.Image"), System.Drawing.Image)
        Me.btnGripperSetup.Location = New System.Drawing.Point(304, 34)
        Me.btnGripperSetup.Name = "btnGripperSetup"
        Me.btnGripperSetup.Size = New System.Drawing.Size(129, 77)
        Me.btnGripperSetup.TabIndex = 9
        Me.btnGripperSetup.Text = "Edit Gripper"
        Me.btnGripperSetup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGripperSetup.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnUndoProfile)
        Me.Panel4.Controls.Add(Me.btnCloseProfile)
        Me.Panel4.Controls.Add(Me.btnSaveProfile)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel4.Location = New System.Drawing.Point(595, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(409, 123)
        Me.Panel4.TabIndex = 3
        '
        'btnUndoProfile
        '
        Me.btnUndoProfile.BackColor = System.Drawing.Color.White
        Me.btnUndoProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoProfile.Image = CType(resources.GetObject("btnUndoProfile.Image"), System.Drawing.Image)
        Me.btnUndoProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoProfile.Location = New System.Drawing.Point(138, 35)
        Me.btnUndoProfile.Name = "btnUndoProfile"
        Me.btnUndoProfile.Size = New System.Drawing.Size(129, 77)
        Me.btnUndoProfile.TabIndex = 3
        Me.btnUndoProfile.Text = "Cancel Change"
        Me.btnUndoProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoProfile.UseVisualStyleBackColor = False
        '
        'btnCloseProfile
        '
        Me.btnCloseProfile.BackColor = System.Drawing.Color.White
        Me.btnCloseProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseProfile.Image = CType(resources.GetObject("btnCloseProfile.Image"), System.Drawing.Image)
        Me.btnCloseProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseProfile.Location = New System.Drawing.Point(273, 35)
        Me.btnCloseProfile.Name = "btnCloseProfile"
        Me.btnCloseProfile.Size = New System.Drawing.Size(129, 77)
        Me.btnCloseProfile.TabIndex = 2
        Me.btnCloseProfile.Text = "Save and Exit"
        Me.btnCloseProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseProfile.UseVisualStyleBackColor = False
        '
        'btnSaveProfile
        '
        Me.btnSaveProfile.BackColor = System.Drawing.Color.White
        Me.btnSaveProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveProfile.Image = CType(resources.GetObject("btnSaveProfile.Image"), System.Drawing.Image)
        Me.btnSaveProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveProfile.Location = New System.Drawing.Point(3, 35)
        Me.btnSaveProfile.Name = "btnSaveProfile"
        Me.btnSaveProfile.Size = New System.Drawing.Size(129, 77)
        Me.btnSaveProfile.TabIndex = 1
        Me.btnSaveProfile.Text = "Save Profile"
        Me.btnSaveProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveProfile.UseVisualStyleBackColor = False
        '
        'dgvProfile
        '
        Me.dgvProfile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProfile.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProfile.Location = New System.Drawing.Point(0, 0)
        Me.dgvProfile.Name = "dgvProfile"
        Me.dgvProfile.Size = New System.Drawing.Size(1004, 461)
        Me.dgvProfile.TabIndex = 1
        '
        'btnZBlock
        '
        Me.btnZBlock.BackColor = System.Drawing.Color.White
        Me.btnZBlock.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnZBlock.Location = New System.Drawing.Point(439, 35)
        Me.btnZBlock.Name = "btnZBlock"
        Me.btnZBlock.Size = New System.Drawing.Size(129, 77)
        Me.btnZBlock.TabIndex = 17
        Me.btnZBlock.Text = "Edit Z Blocks"
        Me.btnZBlock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnZBlock.UseVisualStyleBackColor = False
        '
        'frmEditProfiles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 584)
        Me.Controls.Add(Me.dgvProfile)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEditProfiles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Profiles"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.dgvProfile, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvProfile As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoProfile As System.Windows.Forms.Button
    Friend WithEvents btnCloseProfile As System.Windows.Forms.Button
    Friend WithEvents btnSaveProfile As System.Windows.Forms.Button
    Friend WithEvents btnGripperSetup As System.Windows.Forms.Button
    Friend WithEvents chkShowReplicates As System.Windows.Forms.CheckBox
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents cmbProfSuite As System.Windows.Forms.ComboBox
    Friend WithEvents lblSuite As System.Windows.Forms.Label
    Friend WithEvents btnSyncProfile As System.Windows.Forms.Button
    Friend WithEvents btnCreateProfileSaw As System.Windows.Forms.Button
    Friend WithEvents btnZBlock As System.Windows.Forms.Button
End Class
