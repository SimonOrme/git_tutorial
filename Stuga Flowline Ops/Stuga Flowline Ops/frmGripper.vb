﻿Public Class frmGripper

#Region " Variable Declaration "

    Private PosCalc As New clsPosCalc
    Public frmParent As Form
    Public frmParentUser As frmEditProfiles
    Public frmParentAdmin As frmEngineer

    Dim MouseX As Short
    Dim MouseY As Short
    Dim bInitialising As Boolean = True
    Dim bShowGripperLimits As Boolean = False

    Dim bRadioChanging As Boolean = False

#End Region

#Region " Form - Load "

    Private Sub frmGripper_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        bInitialising = False
        nebGripperHeight.Value = prfPushGripperHeight

        'Setup Poscalc Class
        InitialisePosCalc()

        'Get Profile Data
        GetData()

        lblPullGZ.Text = "PullGZ=" + prfPullGripperZPos.ToString

    End Sub

#End Region

#Region " Button - Up "

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value

        If rbPull.Checked Then
            prfPullGripperZPos += 1
            If prfPullGripperZPos > prfPullGripperMaxZ Then prfPullGripperZPos = prfPullGripperMaxZ
            If prfPullGripperZPos < prfPullGripperMinZ Then prfPullGripperZPos = prfPullGripperMinZ
            lblPullGZ.Text = "PullGZ=" + prfPullGripperZPos.ToString

        Else
            'Increment Gripper Z Value
            Dim MinMaxZ As Integer = sr.Cells(19).Value + nebSteps.Value
            If MinMaxZ < prfPushGripperMinZ Then MinMaxZ = prfPushGripperMinZ
            If MinMaxZ > prfPushGripperMaxZ Then MinMaxZ = prfPushGripperMaxZ
            sr.Cells(19).Value = MinMaxZ
        End If

        'Redraw Palette
        Redraw()

    End Sub

#End Region
#Region " Button - Right "

    Private Sub btnRight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRight.Click

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value

        'Decrement Gripper Y Value
        If rbPush.Checked Then
            Dim MinMaxY As Integer = sr.Cells(18).Value - nebSteps.Value
            If MinMaxY < prfPushGripperMinY Then MinMaxY = prfPushGripperMinY
            If MinMaxY > prfPushGripperMaxY Then MinMaxY = prfPushGripperMaxY
            sr.Cells(18).Value = MinMaxY
        ElseIf rbMk6Wheel.Checked Then
            prfWheelY -= nebSteps.Value
            lblPullGZ.Text = "WheelY=" + prfWheelY.ToString
        Else
            Dim MinMaxY As Integer = sr.Cells(4).Value - nebSteps.Value
            If MinMaxY < prfPullGripperMinY Then MinMaxY = prfPullGripperMinY
            If MinMaxY > prfPullGripperMaxY Then MinMaxY = prfPullGripperMaxY
            sr.Cells(4).Value = MinMaxY
        End If

        Redraw()

    End Sub

#End Region
#Region " Button - Down "

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value


        If rbPull.Checked Then
            prfPullGripperZPos -= 1
            If prfPullGripperZPos > prfPullGripperMaxZ Then prfPullGripperZPos = prfPullGripperMaxZ
            If prfPullGripperZPos < prfPullGripperMinZ Then prfPullGripperZPos = prfPullGripperMinZ
            lblPullGZ.Text = "PullGZ=" + prfPullGripperZPos.ToString

        Else
            'Decrement Gripper Z Value
            Dim MinMaxZ As Integer = sr.Cells(19).Value - nebSteps.Value
            If MinMaxZ < prfPushGripperMinZ Then MinMaxZ = prfPushGripperMinZ
            If MinMaxZ > prfPushGripperMaxZ Then MinMaxZ = prfPushGripperMaxZ
            sr.Cells(19).Value = MinMaxZ

        End If
 
        'redraw palette
        Redraw()

    End Sub

#End Region
#Region " Button - Left "

    Private Sub btnLeft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeft.Click

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value

        'Increment Gripper Y Value
        If rbPush.Checked Then
            Dim MinMaxY As Integer = sr.Cells(18).Value + nebSteps.Value
            If MinMaxY < prfPushGripperMinY Then MinMaxY = prfPushGripperMinY
            If MinMaxY > prfPushGripperMaxY Then MinMaxY = prfPushGripperMaxY
            sr.Cells(18).Value = MinMaxY
        ElseIf rbMk6Wheel.Checked Then
            prfWheelY += nebSteps.Value
            lblPullGZ.Text = "WheelY=" + prfWheelY.ToString
        Else
            Dim MinMaxY As Integer = sr.Cells(4).Value + nebSteps.Value
            If MinMaxY < prfPullGripperMinY Then MinMaxY = prfPullGripperMinY
            If MinMaxY > prfPullGripperMaxY Then MinMaxY = prfPullGripperMaxY
            sr.Cells(4).Value = MinMaxY
        End If
        Redraw()

    End Sub

#End Region
#Region " Button - Update Replicates "

    Private Sub btnUpdateReplicates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateReplicates.Click

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Values
        Dim sProfile As String = sr.Cells(0).Value
        Dim dOldY As Double = sr.Cells(4).Value
        Dim dGripY As Double = sr.Cells(18).Value
        Dim dGripZ As Double = sr.Cells(19).Value

        For Each r As DataRow In tblProfile.Rows
            If r.Item(12) = sProfile Then
                r.Item(4) = dOldY
                r.Item(18) = dGripY
                r.Item(19) = dGripZ
            End If
        Next

        MsgBox("Replicates Updated", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Save Profiles "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        SaveProfileMUL()

        CreateDrawMul()

        MsgBox("Profiles Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo Profiles "

    Private Sub btnUndoProfiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoProfiles.Click

        'Get Profile Data from Profile.Mul
        tblProfile = GetProfileTable()
        dsProfile.Tables.Clear()
        dsProfile.Tables.Add(tblProfile)

        BindProfileGrid(dgvProfiles, False, "ReplicateID = ''")
        RemoveGridColumns()

        Redraw()

    End Sub

#End Region
#Region " Button - Save and Exit "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        SaveProfileMUL()

        CreateDrawMul()

        'Update User Profiles List
        If frmParent.Name = "frmEditProfiles" Then
            frmParentUser = frmParent
            frmParentUser.dgvProfile.Refresh()
        End If

        'Update Admin Profiles List
        If frmParent.Name = "frmEngineer" Then
            frmParentAdmin = frmParent
            frmParentAdmin.dgvProfile.Refresh()
        End If

        Me.Close()

    End Sub

#End Region

#Region " Sub - GetData() "

    Sub GetData()

        BindProfileGrid(dgvProfiles, False, "ReplicateID = '' OR ReplicateID IS NULL")
        RemoveGridColumns()

    End Sub

#End Region
#Region " Sub - InitialisePosCalc() "

    Sub InitialisePosCalc()

        With PosCalc

            'Link Controls
            .pbPosCalc = pbPosCalc

            'Set Scale
            .Scale = iScale

            'Set Centre Point
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2

            .ShowGripperLimits = False

        End With

    End Sub

#End Region
#Region " Sub - RemoveGridColumns() "

    Sub RemoveGridColumns()

        With dgvProfiles
            .Columns(0).Visible = True      ' "CodeName"
            .Columns(1).Visible = False     ' "ZSection"
            .Columns(2).Visible = False     ' "Flip"
            .Columns(3).Visible = False     ' "Width"
            .Columns(4).Visible = True      ' "GripPosition"
            .Columns(4).Width = 60
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Visible = False     ' "StdLength"
            .Columns(6).Visible = False     ' "ReverseLoad"
            .Columns(7).Visible = False     ' "ZRebate"
            .Columns(8).Visible = False     ' "Height"
            .Columns(9).Visible = False     ' "EuroHeight"
            .Columns(10).Visible = False    ' "System"
            .Columns(11).Visible = False    ' "ProfileName"
            .Columns(12).Visible = False    ' "ReplicateID"
            .Columns(13).Visible = False    ' "ProfileTypeID"
            .Columns(14).Visible = False    ' "NeuronStr"
            .Columns(15).Visible = False    ' "Centralise"
            .Columns(16).Visible = False    ' "OptTol"
            .Columns(17).Visible = False    ' "MinOffcut"
            .Columns(18).Visible = True     ' "GripY"
            .Columns(18).Width = 60
            .Columns(18).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(18).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(19).Visible = True     ' "GripZ"
            .Columns(19).Width = 60
            .Columns(19).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(19).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(20).Visible = False    ' "ID"
            .Columns(21).Visible = False    ' "CoreColour"
            .Columns(22).Visible = False    ' "SawCutStyle"
        End With

    End Sub

#End Region
#Region " Sub - Redraw() "

    Sub Redraw()

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value
        Dim dblProfilewidth As Double = CDbl(GetProfileWidth(sProfile))
        Dim dblProfileHeight As Double = CDbl(GetProfileHeight(sProfile))

        'Draw Operation
        With PosCalc
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2
            .LoadDXF(modGlobal.prfDXFLoc & sProfile & ".dxf")
            .YP = 0
            .ZP = 0
            .Angle = 0
            .Plunge = 0
            .ToolDiameter = 5
            .Scale = 3
            If rbPush.Checked Then
                .dblGY = GetPushGripperY(sProfile)
                .dblGZ = GetPushGripperZ(sProfile)
                .GripperType = 2
                .PushGripperHeight = prfPushGripperHeight
                .PushGripperWidth = prfPushGripperWidth
                .ShowGripperLimits = bShowGripperLimits
                .MinGY = prfPushGripperMinY
                .MaxGY = prfPushGripperMaxY
                .MinGZ = prfPushGripperMinZ
                .MaxGZ = prfPushGripperMaxZ
                .PushGripperRotate = chkRotate.Checked
                .Mk6Wheel = False

            ElseIf rbPull.Checked Then
                .dblGY = GetPullGripperY(sProfile)
                .dblGZ = 0
                .GripperType = 1
                .PullGripperHeight = prfPullGripperHeight
                .PullGripperZPos = prfPullGripperZPos
                .ShowGripperLimits = bShowGripperLimits
                .MinGY = prfPullGripperMinY
                .MaxGY = prfPullGripperMaxY
                .MinGZ = prfPullGripperMinZ
                .MaxGZ = prfPullGripperMaxZ
                .Mk6Wheel = False
                ' lblPullGZ.Text = "PullGZ=" + prfPullGripperZPos.ToString
                'lblPullGZ.Visible = True

            ElseIf rbMk6.Checked Then

                .GripperType = 3
                .Mk6Wheel = False
                ' lblPullGZ.Visible = False
            Else
                .GripperType = 0
                .WheelY = prfWheelY
                .Mk6Wheel = True
                .ProfileHeight = dblProfileHeight
                .Mk6Wheel3 = chkWheel3.Checked
            End If
            .bShowGrip = True
            .bShowPrep = False
            .CreateDrawing()
        End With

    End Sub

#End Region
#Region " Sub - FillReplicateList "

    Sub FillReplicateList()


        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvProfiles.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get Profile Code
        Dim sProfile As String = sr.Cells(0).Value

        lstReplicates.Items.Clear()
        For Each r As DataRow In tblProfile.Rows
            Dim sTempProfile As String = r.Item(12).ToString
            If sTempProfile = sProfile Then lstReplicates.Items.Add(r.Item(0))
        Next

    End Sub

#End Region
#Region " Sub - RefreshRadio()- Apply Gripper Type Changes "

    Sub RefreshRadio()

        If bRadioChanging Then Exit Sub
        bRadioChanging = True


        grpJog.Visible = Not rbMk6.Checked
        grpSteps.Visible = Not rbMk6.Checked

        If rbPush.Checked Then

            nebGripperHeight.Value = prfPushGripperHeight

            chkRotate.Visible = True
            lblPullGZ.Visible = False
            grpGripperHeight.Visible = True
            grpJog.Visible = True
            grpSteps.Visible = True
            btnDown.Visible = True
            btnUp.Visible = True
            chkWheel3.Visible = False

        ElseIf rbPull.Checked Then

            nebGripperHeight.Value = prfPullGripperHeight
            lblPullGZ.Text = "PullGZ=" + prfPullGripperZPos.ToString

            chkRotate.Visible = False
            lblPullGZ.Visible = True
            grpGripperHeight.Visible = True
            grpJog.Visible = True
            grpSteps.Visible = True
            btnDown.Visible = True
            btnUp.Visible = True
            chkWheel3.Visible = False

        ElseIf rbMk6.Checked Then

            chkRotate.Visible = False
            lblPullGZ.Visible = False
            grpGripperHeight.Visible = False
            grpJog.Visible = False
            grpSteps.Visible = False
            btnDown.Visible = True
            btnUp.Visible = True
            chkWheel3.Visible = False

        Else

            lblPullGZ.Text = "WheelY=" + prfWheelY.ToString

            chkRotate.Visible = False
            lblPullGZ.Visible = True
            grpGripperHeight.Visible = False
            grpJog.Visible = True
            grpSteps.Visible = True
            btnDown.Visible = False
            btnUp.Visible = False
            chkWheel3.Visible = True

        End If

        Redraw()

        bRadioChanging = False

    End Sub

#End Region

#Region " Event - dgvProfiles_SelectionChanged() "

    Private Sub dgvProfiles_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProfiles.SelectionChanged

        FillReplicateList()
        Redraw()

    End Sub

#End Region
#Region " Event - rbPush_CheckedChanged() - Change Gripper Type "

    Private Sub rbPush_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPush.CheckedChanged

        If rbPush.Checked Then

            grpGripType.Text = "Gripper Type: Push"
            RefreshRadio()

        End If

    End Sub

#End Region
#Region " Event - rbPull_CheckedChanged() "

    Private Sub rbPull_CheckedChanged(sender As Object, e As System.EventArgs) Handles rbPull.CheckedChanged

        If rbPull.Checked Then

            grpGripType.Text = "Gripper Type: Pull"
            RefreshRadio()

        End If

    End Sub

#End Region
#Region " Event - rbMk6_CheckedChanged() "

    Private Sub rbMk6_CheckedChanged(sender As Object, e As System.EventArgs) Handles rbMk6.CheckedChanged

        If rbMk6.Checked Then

            grpGripType.Text = "Gripper Type: Mk6 Gripper"
            RefreshRadio()

        End If

    End Sub

#End Region
#Region " Event - nebGripperHeight_ValueChanged() "

    Private Sub nebGripperHeight_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebGripperHeight.ValueChanged

        If Not bInitialising Then

            If rbPush.Checked Then
                prfPushGripperHeight = nebGripperHeight.Value
            Else
                prfPullGripperHeight = nebGripperHeight.Value
            End If

            Redraw()

        End If

    End Sub

#End Region
#Region " Event - pbPosCalc_MouseMove() - Drag Gripper"

    Private Sub pbPosCalc_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pbPosCalc.MouseMove

        'Declare Variables
        Dim sr As DataGridViewRow
        Dim Button As Short = e.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = e.X
        Dim Y As Single = e.Y

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvProfiles.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvProfiles.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then
            'Row Selected
            sr = dgvProfiles.SelectedRows(0)
        Else
            'Cell Selected
            Dim ri As Integer = dgvProfiles.SelectedCells(0).RowIndex
            sr = dgvProfiles.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub
        End If

        If Button = 1 Then

            If Me.rbPush.Checked Then

                'Get Gripper Y Pos
                Dim MinMaxY As Integer = (-(X - MouseX)) / PosCalc.Scale
                If MinMaxY < prfPushGripperMinY Then MinMaxY = prfPushGripperMinY
                If MinMaxY > prfPushGripperMaxY Then MinMaxY = prfPushGripperMaxY
                PosCalc.dblGY = MinMaxY

                'Get Gripper Z Pos
                Dim MinMaxZ As Integer = (-(Y - MouseY)) / PosCalc.Scale
                If MinMaxZ < prfPushGripperMinZ Then MinMaxZ = prfPushGripperMinZ
                If MinMaxZ > prfPushGripperMaxZ Then MinMaxZ = prfPushGripperMaxZ
                PosCalc.dblGZ = MinMaxZ

                sr.Cells(18).Value = PosCalc.dblGY
                sr.Cells(19).Value = PosCalc.dblGZ

                PosCalc.ShowGripperLimits = bShowGripperLimits
                PosCalc.bShowGrip = True
                PosCalc.CreateDrawing()

            Else

                Dim MinMaxY As Integer = (-(X - MouseX)) / PosCalc.Scale
                If MinMaxY < prfPullGripperMinY Then MinMaxY = prfPullGripperMinY
                If MinMaxY > prfPullGripperMaxY Then MinMaxY = prfPullGripperMaxY
                PosCalc.dblGY = MinMaxY

                PosCalc.dblGZ = 0

                sr.Cells(4).Value = PosCalc.dblGY

                PosCalc.ShowGripperLimits = bShowGripperLimits
                PosCalc.bShowGrip = True
                PosCalc.CreateDrawing()

            End If

        Else

            'If rbPush.Checked Then
            MouseX = X + (PosCalc.dblGY * PosCalc.Scale)
            MouseY = Y + (PosCalc.dblGZ * PosCalc.Scale)
            'End If

            lblMessage.Text = "X=" + MouseX.ToString + ", Y=" + MouseY.ToString

        End If

    End Sub

#End Region
#Region " Event - pbPosCalc_MouseUp() "

    Private Sub pbPosCalc_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pbPosCalc.MouseUp

        bShowGripperLimits = False
        PosCalc.ShowGripperLimits = bShowGripperLimits
        PosCalc.bShowGrip = True
        PosCalc.CreateDrawing()

    End Sub

#End Region
#Region " Event - pbPosCalc_MouseDown() "

    Private Sub pbPosCalc_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pbPosCalc.MouseDown

        bShowGripperLimits = True
        PosCalc.ShowGripperLimits = bShowGripperLimits
        PosCalc.bShowGrip = True
        PosCalc.CreateDrawing()

    End Sub

#End Region
#Region " Event - chkRotate_CheckedChanged() "

    Private Sub chkRotate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRotate.CheckedChanged

        Redraw()

    End Sub

#End Region

    Private Sub rbMk6Wheel_CheckedChanged(sender As Object, e As System.EventArgs) Handles rbMk6Wheel.CheckedChanged

        If rbMk6Wheel.Checked Then

            grpGripType.Text = "Gripper Type: Mk6 Roller"
            RefreshRadio()

        End If

    End Sub

    Private Sub chkWheel3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkWheel3.CheckedChanged

        Redraw()

    End Sub

End Class