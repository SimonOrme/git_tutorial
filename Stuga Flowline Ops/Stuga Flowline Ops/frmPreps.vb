﻿Public Class frmPreps

    Public HardwareCode As String = ""
    Public dPos As Double = 0

#Region " Form - Load "

    Private Sub frmPreps_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        BindPrepsGrid(dgvPreps, HardwareCode)

    End Sub

#End Region

#Region " Event - dgvPreps_CellEnter() "

    Private Sub dgvPreps_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPreps.CellEnter

        Try
            dgvPreps.Rows(e.RowIndex).Cells(1).Value = txtHardwareCode.Text
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region " Button - Create Prep "

    Private Sub btnCreateMND_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateMND.Click

        'Save Preps.mul
        SavePreps()

        'Create MND File
        CreatePreps(False)

        'Create Tilde File
        If rbDouble.Checked Then CreatePreps(True)

        'Add ZStdOpDef Entry
        If rbLock.Checked Then
            AddStdOp(txtHardwareCode.Text, txtName.Text, 5, "Dir", "Yhan", "Zhan", "Ygr", "Zgr", "", "", "")
        Else
            AddStdOp(txtHardwareCode.Text, txtName.Text, 5, "Dir", "Yhan", "Zhan", "", "", "", "", "")
        End If

        'Close Form
        Me.Close()

    End Sub

#End Region
#Region " Button - Save "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        SavePreps()

        MsgBox("Preps Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo "

    Private Sub btnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndo.Click

        'Get Profile Data from Preps.Mul
        tblPreps = GetPrepsTable()
        dsPreps.Tables.Clear()
        dsPreps.Tables.Add(tblPreps)

        BindPrepsGrid(dgvPreps, txtHardwareCode.Text)

    End Sub

#End Region
#Region " Button - Exit "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        SavePreps()
        Me.Close()

    End Sub

#End Region

#Region " Sub - CreatePreps() "

    Sub CreatePreps(ByVal bTilde As Boolean)

        'Initialise Position
        dPos = 0

        'Get File Name
        Dim sLoc As String = prfOpdesignLoc
        If Mid(sLoc, Len(sLoc)) <> "\" Then sLoc += "\"
        If bTilde Then
            sLoc += "~" + Mid(txtHardwareCode.Text, 2) + ".mnd"
        Else
            sLoc += txtHardwareCode.Text + ".mnd"
        End If

        'Try to Create File
        Try
            FileOpen(1, sLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Create Hardware", MsgBoxStyle.Exclamation)
            Exit Sub
        End Try

        'Send Header
        Print(1, ": " + txtName.Text + " sash prep" + vbCrLf)
        Print(1, ";(c) Stuga 2017" + vbCrLf)
        Print(1, ";Created with Flowline Ops.NET" + vbCrLf)
        Print(1, ";Ver 3.0 with Variable Depths" + vbCrLf)
        Print(1, ";" & Format(Now(), "dd MMMM yyyy") + vbCrLf)
        Print(1, "" + vbCrLf)

        Print(1, ";DESC1=Handle Top" + vbCrLf)
        Print(1, ";YP1=V2" + vbCrLf)
        Print(1, ";ZP1=V3" + vbCrLf)
        Print(1, ";ANG1=0" + vbCrLf)
        Print(1, ";PL1=" + CStr(nebTopPlunge.Value) + vbCrLf)
        Print(1, ";TL1=15" + vbCrLf)
        Print(1, ";BW1=15" + vbCrLf)
        Print(1, ";BH1=15" + vbCrLf)
        Print(1, "" + vbCrLf)

        If rbLock.Checked Then
            Print(1, ";DESC2=GBox" + vbCrLf)
            Print(1, ";YP2=V4" + vbCrLf)
            Print(1, ";ZP2=V5" + vbCrLf)
            Print(1, ";ANG2=270" + vbCrLf)
            Print(1, ";PL2=25" + vbCrLf)
            Print(1, ";TL2=16" + vbCrLf)
            Print(1, ";BW2=0" + vbCrLf)
            Print(1, ";BH2=16" + vbCrLf)
        Else
            Print(1, ";DESC2=Handle Btm" + vbCrLf)
            Print(1, ";YP2=V2" + vbCrLf)
            Print(1, ";ZP2=23" + vbCrLf)
            Print(1, ";ANG2=180" + vbCrLf)
            Print(1, ";PL2=" + CStr(nebBtmPlunge.Value) + vbCrLf)
            Print(1, ";TL2=15" + vbCrLf)
            Print(1, ";BW2=15" + vbCrLf)
            Print(1, ";BH2=15" + vbCrLf)
        End If

        Print(1, "" + vbCrLf)

        'Send Variables
        Print(1, "?Direction,dir,F" + vbCrLf)
        Print(1, "?Y Handle c/l,yh,F" + vbCrLf)
        Print(1, "?Z Handle top,zh,F" + vbCrLf)
        If rbLock.Checked Then
            Print(1, "?Y Gearbox pos,yg,F" + vbCrLf)
            Print(1, "?Z Gearbox c/l,zg,F" + vbCrLf)
        End If

        Print(1, "" + vbCrLf)
        Print(1, ";=========================================================" + vbCrLf)
        Print(1, "sp=TRAVERSE" + vbCrLf)
        Print(1, "ma.y=[yh]" + vbCrLf)

        'Alter Z Position by 40 on Double Plunge (added but not tilde files)
        If rbDouble.Checked And bTilde = False Then
            Print(1, "ma.z=([zh]+40)" + vbCrLf)
        Else
            Print(1, "ma.z=[zh]" + vbCrLf)
        End If

        Print(1, "go" + vbCrLf)

        If rbDouble.Checked And Not bTilde Then
            DPHandle()
        Else
            SPHandle()
        End If

        If rbLock.Checked Then

            Print(1, ";=========================================================" + vbCrLf)
            Print(1, ";Rotate to the side for the gearboxes" + vbCrLf)
            Print(1, "sp=TRAVERSE" + vbCrLf)
            Print(1, "ma.y=[yg]" + vbCrLf)
            Print(1, "ma.z=[zg]" + vbCrLf)

            If rbGB10.Checked Then
                Print(1, "tl.2=270" + vbCrLf)
            End If
            If rbGB12.Checked Then
                Print(1, "tl.4=270" + vbCrLf)
            End If
            If rbGB16.Checked Then
                Print(1, "tl.7=270" + vbCrLf)
            End If
            Print(1, "go" + vbCrLf)
            Print(1, "" + vbCrLf)

            For iLoop As Integer = 0 To dgvPreps.Rows.Count - 2

                If dgvPreps.Rows(iLoop).Cells(3).Value = "dr_gbox" Then

                    'Get Variables for this row
                    Dim sRowDesc As String = dgvPreps.Rows(iLoop).Cells(2).Value
                    Dim dRowPos As Double = dgvPreps.Rows(iLoop).Cells(4).Value
                    Dim dRowDim1 As Double = dgvPreps.Rows(iLoop).Cells(6).Value
                    Dim dRowDim2 As Double = dgvPreps.Rows(iLoop).Cells(7).Value
                    Dim dRowYOff As Double = dgvPreps.Rows(iLoop).Cells(8).Value

                    'Calculate X Movement
                    Dim dMove As Double = dRowPos - dPos
                    dPos = dRowPos

                    ' write the approach
                    Print(1, ";- - - - - - - - - - - - - - - - - - - - - - - - - - -" + vbCrLf)
                    Print(1, ";Move to " + sRowDesc + vbCrLf)
                    Print(1, "mr.x=[dir]*" + dMove.ToString + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge in
                    Print(1, ";Cut gearbox slot " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=PIERCE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    Dim sPlungeText As String = "pl=25"
                    If rbGB10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rbGB12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    If rbGB16.Checked Then
                        sPlungeText += "+tool7SPDOffset"
                    End If
                    sPlungeText += vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Cut Slot
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.z=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.z=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=" + dRowDim1.ToString + "-cdia" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.z=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.z=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge Out
                    Print(1, "sp=TRAVERSE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    sPlungeText = "pl=-(25"
                    If rbGB10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rbGB12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    If rbGB16.Checked Then
                        sPlungeText += "+tool7SPDOffset"
                    End If
                    sPlungeText += ")" + vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                End If

            Next

            'Retract Tool
            If rbGB10.Checked Then
                Print(1, "tl.2=off" + vbCrLf)
            End If
            If rbGB12.Checked Then
                Print(1, "tl.4=off" + vbCrLf)
            End If
            If rbGB16.Checked Then
                Print(1, "tl.7=off" + vbCrLf)
            End If
            Print(1, "go" + vbCrLf)

        End If

        'Write the end bits
        Print(1, "#" + vbCrLf)
        Print(1, "" + vbCrLf)

        'Close File
        FileClose(1)

    End Sub

#End Region
#Region " Sub - DPHandle() "

    Sub DPHandle()

        For iLoop As Integer = 0 To dgvPreps.Rows.Count - 2
            If dgvPreps.Rows(iLoop).Cells(3).Value = "dr_slot" Then

                'Get Variables for this row
                Dim sRowDesc As String = dgvPreps.Rows(iLoop).Cells(2).Value
                Dim dRowPos As Double = dgvPreps.Rows(iLoop).Cells(4).Value
                Dim dRowDim1 As Double = dgvPreps.Rows(iLoop).Cells(6).Value
                Dim dRowDim2 As Double = dgvPreps.Rows(iLoop).Cells(7).Value
                Dim dRowYOff As Double = dgvPreps.Rows(iLoop).Cells(8).Value

                'Calculate X Movement
                Dim dMove As Double = dRowPos - dPos
                dPos = dRowPos

                'Move to Operation
                Print(1, ";- - - - - - - - - - - - - - - - - - - - - - - - - - -" + vbCrLf)
                Print(1, ";Move to " + sRowDesc + vbCrLf)
                Print(1, "mr.x=[dir]*" + dMove.ToString + vbCrLf)
                Print(1, "ma.y=[yh]" + Format(dRowYOff, "+0;-0") + vbCrLf)
                Print(1, "go" + vbCrLf)
                If rb10.Checked Then
                    Print(1, "dp.2=0" + vbCrLf)
                Else
                    Print(1, "dp.4=0" + vbCrLf)
                End If
                Print(1, "" + vbCrLf)

                'Rout Slot
                Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                Print(1, "sp=CUT" + vbCrLf)
                Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "" + vbCrLf)

                ' write the retract
                If rb10.Checked Then
                    Print(1, "dp.2=half" + vbCrLf)
                Else
                    Print(1, "dp.4=half" + vbCrLf)
                End If
                Print(1, "sp=TRAVERSE" + vbCrLf)
                Print(1, "" + vbCrLf)

            End If

        Next

        If rb10.Checked Then
            Print(1, "dp.2=off" + vbCrLf)
        Else
            Print(1, "dp.4=off" + vbCrLf)
        End If
        Print(1, "go" + vbCrLf)
        Print(1, "" + vbCrLf)

    End Sub

#End Region
#Region " Sub - SPHandle() "

    Sub SPHandle()

        'MsgBox(dgvPreps.Rows.Count.ToString)

        Print(1, "tl.2=0" + vbCrLf)
        Print(1, "go" + vbCrLf)
        Print(1, "" + vbCrLf)

        For iLoop As Integer = 0 To dgvPreps.Rows.Count - 2

            If dgvPreps.Rows(iLoop).Cells(3).Value = "dr_slot" Then

                'Get Variables for this row
                Dim sRowDesc As String = dgvPreps.Rows(iLoop).Cells(2).Value
                Dim dRowPos As Double = dgvPreps.Rows(iLoop).Cells(4).Value
                Dim dRowDim1 As Double = dgvPreps.Rows(iLoop).Cells(6).Value
                Dim dRowDim2 As Double = dgvPreps.Rows(iLoop).Cells(7).Value
                Dim dRowYOff As Double = dgvPreps.Rows(iLoop).Cells(8).Value

                'Calculate X Movement
                Dim dMove As Double = dRowPos - dPos
                dPos = dRowPos

                'Move to Position
                Print(1, ";- - - - - - - - - - - - - - - - - - - - - - - - - - -" + vbCrLf)
                Print(1, ";Move to " + sRowDesc + vbCrLf)
                Print(1, "mr.x=[dir]*" + dMove.ToString + vbCrLf)
                Print(1, "ma.y=[yh]" & Format(dRowYOff, "+0;-0") + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "" + vbCrLf)

                'Check if Deep Plunge Required
                If chkDeepTop.Checked = False Then

                    'Plunge In
                    Print(1, "sp=PIERCE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    Dim sPlungeText As String = "pl=" + CStr(nebTopPlunge.Value)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge Out
                    Print(1, "sp=TRAVERSE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    sPlungeText = "pl=-(" + CStr(nebTopPlunge.Value)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += ")" + vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                Else

                    'Plunge In Half
                    Print(1, "sp=PIERCE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    Dim sPlungeText As String = "pl=" + CStr(nebTopPlunge.Value / 2)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge In Full
                    Print(1, "sp=PIERCE" + vbCrLf)
                    Print(1, "pl=" + CStr(nebTopPlunge.Value / 2) + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge Out
                    Print(1, "sp=TRAVERSE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    sPlungeText = "pl=-(" + CStr(nebTopPlunge.Value / 2)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += ")" + vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "pl=-" + CStr(nebTopPlunge.Value / 2) + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                End If

            End If
        Next

        ' Write the rotate to underside
        Print(1, ";=========================================================" + vbCrLf)
        Print(1, ";Rotate to underside" + vbCrLf)
        Print(1, "ma.z=23" + vbCrLf)
        Print(1, "tl.2=off" + vbCrLf)
        Print(1, "go" + vbCrLf)
        Print(1, "tl.2=180" + vbCrLf)
        Print(1, "go" + vbCrLf)
        Print(1, "" + vbCrLf)

        For iLoop As Integer = dgvPreps.Rows.Count - 2 To 0 Step -1

            If dgvPreps.Rows(iLoop).Cells(3).Value = "dr_slot" Then

                'Get Variables for this row
                Dim sRowDesc As String = dgvPreps.Rows(iLoop).Cells(2).Value
                Dim dRowPos As Double = dgvPreps.Rows(iLoop).Cells(4).Value
                Dim dRowDim1 As Double = dgvPreps.Rows(iLoop).Cells(6).Value
                Dim dRowDim2 As Double = dgvPreps.Rows(iLoop).Cells(7).Value
                Dim dRowYOff As Double = dgvPreps.Rows(iLoop).Cells(8).Value

                'Calculate X Movement
                Dim dMove As Double = dRowPos - dPos
                dPos = dRowPos

                'Move to Position
                Print(1, ";- - - - - - - - - - - - - - - - - - - - - - - - - - -" + vbCrLf)
                Print(1, ";Move to Bottom " + sRowDesc + vbCrLf)
                Print(1, "mr.x=[dir]*" + dMove.ToString + vbCrLf)
                Print(1, "ma.y=[yh]" & Format(dRowYOff, "+0;-0") + vbCrLf)
                Print(1, "go" + vbCrLf)
                Print(1, "" + vbCrLf)

                'Check if Deep Plunge Required on the Bottom of the Profile
                If chkDeepBtm.Checked = False Then

                    'Plunge In
                    Print(1, "sp=PIERCE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    Dim sPlungeText As String = "pl=" + CStr(nebBtmPlunge.Value)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge Out
                    Print(1, "sp=TRAVERSE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    sPlungeText = "pl=-(" + CStr(nebBtmPlunge.Value)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += ")" + vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                Else

                    'Plunge In Half
                    Print(1, "sp=PIERCE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    Dim sPlungeText As String = "pl=" + CStr(nebBtmPlunge.Value / 2)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge In Full
                    Print(1, "sp=PIERCE" + vbCrLf)
                    Print(1, "pl=" + CStr(nebBtmPlunge.Value / 2) + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Prep Slot
                    Print(1, ";Cut a slot in top " + dRowDim1.ToString + "mm x " + dRowDim2.ToString + "mm" + vbCrLf)
                    Print(1, "sp=CUT" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=-(" + dRowDim1.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=(" + dRowDim2.ToString + "-cdia)" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.x=(" + dRowDim1.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "mr.y=-(" + dRowDim2.ToString + "-cdia)/2" + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                    'Plunge Out
                    Print(1, "sp=TRAVERSE" + vbCrLf)

                    'Incorporate the Single Plunge Depth offsets
                    sPlungeText = "pl=-(" + CStr(nebBtmPlunge.Value / 2)
                    If rb10.Checked Then
                        sPlungeText += "+tool2SPDOffset"
                    End If
                    If rb12.Checked Then
                        sPlungeText += "+tool4SPDOffset"
                    End If
                    sPlungeText += ")" + vbCrLf
                    Print(1, sPlungeText)

                    Print(1, "go" + vbCrLf)
                    Print(1, "pl=-" + CStr(nebBtmPlunge.Value / 2) + vbCrLf)
                    Print(1, "go" + vbCrLf)
                    Print(1, "" + vbCrLf)

                End If

            End If

        Next

        ' Write gearbox header
        Print(1, "tl.2=off" + vbCrLf)
        Print(1, "go" + vbCrLf)
        Print(1, "" + vbCrLf)

    End Sub

#End Region


End Class