﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreps
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreps))
        Me.dgvPreps = New System.Windows.Forms.DataGridView()
        Me.lblHardwareID = New System.Windows.Forms.Label()
        Me.txtHardwareCode = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.Label()
        Me.grpCreate = New System.Windows.Forms.GroupBox()
        Me.rbLock = New System.Windows.Forms.RadioButton()
        Me.rbHandle = New System.Windows.Forms.RadioButton()
        Me.grpPlunge = New System.Windows.Forms.GroupBox()
        Me.rbDouble = New System.Windows.Forms.RadioButton()
        Me.rbSingle = New System.Windows.Forms.RadioButton()
        Me.grpTool = New System.Windows.Forms.GroupBox()
        Me.rb12 = New System.Windows.Forms.RadioButton()
        Me.rb10 = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnUndo = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnCreateMND = New System.Windows.Forms.Button()
        Me.gbGearbox = New System.Windows.Forms.GroupBox()
        Me.rbGB16 = New System.Windows.Forms.RadioButton()
        Me.rbGB12 = New System.Windows.Forms.RadioButton()
        Me.rbGB10 = New System.Windows.Forms.RadioButton()
        Me.grpSPOptions = New System.Windows.Forms.GroupBox()
        Me.chkDeepTop = New System.Windows.Forms.CheckBox()
        Me.chkDeepBtm = New System.Windows.Forms.CheckBox()
        Me.lblPlungeTop = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nebTopPlunge = New System.Windows.Forms.NumericUpDown()
        Me.nebBtmPlunge = New System.Windows.Forms.NumericUpDown()
        CType(Me.dgvPreps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCreate.SuspendLayout()
        Me.grpPlunge.SuspendLayout()
        Me.grpTool.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.gbGearbox.SuspendLayout()
        Me.grpSPOptions.SuspendLayout()
        CType(Me.nebTopPlunge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebBtmPlunge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvPreps
        '
        Me.dgvPreps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPreps.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvPreps.Location = New System.Drawing.Point(0, 0)
        Me.dgvPreps.Name = "dgvPreps"
        Me.dgvPreps.Size = New System.Drawing.Size(502, 475)
        Me.dgvPreps.TabIndex = 0
        '
        'lblHardwareID
        '
        Me.lblHardwareID.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblHardwareID.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHardwareID.ForeColor = System.Drawing.Color.White
        Me.lblHardwareID.Location = New System.Drawing.Point(524, 21)
        Me.lblHardwareID.Name = "lblHardwareID"
        Me.lblHardwareID.Size = New System.Drawing.Size(160, 28)
        Me.lblHardwareID.TabIndex = 2
        Me.lblHardwareID.Text = "Hardware Code"
        Me.lblHardwareID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHardwareCode
        '
        Me.txtHardwareCode.BackColor = System.Drawing.Color.White
        Me.txtHardwareCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHardwareCode.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHardwareCode.Location = New System.Drawing.Point(690, 21)
        Me.txtHardwareCode.Name = "txtHardwareCode"
        Me.txtHardwareCode.Size = New System.Drawing.Size(205, 28)
        Me.txtHardwareCode.TabIndex = 3
        Me.txtHardwareCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblName.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.White
        Me.lblName.Location = New System.Drawing.Point(524, 58)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(160, 28)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "Hardware Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        Me.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtName.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(690, 58)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(205, 28)
        Me.txtName.TabIndex = 5
        Me.txtName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpCreate
        '
        Me.grpCreate.Controls.Add(Me.rbLock)
        Me.grpCreate.Controls.Add(Me.rbHandle)
        Me.grpCreate.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCreate.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.grpCreate.Location = New System.Drawing.Point(523, 89)
        Me.grpCreate.Name = "grpCreate"
        Me.grpCreate.Size = New System.Drawing.Size(372, 71)
        Me.grpCreate.TabIndex = 6
        Me.grpCreate.TabStop = False
        Me.grpCreate.Text = "Create"
        '
        'rbLock
        '
        Me.rbLock.ForeColor = System.Drawing.Color.White
        Me.rbLock.Location = New System.Drawing.Point(167, 29)
        Me.rbLock.Name = "rbLock"
        Me.rbLock.Size = New System.Drawing.Size(178, 23)
        Me.rbLock.TabIndex = 1
        Me.rbLock.Text = "Door Lock"
        Me.rbLock.UseVisualStyleBackColor = True
        '
        'rbHandle
        '
        Me.rbHandle.Checked = True
        Me.rbHandle.ForeColor = System.Drawing.Color.White
        Me.rbHandle.Location = New System.Drawing.Point(15, 25)
        Me.rbHandle.Name = "rbHandle"
        Me.rbHandle.Size = New System.Drawing.Size(146, 30)
        Me.rbHandle.TabIndex = 0
        Me.rbHandle.TabStop = True
        Me.rbHandle.Text = "Handle"
        Me.rbHandle.UseVisualStyleBackColor = True
        '
        'grpPlunge
        '
        Me.grpPlunge.Controls.Add(Me.grpSPOptions)
        Me.grpPlunge.Controls.Add(Me.rbDouble)
        Me.grpPlunge.Controls.Add(Me.rbSingle)
        Me.grpPlunge.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpPlunge.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.grpPlunge.Location = New System.Drawing.Point(523, 166)
        Me.grpPlunge.Name = "grpPlunge"
        Me.grpPlunge.Size = New System.Drawing.Size(372, 148)
        Me.grpPlunge.TabIndex = 7
        Me.grpPlunge.TabStop = False
        Me.grpPlunge.Text = "Plunge"
        '
        'rbDouble
        '
        Me.rbDouble.ForeColor = System.Drawing.Color.White
        Me.rbDouble.Location = New System.Drawing.Point(167, 29)
        Me.rbDouble.Name = "rbDouble"
        Me.rbDouble.Size = New System.Drawing.Size(101, 23)
        Me.rbDouble.TabIndex = 1
        Me.rbDouble.Text = "Double"
        Me.rbDouble.UseVisualStyleBackColor = True
        '
        'rbSingle
        '
        Me.rbSingle.Checked = True
        Me.rbSingle.ForeColor = System.Drawing.Color.White
        Me.rbSingle.Location = New System.Drawing.Point(15, 25)
        Me.rbSingle.Name = "rbSingle"
        Me.rbSingle.Size = New System.Drawing.Size(146, 30)
        Me.rbSingle.TabIndex = 0
        Me.rbSingle.TabStop = True
        Me.rbSingle.Text = "Single"
        Me.rbSingle.UseVisualStyleBackColor = True
        '
        'grpTool
        '
        Me.grpTool.Controls.Add(Me.rb12)
        Me.grpTool.Controls.Add(Me.rb10)
        Me.grpTool.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpTool.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.grpTool.Location = New System.Drawing.Point(523, 320)
        Me.grpTool.Name = "grpTool"
        Me.grpTool.Size = New System.Drawing.Size(372, 71)
        Me.grpTool.TabIndex = 8
        Me.grpTool.TabStop = False
        Me.grpTool.Text = "Handle Tool"
        '
        'rb12
        '
        Me.rb12.ForeColor = System.Drawing.Color.White
        Me.rb12.Location = New System.Drawing.Point(167, 29)
        Me.rb12.Name = "rb12"
        Me.rb12.Size = New System.Drawing.Size(178, 23)
        Me.rb12.TabIndex = 1
        Me.rb12.Text = "12.7mm"
        Me.rb12.UseVisualStyleBackColor = True
        '
        'rb10
        '
        Me.rb10.Checked = True
        Me.rb10.ForeColor = System.Drawing.Color.White
        Me.rb10.Location = New System.Drawing.Point(15, 25)
        Me.rb10.Name = "rb10"
        Me.rb10.Size = New System.Drawing.Size(146, 30)
        Me.rb10.TabIndex = 0
        Me.rb10.TabStop = True
        Me.rb10.Text = "10mm"
        Me.rb10.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.btnCreateMND)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 475)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(904, 87)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.btnUndo)
        Me.Panel2.Controls.Add(Me.btnClose)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(487, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(417, 87)
        Me.Panel2.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(12, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(129, 77)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Save Prep"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnUndo
        '
        Me.btnUndo.BackColor = System.Drawing.Color.White
        Me.btnUndo.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndo.Image = CType(resources.GetObject("btnUndo.Image"), System.Drawing.Image)
        Me.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndo.Location = New System.Drawing.Point(147, 7)
        Me.btnUndo.Name = "btnUndo"
        Me.btnUndo.Size = New System.Drawing.Size(129, 77)
        Me.btnUndo.TabIndex = 6
        Me.btnUndo.Text = "Cancel Change"
        Me.btnUndo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndo.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(282, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(129, 77)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Save and Exit"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnCreateMND
        '
        Me.btnCreateMND.BackColor = System.Drawing.Color.White
        Me.btnCreateMND.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreateMND.Image = CType(resources.GetObject("btnCreateMND.Image"), System.Drawing.Image)
        Me.btnCreateMND.Location = New System.Drawing.Point(12, 7)
        Me.btnCreateMND.Name = "btnCreateMND"
        Me.btnCreateMND.Size = New System.Drawing.Size(151, 77)
        Me.btnCreateMND.TabIndex = 4
        Me.btnCreateMND.Text = "Create Prep"
        Me.btnCreateMND.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCreateMND.UseVisualStyleBackColor = False
        '
        'gbGearbox
        '
        Me.gbGearbox.Controls.Add(Me.rbGB16)
        Me.gbGearbox.Controls.Add(Me.rbGB12)
        Me.gbGearbox.Controls.Add(Me.rbGB10)
        Me.gbGearbox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGearbox.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.gbGearbox.Location = New System.Drawing.Point(523, 397)
        Me.gbGearbox.Name = "gbGearbox"
        Me.gbGearbox.Size = New System.Drawing.Size(372, 71)
        Me.gbGearbox.TabIndex = 9
        Me.gbGearbox.TabStop = False
        Me.gbGearbox.Text = "Gearbox Tool"
        '
        'rbGB16
        '
        Me.rbGB16.ForeColor = System.Drawing.Color.White
        Me.rbGB16.Location = New System.Drawing.Point(244, 29)
        Me.rbGB16.Name = "rbGB16"
        Me.rbGB16.Size = New System.Drawing.Size(109, 23)
        Me.rbGB16.TabIndex = 2
        Me.rbGB16.Text = "16mm"
        Me.rbGB16.UseVisualStyleBackColor = True
        '
        'rbGB12
        '
        Me.rbGB12.ForeColor = System.Drawing.Color.White
        Me.rbGB12.Location = New System.Drawing.Point(129, 29)
        Me.rbGB12.Name = "rbGB12"
        Me.rbGB12.Size = New System.Drawing.Size(109, 23)
        Me.rbGB12.TabIndex = 1
        Me.rbGB12.Text = "12.7mm"
        Me.rbGB12.UseVisualStyleBackColor = True
        '
        'rbGB10
        '
        Me.rbGB10.Checked = True
        Me.rbGB10.ForeColor = System.Drawing.Color.White
        Me.rbGB10.Location = New System.Drawing.Point(15, 25)
        Me.rbGB10.Name = "rbGB10"
        Me.rbGB10.Size = New System.Drawing.Size(108, 30)
        Me.rbGB10.TabIndex = 0
        Me.rbGB10.TabStop = True
        Me.rbGB10.Text = "10mm"
        Me.rbGB10.UseVisualStyleBackColor = False
        '
        'grpSPOptions
        '
        Me.grpSPOptions.Controls.Add(Me.nebBtmPlunge)
        Me.grpSPOptions.Controls.Add(Me.nebTopPlunge)
        Me.grpSPOptions.Controls.Add(Me.Label1)
        Me.grpSPOptions.Controls.Add(Me.lblPlungeTop)
        Me.grpSPOptions.Controls.Add(Me.chkDeepBtm)
        Me.grpSPOptions.Controls.Add(Me.chkDeepTop)
        Me.grpSPOptions.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.grpSPOptions.Location = New System.Drawing.Point(6, 58)
        Me.grpSPOptions.Name = "grpSPOptions"
        Me.grpSPOptions.Size = New System.Drawing.Size(360, 84)
        Me.grpSPOptions.TabIndex = 2
        Me.grpSPOptions.TabStop = False
        Me.grpSPOptions.Text = "Single Plunge Options"
        '
        'chkDeepTop
        '
        Me.chkDeepTop.AutoSize = True
        Me.chkDeepTop.Location = New System.Drawing.Point(9, 25)
        Me.chkDeepTop.Name = "chkDeepTop"
        Me.chkDeepTop.Size = New System.Drawing.Size(111, 23)
        Me.chkDeepTop.TabIndex = 0
        Me.chkDeepTop.Text = "Deep Top?"
        Me.chkDeepTop.UseVisualStyleBackColor = True
        '
        'chkDeepBtm
        '
        Me.chkDeepBtm.AutoSize = True
        Me.chkDeepBtm.Location = New System.Drawing.Point(9, 54)
        Me.chkDeepBtm.Name = "chkDeepBtm"
        Me.chkDeepBtm.Size = New System.Drawing.Size(113, 23)
        Me.chkDeepBtm.TabIndex = 1
        Me.chkDeepBtm.Text = "Deep Btm?"
        Me.chkDeepBtm.UseVisualStyleBackColor = True
        '
        'lblPlungeTop
        '
        Me.lblPlungeTop.AutoSize = True
        Me.lblPlungeTop.Location = New System.Drawing.Point(157, 26)
        Me.lblPlungeTop.Name = "lblPlungeTop"
        Me.lblPlungeTop.Size = New System.Drawing.Size(102, 19)
        Me.lblPlungeTop.TabIndex = 2
        Me.lblPlungeTop.Text = "Top Plunge:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(158, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 19)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Btm Plunge:"
        '
        'nebTopPlunge
        '
        Me.nebTopPlunge.Location = New System.Drawing.Point(265, 24)
        Me.nebTopPlunge.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nebTopPlunge.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nebTopPlunge.Name = "nebTopPlunge"
        Me.nebTopPlunge.Size = New System.Drawing.Size(89, 26)
        Me.nebTopPlunge.TabIndex = 6
        Me.nebTopPlunge.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'nebBtmPlunge
        '
        Me.nebBtmPlunge.Location = New System.Drawing.Point(265, 53)
        Me.nebBtmPlunge.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nebBtmPlunge.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nebBtmPlunge.Name = "nebBtmPlunge"
        Me.nebBtmPlunge.Size = New System.Drawing.Size(89, 26)
        Me.nebBtmPlunge.TabIndex = 7
        Me.nebBtmPlunge.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'frmPreps
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Navy
        Me.ClientSize = New System.Drawing.Size(904, 562)
        Me.Controls.Add(Me.gbGearbox)
        Me.Controls.Add(Me.grpTool)
        Me.Controls.Add(Me.grpPlunge)
        Me.Controls.Add(Me.grpCreate)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtHardwareCode)
        Me.Controls.Add(Me.lblHardwareID)
        Me.Controls.Add(Me.dgvPreps)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPreps"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Prep Design"
        CType(Me.dgvPreps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCreate.ResumeLayout(False)
        Me.grpPlunge.ResumeLayout(False)
        Me.grpTool.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.gbGearbox.ResumeLayout(False)
        Me.grpSPOptions.ResumeLayout(False)
        Me.grpSPOptions.PerformLayout()
        CType(Me.nebTopPlunge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebBtmPlunge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvPreps As System.Windows.Forms.DataGridView
    Friend WithEvents lblHardwareID As System.Windows.Forms.Label
    Friend WithEvents txtHardwareCode As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.Label
    Friend WithEvents grpCreate As System.Windows.Forms.GroupBox
    Friend WithEvents rbLock As System.Windows.Forms.RadioButton
    Friend WithEvents rbHandle As System.Windows.Forms.RadioButton
    Friend WithEvents grpPlunge As System.Windows.Forms.GroupBox
    Friend WithEvents rbDouble As System.Windows.Forms.RadioButton
    Friend WithEvents rbSingle As System.Windows.Forms.RadioButton
    Friend WithEvents grpTool As System.Windows.Forms.GroupBox
    Friend WithEvents rb12 As System.Windows.Forms.RadioButton
    Friend WithEvents rb10 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnCreateMND As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnUndo As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents gbGearbox As System.Windows.Forms.GroupBox
    Friend WithEvents rbGB16 As System.Windows.Forms.RadioButton
    Friend WithEvents rbGB12 As System.Windows.Forms.RadioButton
    Friend WithEvents rbGB10 As System.Windows.Forms.RadioButton
    Friend WithEvents grpSPOptions As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblPlungeTop As System.Windows.Forms.Label
    Friend WithEvents chkDeepBtm As System.Windows.Forms.CheckBox
    Friend WithEvents chkDeepTop As System.Windows.Forms.CheckBox
    Friend WithEvents nebBtmPlunge As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebTopPlunge As System.Windows.Forms.NumericUpDown
End Class
