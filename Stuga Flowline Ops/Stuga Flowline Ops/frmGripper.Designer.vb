﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGripper
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGripper))
        Me.dgvProfiles = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnUndoProfiles = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnUpdateReplicates = New System.Windows.Forms.Button()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.grpGripperHeight = New System.Windows.Forms.GroupBox()
        Me.nebGripperHeight = New System.Windows.Forms.NumericUpDown()
        Me.grpSteps = New System.Windows.Forms.GroupBox()
        Me.nebSteps = New System.Windows.Forms.NumericUpDown()
        Me.grpGripType = New System.Windows.Forms.GroupBox()
        Me.rbMk6Wheel = New System.Windows.Forms.RadioButton()
        Me.rbMk6 = New System.Windows.Forms.RadioButton()
        Me.chkRotate = New System.Windows.Forms.CheckBox()
        Me.lblPullGZ = New System.Windows.Forms.Label()
        Me.rbPull = New System.Windows.Forms.RadioButton()
        Me.rbPush = New System.Windows.Forms.RadioButton()
        Me.grpJog = New System.Windows.Forms.GroupBox()
        Me.btnUp = New System.Windows.Forms.Button()
        Me.btnLeft = New System.Windows.Forms.Button()
        Me.btnRight = New System.Windows.Forms.Button()
        Me.btnDown = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lstReplicates = New System.Windows.Forms.ListBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblReplicates = New System.Windows.Forms.Label()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.pbPosCalc = New System.Windows.Forms.PictureBox()
        Me.chkWheel3 = New System.Windows.Forms.CheckBox()
        CType(Me.dgvProfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.grpGripperHeight.SuspendLayout()
        CType(Me.nebGripperHeight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSteps.SuspendLayout()
        CType(Me.nebSteps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpGripType.SuspendLayout()
        Me.grpJog.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvProfiles
        '
        Me.dgvProfiles.AllowUserToAddRows = False
        Me.dgvProfiles.AllowUserToDeleteRows = False
        Me.dgvProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProfiles.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvProfiles.Location = New System.Drawing.Point(0, 0)
        Me.dgvProfiles.Name = "dgvProfiles"
        Me.dgvProfiles.ReadOnly = True
        Me.dgvProfiles.Size = New System.Drawing.Size(344, 635)
        Me.dgvProfiles.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.lblMessage)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.btnUpdateReplicates)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 635)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(984, 87)
        Me.Panel1.TabIndex = 1
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.ForeColor = System.Drawing.Color.White
        Me.lblMessage.Location = New System.Drawing.Point(149, 16)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(13, 13)
        Me.lblMessage.TabIndex = 5
        Me.lblMessage.Text = "?"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.btnUndoProfiles)
        Me.Panel5.Controls.Add(Me.btnClose)
        Me.Panel5.Controls.Add(Me.btnSave)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(544, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(440, 87)
        Me.Panel5.TabIndex = 4
        '
        'btnUndoProfiles
        '
        Me.btnUndoProfiles.BackColor = System.Drawing.Color.White
        Me.btnUndoProfiles.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoProfiles.Image = CType(resources.GetObject("btnUndoProfiles.Image"), System.Drawing.Image)
        Me.btnUndoProfiles.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoProfiles.Location = New System.Drawing.Point(149, 7)
        Me.btnUndoProfiles.Name = "btnUndoProfiles"
        Me.btnUndoProfiles.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoProfiles.TabIndex = 6
        Me.btnUndoProfiles.Text = "Cancel Change"
        Me.btnUndoProfiles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoProfiles.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(295, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(140, 77)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Save and Exit"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(3, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 77)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save Profile"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnUpdateReplicates
        '
        Me.btnUpdateReplicates.BackColor = System.Drawing.Color.White
        Me.btnUpdateReplicates.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateReplicates.Image = CType(resources.GetObject("btnUpdateReplicates.Image"), System.Drawing.Image)
        Me.btnUpdateReplicates.Location = New System.Drawing.Point(3, 6)
        Me.btnUpdateReplicates.Name = "btnUpdateReplicates"
        Me.btnUpdateReplicates.Size = New System.Drawing.Size(127, 78)
        Me.btnUpdateReplicates.TabIndex = 1
        Me.btnUpdateReplicates.Text = "Update Replicates"
        Me.btnUpdateReplicates.UseVisualStyleBackColor = False
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(344, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(8, 635)
        Me.Splitter1.TabIndex = 2
        Me.Splitter1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Navy
        Me.Panel2.Controls.Add(Me.grpGripperHeight)
        Me.Panel2.Controls.Add(Me.grpSteps)
        Me.Panel2.Controls.Add(Me.grpGripType)
        Me.Panel2.Controls.Add(Me.grpJog)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(352, 457)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(632, 178)
        Me.Panel2.TabIndex = 3
        '
        'grpGripperHeight
        '
        Me.grpGripperHeight.Controls.Add(Me.nebGripperHeight)
        Me.grpGripperHeight.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpGripperHeight.ForeColor = System.Drawing.Color.White
        Me.grpGripperHeight.Location = New System.Drawing.Point(526, 114)
        Me.grpGripperHeight.Name = "grpGripperHeight"
        Me.grpGripperHeight.Size = New System.Drawing.Size(100, 57)
        Me.grpGripperHeight.TabIndex = 18
        Me.grpGripperHeight.TabStop = False
        Me.grpGripperHeight.Text = "Height"
        '
        'nebGripperHeight
        '
        Me.nebGripperHeight.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebGripperHeight.Location = New System.Drawing.Point(6, 19)
        Me.nebGripperHeight.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebGripperHeight.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebGripperHeight.Name = "nebGripperHeight"
        Me.nebGripperHeight.Size = New System.Drawing.Size(75, 32)
        Me.nebGripperHeight.TabIndex = 1
        Me.nebGripperHeight.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'grpSteps
        '
        Me.grpSteps.Controls.Add(Me.nebSteps)
        Me.grpSteps.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpSteps.ForeColor = System.Drawing.Color.White
        Me.grpSteps.Location = New System.Drawing.Point(426, 114)
        Me.grpSteps.Name = "grpSteps"
        Me.grpSteps.Size = New System.Drawing.Size(94, 58)
        Me.grpSteps.TabIndex = 17
        Me.grpSteps.TabStop = False
        Me.grpSteps.Text = "Steps"
        '
        'nebSteps
        '
        Me.nebSteps.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebSteps.Location = New System.Drawing.Point(6, 19)
        Me.nebSteps.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebSteps.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebSteps.Name = "nebSteps"
        Me.nebSteps.Size = New System.Drawing.Size(75, 32)
        Me.nebSteps.TabIndex = 0
        Me.nebSteps.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'grpGripType
        '
        Me.grpGripType.Controls.Add(Me.chkWheel3)
        Me.grpGripType.Controls.Add(Me.rbMk6Wheel)
        Me.grpGripType.Controls.Add(Me.rbMk6)
        Me.grpGripType.Controls.Add(Me.chkRotate)
        Me.grpGripType.Controls.Add(Me.lblPullGZ)
        Me.grpGripType.Controls.Add(Me.rbPull)
        Me.grpGripType.Controls.Add(Me.rbPush)
        Me.grpGripType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpGripType.ForeColor = System.Drawing.Color.White
        Me.grpGripType.Location = New System.Drawing.Point(426, 8)
        Me.grpGripType.Name = "grpGripType"
        Me.grpGripType.Size = New System.Drawing.Size(200, 106)
        Me.grpGripType.TabIndex = 16
        Me.grpGripType.TabStop = False
        Me.grpGripType.Text = "Gripper Type"
        '
        'rbMk6Wheel
        '
        Me.rbMk6Wheel.AutoSize = True
        Me.rbMk6Wheel.Location = New System.Drawing.Point(18, 80)
        Me.rbMk6Wheel.Name = "rbMk6Wheel"
        Me.rbMk6Wheel.Size = New System.Drawing.Size(96, 20)
        Me.rbMk6Wheel.TabIndex = 5
        Me.rbMk6Wheel.Text = "Mk6 Wheel"
        Me.rbMk6Wheel.UseVisualStyleBackColor = True
        '
        'rbMk6
        '
        Me.rbMk6.AutoSize = True
        Me.rbMk6.Location = New System.Drawing.Point(18, 61)
        Me.rbMk6.Name = "rbMk6"
        Me.rbMk6.Size = New System.Drawing.Size(82, 20)
        Me.rbMk6.TabIndex = 4
        Me.rbMk6.Text = "Mk6 Grip"
        Me.rbMk6.UseVisualStyleBackColor = True
        '
        'chkRotate
        '
        Me.chkRotate.Location = New System.Drawing.Point(115, 21)
        Me.chkRotate.Name = "chkRotate"
        Me.chkRotate.Size = New System.Drawing.Size(79, 20)
        Me.chkRotate.TabIndex = 3
        Me.chkRotate.Text = "Rotate"
        Me.chkRotate.UseVisualStyleBackColor = True
        '
        'lblPullGZ
        '
        Me.lblPullGZ.Location = New System.Drawing.Point(87, 42)
        Me.lblPullGZ.Name = "lblPullGZ"
        Me.lblPullGZ.Size = New System.Drawing.Size(107, 18)
        Me.lblPullGZ.TabIndex = 2
        Me.lblPullGZ.Text = "PullGZ=?"
        Me.lblPullGZ.Visible = False
        '
        'rbPull
        '
        Me.rbPull.AutoSize = True
        Me.rbPull.Location = New System.Drawing.Point(18, 40)
        Me.rbPull.Name = "rbPull"
        Me.rbPull.Size = New System.Drawing.Size(51, 20)
        Me.rbPull.TabIndex = 1
        Me.rbPull.Text = "Pull"
        Me.rbPull.UseVisualStyleBackColor = True
        '
        'rbPush
        '
        Me.rbPush.AutoSize = True
        Me.rbPush.Checked = True
        Me.rbPush.Location = New System.Drawing.Point(18, 21)
        Me.rbPush.Name = "rbPush"
        Me.rbPush.Size = New System.Drawing.Size(57, 20)
        Me.rbPush.TabIndex = 0
        Me.rbPush.TabStop = True
        Me.rbPush.Text = "Push"
        Me.rbPush.UseVisualStyleBackColor = True
        '
        'grpJog
        '
        Me.grpJog.Controls.Add(Me.btnUp)
        Me.grpJog.Controls.Add(Me.btnLeft)
        Me.grpJog.Controls.Add(Me.btnRight)
        Me.grpJog.Controls.Add(Me.btnDown)
        Me.grpJog.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpJog.ForeColor = System.Drawing.Color.White
        Me.grpJog.Location = New System.Drawing.Point(202, 8)
        Me.grpJog.Name = "grpJog"
        Me.grpJog.Size = New System.Drawing.Size(210, 164)
        Me.grpJog.TabIndex = 15
        Me.grpJog.TabStop = False
        Me.grpJog.Text = "Jog"
        '
        'btnUp
        '
        Me.btnUp.BackColor = System.Drawing.Color.LightGreen
        Me.btnUp.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Location = New System.Drawing.Point(72, 20)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(60, 40)
        Me.btnUp.TabIndex = 7
        Me.btnUp.UseVisualStyleBackColor = False
        '
        'btnLeft
        '
        Me.btnLeft.BackColor = System.Drawing.Color.LightGreen
        Me.btnLeft.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeft.Image = CType(resources.GetObject("btnLeft.Image"), System.Drawing.Image)
        Me.btnLeft.Location = New System.Drawing.Point(6, 66)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(60, 40)
        Me.btnLeft.TabIndex = 8
        Me.btnLeft.UseVisualStyleBackColor = False
        '
        'btnRight
        '
        Me.btnRight.BackColor = System.Drawing.Color.LightGreen
        Me.btnRight.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRight.Image = CType(resources.GetObject("btnRight.Image"), System.Drawing.Image)
        Me.btnRight.Location = New System.Drawing.Point(138, 66)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(60, 40)
        Me.btnRight.TabIndex = 9
        Me.btnRight.UseVisualStyleBackColor = False
        '
        'btnDown
        '
        Me.btnDown.BackColor = System.Drawing.Color.LightGreen
        Me.btnDown.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Location = New System.Drawing.Point(72, 112)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(60, 40)
        Me.btnDown.TabIndex = 10
        Me.btnDown.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lstReplicates)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(196, 178)
        Me.Panel3.TabIndex = 2
        '
        'lstReplicates
        '
        Me.lstReplicates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstReplicates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstReplicates.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstReplicates.FormattingEnabled = True
        Me.lstReplicates.ItemHeight = 18
        Me.lstReplicates.Location = New System.Drawing.Point(0, 32)
        Me.lstReplicates.Name = "lstReplicates"
        Me.lstReplicates.Size = New System.Drawing.Size(196, 146)
        Me.lstReplicates.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblReplicates)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(196, 32)
        Me.Panel4.TabIndex = 0
        '
        'lblReplicates
        '
        Me.lblReplicates.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblReplicates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblReplicates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblReplicates.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReplicates.ForeColor = System.Drawing.Color.White
        Me.lblReplicates.Location = New System.Drawing.Point(0, 0)
        Me.lblReplicates.Name = "lblReplicates"
        Me.lblReplicates.Size = New System.Drawing.Size(196, 32)
        Me.lblReplicates.TabIndex = 0
        Me.lblReplicates.Text = "Replicates"
        Me.lblReplicates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter2.Location = New System.Drawing.Point(352, 447)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(632, 10)
        Me.Splitter2.TabIndex = 4
        Me.Splitter2.TabStop = False
        '
        'pbPosCalc
        '
        Me.pbPosCalc.BackColor = System.Drawing.Color.White
        Me.pbPosCalc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPosCalc.Location = New System.Drawing.Point(352, 0)
        Me.pbPosCalc.Name = "pbPosCalc"
        Me.pbPosCalc.Size = New System.Drawing.Size(632, 447)
        Me.pbPosCalc.TabIndex = 5
        Me.pbPosCalc.TabStop = False
        '
        'chkWheel3
        '
        Me.chkWheel3.AutoSize = True
        Me.chkWheel3.Location = New System.Drawing.Point(115, 80)
        Me.chkWheel3.Name = "chkWheel3"
        Me.chkWheel3.Size = New System.Drawing.Size(79, 20)
        Me.chkWheel3.TabIndex = 6
        Me.chkWheel3.Text = "Wheel 3"
        Me.chkWheel3.UseVisualStyleBackColor = True
        '
        'frmGripper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 722)
        Me.Controls.Add(Me.pbPosCalc)
        Me.Controls.Add(Me.Splitter2)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.dgvProfiles)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGripper"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gripper Setup"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvProfiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.grpGripperHeight.ResumeLayout(False)
        CType(Me.nebGripperHeight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSteps.ResumeLayout(False)
        CType(Me.nebSteps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpGripType.ResumeLayout(False)
        Me.grpGripType.PerformLayout()
        Me.grpJog.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvProfiles As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents pbPosCalc As System.Windows.Forms.PictureBox
    Friend WithEvents lstReplicates As System.Windows.Forms.ListBox
    Friend WithEvents btnUpdateReplicates As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblReplicates As System.Windows.Forms.Label
    Friend WithEvents grpJog As System.Windows.Forms.GroupBox
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnLeft As System.Windows.Forms.Button
    Friend WithEvents btnRight As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnUndoProfiles As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents grpSteps As System.Windows.Forms.GroupBox
    Friend WithEvents nebSteps As System.Windows.Forms.NumericUpDown
    Friend WithEvents grpGripType As System.Windows.Forms.GroupBox
    Friend WithEvents rbPull As System.Windows.Forms.RadioButton
    Friend WithEvents rbPush As System.Windows.Forms.RadioButton
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblPullGZ As System.Windows.Forms.Label
    Friend WithEvents grpGripperHeight As System.Windows.Forms.GroupBox
    Friend WithEvents nebGripperHeight As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkRotate As System.Windows.Forms.CheckBox
    Friend WithEvents rbMk6 As System.Windows.Forms.RadioButton
    Friend WithEvents rbMk6Wheel As System.Windows.Forms.RadioButton
    Friend WithEvents chkWheel3 As System.Windows.Forms.CheckBox
End Class
