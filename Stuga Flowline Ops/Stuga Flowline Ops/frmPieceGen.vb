﻿Public Class frmPieceGen

#Region " Form - Load "

    Private Sub frmPieceGen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FillProfiles(cmbProfile)

    End Sub

#End Region

#Region " Event - cmbProfile_TextChanged() "

    Private Sub cmbProfile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProfile.TextChanged

        FillOperationsCombo(Me.dgvOperations, cmbProfile.Text, True, True)

    End Sub

#End Region

#Region " Button - Create "

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        Dim sCellValue As String
        Dim iSelectedRows As Integer = dgvOperations.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvOperations.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            MsgBox("Please select an Operation.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            Dim sr As DataGridViewRow = dgvOperations.SelectedRows(0)
            sCellValue = sr.Cells.Item(0).Value

        Else

            'Cell Selected
            Dim sc As DataGridViewCell = dgvOperations.SelectedCells(0)
            Dim ri As Integer = sc.RowIndex
            sCellValue = dgvOperations.Rows(ri).Cells.Item(0).Value

        End If

        'Check Profile
        If cmbProfile.Text = "" Then
            MsgBox("Invalid Profile", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Check Size
        Try
            Dim d As Double = CDbl(cmbSize.Text)
        Catch ex As Exception
            MsgBox("Invalid Size", MsgBoxStyle.Exclamation)
            Exit Sub
        End Try

        'Check End Preps
        If cmbCut.Text = "" Then
            MsgBox("Invalid Cut", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim sLeftPrep As String = Mid(cmbCut.Text, 1, 1)
        Dim sRightPrep As String = Mid(cmbCut.Text, 3, 1)
        Dim sLeftNos As String = ""
        Dim sRightNos As String = ""
        Select Case sLeftPrep
            Case "|"
                sLeftNos = "0900"
            Case "\"
                sLeftNos = "0450"
            Case "<"
                sLeftNos = "0000"
            Case Else
                MsgBox("Invalid Cut", MsgBoxStyle.Exclamation)
                Exit Sub
        End Select
        Select Case sRightPrep
            Case "|"
                sRightNos = "0900"
            Case "/"
                sRightNos = "0450"
            Case ">"
                sRightNos = "0000"
            Case Else
                MsgBox("Invalid Cut", MsgBoxStyle.Exclamation)
                Exit Sub
        End Select

        'Check Qty
        Try
            Dim i As Integer = CInt(txtQty.Text)
        Catch ex As Exception
            MsgBox("Invalid Quantity", MsgBoxStyle.Exclamation)
            Exit Sub
        End Try

        'Determine File Location
        Dim sFL As String = ""
        If Mid(prfBatchLoc, Len(prfBatchLoc), 1) <> "\" Then
            sFL = prfBatchLoc + "\" + cmbProfile.Text + ".449"
        Else
            sFL = prfBatchLoc + cmbProfile.Text + ".449"
        End If


        'Try to Create File
        Try
            FileOpen(1, sFL, OpenMode.Output, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Create " + sFL, MsgBoxStyle.Exclamation)
            Exit Sub
        End Try

        'Write Batch Record
        Print(1, "B," + cmbProfile.Text + ",S" + vbCrLf)

        'Write Load Record
        Print(1, "L," + cmbProfile.Text + ",WHITE,1,60000,001" + vbCrLf)

        Dim iSize As Integer = CInt(CDbl(cmbSize.Text) * 10)
        Dim sSize As String = CStr(iSize)
        Do While Len(sSize) < 5
            sSize = "0" + sSize
        Loop

        Dim iPos As Integer = iSize / 2
        Dim sPos As String = CStr(iPos)
        Do While Len(sPos) < 5
            sPos = "0" + sPos
        Loop

        For iloop As Integer = 1 To CInt(txtQty.Text)
            Dim sID As String = CStr(iloop)
            If Len(sID) < 2 Then sID = "0" + sID
            Print(1, "P," + sSize + "," + sLeftPrep + sRightPrep + ",01," + sID + ",                                        ," + sLeftNos + "," + sRightNos + ",0000000000" + sID + vbCrLf)
            If sCellValue <> "<None>" Then Print(1, "O," + sPos + "," + sCellValue + ",000,000,000" + vbCrLf)
        Next

        FileClose(1)

        MsgBox("File Created", MsgBoxStyle.Information)

    End Sub

#End Region

End Class