﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditMND
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditMND))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnCloseStdOps = New System.Windows.Forms.Button()
        Me.rtMND = New System.Windows.Forms.RichTextBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 387)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(639, 87)
        Me.Panel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnCloseStdOps)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(491, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(148, 87)
        Me.Panel2.TabIndex = 4
        '
        'btnCloseStdOps
        '
        Me.btnCloseStdOps.BackColor = System.Drawing.Color.White
        Me.btnCloseStdOps.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseStdOps.Image = CType(resources.GetObject("btnCloseStdOps.Image"), System.Drawing.Image)
        Me.btnCloseStdOps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloseStdOps.Location = New System.Drawing.Point(3, 6)
        Me.btnCloseStdOps.Name = "btnCloseStdOps"
        Me.btnCloseStdOps.Size = New System.Drawing.Size(140, 77)
        Me.btnCloseStdOps.TabIndex = 3
        Me.btnCloseStdOps.Text = "Save and Exit"
        Me.btnCloseStdOps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloseStdOps.UseVisualStyleBackColor = False
        '
        'rtMND
        '
        Me.rtMND.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtMND.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtMND.Location = New System.Drawing.Point(0, 0)
        Me.rtMND.Name = "rtMND"
        Me.rtMND.Size = New System.Drawing.Size(639, 387)
        Me.rtMND.TabIndex = 1
        Me.rtMND.Text = ""
        '
        'frmEditMND
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(639, 474)
        Me.Controls.Add(Me.rtMND)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmEditMND"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit MND"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rtMND As System.Windows.Forms.RichTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnCloseStdOps As System.Windows.Forms.Button
End Class
