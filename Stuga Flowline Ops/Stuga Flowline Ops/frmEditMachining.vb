﻿Public Class frmEditMachining

#Region " Variable Declaration "

    Private PosCalc As New clsPosCalc
    Private ParseMND As New clsParseMND

#End Region

#Region " Form - Load "

    Private Sub frmEditMachining_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Set Default Scale
        nebZoom.Value = iScale

        'Set Up PosCalc Class
        InitialisePosCalc()

        'Fill Operations Listing
        FillOperationsGrid(dgvMachining, "", False, "", False, "", False, False)

        chkRingClash.Checked = ShowDangerRings

        Me.txtProfile.Focus()

    End Sub

#End Region

#Region " Button - Notch + "

    Private Sub btnNotchPlus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNotchPlus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetNotchStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogNotch/" + btnNotchPlus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnNotchPlus, GetNotchStep, False)

    End Sub

#End Region
#Region " Button - Notch - "

    Private Sub btnNotchMinus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNotchMinus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetNotchStep() * -1
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogNotch/" + btnNotchMinus.Tag.ToString + "/" + dblValue.ToString)


        Jog(btnNotchMinus, GetNotchStep() * -1, False)

    End Sub

#End Region
#Region " Button - Y+ "

    Private Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogRight/" + btnRight.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnRight, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Y- "

    Private Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogLeft/" + btnLeft.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnLeft, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Z+ "

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogUp/" + btnUp.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnUp, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Z- "

    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogDown/" + btnDown.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnDown, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Angle + "

    Private Sub btnAngPlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAngPlus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogAnglePlus/" + btnAngPlus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnAngPlus, GetOpsStep, True)

    End Sub

#End Region
#Region " Button - Angle - "

    Private Sub btnAngMinus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAngMinus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogAngleMinus/" + btnAngMinus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnAngMinus, GetOpsStep() * -1, True)

    End Sub

#End Region
#Region " Button - Plunge + "

    Private Sub btnPLPlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPLPlus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogPlungePlus/" + btnPLPlus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnPLPlus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Plunge - "

    Private Sub btnPLMinus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPLMinus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1
        AddLogRecord(sProfile, sToolCode, "Edit Machining: JogPlungeMinus/" + btnPLMinus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnPLMinus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - BoxWidth + "

    Private Sub btnBWPlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBWPlus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()
        AddLogRecord(sProfile, sToolCode, "Edit Machining: BoxWidthPlus/" + btnBWPlus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnBWPlus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - BoxWidth - "

    Private Sub btnBWMinus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBWMinus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: BoxWidthMinus/" + btnBWMinus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnBWMinus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - BoxHeight + "

    Private Sub btnBHPlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBHPlus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()

        AddLogRecord(sProfile, sToolCode, "Edit Machining: BoxHeightPlus/" + btnBHPlus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnBHPlus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - BoxHeight - "

    Private Sub btnBHMinus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBHMinus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: BoxHeightMinus/" + btnBHMinus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnBHMinus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Misc1 + "

    Private Sub btnMisc1Plus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc1Plus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc1Plus/" + btnMisc1Plus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc1Plus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Misc1 - "

    Private Sub btnMisc1Minus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc1Minus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc1Minus/" + btnMisc1Minus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc1Minus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Misc2 + "

    Private Sub btnMisc2Plus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc2Plus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc2Plus/" + btnMisc2Plus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc2Plus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Misc2 - "

    Private Sub btnMisc2Minus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc2Minus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc2Minus/" + btnMisc2Minus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc2Minus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Misc3 + "

    Private Sub btnMisc3Plus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc3Plus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc3Plus/" + btnMisc3Plus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc3Plus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Misc3 - "

    Private Sub btnMisc3Minus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc3Minus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc3Minus/" + btnMisc3Minus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc3Minus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Misc4 + "

    Private Sub btnMisc4Plus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc4Plus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep()

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc4Plus/" + btnMisc4Plus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc4Plus, GetOpsStep, False)

    End Sub

#End Region
#Region " Button - Misc4 - "

    Private Sub btnMisc4Minus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMisc4Minus.Click

        'Declare Variables
        Dim sr As DataGridViewRow = GetGridRow()
        Dim sProfile As String = sr.Cells(1).Value
        Dim sToolCode As String = sr.Cells(2).Value
        Dim dblValue As Double = GetOpsStep() * -1

        AddLogRecord(sProfile, sToolCode, "Edit Machining: Misc4Minus/" + btnMisc4Minus.Tag.ToString + "/" + dblValue.ToString)

        Jog(btnMisc4Minus, GetOpsStep() * -1, False)

    End Sub

#End Region
#Region " Button - Save "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        SaveStdOpsMUL()

        Me.Close()

    End Sub

#End Region

#Region " Event - dgvMachining_SelectionChanged() "

    Private Sub dgvMachining_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMachining.SelectionChanged

        Redraw()

    End Sub

#End Region
#Region " Event - rb1_CheckedChanged() "

    Private Sub rb1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb1.CheckedChanged

        Redraw()

    End Sub

#End Region
#Region " Event - nebScale_ValueChanged() "

    Private Sub nebScale_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Redraw()

    End Sub

#End Region
#Region " Event - chkRingClash_CheckedChanged "

    Private Sub chkRingClash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRingClash.CheckedChanged

        ShowDangerRings = chkRingClash.Checked
        Redraw()

    End Sub

#End Region
#Region " Event - optNoGrip_CheckedChanged() "

    Private Sub optNoGrip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNoGrip.CheckedChanged

        If optNoGrip.Checked Then
            ShowNewGripper = False
            Redraw()

        End If

    End Sub

#End Region
#Region " Event - optPushGrip_CheckedChanged() "

    Private Sub optPushGrip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPushGrip.CheckedChanged

        If optPushGrip.Checked Then
            ShowNewGripper = True
            Redraw()

        End If

    End Sub

#End Region
#Region " Event - optPullGrip_CheckedChanged() "

    Private Sub optPullGrip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPullGrip.CheckedChanged

        If optPullGrip.Checked Then
            ShowNewGripper = True
            Redraw()

        End If

    End Sub

#End Region

#Region " Sub - InitialisePosCalc() "

    Sub InitialisePosCalc()

        With PosCalc

            'Link Controls
            .pbPosCalc = pbPoscalc

            'Set Scale
            .Scale = iScale

            'Set Centre Point
            .XOffset = pbPoscalc.Width / 2
            .YOffset = pbPoscalc.Height / 2

        End With

    End Sub

#End Region
#Region " Sub - Redraw() "

    Sub Redraw()

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvMachining.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvMachining.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvMachining.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvMachining.SelectedCells(0).RowIndex
            sr = dgvMachining.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If
        ProfileWidth = GetProfileWidth(sr.Cells(1).Value)
        ProfileHeight = GetProfileHeight(sr.Cells(1).Value)

        'Get MND File and Variables
        Dim sString As String = sr.Cells(3).Value
        Dim V1 As Double = sr.Cells(6).Value
        Dim V2 As Double = sr.Cells(7).Value
        Dim V3 As Double = sr.Cells(8).Value
        Dim V4 As Double = sr.Cells(9).Value
        Dim V5 As Double = sr.Cells(10).Value
        Dim V6 As Double = sr.Cells(11).Value
        Dim V7 As Double = sr.Cells(12).Value
        Dim V8 As Double = sr.Cells(13).Value
        Dim sProfile As String = sr.Cells(1).Value
        Dim sOpToolCode As String = sr.Cells(2).Value
        Dim sLTC As String = sOpToolCode.Substring(0, 2)

        lblStdOp.Text = "StdOp: " + sString

        'Parse MND file
        Dim sFile As String = prfOpdesignLoc + sString + ".MND"
        ParseMND.ParseMNDFile(sFile, V1, V2, V3, V4, V5, V6, V7, V8, rb1, nebYP, nebZP, nebANG, nebPL, nebTL)
        If ParseMND.Description1 <> "" Then rb1.Text = ParseMND.Description1 Else rb1.Text = "Position 1"
        If ParseMND.Description2 <> "" Then rb2.Text = ParseMND.Description2 Else rb2.Text = "Position 2"

        If rb1.Text = "No Op" Or rb1.Text = "Position 1" Then
            rb1.Visible = False
        Else
            rb1.Visible = True
        End If
        If rb2.Text = "No Op" Or rb2.Text = "Position 2" Then
            rb2.Visible = False
        Else
            rb2.Visible = True
        End If
        If rb1.Checked Then
            btnLeft.Tag = ParseMND.YP1
            btnRight.Tag = ParseMND.YP1
            btnUp.Tag = ParseMND.ZP1
            btnDown.Tag = ParseMND.ZP1
            btnAngMinus.Tag = ParseMND.ANG1
            btnAngPlus.Tag = ParseMND.ANG1
            btnPLMinus.Tag = ParseMND.PL1
            btnPLPlus.Tag = ParseMND.PL1
            btnBWMinus.Tag = ParseMND.BW1
            btnBWPlus.Tag = ParseMND.BW1
            btnBHMinus.Tag = ParseMND.BH1
            btnBHPlus.Tag = ParseMND.BH1
            grpWidth.Text = "Slot Length: " + ParseMND.dBoxWidth1.ToString
            grpHeight.Text = "Slot Height: " + ParseMND.dBoxHeight1.ToString
            If sLTC.ToUpper = "VF" Or sLTC.ToUpper = "VR" Or sLTC.ToUpper = "YF" Or sLTC.ToUpper = "YR" Then
                Me.grpNotch.Visible = True
                grpNotch.Text = "Notch Depth: " + V1.ToString
            Else
                Me.grpNotch.Visible = False
                'MsgBox(sLTC)
            End If
            grpJog.Text = "Jog: Horiz=" + Format(nebYP.Value, "0.0") + " / Vert=" + nebZP.Value.ToString
            lblAngPL.Text = "Ang: " + Format(nebANG.Value, "0.0") + " / PL: " + Format(nebPL.Value, "0.0")
            btnMisc1Minus.Tag = ParseMND.sMisc1Var
            btnMisc1Plus.Tag = ParseMND.sMisc1Var
            grpMisc1.Text = ParseMND.sMisc1Desc + ": " + ParseMND.dMisc1Pos.ToString

            btnMisc2Minus.Tag = ParseMND.sMisc2Var
            btnMisc2Plus.Tag = ParseMND.sMisc2Var
            grpMisc2.Text = ParseMND.sMisc2Desc + ": " + ParseMND.dMisc2Pos.ToString

            btnMisc3Minus.Tag = ParseMND.sMisc3Var
            btnMisc3Plus.Tag = ParseMND.sMisc3Var
            grpMisc3.Text = ParseMND.sMisc3Desc + ": " + ParseMND.dMisc3Pos.ToString

            btnMisc4Minus.Tag = ParseMND.sMisc4Var
            btnMisc4Plus.Tag = ParseMND.sMisc4Var
            grpMisc4.Text = ParseMND.sMisc4Desc + ": " + ParseMND.dMisc4Pos.ToString

        Else
            btnLeft.Tag = ParseMND.YP2
            btnRight.Tag = ParseMND.YP2
            btnUp.Tag = ParseMND.ZP2
            btnDown.Tag = ParseMND.ZP2
            btnAngMinus.Tag = ParseMND.ANG2
            btnAngPlus.Tag = ParseMND.ANG2
            btnPLMinus.Tag = ParseMND.PL2
            btnPLPlus.Tag = ParseMND.PL2
            btnBWMinus.Tag = ParseMND.BW2
            btnBWPlus.Tag = ParseMND.BW2
            btnBHMinus.Tag = ParseMND.BH2
            btnBHPlus.Tag = ParseMND.BH2
            grpWidth.Text = "Slot Length: " + ParseMND.dBoxWidth2.ToString
            grpHeight.Text = "Slot Height: " + ParseMND.dBoxHeight2.ToString
            If sLTC.ToUpper = "VF" Or sLTC.ToUpper = "VR" Or sLTC.ToUpper = "YF" Or sLTC.ToUpper = "YR" Then
                Me.grpNotch.Visible = True
                grpNotch.Text = "Notch Depth: " + V1.ToString
            Else
                Me.grpNotch.Visible = False
                'MsgBox(sLTC)
            End If
            grpJog.Text = "Jog: Horiz=" + nebYP.Value.ToString + " / Vert=" + nebZP.Value.ToString
            lblAngPL.Text = "Ang: " + Format(nebANG.Value, "0.0") + " / PL: " + Format(nebPL.Value, "0.0")
            btnMisc1Minus.Tag = ParseMND.sMisc1Var
            btnMisc1Plus.Tag = ParseMND.sMisc1Var
            grpMisc1.Text = ParseMND.sMisc1Desc + ": " + ParseMND.dMisc1Pos.ToString

            btnMisc2Minus.Tag = ParseMND.sMisc2Var
            btnMisc2Plus.Tag = ParseMND.sMisc2Var
            grpMisc2.Text = ParseMND.sMisc2Desc + ": " + ParseMND.dMisc2Pos.ToString

            btnMisc3Minus.Tag = ParseMND.sMisc3Var
            btnMisc3Plus.Tag = ParseMND.sMisc3Var
            grpMisc3.Text = ParseMND.sMisc3Desc + ": " + ParseMND.dMisc3Pos.ToString

            btnMisc4Minus.Tag = ParseMND.sMisc4Var
            btnMisc4Plus.Tag = ParseMND.sMisc4Var
            grpMisc4.Text = ParseMND.sMisc4Desc + ": " + ParseMND.dMisc4Pos.ToString
        End If

        If btnLeft.Tag <> "" Then
            'btnLeft.Enabled = True
            btnLeft.BackColor = Color.LightGreen
            btnLeft.Visible = True
        Else
            'btnLeft.Enabled = False
            btnLeft.BackColor = Color.Red
            btnLeft.Visible = False
        End If

        If btnRight.Tag <> "" Then
            'btnRight.Enabled = True
            btnRight.BackColor = Color.LightGreen
            btnRight.Visible = True
        Else
            'btnRight.Enabled = False
            btnRight.BackColor = Color.Red
            btnRight.Visible = False
        End If

        If btnUp.Tag <> "" Then
            'btnUp.Enabled = True
            btnUp.BackColor = Color.LightGreen
            btnUp.Visible = True
        Else
            'btnUp.Enabled = False
            btnUp.BackColor = Color.Red
            btnUp.Visible = False
        End If

        If btnDown.Tag <> "" Then
            'btnDown.Enabled = True
            btnDown.BackColor = Color.LightGreen
            btnDown.Visible = True
        Else
            'btnDown.Enabled = False
            btnDown.BackColor = Color.Red
            btnDown.Visible = False
        End If

        If btnAngMinus.Tag <> "" Then
            'btnAngMinus.Enabled = True
            btnAngMinus.BackColor = Color.LightGreen
            btnAngMinus.Visible = True
        Else
            'btnAngMinus.Enabled = False
            btnAngMinus.BackColor = Color.Red
            btnAngMinus.Visible = False
        End If

        If btnAngPlus.Tag <> "" Then
            'btnAngPlus.Enabled = True
            btnAngPlus.BackColor = Color.LightGreen
            btnAngPlus.Visible = True
        Else
            'btnAngPlus.Enabled = False
            btnAngPlus.BackColor = Color.Red
            btnAngPlus.Visible = False
        End If

        If btnPLMinus.Tag <> "" Then
            'btnPLMinus.Enabled = True
            btnPLMinus.BackColor = Color.LightGreen
            btnPLMinus.Visible = True
        Else
            'btnPLMinus.Enabled = False
            btnPLMinus.BackColor = Color.Red
            btnPLMinus.Visible = False
        End If

        If btnPLPlus.Tag <> "" Then
            'btnPLPlus.Enabled = True
            btnPLPlus.BackColor = Color.LightGreen
            btnPLPlus.Visible = True
        Else
            'btnPLPlus.Enabled = False
            btnPLPlus.BackColor = Color.Red
            btnPLPlus.Visible = False
        End If

        If btnBWMinus.Tag <> "" Then
            'btnBWMinus.Enabled = True
            btnBWMinus.BackColor = Color.LightGreen
            btnBWMinus.Visible = True
        Else
            'btnBWMinus.Enabled = False
            btnBWMinus.BackColor = Color.Red
            btnBWMinus.Visible = False
        End If

        If btnBWPlus.Tag <> "" Then
            'btnBWPlus.Enabled = True
            btnBWPlus.BackColor = Color.LightGreen
            btnBWPlus.Visible = True
        Else
            'btnBWPlus.Enabled = False
            btnBWPlus.BackColor = Color.Red
            btnBWPlus.Visible = False
        End If

        If btnBHMinus.Tag <> "" Then
            'btnBHMinus.Enabled = True
            btnBHMinus.BackColor = Color.LightGreen
            btnBHMinus.Visible = True
        Else
            'btnBHMinus.Enabled = False
            btnBHMinus.BackColor = Color.Red
            btnBHMinus.Visible = False
        End If

        If btnBHPlus.Tag <> "" Then
            'btnBHPlus.Enabled = True
            btnBHPlus.BackColor = Color.LightGreen
            btnBHPlus.Visible = True
        Else
            'btnBHPlus.Enabled = False
            btnBHPlus.BackColor = Color.Red
            btnBHPlus.Visible = False
        End If

        If btnMisc1Minus.Tag <> "" Then
            'btnMisc1Minus.Enabled = True
            btnMisc1Minus.BackColor = Color.LightGreen
            btnMisc1Minus.Visible = True
            grpMisc1.Visible = True
        Else
            'btnMisc1Minus.Enabled = False
            btnMisc1Minus.BackColor = Color.Red
            btnMisc1Minus.Visible = False
            grpMisc1.Visible = False
        End If

        If btnMisc1Plus.Tag <> "" Then
            'btnMisc1Plus.Enabled = True
            btnMisc1Plus.BackColor = Color.LightGreen
            btnMisc1Plus.Visible = True
            grpMisc1.Visible = True
        Else
            'btnMisc1Plus.Enabled = False
            btnMisc1Plus.BackColor = Color.Red
            btnMisc1Plus.Visible = False
            grpMisc1.Visible = False
        End If

        If btnMisc2Minus.Tag <> "" Then
            'btnMisc2Minus.Enabled = True
            btnMisc2Minus.BackColor = Color.LightGreen
            btnMisc2Minus.Visible = True
            grpMisc2.Visible = True
        Else
            'btnMisc2Minus.Enabled = False
            btnMisc2Minus.BackColor = Color.Red
            btnMisc2Minus.Visible = False
            grpMisc2.Visible = False
        End If

        If btnMisc2Plus.Tag <> "" Then
            'btnMisc2Plus.Enabled = True
            btnMisc2Plus.BackColor = Color.LightGreen
            btnMisc2Plus.Visible = True
            grpMisc2.Visible = True
        Else
            'btnMisc2Plus.Enabled = False
            btnMisc2Plus.BackColor = Color.Red
            btnMisc2Plus.Visible = False
            grpMisc2.Visible = False
        End If

        If btnMisc3Minus.Tag <> "" Then
            'btnMisc3Minus.Enabled = True
            btnMisc3Minus.BackColor = Color.LightGreen
            btnMisc3Minus.Visible = True
            grpMisc3.Visible = True
        Else
            'btnMisc3Minus.Enabled = False
            btnMisc3Minus.BackColor = Color.Red
            btnMisc3Minus.Visible = False
            grpMisc3.Visible = False
        End If

        If btnMisc3Plus.Tag <> "" Then
            'btnMisc3Plus.Enabled = True
            btnMisc3Plus.BackColor = Color.LightGreen
            btnMisc3Plus.Visible = True
            grpMisc3.Visible = True
        Else
            'btnMisc3Plus.Enabled = False
            btnMisc3Plus.BackColor = Color.Red
            btnMisc3Plus.Visible = False
            grpMisc3.Visible = False
        End If

        If btnMisc4Minus.Tag <> "" Then
            'btnMisc4Minus.Enabled = True
            btnMisc4Minus.BackColor = Color.LightGreen
            btnMisc4Minus.Visible = True
            grpMisc4.Visible = True
        Else
            'btnMisc4Minus.Enabled = False
            btnMisc4Minus.BackColor = Color.Red
            btnMisc4Minus.Visible = False
            grpMisc4.Visible = False
        End If

        If btnMisc4Plus.Tag <> "" Then
            'btnMisc4Plus.Enabled = True
            btnMisc4Plus.BackColor = Color.LightGreen
            btnMisc4Plus.Visible = True
            grpMisc4.Visible = True
        Else
            'btnMisc4Plus.Enabled = False
            btnMisc4Plus.BackColor = Color.Red
            btnMisc4Plus.Visible = False
            grpMisc4.Visible = False
        End If

        'Draw Operation
        With PosCalc
            .XOffset = pbPoscalc.Width / 2
            .YOffset = pbPoscalc.Height / 2
            .LoadDXF(modGlobal.prfDXFLoc & sProfile & ".dxf")
            .YP = nebYP.Value
            .ZP = nebZP.Value
            .Angle = nebANG.Value
            .Plunge = nebPL.Value
            .ToolDiameter = nebTL.Value
            .Scale = nebZoom.Value
            '.bShowGrip = ShowNewGripper
            'If optPullGrip.Checked Then
            '.GripperType = 1
            '.dblGY = GetPullGripperY(sProfile)
            '.dblGZ = 0
            '.PullGripperHeight = prfPullGripperHeight
            '.PullGripperZPos = prfPullGripperZPos
            'End If
            'If optPushGrip.Checked Then
            '.GripperType = 2
            '.dblGY = GetPushGripperY(sProfile)
            '.dblGZ = GetPushGripperZ(sProfile)
            '.PushGripperHeight = prfPushGripperHeight
            '.PushGripperWidth = prfPushGripperWidth
            'End If
            '.PushGripperHeight = prfPushGripperHeight
            '.PushGripperWidth = prfPushGripperWidth
            .CreateDrawing()
        End With

    End Sub

#End Region
#Region " Sub - Jog() "

    Sub Jog(ByVal btn As Button, ByVal dAmount As Double, ByVal bAngle As Boolean)

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvMachining.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvMachining.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvMachining.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvMachining.SelectedCells(0).RowIndex
            sr = dgvMachining.Rows(ri)

        End If

        'Jog Value
        Select Case btn.Tag
            Case "V1"
                sr.Cells(6).Value += dAmount
                If bAngle And sr.Cells(6).Value < 0 Then sr.Cells(6).Value += 360
                If bAngle And sr.Cells(6).Value >= 360 Then sr.Cells(6).Value -= 360
            Case "V2"
                sr.Cells(7).Value += dAmount
                If bAngle And sr.Cells(7).Value < 0 Then sr.Cells(7).Value += 360
                If bAngle And sr.Cells(7).Value >= 360 Then sr.Cells(7).Value -= 360
            Case "V3"
                sr.Cells(8).Value += dAmount
                If bAngle And sr.Cells(8).Value < 0 Then sr.Cells(8).Value += 360
                If bAngle And sr.Cells(8).Value >= 360 Then sr.Cells(8).Value -= 360
            Case "V4"
                sr.Cells(9).Value += dAmount
                If bAngle And sr.Cells(9).Value < 0 Then sr.Cells(9).Value += 360
                If bAngle And sr.Cells(9).Value >= 360 Then sr.Cells(9).Value -= 360
            Case "V5"
                sr.Cells(10).Value += dAmount
                If bAngle And sr.Cells(10).Value < 0 Then sr.Cells(10).Value += 360
                If bAngle And sr.Cells(10).Value >= 360 Then sr.Cells(10).Value -= 360
            Case "V6"
                sr.Cells(11).Value += dAmount
                If bAngle And sr.Cells(11).Value < 0 Then sr.Cells(11).Value += 360
                If bAngle And sr.Cells(11).Value >= 360 Then sr.Cells(11).Value -= 360
            Case "V7"
                sr.Cells(12).Value += dAmount
                If bAngle And sr.Cells(12).Value < 0 Then sr.Cells(12).Value += 360
                If bAngle And sr.Cells(12).Value >= 360 Then sr.Cells(12).Value -= 360
            Case "V8"
                sr.Cells(13).Value += dAmount
                If bAngle And sr.Cells(13).Value < 0 Then sr.Cells(13).Value += 360
                If bAngle And sr.Cells(13).Value >= 360 Then sr.Cells(13).Value -= 360
        End Select

        Redraw()

    End Sub

#End Region
#Region " Function - GetOpsStep() "

    Function GetOpsStep() As Double

        If rbOps1.Checked Then Return 0.1
        If rbOps5.Checked Then Return 0.5
        If rbOps10.Checked Then Return 1
        If rbOpsCustom.Checked Then Return nebCustom.Value

        Return 0.5

    End Function

#End Region
#Region " Function - GetNotchStep() "

    Function GetNotchStep() As Double

        If rbNotch1.Checked Then Return 0.1
        If rbNotch25.Checked Then Return 0.25
        If rbNotch5.Checked Then Return 0.5

        Return 0.25

    End Function

#End Region

#Region " Filter - Profile "

    Private Sub txtProfile_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProfile.TextChanged

        FillOperationsGrid(dgvMachining, txtProfile.Text, False, txtToolCode.Text, False, "", False, False)

    End Sub

#End Region
#Region " Filter - ToolCode "

    Private Sub txtToolCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToolCode.TextChanged

        FillOperationsGrid(dgvMachining, txtProfile.Text, False, txtToolCode.Text, False, "", False, False)

    End Sub

#End Region

#Region " Trackbar - Zoom  "

    Private Sub nebZoom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebZoom.ValueChanged

        Redraw()

    End Sub

#End Region
#Region " Button - Zoom Out "

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If nebZoom.Value > 1 Then
            nebZoom.Value -= 1
        End If
    End Sub

    Private Sub PictureBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If nebZoom.Value > 1 Then
            nebZoom.Value -= 1
        End If
    End Sub

#End Region
#Region " Button - Zoom In "

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If nebZoom.Value < 20 Then
            nebZoom.Value += 1
        End If
    End Sub

    Private Sub PictureBox2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If nebZoom.Value < 20 Then
            nebZoom.Value += 1
        End If
    End Sub

#End Region

    Private Sub btnUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndo.Click

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Clear()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Set Default Scale
        'nebZoom.Value = iScale

        'Set Up PosCalc Class
        InitialisePosCalc()

        'Fill Operations Listing
        FillOperationsGrid(dgvMachining, txtProfile.Text, False, txtToolCode.Text, False, "", False, False)
        dgvMachining.AllowUserToAddRows = False

        chkRingClash.Checked = ShowDangerRings

        Me.txtProfile.Focus()

    End Sub

#Region " Function - GetGridRow() "

    Function GetGridRow() As DataGridViewRow

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvMachining.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvMachining.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Return New DataGridViewRow
            Exit Function
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvMachining.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvMachining.SelectedCells(0).RowIndex
            sr = dgvMachining.Rows(ri)

        End If

        Return sr

    End Function

#End Region

End Class