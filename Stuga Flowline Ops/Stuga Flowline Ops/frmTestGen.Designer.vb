﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestGen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTestGen))
        Me.lblProfile = New System.Windows.Forms.Label()
        Me.cmbProfile = New System.Windows.Forms.ComboBox()
        Me.lblOperations = New System.Windows.Forms.Label()
        Me.dgvOperations = New System.Windows.Forms.DataGridView()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.lblBar1 = New System.Windows.Forms.Label()
        Me.lblBar2 = New System.Windows.Forms.Label()
        Me.lblBar3 = New System.Windows.Forms.Label()
        Me.lblBar4 = New System.Windows.Forms.Label()
        Me.lblBar5 = New System.Windows.Forms.Label()
        Me.lblOp = New System.Windows.Forms.Label()
        Me.lblOpData = New System.Windows.Forms.Label()
        Me.lblBar = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.dgvBar1 = New System.Windows.Forms.DataGridView()
        Me.dgvBar2 = New System.Windows.Forms.DataGridView()
        Me.dgvBar3 = New System.Windows.Forms.DataGridView()
        Me.dgvBar4 = New System.Windows.Forms.DataGridView()
        Me.dgvBar5 = New System.Windows.Forms.DataGridView()
        Me.dgvBar6 = New System.Windows.Forms.DataGridView()
        Me.grpBars = New System.Windows.Forms.GroupBox()
        Me.btnBar6 = New System.Windows.Forms.Button()
        Me.btnBar5 = New System.Windows.Forms.Button()
        Me.btnBar4 = New System.Windows.Forms.Button()
        Me.btnBar3 = New System.Windows.Forms.Button()
        Me.btnBar2 = New System.Windows.Forms.Button()
        Me.btnBar1 = New System.Windows.Forms.Button()
        Me.grpPosition = New System.Windows.Forms.GroupBox()
        Me.btn966 = New System.Windows.Forms.Button()
        Me.btn950 = New System.Windows.Forms.Button()
        Me.btn933 = New System.Windows.Forms.Button()
        Me.btn900 = New System.Windows.Forms.Button()
        Me.btn866 = New System.Windows.Forms.Button()
        Me.btn850 = New System.Windows.Forms.Button()
        Me.btn833 = New System.Windows.Forms.Button()
        Me.btn800 = New System.Windows.Forms.Button()
        Me.btn766 = New System.Windows.Forms.Button()
        Me.btn750 = New System.Windows.Forms.Button()
        Me.btn733 = New System.Windows.Forms.Button()
        Me.btn700 = New System.Windows.Forms.Button()
        Me.btn666 = New System.Windows.Forms.Button()
        Me.btn650 = New System.Windows.Forms.Button()
        Me.btn633 = New System.Windows.Forms.Button()
        Me.btn600 = New System.Windows.Forms.Button()
        Me.btn566 = New System.Windows.Forms.Button()
        Me.btn550 = New System.Windows.Forms.Button()
        Me.btn533 = New System.Windows.Forms.Button()
        Me.btn500 = New System.Windows.Forms.Button()
        Me.btn466 = New System.Windows.Forms.Button()
        Me.btn450 = New System.Windows.Forms.Button()
        Me.btn433 = New System.Windows.Forms.Button()
        Me.btn400 = New System.Windows.Forms.Button()
        Me.btn366 = New System.Windows.Forms.Button()
        Me.btn350 = New System.Windows.Forms.Button()
        Me.btn333 = New System.Windows.Forms.Button()
        Me.btn300 = New System.Windows.Forms.Button()
        Me.btn266 = New System.Windows.Forms.Button()
        Me.btn250 = New System.Windows.Forms.Button()
        Me.btn233 = New System.Windows.Forms.Button()
        Me.btn200 = New System.Windows.Forms.Button()
        Me.btn166 = New System.Windows.Forms.Button()
        Me.btn150 = New System.Windows.Forms.Button()
        Me.btn133 = New System.Windows.Forms.Button()
        Me.btn100 = New System.Windows.Forms.Button()
        Me.lblBarData = New System.Windows.Forms.Label()
        Me.lblPos = New System.Windows.Forms.Label()
        Me.grpOpDetail = New System.Windows.Forms.GroupBox()
        Me.txtPosData = New System.Windows.Forms.TextBox()
        Me.btn449 = New System.Windows.Forms.Button()
        Me.lblBar6 = New System.Windows.Forms.Label()
        CType(Me.dgvOperations, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBar6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBars.SuspendLayout()
        Me.grpPosition.SuspendLayout()
        Me.grpOpDetail.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblProfile
        '
        Me.lblProfile.AutoSize = True
        Me.lblProfile.BackColor = System.Drawing.Color.Navy
        Me.lblProfile.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfile.ForeColor = System.Drawing.Color.White
        Me.lblProfile.Location = New System.Drawing.Point(13, 9)
        Me.lblProfile.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProfile.Name = "lblProfile"
        Me.lblProfile.Size = New System.Drawing.Size(48, 16)
        Me.lblProfile.TabIndex = 0
        Me.lblProfile.Text = "Profile"
        '
        'cmbProfile
        '
        Me.cmbProfile.FormattingEnabled = True
        Me.cmbProfile.Location = New System.Drawing.Point(99, 6)
        Me.cmbProfile.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbProfile.Name = "cmbProfile"
        Me.cmbProfile.Size = New System.Drawing.Size(530, 24)
        Me.cmbProfile.TabIndex = 1
        '
        'lblOperations
        '
        Me.lblOperations.AutoSize = True
        Me.lblOperations.BackColor = System.Drawing.Color.Navy
        Me.lblOperations.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperations.ForeColor = System.Drawing.Color.White
        Me.lblOperations.Location = New System.Drawing.Point(16, 46)
        Me.lblOperations.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOperations.Name = "lblOperations"
        Me.lblOperations.Size = New System.Drawing.Size(75, 16)
        Me.lblOperations.TabIndex = 2
        Me.lblOperations.Text = "Operations"
        '
        'dgvOperations
        '
        Me.dgvOperations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOperations.Location = New System.Drawing.Point(16, 69)
        Me.dgvOperations.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvOperations.Name = "dgvOperations"
        Me.dgvOperations.Size = New System.Drawing.Size(615, 314)
        Me.dgvOperations.TabIndex = 3
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.White
        Me.btnClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnClear.Location = New System.Drawing.Point(274, 16)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(79, 60)
        Me.btnClear.TabIndex = 4
        Me.btnClear.Text = "Clear All"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'lblBar1
        '
        Me.lblBar1.AutoSize = True
        Me.lblBar1.BackColor = System.Drawing.Color.Navy
        Me.lblBar1.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar1.ForeColor = System.Drawing.Color.White
        Me.lblBar1.Location = New System.Drawing.Point(638, 8)
        Me.lblBar1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar1.Name = "lblBar1"
        Me.lblBar1.Size = New System.Drawing.Size(36, 16)
        Me.lblBar1.TabIndex = 5
        Me.lblBar1.Text = "Bar1"
        '
        'lblBar2
        '
        Me.lblBar2.AutoSize = True
        Me.lblBar2.BackColor = System.Drawing.Color.Navy
        Me.lblBar2.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar2.ForeColor = System.Drawing.Color.White
        Me.lblBar2.Location = New System.Drawing.Point(638, 126)
        Me.lblBar2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar2.Name = "lblBar2"
        Me.lblBar2.Size = New System.Drawing.Size(36, 16)
        Me.lblBar2.TabIndex = 6
        Me.lblBar2.Text = "Bar2"
        '
        'lblBar3
        '
        Me.lblBar3.AutoSize = True
        Me.lblBar3.BackColor = System.Drawing.Color.Navy
        Me.lblBar3.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar3.ForeColor = System.Drawing.Color.White
        Me.lblBar3.Location = New System.Drawing.Point(638, 244)
        Me.lblBar3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar3.Name = "lblBar3"
        Me.lblBar3.Size = New System.Drawing.Size(36, 16)
        Me.lblBar3.TabIndex = 7
        Me.lblBar3.Text = "Bar3"
        '
        'lblBar4
        '
        Me.lblBar4.AutoSize = True
        Me.lblBar4.BackColor = System.Drawing.Color.Navy
        Me.lblBar4.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar4.ForeColor = System.Drawing.Color.White
        Me.lblBar4.Location = New System.Drawing.Point(638, 360)
        Me.lblBar4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar4.Name = "lblBar4"
        Me.lblBar4.Size = New System.Drawing.Size(36, 16)
        Me.lblBar4.TabIndex = 8
        Me.lblBar4.Text = "Bar4"
        '
        'lblBar5
        '
        Me.lblBar5.AutoSize = True
        Me.lblBar5.BackColor = System.Drawing.Color.Navy
        Me.lblBar5.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar5.ForeColor = System.Drawing.Color.White
        Me.lblBar5.Location = New System.Drawing.Point(638, 478)
        Me.lblBar5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar5.Name = "lblBar5"
        Me.lblBar5.Size = New System.Drawing.Size(36, 16)
        Me.lblBar5.TabIndex = 9
        Me.lblBar5.Text = "Bar5"
        '
        'lblOp
        '
        Me.lblOp.BackColor = System.Drawing.Color.Navy
        Me.lblOp.ForeColor = System.Drawing.Color.White
        Me.lblOp.Location = New System.Drawing.Point(9, 19)
        Me.lblOp.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOp.Name = "lblOp"
        Me.lblOp.Size = New System.Drawing.Size(69, 20)
        Me.lblOp.TabIndex = 11
        Me.lblOp.Text = "Operation"
        '
        'lblOpData
        '
        Me.lblOpData.BackColor = System.Drawing.Color.White
        Me.lblOpData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOpData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOpData.Location = New System.Drawing.Point(8, 44)
        Me.lblOpData.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOpData.Name = "lblOpData"
        Me.lblOpData.Size = New System.Drawing.Size(166, 20)
        Me.lblOpData.TabIndex = 12
        Me.lblOpData.Text = "?"
        '
        'lblBar
        '
        Me.lblBar.BackColor = System.Drawing.Color.Navy
        Me.lblBar.ForeColor = System.Drawing.Color.White
        Me.lblBar.Location = New System.Drawing.Point(179, 19)
        Me.lblBar.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar.Name = "lblBar"
        Me.lblBar.Size = New System.Drawing.Size(31, 20)
        Me.lblBar.TabIndex = 13
        Me.lblBar.Text = "Bar"
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAdd.Location = New System.Drawing.Point(361, 16)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(78, 61)
        Me.btnAdd.TabIndex = 14
        Me.btnAdd.Text = "Add Operation"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'dgvBar1
        '
        Me.dgvBar1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar1.Location = New System.Drawing.Point(682, 6)
        Me.dgvBar1.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar1.Name = "dgvBar1"
        Me.dgvBar1.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar1.TabIndex = 15
        '
        'dgvBar2
        '
        Me.dgvBar2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar2.Location = New System.Drawing.Point(682, 124)
        Me.dgvBar2.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar2.Name = "dgvBar2"
        Me.dgvBar2.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar2.TabIndex = 16
        '
        'dgvBar3
        '
        Me.dgvBar3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar3.Location = New System.Drawing.Point(682, 242)
        Me.dgvBar3.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar3.Name = "dgvBar3"
        Me.dgvBar3.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar3.TabIndex = 17
        '
        'dgvBar4
        '
        Me.dgvBar4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar4.Location = New System.Drawing.Point(682, 360)
        Me.dgvBar4.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar4.Name = "dgvBar4"
        Me.dgvBar4.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar4.TabIndex = 18
        '
        'dgvBar5
        '
        Me.dgvBar5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar5.Location = New System.Drawing.Point(682, 478)
        Me.dgvBar5.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar5.Name = "dgvBar5"
        Me.dgvBar5.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar5.TabIndex = 19
        '
        'dgvBar6
        '
        Me.dgvBar6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBar6.Location = New System.Drawing.Point(682, 596)
        Me.dgvBar6.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvBar6.Name = "dgvBar6"
        Me.dgvBar6.Size = New System.Drawing.Size(297, 110)
        Me.dgvBar6.TabIndex = 20
        '
        'grpBars
        '
        Me.grpBars.Controls.Add(Me.btnBar6)
        Me.grpBars.Controls.Add(Me.btnBar5)
        Me.grpBars.Controls.Add(Me.btnBar4)
        Me.grpBars.Controls.Add(Me.btnBar3)
        Me.grpBars.Controls.Add(Me.btnBar2)
        Me.grpBars.Controls.Add(Me.btnBar1)
        Me.grpBars.ForeColor = System.Drawing.Color.White
        Me.grpBars.Location = New System.Drawing.Point(16, 394)
        Me.grpBars.Margin = New System.Windows.Forms.Padding(4)
        Me.grpBars.Name = "grpBars"
        Me.grpBars.Padding = New System.Windows.Forms.Padding(4)
        Me.grpBars.Size = New System.Drawing.Size(75, 309)
        Me.grpBars.TabIndex = 21
        Me.grpBars.TabStop = False
        Me.grpBars.Text = "Bar"
        '
        'btnBar6
        '
        Me.btnBar6.BackColor = System.Drawing.Color.White
        Me.btnBar6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar6.Location = New System.Drawing.Point(11, 258)
        Me.btnBar6.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar6.Name = "btnBar6"
        Me.btnBar6.Size = New System.Drawing.Size(50, 40)
        Me.btnBar6.TabIndex = 20
        Me.btnBar6.Text = "6"
        Me.btnBar6.UseVisualStyleBackColor = False
        '
        'btnBar5
        '
        Me.btnBar5.BackColor = System.Drawing.Color.White
        Me.btnBar5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar5.Location = New System.Drawing.Point(11, 210)
        Me.btnBar5.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar5.Name = "btnBar5"
        Me.btnBar5.Size = New System.Drawing.Size(50, 40)
        Me.btnBar5.TabIndex = 19
        Me.btnBar5.Text = "5"
        Me.btnBar5.UseVisualStyleBackColor = False
        '
        'btnBar4
        '
        Me.btnBar4.BackColor = System.Drawing.Color.White
        Me.btnBar4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar4.Location = New System.Drawing.Point(11, 162)
        Me.btnBar4.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar4.Name = "btnBar4"
        Me.btnBar4.Size = New System.Drawing.Size(50, 40)
        Me.btnBar4.TabIndex = 18
        Me.btnBar4.Text = "4"
        Me.btnBar4.UseVisualStyleBackColor = False
        '
        'btnBar3
        '
        Me.btnBar3.BackColor = System.Drawing.Color.White
        Me.btnBar3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar3.Location = New System.Drawing.Point(11, 114)
        Me.btnBar3.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar3.Name = "btnBar3"
        Me.btnBar3.Size = New System.Drawing.Size(50, 40)
        Me.btnBar3.TabIndex = 17
        Me.btnBar3.Text = "3"
        Me.btnBar3.UseVisualStyleBackColor = False
        '
        'btnBar2
        '
        Me.btnBar2.BackColor = System.Drawing.Color.White
        Me.btnBar2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar2.Location = New System.Drawing.Point(11, 66)
        Me.btnBar2.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar2.Name = "btnBar2"
        Me.btnBar2.Size = New System.Drawing.Size(50, 40)
        Me.btnBar2.TabIndex = 16
        Me.btnBar2.Text = "2"
        Me.btnBar2.UseVisualStyleBackColor = False
        '
        'btnBar1
        '
        Me.btnBar1.BackColor = System.Drawing.Color.White
        Me.btnBar1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBar1.Location = New System.Drawing.Point(11, 18)
        Me.btnBar1.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBar1.Name = "btnBar1"
        Me.btnBar1.Size = New System.Drawing.Size(50, 40)
        Me.btnBar1.TabIndex = 15
        Me.btnBar1.Text = "1"
        Me.btnBar1.UseVisualStyleBackColor = False
        '
        'grpPosition
        '
        Me.grpPosition.Controls.Add(Me.btn966)
        Me.grpPosition.Controls.Add(Me.btn950)
        Me.grpPosition.Controls.Add(Me.btn933)
        Me.grpPosition.Controls.Add(Me.btn900)
        Me.grpPosition.Controls.Add(Me.btn866)
        Me.grpPosition.Controls.Add(Me.btn850)
        Me.grpPosition.Controls.Add(Me.btn833)
        Me.grpPosition.Controls.Add(Me.btn800)
        Me.grpPosition.Controls.Add(Me.btn766)
        Me.grpPosition.Controls.Add(Me.btn750)
        Me.grpPosition.Controls.Add(Me.btn733)
        Me.grpPosition.Controls.Add(Me.btn700)
        Me.grpPosition.Controls.Add(Me.btn666)
        Me.grpPosition.Controls.Add(Me.btn650)
        Me.grpPosition.Controls.Add(Me.btn633)
        Me.grpPosition.Controls.Add(Me.btn600)
        Me.grpPosition.Controls.Add(Me.btn566)
        Me.grpPosition.Controls.Add(Me.btn550)
        Me.grpPosition.Controls.Add(Me.btn533)
        Me.grpPosition.Controls.Add(Me.btn500)
        Me.grpPosition.Controls.Add(Me.btn466)
        Me.grpPosition.Controls.Add(Me.btn450)
        Me.grpPosition.Controls.Add(Me.btn433)
        Me.grpPosition.Controls.Add(Me.btn400)
        Me.grpPosition.Controls.Add(Me.btn366)
        Me.grpPosition.Controls.Add(Me.btn350)
        Me.grpPosition.Controls.Add(Me.btn333)
        Me.grpPosition.Controls.Add(Me.btn300)
        Me.grpPosition.Controls.Add(Me.btn266)
        Me.grpPosition.Controls.Add(Me.btn250)
        Me.grpPosition.Controls.Add(Me.btn233)
        Me.grpPosition.Controls.Add(Me.btn200)
        Me.grpPosition.Controls.Add(Me.btn166)
        Me.grpPosition.Controls.Add(Me.btn150)
        Me.grpPosition.Controls.Add(Me.btn133)
        Me.grpPosition.Controls.Add(Me.btn100)
        Me.grpPosition.ForeColor = System.Drawing.Color.White
        Me.grpPosition.Location = New System.Drawing.Point(99, 393)
        Me.grpPosition.Margin = New System.Windows.Forms.Padding(4)
        Me.grpPosition.Name = "grpPosition"
        Me.grpPosition.Padding = New System.Windows.Forms.Padding(4)
        Me.grpPosition.Size = New System.Drawing.Size(532, 215)
        Me.grpPosition.TabIndex = 22
        Me.grpPosition.TabStop = False
        Me.grpPosition.Text = "Position"
        '
        'btn966
        '
        Me.btn966.BackColor = System.Drawing.Color.White
        Me.btn966.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn966.Location = New System.Drawing.Point(470, 163)
        Me.btn966.Margin = New System.Windows.Forms.Padding(4)
        Me.btn966.Name = "btn966"
        Me.btn966.Size = New System.Drawing.Size(50, 40)
        Me.btn966.TabIndex = 51
        Me.btn966.Text = "966"
        Me.btn966.UseVisualStyleBackColor = False
        '
        'btn950
        '
        Me.btn950.BackColor = System.Drawing.Color.White
        Me.btn950.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn950.Location = New System.Drawing.Point(470, 116)
        Me.btn950.Margin = New System.Windows.Forms.Padding(4)
        Me.btn950.Name = "btn950"
        Me.btn950.Size = New System.Drawing.Size(50, 40)
        Me.btn950.TabIndex = 50
        Me.btn950.Text = "950"
        Me.btn950.UseVisualStyleBackColor = False
        '
        'btn933
        '
        Me.btn933.BackColor = System.Drawing.Color.White
        Me.btn933.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn933.Location = New System.Drawing.Point(470, 68)
        Me.btn933.Margin = New System.Windows.Forms.Padding(4)
        Me.btn933.Name = "btn933"
        Me.btn933.Size = New System.Drawing.Size(50, 40)
        Me.btn933.TabIndex = 49
        Me.btn933.Text = "933"
        Me.btn933.UseVisualStyleBackColor = False
        '
        'btn900
        '
        Me.btn900.BackColor = System.Drawing.Color.White
        Me.btn900.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn900.Location = New System.Drawing.Point(470, 19)
        Me.btn900.Margin = New System.Windows.Forms.Padding(4)
        Me.btn900.Name = "btn900"
        Me.btn900.Size = New System.Drawing.Size(50, 40)
        Me.btn900.TabIndex = 48
        Me.btn900.Text = "900"
        Me.btn900.UseVisualStyleBackColor = False
        '
        'btn866
        '
        Me.btn866.BackColor = System.Drawing.Color.White
        Me.btn866.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn866.Location = New System.Drawing.Point(412, 163)
        Me.btn866.Margin = New System.Windows.Forms.Padding(4)
        Me.btn866.Name = "btn866"
        Me.btn866.Size = New System.Drawing.Size(50, 40)
        Me.btn866.TabIndex = 47
        Me.btn866.Text = "866"
        Me.btn866.UseVisualStyleBackColor = False
        '
        'btn850
        '
        Me.btn850.BackColor = System.Drawing.Color.White
        Me.btn850.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn850.Location = New System.Drawing.Point(412, 115)
        Me.btn850.Margin = New System.Windows.Forms.Padding(4)
        Me.btn850.Name = "btn850"
        Me.btn850.Size = New System.Drawing.Size(50, 40)
        Me.btn850.TabIndex = 46
        Me.btn850.Text = "850"
        Me.btn850.UseVisualStyleBackColor = False
        '
        'btn833
        '
        Me.btn833.BackColor = System.Drawing.Color.White
        Me.btn833.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn833.Location = New System.Drawing.Point(412, 68)
        Me.btn833.Margin = New System.Windows.Forms.Padding(4)
        Me.btn833.Name = "btn833"
        Me.btn833.Size = New System.Drawing.Size(50, 40)
        Me.btn833.TabIndex = 45
        Me.btn833.Text = "833"
        Me.btn833.UseVisualStyleBackColor = False
        '
        'btn800
        '
        Me.btn800.BackColor = System.Drawing.Color.White
        Me.btn800.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn800.Location = New System.Drawing.Point(412, 19)
        Me.btn800.Margin = New System.Windows.Forms.Padding(4)
        Me.btn800.Name = "btn800"
        Me.btn800.Size = New System.Drawing.Size(50, 40)
        Me.btn800.TabIndex = 44
        Me.btn800.Text = "800"
        Me.btn800.UseVisualStyleBackColor = False
        '
        'btn766
        '
        Me.btn766.BackColor = System.Drawing.Color.White
        Me.btn766.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn766.Location = New System.Drawing.Point(354, 164)
        Me.btn766.Margin = New System.Windows.Forms.Padding(4)
        Me.btn766.Name = "btn766"
        Me.btn766.Size = New System.Drawing.Size(50, 40)
        Me.btn766.TabIndex = 43
        Me.btn766.Text = "766"
        Me.btn766.UseVisualStyleBackColor = False
        '
        'btn750
        '
        Me.btn750.BackColor = System.Drawing.Color.White
        Me.btn750.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn750.Location = New System.Drawing.Point(354, 116)
        Me.btn750.Margin = New System.Windows.Forms.Padding(4)
        Me.btn750.Name = "btn750"
        Me.btn750.Size = New System.Drawing.Size(50, 40)
        Me.btn750.TabIndex = 42
        Me.btn750.Text = "750"
        Me.btn750.UseVisualStyleBackColor = False
        '
        'btn733
        '
        Me.btn733.BackColor = System.Drawing.Color.White
        Me.btn733.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn733.Location = New System.Drawing.Point(354, 68)
        Me.btn733.Margin = New System.Windows.Forms.Padding(4)
        Me.btn733.Name = "btn733"
        Me.btn733.Size = New System.Drawing.Size(50, 40)
        Me.btn733.TabIndex = 41
        Me.btn733.Text = "733"
        Me.btn733.UseVisualStyleBackColor = False
        '
        'btn700
        '
        Me.btn700.BackColor = System.Drawing.Color.White
        Me.btn700.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn700.Location = New System.Drawing.Point(354, 20)
        Me.btn700.Margin = New System.Windows.Forms.Padding(4)
        Me.btn700.Name = "btn700"
        Me.btn700.Size = New System.Drawing.Size(50, 40)
        Me.btn700.TabIndex = 40
        Me.btn700.Text = "700"
        Me.btn700.UseVisualStyleBackColor = False
        '
        'btn666
        '
        Me.btn666.BackColor = System.Drawing.Color.White
        Me.btn666.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn666.Location = New System.Drawing.Point(299, 163)
        Me.btn666.Margin = New System.Windows.Forms.Padding(4)
        Me.btn666.Name = "btn666"
        Me.btn666.Size = New System.Drawing.Size(50, 40)
        Me.btn666.TabIndex = 39
        Me.btn666.Text = "666"
        Me.btn666.UseVisualStyleBackColor = False
        '
        'btn650
        '
        Me.btn650.BackColor = System.Drawing.Color.White
        Me.btn650.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn650.Location = New System.Drawing.Point(299, 116)
        Me.btn650.Margin = New System.Windows.Forms.Padding(4)
        Me.btn650.Name = "btn650"
        Me.btn650.Size = New System.Drawing.Size(50, 40)
        Me.btn650.TabIndex = 38
        Me.btn650.Text = "650"
        Me.btn650.UseVisualStyleBackColor = False
        '
        'btn633
        '
        Me.btn633.BackColor = System.Drawing.Color.White
        Me.btn633.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn633.Location = New System.Drawing.Point(299, 68)
        Me.btn633.Margin = New System.Windows.Forms.Padding(4)
        Me.btn633.Name = "btn633"
        Me.btn633.Size = New System.Drawing.Size(50, 40)
        Me.btn633.TabIndex = 37
        Me.btn633.Text = "633"
        Me.btn633.UseVisualStyleBackColor = False
        '
        'btn600
        '
        Me.btn600.BackColor = System.Drawing.Color.White
        Me.btn600.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn600.Location = New System.Drawing.Point(296, 20)
        Me.btn600.Margin = New System.Windows.Forms.Padding(4)
        Me.btn600.Name = "btn600"
        Me.btn600.Size = New System.Drawing.Size(50, 40)
        Me.btn600.TabIndex = 36
        Me.btn600.Text = "600"
        Me.btn600.UseVisualStyleBackColor = False
        '
        'btn566
        '
        Me.btn566.BackColor = System.Drawing.Color.White
        Me.btn566.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn566.Location = New System.Drawing.Point(238, 163)
        Me.btn566.Margin = New System.Windows.Forms.Padding(4)
        Me.btn566.Name = "btn566"
        Me.btn566.Size = New System.Drawing.Size(50, 40)
        Me.btn566.TabIndex = 35
        Me.btn566.Text = "566"
        Me.btn566.UseVisualStyleBackColor = False
        '
        'btn550
        '
        Me.btn550.BackColor = System.Drawing.Color.White
        Me.btn550.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn550.Location = New System.Drawing.Point(238, 115)
        Me.btn550.Margin = New System.Windows.Forms.Padding(4)
        Me.btn550.Name = "btn550"
        Me.btn550.Size = New System.Drawing.Size(50, 40)
        Me.btn550.TabIndex = 34
        Me.btn550.Text = "550"
        Me.btn550.UseVisualStyleBackColor = False
        '
        'btn533
        '
        Me.btn533.BackColor = System.Drawing.Color.White
        Me.btn533.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn533.Location = New System.Drawing.Point(238, 68)
        Me.btn533.Margin = New System.Windows.Forms.Padding(4)
        Me.btn533.Name = "btn533"
        Me.btn533.Size = New System.Drawing.Size(50, 40)
        Me.btn533.TabIndex = 33
        Me.btn533.Text = "533"
        Me.btn533.UseVisualStyleBackColor = False
        '
        'btn500
        '
        Me.btn500.BackColor = System.Drawing.Color.White
        Me.btn500.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn500.Location = New System.Drawing.Point(238, 20)
        Me.btn500.Margin = New System.Windows.Forms.Padding(4)
        Me.btn500.Name = "btn500"
        Me.btn500.Size = New System.Drawing.Size(50, 40)
        Me.btn500.TabIndex = 32
        Me.btn500.Text = "500"
        Me.btn500.UseVisualStyleBackColor = False
        '
        'btn466
        '
        Me.btn466.BackColor = System.Drawing.Color.White
        Me.btn466.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn466.Location = New System.Drawing.Point(182, 163)
        Me.btn466.Margin = New System.Windows.Forms.Padding(4)
        Me.btn466.Name = "btn466"
        Me.btn466.Size = New System.Drawing.Size(50, 40)
        Me.btn466.TabIndex = 31
        Me.btn466.Text = "466"
        Me.btn466.UseVisualStyleBackColor = False
        '
        'btn450
        '
        Me.btn450.BackColor = System.Drawing.Color.White
        Me.btn450.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn450.Location = New System.Drawing.Point(182, 115)
        Me.btn450.Margin = New System.Windows.Forms.Padding(4)
        Me.btn450.Name = "btn450"
        Me.btn450.Size = New System.Drawing.Size(50, 40)
        Me.btn450.TabIndex = 30
        Me.btn450.Text = "450"
        Me.btn450.UseVisualStyleBackColor = False
        '
        'btn433
        '
        Me.btn433.BackColor = System.Drawing.Color.White
        Me.btn433.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn433.Location = New System.Drawing.Point(182, 66)
        Me.btn433.Margin = New System.Windows.Forms.Padding(4)
        Me.btn433.Name = "btn433"
        Me.btn433.Size = New System.Drawing.Size(50, 40)
        Me.btn433.TabIndex = 29
        Me.btn433.Text = "433"
        Me.btn433.UseVisualStyleBackColor = False
        '
        'btn400
        '
        Me.btn400.BackColor = System.Drawing.Color.White
        Me.btn400.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn400.Location = New System.Drawing.Point(182, 20)
        Me.btn400.Margin = New System.Windows.Forms.Padding(4)
        Me.btn400.Name = "btn400"
        Me.btn400.Size = New System.Drawing.Size(50, 40)
        Me.btn400.TabIndex = 28
        Me.btn400.Text = "400"
        Me.btn400.UseVisualStyleBackColor = False
        '
        'btn366
        '
        Me.btn366.BackColor = System.Drawing.Color.White
        Me.btn366.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn366.Location = New System.Drawing.Point(124, 163)
        Me.btn366.Margin = New System.Windows.Forms.Padding(4)
        Me.btn366.Name = "btn366"
        Me.btn366.Size = New System.Drawing.Size(50, 40)
        Me.btn366.TabIndex = 27
        Me.btn366.Text = "366"
        Me.btn366.UseVisualStyleBackColor = False
        '
        'btn350
        '
        Me.btn350.BackColor = System.Drawing.Color.White
        Me.btn350.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn350.Location = New System.Drawing.Point(124, 115)
        Me.btn350.Margin = New System.Windows.Forms.Padding(4)
        Me.btn350.Name = "btn350"
        Me.btn350.Size = New System.Drawing.Size(50, 40)
        Me.btn350.TabIndex = 26
        Me.btn350.Text = "350"
        Me.btn350.UseVisualStyleBackColor = False
        '
        'btn333
        '
        Me.btn333.BackColor = System.Drawing.Color.White
        Me.btn333.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn333.Location = New System.Drawing.Point(124, 66)
        Me.btn333.Margin = New System.Windows.Forms.Padding(4)
        Me.btn333.Name = "btn333"
        Me.btn333.Size = New System.Drawing.Size(50, 40)
        Me.btn333.TabIndex = 25
        Me.btn333.Text = "333"
        Me.btn333.UseVisualStyleBackColor = False
        '
        'btn300
        '
        Me.btn300.BackColor = System.Drawing.Color.White
        Me.btn300.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn300.Location = New System.Drawing.Point(124, 20)
        Me.btn300.Margin = New System.Windows.Forms.Padding(4)
        Me.btn300.Name = "btn300"
        Me.btn300.Size = New System.Drawing.Size(50, 40)
        Me.btn300.TabIndex = 24
        Me.btn300.Text = "300"
        Me.btn300.UseVisualStyleBackColor = False
        '
        'btn266
        '
        Me.btn266.BackColor = System.Drawing.Color.White
        Me.btn266.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn266.Location = New System.Drawing.Point(66, 163)
        Me.btn266.Margin = New System.Windows.Forms.Padding(4)
        Me.btn266.Name = "btn266"
        Me.btn266.Size = New System.Drawing.Size(50, 40)
        Me.btn266.TabIndex = 23
        Me.btn266.Text = "266"
        Me.btn266.UseVisualStyleBackColor = False
        '
        'btn250
        '
        Me.btn250.BackColor = System.Drawing.Color.White
        Me.btn250.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn250.Location = New System.Drawing.Point(66, 115)
        Me.btn250.Margin = New System.Windows.Forms.Padding(4)
        Me.btn250.Name = "btn250"
        Me.btn250.Size = New System.Drawing.Size(50, 40)
        Me.btn250.TabIndex = 22
        Me.btn250.Text = "250"
        Me.btn250.UseVisualStyleBackColor = False
        '
        'btn233
        '
        Me.btn233.BackColor = System.Drawing.Color.White
        Me.btn233.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn233.Location = New System.Drawing.Point(66, 66)
        Me.btn233.Margin = New System.Windows.Forms.Padding(4)
        Me.btn233.Name = "btn233"
        Me.btn233.Size = New System.Drawing.Size(50, 40)
        Me.btn233.TabIndex = 21
        Me.btn233.Text = "233"
        Me.btn233.UseVisualStyleBackColor = False
        '
        'btn200
        '
        Me.btn200.BackColor = System.Drawing.Color.White
        Me.btn200.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn200.Location = New System.Drawing.Point(66, 20)
        Me.btn200.Margin = New System.Windows.Forms.Padding(4)
        Me.btn200.Name = "btn200"
        Me.btn200.Size = New System.Drawing.Size(50, 40)
        Me.btn200.TabIndex = 20
        Me.btn200.Text = "200"
        Me.btn200.UseVisualStyleBackColor = False
        '
        'btn166
        '
        Me.btn166.BackColor = System.Drawing.Color.White
        Me.btn166.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn166.Location = New System.Drawing.Point(8, 163)
        Me.btn166.Margin = New System.Windows.Forms.Padding(4)
        Me.btn166.Name = "btn166"
        Me.btn166.Size = New System.Drawing.Size(50, 40)
        Me.btn166.TabIndex = 19
        Me.btn166.Text = "166"
        Me.btn166.UseVisualStyleBackColor = False
        '
        'btn150
        '
        Me.btn150.BackColor = System.Drawing.Color.White
        Me.btn150.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn150.Location = New System.Drawing.Point(8, 115)
        Me.btn150.Margin = New System.Windows.Forms.Padding(4)
        Me.btn150.Name = "btn150"
        Me.btn150.Size = New System.Drawing.Size(50, 40)
        Me.btn150.TabIndex = 18
        Me.btn150.Text = "150"
        Me.btn150.UseVisualStyleBackColor = False
        '
        'btn133
        '
        Me.btn133.BackColor = System.Drawing.Color.White
        Me.btn133.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn133.Location = New System.Drawing.Point(8, 67)
        Me.btn133.Margin = New System.Windows.Forms.Padding(4)
        Me.btn133.Name = "btn133"
        Me.btn133.Size = New System.Drawing.Size(50, 40)
        Me.btn133.TabIndex = 17
        Me.btn133.Text = "133"
        Me.btn133.UseVisualStyleBackColor = False
        '
        'btn100
        '
        Me.btn100.BackColor = System.Drawing.Color.White
        Me.btn100.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn100.Location = New System.Drawing.Point(8, 20)
        Me.btn100.Margin = New System.Windows.Forms.Padding(4)
        Me.btn100.Name = "btn100"
        Me.btn100.Size = New System.Drawing.Size(50, 40)
        Me.btn100.TabIndex = 16
        Me.btn100.Text = "100"
        Me.btn100.UseVisualStyleBackColor = False
        '
        'lblBarData
        '
        Me.lblBarData.BackColor = System.Drawing.Color.White
        Me.lblBarData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBarData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBarData.Location = New System.Drawing.Point(218, 19)
        Me.lblBarData.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBarData.Name = "lblBarData"
        Me.lblBarData.Size = New System.Drawing.Size(44, 20)
        Me.lblBarData.TabIndex = 23
        Me.lblBarData.Text = "1"
        '
        'lblPos
        '
        Me.lblPos.BackColor = System.Drawing.Color.Navy
        Me.lblPos.ForeColor = System.Drawing.Color.White
        Me.lblPos.Location = New System.Drawing.Point(180, 45)
        Me.lblPos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPos.Name = "lblPos"
        Me.lblPos.Size = New System.Drawing.Size(30, 20)
        Me.lblPos.TabIndex = 24
        Me.lblPos.Text = "Pos"
        '
        'grpOpDetail
        '
        Me.grpOpDetail.Controls.Add(Me.txtPosData)
        Me.grpOpDetail.Controls.Add(Me.lblOp)
        Me.grpOpDetail.Controls.Add(Me.lblOpData)
        Me.grpOpDetail.Controls.Add(Me.lblPos)
        Me.grpOpDetail.Controls.Add(Me.lblBar)
        Me.grpOpDetail.Controls.Add(Me.lblBarData)
        Me.grpOpDetail.Controls.Add(Me.btnAdd)
        Me.grpOpDetail.Controls.Add(Me.btnClear)
        Me.grpOpDetail.ForeColor = System.Drawing.Color.White
        Me.grpOpDetail.Location = New System.Drawing.Point(99, 616)
        Me.grpOpDetail.Margin = New System.Windows.Forms.Padding(4)
        Me.grpOpDetail.Name = "grpOpDetail"
        Me.grpOpDetail.Padding = New System.Windows.Forms.Padding(4)
        Me.grpOpDetail.Size = New System.Drawing.Size(445, 87)
        Me.grpOpDetail.TabIndex = 26
        Me.grpOpDetail.TabStop = False
        Me.grpOpDetail.Text = "Operation Detail"
        '
        'txtPosData
        '
        Me.txtPosData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPosData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPosData.Location = New System.Drawing.Point(217, 45)
        Me.txtPosData.Name = "txtPosData"
        Me.txtPosData.Size = New System.Drawing.Size(45, 22)
        Me.txtPosData.TabIndex = 27
        Me.txtPosData.Text = "400"
        '
        'btn449
        '
        Me.btn449.BackColor = System.Drawing.Color.White
        Me.btn449.Location = New System.Drawing.Point(553, 632)
        Me.btn449.Margin = New System.Windows.Forms.Padding(4)
        Me.btn449.Name = "btn449"
        Me.btn449.Size = New System.Drawing.Size(121, 61)
        Me.btn449.TabIndex = 26
        Me.btn449.Text = "Create Batch"
        Me.btn449.UseVisualStyleBackColor = False
        '
        'lblBar6
        '
        Me.lblBar6.AutoSize = True
        Me.lblBar6.BackColor = System.Drawing.Color.Navy
        Me.lblBar6.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBar6.ForeColor = System.Drawing.Color.White
        Me.lblBar6.Location = New System.Drawing.Point(638, 596)
        Me.lblBar6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBar6.Name = "lblBar6"
        Me.lblBar6.Size = New System.Drawing.Size(36, 16)
        Me.lblBar6.TabIndex = 27
        Me.lblBar6.Text = "Bar6"
        '
        'frmTestGen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Navy
        Me.ClientSize = New System.Drawing.Size(992, 716)
        Me.Controls.Add(Me.lblBar6)
        Me.Controls.Add(Me.btn449)
        Me.Controls.Add(Me.grpOpDetail)
        Me.Controls.Add(Me.grpPosition)
        Me.Controls.Add(Me.grpBars)
        Me.Controls.Add(Me.dgvBar6)
        Me.Controls.Add(Me.dgvBar5)
        Me.Controls.Add(Me.dgvBar4)
        Me.Controls.Add(Me.dgvBar3)
        Me.Controls.Add(Me.dgvBar2)
        Me.Controls.Add(Me.dgvBar1)
        Me.Controls.Add(Me.lblBar5)
        Me.Controls.Add(Me.lblBar4)
        Me.Controls.Add(Me.lblBar3)
        Me.Controls.Add(Me.lblBar2)
        Me.Controls.Add(Me.lblBar1)
        Me.Controls.Add(Me.dgvOperations)
        Me.Controls.Add(Me.lblOperations)
        Me.Controls.Add(Me.cmbProfile)
        Me.Controls.Add(Me.lblProfile)
        Me.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmTestGen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Test Generator"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvOperations, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBar6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBars.ResumeLayout(False)
        Me.grpPosition.ResumeLayout(False)
        Me.grpOpDetail.ResumeLayout(False)
        Me.grpOpDetail.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblProfile As System.Windows.Forms.Label
    Friend WithEvents cmbProfile As System.Windows.Forms.ComboBox
    Friend WithEvents lblOperations As System.Windows.Forms.Label
    Friend WithEvents dgvOperations As System.Windows.Forms.DataGridView
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents lblBar1 As System.Windows.Forms.Label
    Friend WithEvents lblBar2 As System.Windows.Forms.Label
    Friend WithEvents lblBar3 As System.Windows.Forms.Label
    Friend WithEvents lblBar4 As System.Windows.Forms.Label
    Friend WithEvents lblBar5 As System.Windows.Forms.Label
    Friend WithEvents lblOp As System.Windows.Forms.Label
    Friend WithEvents lblOpData As System.Windows.Forms.Label
    Friend WithEvents lblBar As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvBar1 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBar2 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBar3 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBar4 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBar5 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBar6 As System.Windows.Forms.DataGridView
    Friend WithEvents grpBars As System.Windows.Forms.GroupBox
    Friend WithEvents btnBar6 As System.Windows.Forms.Button
    Friend WithEvents btnBar5 As System.Windows.Forms.Button
    Friend WithEvents btnBar4 As System.Windows.Forms.Button
    Friend WithEvents btnBar3 As System.Windows.Forms.Button
    Friend WithEvents btnBar2 As System.Windows.Forms.Button
    Friend WithEvents btnBar1 As System.Windows.Forms.Button
    Friend WithEvents grpPosition As System.Windows.Forms.GroupBox
    Friend WithEvents btn966 As System.Windows.Forms.Button
    Friend WithEvents btn950 As System.Windows.Forms.Button
    Friend WithEvents btn933 As System.Windows.Forms.Button
    Friend WithEvents btn900 As System.Windows.Forms.Button
    Friend WithEvents btn866 As System.Windows.Forms.Button
    Friend WithEvents btn850 As System.Windows.Forms.Button
    Friend WithEvents btn833 As System.Windows.Forms.Button
    Friend WithEvents btn800 As System.Windows.Forms.Button
    Friend WithEvents btn766 As System.Windows.Forms.Button
    Friend WithEvents btn750 As System.Windows.Forms.Button
    Friend WithEvents btn733 As System.Windows.Forms.Button
    Friend WithEvents btn700 As System.Windows.Forms.Button
    Friend WithEvents btn666 As System.Windows.Forms.Button
    Friend WithEvents btn650 As System.Windows.Forms.Button
    Friend WithEvents btn633 As System.Windows.Forms.Button
    Friend WithEvents btn600 As System.Windows.Forms.Button
    Friend WithEvents btn566 As System.Windows.Forms.Button
    Friend WithEvents btn550 As System.Windows.Forms.Button
    Friend WithEvents btn533 As System.Windows.Forms.Button
    Friend WithEvents btn500 As System.Windows.Forms.Button
    Friend WithEvents btn466 As System.Windows.Forms.Button
    Friend WithEvents btn450 As System.Windows.Forms.Button
    Friend WithEvents btn433 As System.Windows.Forms.Button
    Friend WithEvents btn400 As System.Windows.Forms.Button
    Friend WithEvents btn366 As System.Windows.Forms.Button
    Friend WithEvents btn350 As System.Windows.Forms.Button
    Friend WithEvents btn333 As System.Windows.Forms.Button
    Friend WithEvents btn300 As System.Windows.Forms.Button
    Friend WithEvents btn266 As System.Windows.Forms.Button
    Friend WithEvents btn250 As System.Windows.Forms.Button
    Friend WithEvents btn233 As System.Windows.Forms.Button
    Friend WithEvents btn200 As System.Windows.Forms.Button
    Friend WithEvents btn166 As System.Windows.Forms.Button
    Friend WithEvents btn150 As System.Windows.Forms.Button
    Friend WithEvents btn133 As System.Windows.Forms.Button
    Friend WithEvents btn100 As System.Windows.Forms.Button
    Friend WithEvents lblBarData As System.Windows.Forms.Label
    Friend WithEvents lblPos As System.Windows.Forms.Label
    Friend WithEvents grpOpDetail As System.Windows.Forms.GroupBox
    Friend WithEvents lblBar6 As System.Windows.Forms.Label
    Friend WithEvents btn449 As System.Windows.Forms.Button
    Friend WithEvents txtPosData As System.Windows.Forms.TextBox
End Class
