﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProfile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProfile))
        Me.pbPosCalc = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.chkGripRotate = New System.Windows.Forms.CheckBox()
        Me.lblGripRotate = New System.Windows.Forms.Label()
        Me.lblRingClash = New System.Windows.Forms.Label()
        Me.txtGripPos = New System.Windows.Forms.NumericUpDown()
        Me.grpGripper = New System.Windows.Forms.GroupBox()
        Me.optPullGrip = New System.Windows.Forms.RadioButton()
        Me.optPushGrip = New System.Windows.Forms.RadioButton()
        Me.optNoGrip = New System.Windows.Forms.RadioButton()
        Me.chkRingClash = New System.Windows.Forms.CheckBox()
        Me.cmbVersion = New System.Windows.Forms.ComboBox()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.nebSawCutStyle = New System.Windows.Forms.NumericUpDown()
        Me.nebCoreColour = New System.Windows.Forms.NumericUpDown()
        Me.nebID = New System.Windows.Forms.NumericUpDown()
        Me.nebGripZ = New System.Windows.Forms.NumericUpDown()
        Me.nebGripY = New System.Windows.Forms.NumericUpDown()
        Me.nebMinOffcut = New System.Windows.Forms.NumericUpDown()
        Me.nebOptTol = New System.Windows.Forms.NumericUpDown()
        Me.nebCentralise = New System.Windows.Forms.NumericUpDown()
        Me.lblSawCutStyle = New System.Windows.Forms.Label()
        Me.lblCoreColour = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblGripZ = New System.Windows.Forms.Label()
        Me.lblGripY = New System.Windows.Forms.Label()
        Me.lblMinOffcut = New System.Windows.Forms.Label()
        Me.lblOptTol = New System.Windows.Forms.Label()
        Me.lblCentralise = New System.Windows.Forms.Label()
        Me.lblNeuron = New System.Windows.Forms.Label()
        Me.txtNeuron = New System.Windows.Forms.TextBox()
        Me.lblGripPos = New System.Windows.Forms.Label()
        Me.lblEuroHeight = New System.Windows.Forms.Label()
        Me.lblWidth = New System.Windows.Forms.Label()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.lblRebate = New System.Windows.Forms.Label()
        Me.lblFlip = New System.Windows.Forms.Label()
        Me.lblZBlock = New System.Windows.Forms.Label()
        Me.lblStdLen = New System.Windows.Forms.Label()
        Me.lblReplicate = New System.Windows.Forms.Label()
        Me.txtEuroHeight = New System.Windows.Forms.TextBox()
        Me.txtWidth = New System.Windows.Forms.TextBox()
        Me.txtHeight = New System.Windows.Forms.TextBox()
        Me.txtRebate = New System.Windows.Forms.TextBox()
        Me.cmbFlip = New System.Windows.Forms.ComboBox()
        Me.txtZSupport = New System.Windows.Forms.TextBox()
        Me.txtStdLength = New System.Windows.Forms.TextBox()
        Me.cmbReplicates = New System.Windows.Forms.ComboBox()
        Me.cmbProfileTypes = New System.Windows.Forms.ComboBox()
        Me.cmbSystems = New System.Windows.Forms.ComboBox()
        Me.txtProfileDesc = New System.Windows.Forms.TextBox()
        Me.lblProfileType = New System.Windows.Forms.Label()
        Me.lblSystem = New System.Windows.Forms.Label()
        Me.lblProfileDesc = New System.Windows.Forms.Label()
        Me.txtProfileCode = New System.Windows.Forms.TextBox()
        Me.lblProfileCode = New System.Windows.Forms.Label()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.nebPL = New System.Windows.Forms.NumericUpDown()
        Me.nebToolDia = New System.Windows.Forms.NumericUpDown()
        Me.nebZoom = New System.Windows.Forms.NumericUpDown()
        Me.gbTool = New System.Windows.Forms.GroupBox()
        Me.rb2 = New System.Windows.Forms.RadioButton()
        Me.rb1 = New System.Windows.Forms.RadioButton()
        Me.lbltool = New System.Windows.Forms.Label()
        Me.btnDraw = New System.Windows.Forms.Button()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.lblPL = New System.Windows.Forms.Label()
        Me.lblANG = New System.Windows.Forms.Label()
        Me.lblZP = New System.Windows.Forms.Label()
        Me.nebANG = New System.Windows.Forms.NumericUpDown()
        Me.nebZP = New System.Windows.Forms.NumericUpDown()
        Me.lblYP = New System.Windows.Forms.Label()
        Me.nebYP = New System.Windows.Forms.NumericUpDown()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.dgvOperations = New System.Windows.Forms.DataGridView()
        Me.ctmOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExportToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ilContext = New System.Windows.Forms.ImageList(Me.components)
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.txtGripPos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpGripper.SuspendLayout()
        CType(Me.nebSawCutStyle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebCoreColour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebGripZ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebGripY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebMinOffcut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebOptTol, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebCentralise, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebToolDia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTool.SuspendLayout()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvOperations, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctmOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pbPosCalc
        '
        Me.pbPosCalc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbPosCalc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPosCalc.Location = New System.Drawing.Point(0, 0)
        Me.pbPosCalc.Margin = New System.Windows.Forms.Padding(4)
        Me.pbPosCalc.Name = "pbPosCalc"
        Me.pbPosCalc.Size = New System.Drawing.Size(441, 342)
        Me.pbPosCalc.TabIndex = 0
        Me.pbPosCalc.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Splitter2)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(992, 437)
        Me.Panel1.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Navy
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.chkGripRotate)
        Me.Panel6.Controls.Add(Me.lblGripRotate)
        Me.Panel6.Controls.Add(Me.lblRingClash)
        Me.Panel6.Controls.Add(Me.txtGripPos)
        Me.Panel6.Controls.Add(Me.grpGripper)
        Me.Panel6.Controls.Add(Me.chkRingClash)
        Me.Panel6.Controls.Add(Me.cmbVersion)
        Me.Panel6.Controls.Add(Me.lblVersion)
        Me.Panel6.Controls.Add(Me.nebSawCutStyle)
        Me.Panel6.Controls.Add(Me.nebCoreColour)
        Me.Panel6.Controls.Add(Me.nebID)
        Me.Panel6.Controls.Add(Me.nebGripZ)
        Me.Panel6.Controls.Add(Me.nebGripY)
        Me.Panel6.Controls.Add(Me.nebMinOffcut)
        Me.Panel6.Controls.Add(Me.nebOptTol)
        Me.Panel6.Controls.Add(Me.nebCentralise)
        Me.Panel6.Controls.Add(Me.lblSawCutStyle)
        Me.Panel6.Controls.Add(Me.lblCoreColour)
        Me.Panel6.Controls.Add(Me.lblID)
        Me.Panel6.Controls.Add(Me.lblGripZ)
        Me.Panel6.Controls.Add(Me.lblGripY)
        Me.Panel6.Controls.Add(Me.lblMinOffcut)
        Me.Panel6.Controls.Add(Me.lblOptTol)
        Me.Panel6.Controls.Add(Me.lblCentralise)
        Me.Panel6.Controls.Add(Me.lblNeuron)
        Me.Panel6.Controls.Add(Me.txtNeuron)
        Me.Panel6.Controls.Add(Me.lblGripPos)
        Me.Panel6.Controls.Add(Me.lblEuroHeight)
        Me.Panel6.Controls.Add(Me.lblWidth)
        Me.Panel6.Controls.Add(Me.lblHeight)
        Me.Panel6.Controls.Add(Me.lblRebate)
        Me.Panel6.Controls.Add(Me.lblFlip)
        Me.Panel6.Controls.Add(Me.lblZBlock)
        Me.Panel6.Controls.Add(Me.lblStdLen)
        Me.Panel6.Controls.Add(Me.lblReplicate)
        Me.Panel6.Controls.Add(Me.txtEuroHeight)
        Me.Panel6.Controls.Add(Me.txtWidth)
        Me.Panel6.Controls.Add(Me.txtHeight)
        Me.Panel6.Controls.Add(Me.txtRebate)
        Me.Panel6.Controls.Add(Me.cmbFlip)
        Me.Panel6.Controls.Add(Me.txtZSupport)
        Me.Panel6.Controls.Add(Me.txtStdLength)
        Me.Panel6.Controls.Add(Me.cmbReplicates)
        Me.Panel6.Controls.Add(Me.cmbProfileTypes)
        Me.Panel6.Controls.Add(Me.cmbSystems)
        Me.Panel6.Controls.Add(Me.txtProfileDesc)
        Me.Panel6.Controls.Add(Me.lblProfileType)
        Me.Panel6.Controls.Add(Me.lblSystem)
        Me.Panel6.Controls.Add(Me.lblProfileDesc)
        Me.Panel6.Controls.Add(Me.txtProfileCode)
        Me.Panel6.Controls.Add(Me.lblProfileCode)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(541, 437)
        Me.Panel6.TabIndex = 3
        '
        'chkGripRotate
        '
        Me.chkGripRotate.AutoSize = True
        Me.chkGripRotate.Location = New System.Drawing.Point(446, 144)
        Me.chkGripRotate.Name = "chkGripRotate"
        Me.chkGripRotate.Size = New System.Drawing.Size(15, 14)
        Me.chkGripRotate.TabIndex = 58
        Me.chkGripRotate.UseVisualStyleBackColor = True
        '
        'lblGripRotate
        '
        Me.lblGripRotate.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblGripRotate.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGripRotate.ForeColor = System.Drawing.Color.White
        Me.lblGripRotate.Location = New System.Drawing.Point(344, 138)
        Me.lblGripRotate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblGripRotate.Name = "lblGripRotate"
        Me.lblGripRotate.Size = New System.Drawing.Size(94, 24)
        Me.lblGripRotate.TabIndex = 57
        Me.lblGripRotate.Text = "Grip Rotate:"
        Me.lblGripRotate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRingClash
        '
        Me.lblRingClash.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblRingClash.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRingClash.ForeColor = System.Drawing.Color.White
        Me.lblRingClash.Location = New System.Drawing.Point(344, 242)
        Me.lblRingClash.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRingClash.Name = "lblRingClash"
        Me.lblRingClash.Size = New System.Drawing.Size(94, 24)
        Me.lblRingClash.TabIndex = 56
        Me.lblRingClash.Text = "Show Ring Clash:"
        Me.lblRingClash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGripPos
        '
        Me.txtGripPos.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGripPos.Location = New System.Drawing.Point(92, 322)
        Me.txtGripPos.Name = "txtGripPos"
        Me.txtGripPos.Size = New System.Drawing.Size(86, 22)
        Me.txtGripPos.TabIndex = 16
        '
        'grpGripper
        '
        Me.grpGripper.Controls.Add(Me.optPullGrip)
        Me.grpGripper.Controls.Add(Me.optPushGrip)
        Me.grpGripper.Controls.Add(Me.optNoGrip)
        Me.grpGripper.ForeColor = System.Drawing.Color.White
        Me.grpGripper.Location = New System.Drawing.Point(347, 316)
        Me.grpGripper.Name = "grpGripper"
        Me.grpGripper.Size = New System.Drawing.Size(168, 113)
        Me.grpGripper.TabIndex = 55
        Me.grpGripper.TabStop = False
        Me.grpGripper.Text = "Gripper"
        '
        'optPullGrip
        '
        Me.optPullGrip.Location = New System.Drawing.Point(13, 82)
        Me.optPullGrip.Name = "optPullGrip"
        Me.optPullGrip.Size = New System.Drawing.Size(92, 20)
        Me.optPullGrip.TabIndex = 5
        Me.optPullGrip.Text = "Pull"
        Me.optPullGrip.UseVisualStyleBackColor = True
        '
        'optPushGrip
        '
        Me.optPushGrip.Location = New System.Drawing.Point(13, 56)
        Me.optPushGrip.Name = "optPushGrip"
        Me.optPushGrip.Size = New System.Drawing.Size(92, 20)
        Me.optPushGrip.TabIndex = 4
        Me.optPushGrip.Text = "Push"
        Me.optPushGrip.UseVisualStyleBackColor = True
        '
        'optNoGrip
        '
        Me.optNoGrip.Checked = True
        Me.optNoGrip.Location = New System.Drawing.Point(13, 30)
        Me.optNoGrip.Name = "optNoGrip"
        Me.optNoGrip.Size = New System.Drawing.Size(92, 20)
        Me.optNoGrip.TabIndex = 3
        Me.optNoGrip.TabStop = True
        Me.optNoGrip.Text = "None"
        Me.optNoGrip.UseVisualStyleBackColor = True
        '
        'chkRingClash
        '
        Me.chkRingClash.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRingClash.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRingClash.ForeColor = System.Drawing.Color.White
        Me.chkRingClash.Location = New System.Drawing.Point(446, 245)
        Me.chkRingClash.Name = "chkRingClash"
        Me.chkRingClash.Size = New System.Drawing.Size(18, 20)
        Me.chkRingClash.TabIndex = 53
        Me.chkRingClash.Text = "Show Ring Clash"
        Me.chkRingClash.UseVisualStyleBackColor = True
        '
        'cmbVersion
        '
        Me.cmbVersion.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbVersion.FormattingEnabled = True
        Me.cmbVersion.Items.AddRange(New Object() {"V1", "V2", "V3", "V4"})
        Me.cmbVersion.Location = New System.Drawing.Point(446, 267)
        Me.cmbVersion.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbVersion.Name = "cmbVersion"
        Me.cmbVersion.Size = New System.Drawing.Size(69, 24)
        Me.cmbVersion.TabIndex = 52
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblVersion.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.ForeColor = System.Drawing.Color.White
        Me.lblVersion.Location = New System.Drawing.Point(344, 268)
        Me.lblVersion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(94, 24)
        Me.lblVersion.TabIndex = 51
        Me.lblVersion.Text = "Profile Version:"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nebSawCutStyle
        '
        Me.nebSawCutStyle.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebSawCutStyle.Location = New System.Drawing.Point(446, 216)
        Me.nebSawCutStyle.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nebSawCutStyle.Name = "nebSawCutStyle"
        Me.nebSawCutStyle.Size = New System.Drawing.Size(69, 22)
        Me.nebSawCutStyle.TabIndex = 50
        '
        'nebCoreColour
        '
        Me.nebCoreColour.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebCoreColour.Location = New System.Drawing.Point(446, 190)
        Me.nebCoreColour.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nebCoreColour.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.nebCoreColour.Name = "nebCoreColour"
        Me.nebCoreColour.Size = New System.Drawing.Size(69, 22)
        Me.nebCoreColour.TabIndex = 49
        Me.nebCoreColour.Value = New Decimal(New Integer() {1, 0, 0, -2147483648})
        '
        'nebID
        '
        Me.nebID.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebID.Location = New System.Drawing.Point(446, 164)
        Me.nebID.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nebID.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.nebID.Name = "nebID"
        Me.nebID.Size = New System.Drawing.Size(69, 22)
        Me.nebID.TabIndex = 48
        Me.nebID.Value = New Decimal(New Integer() {1, 0, 0, -2147483648})
        '
        'nebGripZ
        '
        Me.nebGripZ.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebGripZ.Location = New System.Drawing.Point(446, 112)
        Me.nebGripZ.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nebGripZ.Minimum = New Decimal(New Integer() {25, 0, 0, 0})
        Me.nebGripZ.Name = "nebGripZ"
        Me.nebGripZ.Size = New System.Drawing.Size(69, 22)
        Me.nebGripZ.TabIndex = 47
        Me.nebGripZ.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'nebGripY
        '
        Me.nebGripY.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebGripY.Location = New System.Drawing.Point(446, 86)
        Me.nebGripY.Maximum = New Decimal(New Integer() {150, 0, 0, 0})
        Me.nebGripY.Minimum = New Decimal(New Integer() {2, 0, 0, -2147483648})
        Me.nebGripY.Name = "nebGripY"
        Me.nebGripY.Size = New System.Drawing.Size(69, 22)
        Me.nebGripY.TabIndex = 46
        '
        'nebMinOffcut
        '
        Me.nebMinOffcut.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebMinOffcut.Location = New System.Drawing.Point(446, 60)
        Me.nebMinOffcut.Maximum = New Decimal(New Integer() {6500, 0, 0, 0})
        Me.nebMinOffcut.Minimum = New Decimal(New Integer() {700, 0, 0, 0})
        Me.nebMinOffcut.Name = "nebMinOffcut"
        Me.nebMinOffcut.Size = New System.Drawing.Size(69, 22)
        Me.nebMinOffcut.TabIndex = 45
        Me.nebMinOffcut.Value = New Decimal(New Integer() {700, 0, 0, 0})
        '
        'nebOptTol
        '
        Me.nebOptTol.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebOptTol.Location = New System.Drawing.Point(446, 34)
        Me.nebOptTol.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nebOptTol.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nebOptTol.Name = "nebOptTol"
        Me.nebOptTol.Size = New System.Drawing.Size(69, 22)
        Me.nebOptTol.TabIndex = 44
        Me.nebOptTol.Value = New Decimal(New Integer() {50, 0, 0, 0})
        '
        'nebCentralise
        '
        Me.nebCentralise.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebCentralise.Location = New System.Drawing.Point(446, 8)
        Me.nebCentralise.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebCentralise.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.nebCentralise.Name = "nebCentralise"
        Me.nebCentralise.Size = New System.Drawing.Size(69, 22)
        Me.nebCentralise.TabIndex = 43
        '
        'lblSawCutStyle
        '
        Me.lblSawCutStyle.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblSawCutStyle.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSawCutStyle.ForeColor = System.Drawing.Color.White
        Me.lblSawCutStyle.Location = New System.Drawing.Point(344, 216)
        Me.lblSawCutStyle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSawCutStyle.Name = "lblSawCutStyle"
        Me.lblSawCutStyle.Size = New System.Drawing.Size(94, 24)
        Me.lblSawCutStyle.TabIndex = 42
        Me.lblSawCutStyle.Text = "Saw Cut Style:"
        Me.lblSawCutStyle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCoreColour
        '
        Me.lblCoreColour.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblCoreColour.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoreColour.ForeColor = System.Drawing.Color.White
        Me.lblCoreColour.Location = New System.Drawing.Point(344, 190)
        Me.lblCoreColour.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCoreColour.Name = "lblCoreColour"
        Me.lblCoreColour.Size = New System.Drawing.Size(94, 24)
        Me.lblCoreColour.TabIndex = 40
        Me.lblCoreColour.Text = "Core Colour:"
        Me.lblCoreColour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblID
        '
        Me.lblID.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblID.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblID.ForeColor = System.Drawing.Color.White
        Me.lblID.Location = New System.Drawing.Point(344, 164)
        Me.lblID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(94, 24)
        Me.lblID.TabIndex = 38
        Me.lblID.Text = "ID:"
        Me.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGripZ
        '
        Me.lblGripZ.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblGripZ.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGripZ.ForeColor = System.Drawing.Color.White
        Me.lblGripZ.Location = New System.Drawing.Point(344, 112)
        Me.lblGripZ.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblGripZ.Name = "lblGripZ"
        Me.lblGripZ.Size = New System.Drawing.Size(94, 24)
        Me.lblGripZ.TabIndex = 36
        Me.lblGripZ.Text = "Grip Z:"
        Me.lblGripZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGripY
        '
        Me.lblGripY.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblGripY.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGripY.ForeColor = System.Drawing.Color.White
        Me.lblGripY.Location = New System.Drawing.Point(344, 86)
        Me.lblGripY.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblGripY.Name = "lblGripY"
        Me.lblGripY.Size = New System.Drawing.Size(94, 24)
        Me.lblGripY.TabIndex = 34
        Me.lblGripY.Text = "Grip Y:"
        Me.lblGripY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMinOffcut
        '
        Me.lblMinOffcut.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblMinOffcut.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinOffcut.ForeColor = System.Drawing.Color.White
        Me.lblMinOffcut.Location = New System.Drawing.Point(344, 60)
        Me.lblMinOffcut.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMinOffcut.Name = "lblMinOffcut"
        Me.lblMinOffcut.Size = New System.Drawing.Size(94, 24)
        Me.lblMinOffcut.TabIndex = 32
        Me.lblMinOffcut.Text = "Min Offcut:"
        Me.lblMinOffcut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOptTol
        '
        Me.lblOptTol.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblOptTol.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOptTol.ForeColor = System.Drawing.Color.White
        Me.lblOptTol.Location = New System.Drawing.Point(344, 34)
        Me.lblOptTol.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOptTol.Name = "lblOptTol"
        Me.lblOptTol.Size = New System.Drawing.Size(94, 24)
        Me.lblOptTol.TabIndex = 30
        Me.lblOptTol.Text = "OptTol:"
        Me.lblOptTol.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCentralise
        '
        Me.lblCentralise.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblCentralise.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCentralise.ForeColor = System.Drawing.Color.White
        Me.lblCentralise.Location = New System.Drawing.Point(344, 8)
        Me.lblCentralise.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCentralise.Name = "lblCentralise"
        Me.lblCentralise.Size = New System.Drawing.Size(95, 24)
        Me.lblCentralise.TabIndex = 28
        Me.lblCentralise.Text = "Centralise:"
        Me.lblCentralise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNeuron
        '
        Me.lblNeuron.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblNeuron.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNeuron.ForeColor = System.Drawing.Color.White
        Me.lblNeuron.Location = New System.Drawing.Point(4, 346)
        Me.lblNeuron.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNeuron.Name = "lblNeuron"
        Me.lblNeuron.Size = New System.Drawing.Size(80, 24)
        Me.lblNeuron.TabIndex = 27
        Me.lblNeuron.Text = "Optim String:"
        Me.lblNeuron.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNeuron
        '
        Me.txtNeuron.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNeuron.Location = New System.Drawing.Point(92, 347)
        Me.txtNeuron.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNeuron.Name = "txtNeuron"
        Me.txtNeuron.Size = New System.Drawing.Size(165, 22)
        Me.txtNeuron.TabIndex = 26
        '
        'lblGripPos
        '
        Me.lblGripPos.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblGripPos.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGripPos.ForeColor = System.Drawing.Color.White
        Me.lblGripPos.Location = New System.Drawing.Point(4, 320)
        Me.lblGripPos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblGripPos.Name = "lblGripPos"
        Me.lblGripPos.Size = New System.Drawing.Size(80, 24)
        Me.lblGripPos.TabIndex = 25
        Me.lblGripPos.Text = "Grip Pos (mm):"
        Me.lblGripPos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEuroHeight
        '
        Me.lblEuroHeight.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblEuroHeight.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEuroHeight.ForeColor = System.Drawing.Color.White
        Me.lblEuroHeight.Location = New System.Drawing.Point(4, 294)
        Me.lblEuroHeight.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEuroHeight.Name = "lblEuroHeight"
        Me.lblEuroHeight.Size = New System.Drawing.Size(80, 24)
        Me.lblEuroHeight.TabIndex = 24
        Me.lblEuroHeight.Text = "Euro Height:"
        Me.lblEuroHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWidth
        '
        Me.lblWidth.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblWidth.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWidth.ForeColor = System.Drawing.Color.White
        Me.lblWidth.Location = New System.Drawing.Point(4, 268)
        Me.lblWidth.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(80, 24)
        Me.lblWidth.TabIndex = 23
        Me.lblWidth.Text = "Width (mm):"
        Me.lblWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHeight
        '
        Me.lblHeight.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblHeight.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeight.ForeColor = System.Drawing.Color.White
        Me.lblHeight.Location = New System.Drawing.Point(4, 242)
        Me.lblHeight.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(80, 24)
        Me.lblHeight.TabIndex = 22
        Me.lblHeight.Text = "Height (mm):"
        Me.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRebate
        '
        Me.lblRebate.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblRebate.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRebate.ForeColor = System.Drawing.Color.White
        Me.lblRebate.Location = New System.Drawing.Point(4, 216)
        Me.lblRebate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRebate.Name = "lblRebate"
        Me.lblRebate.Size = New System.Drawing.Size(80, 24)
        Me.lblRebate.TabIndex = 21
        Me.lblRebate.Text = "Rebate (mm):"
        Me.lblRebate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFlip
        '
        Me.lblFlip.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblFlip.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFlip.ForeColor = System.Drawing.Color.White
        Me.lblFlip.Location = New System.Drawing.Point(4, 190)
        Me.lblFlip.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFlip.Name = "lblFlip"
        Me.lblFlip.Size = New System.Drawing.Size(80, 24)
        Me.lblFlip.TabIndex = 20
        Me.lblFlip.Text = "Flip:"
        Me.lblFlip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblZBlock
        '
        Me.lblZBlock.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblZBlock.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZBlock.ForeColor = System.Drawing.Color.White
        Me.lblZBlock.Location = New System.Drawing.Point(4, 164)
        Me.lblZBlock.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblZBlock.Name = "lblZBlock"
        Me.lblZBlock.Size = New System.Drawing.Size(80, 24)
        Me.lblZBlock.TabIndex = 19
        Me.lblZBlock.Text = "Z Support No:"
        Me.lblZBlock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStdLen
        '
        Me.lblStdLen.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblStdLen.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdLen.ForeColor = System.Drawing.Color.White
        Me.lblStdLen.Location = New System.Drawing.Point(3, 138)
        Me.lblStdLen.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStdLen.Name = "lblStdLen"
        Me.lblStdLen.Size = New System.Drawing.Size(81, 24)
        Me.lblStdLen.TabIndex = 18
        Me.lblStdLen.Text = "Std Length mm:"
        Me.lblStdLen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReplicate
        '
        Me.lblReplicate.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblReplicate.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReplicate.ForeColor = System.Drawing.Color.White
        Me.lblReplicate.Location = New System.Drawing.Point(4, 112)
        Me.lblReplicate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblReplicate.Name = "lblReplicate"
        Me.lblReplicate.Size = New System.Drawing.Size(80, 24)
        Me.lblReplicate.TabIndex = 17
        Me.lblReplicate.Text = "Replicate:"
        Me.lblReplicate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEuroHeight
        '
        Me.txtEuroHeight.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEuroHeight.Location = New System.Drawing.Point(92, 295)
        Me.txtEuroHeight.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEuroHeight.Name = "txtEuroHeight"
        Me.txtEuroHeight.Size = New System.Drawing.Size(86, 22)
        Me.txtEuroHeight.TabIndex = 15
        '
        'txtWidth
        '
        Me.txtWidth.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWidth.Location = New System.Drawing.Point(92, 269)
        Me.txtWidth.Margin = New System.Windows.Forms.Padding(4)
        Me.txtWidth.Name = "txtWidth"
        Me.txtWidth.Size = New System.Drawing.Size(86, 22)
        Me.txtWidth.TabIndex = 14
        '
        'txtHeight
        '
        Me.txtHeight.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeight.Location = New System.Drawing.Point(92, 243)
        Me.txtHeight.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(86, 22)
        Me.txtHeight.TabIndex = 13
        '
        'txtRebate
        '
        Me.txtRebate.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRebate.Location = New System.Drawing.Point(92, 217)
        Me.txtRebate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtRebate.Name = "txtRebate"
        Me.txtRebate.Size = New System.Drawing.Size(86, 22)
        Me.txtRebate.TabIndex = 12
        '
        'cmbFlip
        '
        Me.cmbFlip.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFlip.FormattingEnabled = True
        Me.cmbFlip.Location = New System.Drawing.Point(92, 191)
        Me.cmbFlip.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbFlip.Name = "cmbFlip"
        Me.cmbFlip.Size = New System.Drawing.Size(86, 24)
        Me.cmbFlip.TabIndex = 11
        '
        'txtZSupport
        '
        Me.txtZSupport.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZSupport.Location = New System.Drawing.Point(92, 165)
        Me.txtZSupport.Margin = New System.Windows.Forms.Padding(4)
        Me.txtZSupport.Name = "txtZSupport"
        Me.txtZSupport.Size = New System.Drawing.Size(86, 22)
        Me.txtZSupport.TabIndex = 10
        '
        'txtStdLength
        '
        Me.txtStdLength.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStdLength.Location = New System.Drawing.Point(92, 139)
        Me.txtStdLength.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStdLength.Name = "txtStdLength"
        Me.txtStdLength.Size = New System.Drawing.Size(86, 22)
        Me.txtStdLength.TabIndex = 9
        '
        'cmbReplicates
        '
        Me.cmbReplicates.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbReplicates.FormattingEnabled = True
        Me.cmbReplicates.Location = New System.Drawing.Point(92, 113)
        Me.cmbReplicates.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbReplicates.Name = "cmbReplicates"
        Me.cmbReplicates.Size = New System.Drawing.Size(165, 24)
        Me.cmbReplicates.TabIndex = 8
        '
        'cmbProfileTypes
        '
        Me.cmbProfileTypes.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProfileTypes.FormattingEnabled = True
        Me.cmbProfileTypes.Location = New System.Drawing.Point(92, 87)
        Me.cmbProfileTypes.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbProfileTypes.Name = "cmbProfileTypes"
        Me.cmbProfileTypes.Size = New System.Drawing.Size(243, 24)
        Me.cmbProfileTypes.TabIndex = 7
        '
        'cmbSystems
        '
        Me.cmbSystems.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSystems.FormattingEnabled = True
        Me.cmbSystems.Location = New System.Drawing.Point(92, 60)
        Me.cmbSystems.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbSystems.Name = "cmbSystems"
        Me.cmbSystems.Size = New System.Drawing.Size(243, 24)
        Me.cmbSystems.TabIndex = 6
        '
        'txtProfileDesc
        '
        Me.txtProfileDesc.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfileDesc.Location = New System.Drawing.Point(92, 35)
        Me.txtProfileDesc.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProfileDesc.Name = "txtProfileDesc"
        Me.txtProfileDesc.Size = New System.Drawing.Size(243, 22)
        Me.txtProfileDesc.TabIndex = 5
        '
        'lblProfileType
        '
        Me.lblProfileType.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblProfileType.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileType.ForeColor = System.Drawing.Color.White
        Me.lblProfileType.Location = New System.Drawing.Point(4, 86)
        Me.lblProfileType.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProfileType.Name = "lblProfileType"
        Me.lblProfileType.Size = New System.Drawing.Size(80, 24)
        Me.lblProfileType.TabIndex = 4
        Me.lblProfileType.Text = "Profile Type:"
        Me.lblProfileType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSystem
        '
        Me.lblSystem.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblSystem.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSystem.ForeColor = System.Drawing.Color.White
        Me.lblSystem.Location = New System.Drawing.Point(4, 60)
        Me.lblSystem.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSystem.Name = "lblSystem"
        Me.lblSystem.Size = New System.Drawing.Size(80, 24)
        Me.lblSystem.TabIndex = 3
        Me.lblSystem.Text = "System:"
        Me.lblSystem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProfileDesc
        '
        Me.lblProfileDesc.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblProfileDesc.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileDesc.ForeColor = System.Drawing.Color.White
        Me.lblProfileDesc.Location = New System.Drawing.Point(4, 34)
        Me.lblProfileDesc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProfileDesc.Name = "lblProfileDesc"
        Me.lblProfileDesc.Size = New System.Drawing.Size(80, 24)
        Me.lblProfileDesc.TabIndex = 2
        Me.lblProfileDesc.Text = "Profile Name:"
        Me.lblProfileDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProfileCode
        '
        Me.txtProfileCode.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfileCode.Location = New System.Drawing.Point(92, 9)
        Me.txtProfileCode.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProfileCode.Name = "txtProfileCode"
        Me.txtProfileCode.Size = New System.Drawing.Size(165, 22)
        Me.txtProfileCode.TabIndex = 1
        '
        'lblProfileCode
        '
        Me.lblProfileCode.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblProfileCode.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfileCode.ForeColor = System.Drawing.Color.White
        Me.lblProfileCode.Location = New System.Drawing.Point(4, 8)
        Me.lblProfileCode.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProfileCode.Name = "lblProfileCode"
        Me.lblProfileCode.Size = New System.Drawing.Size(80, 24)
        Me.lblProfileCode.TabIndex = 0
        Me.lblProfileCode.Text = "Code Name:"
        Me.lblProfileCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter2.Location = New System.Drawing.Point(541, 0)
        Me.Splitter2.Margin = New System.Windows.Forms.Padding(4)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(10, 437)
        Me.Splitter2.TabIndex = 2
        Me.Splitter2.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.pbPosCalc)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(551, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(441, 437)
        Me.Panel2.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Navy
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.nebPL)
        Me.Panel4.Controls.Add(Me.nebToolDia)
        Me.Panel4.Controls.Add(Me.nebZoom)
        Me.Panel4.Controls.Add(Me.gbTool)
        Me.Panel4.Controls.Add(Me.lbltool)
        Me.Panel4.Controls.Add(Me.btnDraw)
        Me.Panel4.Controls.Add(Me.lblZoom)
        Me.Panel4.Controls.Add(Me.lblPL)
        Me.Panel4.Controls.Add(Me.lblANG)
        Me.Panel4.Controls.Add(Me.lblZP)
        Me.Panel4.Controls.Add(Me.nebANG)
        Me.Panel4.Controls.Add(Me.nebZP)
        Me.Panel4.Controls.Add(Me.lblYP)
        Me.Panel4.Controls.Add(Me.nebYP)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 342)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(441, 95)
        Me.Panel4.TabIndex = 0
        '
        'nebPL
        '
        Me.nebPL.DecimalPlaces = 1
        Me.nebPL.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebPL.Location = New System.Drawing.Point(162, 6)
        Me.nebPL.Margin = New System.Windows.Forms.Padding(4)
        Me.nebPL.Maximum = New Decimal(New Integer() {150, 0, 0, 0})
        Me.nebPL.Name = "nebPL"
        Me.nebPL.Size = New System.Drawing.Size(65, 25)
        Me.nebPL.TabIndex = 7
        Me.nebPL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nebToolDia
        '
        Me.nebToolDia.DecimalPlaces = 1
        Me.nebToolDia.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebToolDia.Location = New System.Drawing.Point(162, 34)
        Me.nebToolDia.Margin = New System.Windows.Forms.Padding(4)
        Me.nebToolDia.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nebToolDia.Name = "nebToolDia"
        Me.nebToolDia.Size = New System.Drawing.Size(65, 25)
        Me.nebToolDia.TabIndex = 9
        Me.nebToolDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nebToolDia.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nebZoom
        '
        Me.nebZoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebZoom.Location = New System.Drawing.Point(162, 62)
        Me.nebZoom.Margin = New System.Windows.Forms.Padding(4)
        Me.nebZoom.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebZoom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebZoom.Name = "nebZoom"
        Me.nebZoom.Size = New System.Drawing.Size(65, 25)
        Me.nebZoom.TabIndex = 11
        Me.nebZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nebZoom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'gbTool
        '
        Me.gbTool.Controls.Add(Me.rb2)
        Me.gbTool.Controls.Add(Me.rb1)
        Me.gbTool.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTool.ForeColor = System.Drawing.Color.White
        Me.gbTool.Location = New System.Drawing.Point(235, 8)
        Me.gbTool.Margin = New System.Windows.Forms.Padding(4)
        Me.gbTool.Name = "gbTool"
        Me.gbTool.Padding = New System.Windows.Forms.Padding(4)
        Me.gbTool.Size = New System.Drawing.Size(80, 79)
        Me.gbTool.TabIndex = 13
        Me.gbTool.TabStop = False
        Me.gbTool.Text = "Position"
        '
        'rb2
        '
        Me.rb2.AutoSize = True
        Me.rb2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb2.ForeColor = System.Drawing.Color.White
        Me.rb2.Location = New System.Drawing.Point(38, 48)
        Me.rb2.Margin = New System.Windows.Forms.Padding(4)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(34, 22)
        Me.rb2.TabIndex = 1
        Me.rb2.Text = "2"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb1
        '
        Me.rb1.AutoSize = True
        Me.rb1.Checked = True
        Me.rb1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb1.ForeColor = System.Drawing.Color.White
        Me.rb1.Location = New System.Drawing.Point(8, 18)
        Me.rb1.Margin = New System.Windows.Forms.Padding(4)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(34, 22)
        Me.rb1.TabIndex = 0
        Me.rb1.TabStop = True
        Me.rb1.Text = "1"
        Me.rb1.UseVisualStyleBackColor = True
        '
        'lbltool
        '
        Me.lbltool.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltool.ForeColor = System.Drawing.Color.White
        Me.lbltool.Location = New System.Drawing.Point(115, 34)
        Me.lbltool.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbltool.Name = "lbltool"
        Me.lbltool.Size = New System.Drawing.Size(49, 26)
        Me.lbltool.TabIndex = 8
        Me.lbltool.Text = "Tool"
        Me.lbltool.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDraw
        '
        Me.btnDraw.BackColor = System.Drawing.Color.White
        Me.btnDraw.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDraw.Location = New System.Drawing.Point(323, 3)
        Me.btnDraw.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDraw.Name = "btnDraw"
        Me.btnDraw.Size = New System.Drawing.Size(110, 83)
        Me.btnDraw.TabIndex = 12
        Me.btnDraw.Text = "Draw"
        Me.btnDraw.UseVisualStyleBackColor = False
        '
        'lblZoom
        '
        Me.lblZoom.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZoom.ForeColor = System.Drawing.Color.White
        Me.lblZoom.Location = New System.Drawing.Point(116, 62)
        Me.lblZoom.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(48, 26)
        Me.lblZoom.TabIndex = 10
        Me.lblZoom.Text = "Zoom"
        Me.lblZoom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPL
        '
        Me.lblPL.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPL.ForeColor = System.Drawing.Color.White
        Me.lblPL.Location = New System.Drawing.Point(115, 5)
        Me.lblPL.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPL.Name = "lblPL"
        Me.lblPL.Size = New System.Drawing.Size(49, 26)
        Me.lblPL.TabIndex = 6
        Me.lblPL.Text = "PL"
        Me.lblPL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblANG
        '
        Me.lblANG.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblANG.ForeColor = System.Drawing.Color.White
        Me.lblANG.Location = New System.Drawing.Point(7, 64)
        Me.lblANG.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblANG.Name = "lblANG"
        Me.lblANG.Size = New System.Drawing.Size(37, 26)
        Me.lblANG.TabIndex = 4
        Me.lblANG.Text = "Ang"
        Me.lblANG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblZP
        '
        Me.lblZP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZP.ForeColor = System.Drawing.Color.White
        Me.lblZP.Location = New System.Drawing.Point(7, 34)
        Me.lblZP.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblZP.Name = "lblZP"
        Me.lblZP.Size = New System.Drawing.Size(37, 26)
        Me.lblZP.TabIndex = 2
        Me.lblZP.Text = "Zp"
        Me.lblZP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nebANG
        '
        Me.nebANG.DecimalPlaces = 1
        Me.nebANG.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebANG.Location = New System.Drawing.Point(42, 63)
        Me.nebANG.Margin = New System.Windows.Forms.Padding(4)
        Me.nebANG.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.nebANG.Name = "nebANG"
        Me.nebANG.Size = New System.Drawing.Size(65, 25)
        Me.nebANG.TabIndex = 5
        Me.nebANG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nebZP
        '
        Me.nebZP.DecimalPlaces = 1
        Me.nebZP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebZP.Location = New System.Drawing.Point(42, 34)
        Me.nebZP.Margin = New System.Windows.Forms.Padding(4)
        Me.nebZP.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nebZP.Minimum = New Decimal(New Integer() {120, 0, 0, -2147483648})
        Me.nebZP.Name = "nebZP"
        Me.nebZP.Size = New System.Drawing.Size(65, 25)
        Me.nebZP.TabIndex = 3
        Me.nebZP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblYP
        '
        Me.lblYP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYP.ForeColor = System.Drawing.Color.White
        Me.lblYP.Location = New System.Drawing.Point(7, 5)
        Me.lblYP.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblYP.Name = "lblYP"
        Me.lblYP.Size = New System.Drawing.Size(37, 26)
        Me.lblYP.TabIndex = 0
        Me.lblYP.Text = "Yp"
        Me.lblYP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nebYP
        '
        Me.nebYP.DecimalPlaces = 1
        Me.nebYP.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebYP.Location = New System.Drawing.Point(42, 5)
        Me.nebYP.Margin = New System.Windows.Forms.Padding(4)
        Me.nebYP.Maximum = New Decimal(New Integer() {125, 0, 0, 0})
        Me.nebYP.Minimum = New Decimal(New Integer() {125, 0, 0, -2147483648})
        Me.nebYP.Name = "nebYP"
        Me.nebYP.Size = New System.Drawing.Size(65, 25)
        Me.nebYP.TabIndex = 1
        Me.nebYP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Navy
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Panel7)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 613)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(992, 53)
        Me.Panel3.TabIndex = 2
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.btnSave)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(811, 0)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(179, 51)
        Me.Panel7.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(19, 7)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(149, 37)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save and Exit"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Splitter1
        '
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter1.Location = New System.Drawing.Point(0, 437)
        Me.Splitter1.Margin = New System.Windows.Forms.Padding(4)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(992, 12)
        Me.Splitter1.TabIndex = 3
        Me.Splitter1.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.dgvOperations)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 449)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(992, 164)
        Me.Panel5.TabIndex = 4
        '
        'dgvOperations
        '
        Me.dgvOperations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOperations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOperations.Location = New System.Drawing.Point(0, 0)
        Me.dgvOperations.Name = "dgvOperations"
        Me.dgvOperations.Size = New System.Drawing.Size(990, 162)
        Me.dgvOperations.TabIndex = 0
        '
        'ctmOperations
        '
        Me.ctmOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.ToolStripSeparator1, Me.ExportToolStripMenuItem1, Me.ImportToolStripMenuItem, Me.DeleteToolStripMenuItem})
        Me.ctmOperations.Name = "ctmOperations"
        Me.ctmOperations.Size = New System.Drawing.Size(111, 120)
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(107, 6)
        '
        'ExportToolStripMenuItem1
        '
        Me.ExportToolStripMenuItem1.Name = "ExportToolStripMenuItem1"
        Me.ExportToolStripMenuItem1.Size = New System.Drawing.Size(110, 22)
        Me.ExportToolStripMenuItem1.Text = "Export"
        '
        'ImportToolStripMenuItem
        '
        Me.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem"
        Me.ImportToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.ImportToolStripMenuItem.Text = "Import"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'ilContext
        '
        Me.ilContext.ImageStream = CType(resources.GetObject("ilContext.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilContext.TransparentColor = System.Drawing.Color.Silver
        Me.ilContext.Images.SetKeyName(0, "Copy.png")
        Me.ilContext.Images.SetKeyName(1, "Paste.png")
        '
        'frmProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(992, 666)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmProfile"
        Me.Text = "Profiles"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.txtGripPos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpGripper.ResumeLayout(False)
        CType(Me.nebSawCutStyle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebCoreColour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebGripZ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebGripY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebMinOffcut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebOptTol, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebCentralise, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.nebPL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebToolDia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTool.ResumeLayout(False)
        Me.gbTool.PerformLayout()
        CType(Me.nebANG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebZP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nebYP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.dgvOperations, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctmOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pbPosCalc As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents nebZP As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblYP As System.Windows.Forms.Label
    Friend WithEvents nebYP As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblZoom As System.Windows.Forms.Label
    Friend WithEvents lblPL As System.Windows.Forms.Label
    Friend WithEvents lblANG As System.Windows.Forms.Label
    Friend WithEvents lblZP As System.Windows.Forms.Label
    Friend WithEvents nebZoom As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebPL As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebANG As System.Windows.Forms.NumericUpDown
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents btnDraw As System.Windows.Forms.Button
    Friend WithEvents txtProfileCode As System.Windows.Forms.TextBox
    Friend WithEvents lblProfileCode As System.Windows.Forms.Label
    Friend WithEvents cmbSystems As System.Windows.Forms.ComboBox
    Friend WithEvents txtProfileDesc As System.Windows.Forms.TextBox
    Friend WithEvents lblProfileType As System.Windows.Forms.Label
    Friend WithEvents lblSystem As System.Windows.Forms.Label
    Friend WithEvents lblProfileDesc As System.Windows.Forms.Label
    Friend WithEvents lblZBlock As System.Windows.Forms.Label
    Friend WithEvents lblStdLen As System.Windows.Forms.Label
    Friend WithEvents lblReplicate As System.Windows.Forms.Label
    Friend WithEvents txtEuroHeight As System.Windows.Forms.TextBox
    Friend WithEvents txtWidth As System.Windows.Forms.TextBox
    Friend WithEvents txtHeight As System.Windows.Forms.TextBox
    Friend WithEvents txtRebate As System.Windows.Forms.TextBox
    Friend WithEvents cmbFlip As System.Windows.Forms.ComboBox
    Friend WithEvents txtZSupport As System.Windows.Forms.TextBox
    Friend WithEvents txtStdLength As System.Windows.Forms.TextBox
    Friend WithEvents cmbReplicates As System.Windows.Forms.ComboBox
    Friend WithEvents cmbProfileTypes As System.Windows.Forms.ComboBox
    Friend WithEvents lblGripPos As System.Windows.Forms.Label
    Friend WithEvents lblEuroHeight As System.Windows.Forms.Label
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblRebate As System.Windows.Forms.Label
    Friend WithEvents lblFlip As System.Windows.Forms.Label
    Friend WithEvents lbltool As System.Windows.Forms.Label
    Friend WithEvents nebToolDia As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblNeuron As System.Windows.Forms.Label
    Friend WithEvents txtNeuron As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents gbTool As System.Windows.Forms.GroupBox
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents dgvOperations As System.Windows.Forms.DataGridView
    Friend WithEvents ctmOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExportToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCentralise As System.Windows.Forms.Label
    Friend WithEvents lblSawCutStyle As System.Windows.Forms.Label
    Friend WithEvents lblCoreColour As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblGripZ As System.Windows.Forms.Label
    Friend WithEvents lblGripY As System.Windows.Forms.Label
    Friend WithEvents lblMinOffcut As System.Windows.Forms.Label
    Friend WithEvents lblOptTol As System.Windows.Forms.Label
    Friend WithEvents nebMinOffcut As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebOptTol As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebCentralise As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebGripY As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebSawCutStyle As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebCoreColour As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebID As System.Windows.Forms.NumericUpDown
    Friend WithEvents nebGripZ As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbVersion As System.Windows.Forms.ComboBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents chkRingClash As System.Windows.Forms.CheckBox
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ilContext As System.Windows.Forms.ImageList
    Friend WithEvents grpGripper As System.Windows.Forms.GroupBox
    Friend WithEvents optPullGrip As System.Windows.Forms.RadioButton
    Friend WithEvents optPushGrip As System.Windows.Forms.RadioButton
    Friend WithEvents optNoGrip As System.Windows.Forms.RadioButton
    Friend WithEvents txtGripPos As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRingClash As System.Windows.Forms.Label
    Friend WithEvents lblGripRotate As System.Windows.Forms.Label
    Friend WithEvents chkGripRotate As System.Windows.Forms.CheckBox
End Class
