﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserPass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserPass))
        Me.lblOldPassword = New System.Windows.Forms.Label()
        Me.txtOldPassword = New System.Windows.Forms.TextBox()
        Me.lblNewPass1 = New System.Windows.Forms.Label()
        Me.txtPass1 = New System.Windows.Forms.TextBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.lblNewPass2 = New System.Windows.Forms.Label()
        Me.txtPass2 = New System.Windows.Forms.TextBox()
        Me.btnNoPass = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblOldPassword
        '
        Me.lblOldPassword.AutoSize = True
        Me.lblOldPassword.Location = New System.Drawing.Point(12, 9)
        Me.lblOldPassword.Name = "lblOldPassword"
        Me.lblOldPassword.Size = New System.Drawing.Size(102, 13)
        Me.lblOldPassword.TabIndex = 0
        Me.lblOldPassword.Text = "Type Old Password:"
        '
        'txtOldPassword
        '
        Me.txtOldPassword.Location = New System.Drawing.Point(136, 6)
        Me.txtOldPassword.Name = "txtOldPassword"
        Me.txtOldPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtOldPassword.Size = New System.Drawing.Size(189, 20)
        Me.txtOldPassword.TabIndex = 1
        '
        'lblNewPass1
        '
        Me.lblNewPass1.AutoSize = True
        Me.lblNewPass1.Location = New System.Drawing.Point(12, 35)
        Me.lblNewPass1.Name = "lblNewPass1"
        Me.lblNewPass1.Size = New System.Drawing.Size(108, 13)
        Me.lblNewPass1.TabIndex = 2
        Me.lblNewPass1.Text = "Type New Password:"
        '
        'txtPass1
        '
        Me.txtPass1.Location = New System.Drawing.Point(136, 32)
        Me.txtPass1.Name = "txtPass1"
        Me.txtPass1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass1.Size = New System.Drawing.Size(189, 20)
        Me.txtPass1.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(181, 84)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(144, 29)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Text = "Set New Password"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'lblNewPass2
        '
        Me.lblNewPass2.AutoSize = True
        Me.lblNewPass2.Location = New System.Drawing.Point(12, 61)
        Me.lblNewPass2.Name = "lblNewPass2"
        Me.lblNewPass2.Size = New System.Drawing.Size(118, 13)
        Me.lblNewPass2.TabIndex = 4
        Me.lblNewPass2.Text = "Retype New Password:"
        '
        'txtPass2
        '
        Me.txtPass2.Location = New System.Drawing.Point(136, 58)
        Me.txtPass2.Name = "txtPass2"
        Me.txtPass2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass2.Size = New System.Drawing.Size(189, 20)
        Me.txtPass2.TabIndex = 5
        '
        'btnNoPass
        '
        Me.btnNoPass.Location = New System.Drawing.Point(15, 84)
        Me.btnNoPass.Name = "btnNoPass"
        Me.btnNoPass.Size = New System.Drawing.Size(160, 29)
        Me.btnNoPass.TabIndex = 7
        Me.btnNoPass.Text = "No Password Required"
        Me.btnNoPass.UseVisualStyleBackColor = True
        '
        'frmUserPass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 125)
        Me.Controls.Add(Me.btnNoPass)
        Me.Controls.Add(Me.txtPass2)
        Me.Controls.Add(Me.lblNewPass2)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.txtPass1)
        Me.Controls.Add(Me.lblNewPass1)
        Me.Controls.Add(Me.txtOldPassword)
        Me.Controls.Add(Me.lblOldPassword)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserPass"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set User Password"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOldPassword As System.Windows.Forms.Label
    Friend WithEvents txtOldPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblNewPass1 As System.Windows.Forms.Label
    Friend WithEvents txtPass1 As System.Windows.Forms.TextBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents lblNewPass2 As System.Windows.Forms.Label
    Friend WithEvents txtPass2 As System.Windows.Forms.TextBox
    Friend WithEvents btnNoPass As System.Windows.Forms.Button
End Class
