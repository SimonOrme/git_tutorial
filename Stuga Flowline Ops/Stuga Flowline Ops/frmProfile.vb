﻿Imports System.IO

Public Class frmProfile

#Region " Declare Variables "

    Private PosCalc As New clsPosCalc
    Private ParseMND As New clsParseMND

    Dim MouseX As Short
    Dim MouseY As Short
    Dim DragMode As Byte
    Private RedrawOK As Boolean = True 'Stops Numeric Edit Boxes from redrawing when setting their values

    Dim bCreating As Boolean = False

#End Region

#Region " Form - Load "

    Private Sub frmProfile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Set Zoom
        nebZoom.Value = iScale

        'Setup Poscalc Class
        InitialisePosCalc()

        'Get Profile Data
        GetData()

        'Fill Operations
        FillOperationsGrid(dgvOperations, txtProfileCode.Text, True, "", False, "", False, True)

        'Set Profile.mul Version
        cmbVersion.Text = ProfileVersion

        'Set Ring Clach Visibility
        chkRingClash.Checked = ShowDangerRings

        'Set to show No Gripper
        optPullGrip.Checked = False
        optPushGrip.Checked = False
        optNoGrip.Checked = True

    End Sub

#End Region

#Region " Button - Draw "

    Private Sub btnDraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDraw.Click

        Redraw()

    End Sub

#End Region
#Region " Button - Save And Exit "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        UpdateProfiles()
        SaveMuls()

        Dim frmEngineer As New frmEngineer
        frmEngineer.Show()

        ProfileWidth = 0
        ProfileHeight = 0

        Me.Close()

    End Sub

#End Region

#Region " Event - dgvOperations_SelectionChanged() "


    Private Sub dgvOperations_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOperations.SelectionChanged

        If Not bCreating Then

            Try
                If dgvOperations.CurrentRow.IsNewRow = False Then
                    bCreating = False
                    Redraw()
                Else
                    bCreating = True
                End If
            Catch ex As Exception
                bCreating = False
            End Try

        End If

    End Sub

#End Region
#Region " Event - rb1_CheckedChanged() "

    Private Sub rb1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb1.CheckedChanged

        Redraw()

    End Sub

#End Region
#Region " Event - nebYP_ValueChanged() "

    Private Sub nebYP_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebYP.ValueChanged

        If RedrawOK Then
            PosCalc.YP = nebYP.Value
            PosCalc.CreateDrawing()
        End If

    End Sub

#End Region
#Region " Event - nebZP_ValueChanged() "

    Private Sub nebZP_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebZP.ValueChanged

        If RedrawOK Then
            PosCalc.ZP = nebZP.Value
            PosCalc.CreateDrawing()
        End If

    End Sub
#End Region
#Region " Event - nebANG_ValueChanged() "

    Private Sub nebANG_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebANG.ValueChanged

        If RedrawOK Then
            PosCalc.Angle = nebANG.Value
            PosCalc.CreateDrawing()
        End If

    End Sub

#End Region
#Region " Event - nebPL_ValueChanged() "

    Private Sub nebPL_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebPL.ValueChanged

        If RedrawOK Then
            PosCalc.Plunge = nebPL.Value
            PosCalc.CreateDrawing()
        End If

    End Sub

#End Region
#Region " Event - nebToolDia_ValueChanged() "

    Private Sub nebToolDia_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebToolDia.ValueChanged

        If RedrawOK Then
            PosCalc.ToolDiameter = nebToolDia.Value
            PosCalc.CreateDrawing()
        End If

    End Sub

#End Region
#Region " Event - nebZoom_ValueChanged() "

    Private Sub nebZoom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebZoom.ValueChanged

        Redraw()

    End Sub

#End Region
#Region " Event - pbPosCalc_Paint() "

    Private Sub pbPosCalc_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pbPosCalc.Paint

        Console.Write("pbPosCalc_Paint" + vbCrLf)
        With PosCalc
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2
        End With

        Redraw()

    End Sub

#End Region
#Region " Event - pbPosCalc_SizeChanged() "

    Private Sub pbPosCalc_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pbPosCalc.SizeChanged

        With PosCalc
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2
        End With

        Redraw()

    End Sub

#End Region
#Region " Event - pbPosCalc_MouseMove() "

    Private Sub pbPosCalc_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles pbPosCalc.MouseMove

        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = eventArgs.X
        Dim Y As Single = eventArgs.Y
        Dim s As Double
        Dim c As Double
        Dim a1 As Double

        If Button = 1 Then

            If DragMode = 1 Then 'Rotate

                'Calculate angle
                If (MouseY - Y) = 0 Then
                    If MouseX - X > 0 Then PosCalc.Angle = 90 Else PosCalc.Angle = 90 = 270
                Else
                    PosCalc.Angle = 180 * System.Math.Atan((MouseX - X) / (MouseY - Y)) / PosCalc.pi
                    If (MouseY - Y) < 0 Then PosCalc.Angle = PosCalc.Angle + 180
                    If PosCalc.Angle < 0 Then PosCalc.Angle = PosCalc.Angle + 360
                End If

                'Generate Sin / Cos of Angle
                a1 = -PosCalc.Angle * 3.14159265358979 / 180
                c = System.Math.Sin(a1)
                s = System.Math.Cos(a1)

                'Check Min/Max
                Dim MinMaxY As Integer = (MouseX - (PosCalc.shtRingDia * c) - PosCalc.XOffset) / PosCalc.Scale
                If MinMaxY < nebYP.Minimum Then MinMaxY = nebYP.Minimum
                If MinMaxY > nebYP.Maximum Then MinMaxY = nebYP.Maximum

                Dim MinMaxZ As Integer = (-MouseY - (PosCalc.shtRingDia * s) + PosCalc.YOffset) / PosCalc.Scale
                If MinMaxZ < nebZP.Minimum Then MinMaxZ = nebZP.Minimum
                If MinMaxZ > nebZP.Maximum Then MinMaxZ = nebZP.Maximum

                'Recalc ring pos
                PosCalc.YP = MinMaxY
                PosCalc.ZP = MinMaxZ

                'Draw Ring
                RedrawOK = False
                nebYP.Value = PosCalc.YP
                nebZP.Value = PosCalc.ZP
                nebANG.Value = PosCalc.Angle
                RedrawOK = True
                PosCalc.CreateDrawing()

            Else ' Drag
                Dim MinMaxY As Integer = (X - MouseX) / PosCalc.Scale
                If MinMaxY < nebYP.Minimum Then MinMaxY = nebYP.Minimum
                If MinMaxY > nebYP.Maximum Then MinMaxY = nebYP.Maximum
                PosCalc.YP = MinMaxY

                Dim MinMaxZ As Integer = (-(Y - MouseY)) / PosCalc.Scale
                If MinMaxZ < nebZP.Minimum Then MinMaxZ = nebZP.Minimum
                If MinMaxZ > nebZP.Maximum Then MinMaxZ = nebZP.Maximum
                PosCalc.ZP = MinMaxZ

                RedrawOK = False
                nebYP.Value = PosCalc.YP
                nebZP.Value = PosCalc.ZP
                nebANG.Value = PosCalc.Angle
                RedrawOK = True
                PosCalc.CreateDrawing()
            End If

        Else
            a1 = -PosCalc.Angle * 3.14159265358979 / 180
            c = System.Math.Sin(a1)
            s = System.Math.Cos(a1)

            Dim iMX As Integer = PosCalc.tx + ((prfTipToCollet + 15) * c * PosCalc.Scale)
            Dim iMY As Integer = PosCalc.ty - ((prfTipToCollet + 15) * s * PosCalc.Scale)
            Dim iXoff As Integer = X - iMX
            Dim iYoff As Integer = Y - iMY
            If iXoff > -32 And iXoff < 32 And iYoff > -32 And iYoff < 32 Then
                DragMode = 1
                MouseX = PosCalc.tx
                MouseY = PosCalc.ty
            Else
                DragMode = 0
                MouseX = X - (PosCalc.YP * PosCalc.Scale)
                MouseY = Y + (PosCalc.ZP * PosCalc.Scale)
            End If

        End If

    End Sub

#End Region
#Region " Event - dgvOperations_RowEnter() "

    Private Sub dgvOperations_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOperations.RowEnter

        'Console.WriteLine("Row Enter" + vbCrLf)
        FillNewRow(e.RowIndex)

    End Sub

#End Region
#Region " Event - chkRingClash_CheckedChanged "

    Private Sub chkRingClash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRingClash.CheckedChanged

        ShowDangerRings = chkRingClash.Checked
        Redraw()

    End Sub

#End Region

#Region " Function - GetData() "

    Function GetData() As Boolean

        'Set up Standard Variables
        'Dim conData As SqlClient.SqlConnection
        Dim bOK As Boolean = True

        'Open Data Connection
        'Try
        'conData = New SqlClient.SqlConnection(conString)
        'conData.Open()
        'Catch ex As Exception
        'MsgBox("Could not Connect to Database", MsgBoxStyle.Critical)
        'Return False
        'End Try

        'Fill Combo Boxes
        FillSystems(cmbSystems)
        FillProfileTypes(cmbProfileTypes)
        FillReplicates(cmbReplicates)

        'Close Data Connection
        'conData.Close()

        'Fill Flip Combo
        cmbFlip.Items.Clear()
        cmbFlip.Items.Add("N")
        cmbFlip.Items.Add("Y")
        cmbFlip.Items.Add("D")
        cmbFlip.Items.Add("C")

        'Get Data
        Dim foundRows() As Data.DataRow
        foundRows = tblProfile.Select("CodeName = '" + txtProfileCode.Text + "'")

        'Fill text Boxes
        txtProfileDesc.Text = foundRows(0).Item("ProfileName").ToString
        cmbSystems.SelectedValue = foundRows(0).Item("System").ToString
        cmbProfileTypes.SelectedValue = foundRows(0).Item("ProfileTypeID").ToString
        cmbReplicates.Text = foundRows(0).Item("ReplicateID").ToString
        txtStdLength.Text = foundRows(0).Item("StdLength").ToString
        txtZSupport.Text = foundRows(0).Item("ZSection").ToString
        cmbFlip.SelectedText = foundRows(0).Item("Flip").ToString
        txtRebate.Text = foundRows(0).Item("ZRebate").ToString
        txtHeight.Text = foundRows(0).Item("Height").ToString
        ProfileHeight = foundRows(0).Item("Height").ToString
        txtWidth.Text = foundRows(0).Item("Width").ToString
        ProfileWidth = foundRows(0).Item("Width").ToString
        txtEuroHeight.Text = foundRows(0).Item("EuroHeight").ToString
        txtGripPos.Value = foundRows(0).Item("GripPosition")
        txtNeuron.Text = foundRows(0).Item("NeuronStr").ToString
        nebCentralise.Value = foundRows(0).Item("Centralise")
        nebOptTol.Value = foundRows(0).Item("OptTol")
        nebMinOffcut.Value = foundRows(0).Item("MinOffcut")
        nebGripY.Value = foundRows(0).Item("GripY")
        nebGripZ.Value = foundRows(0).Item("GripZ")
        nebID.Value = foundRows(0).Item("ID")
        nebCoreColour.Value = foundRows(0).Item("CoreColour")
        nebSawCutStyle.Value = foundRows(0).Item("SawCutStyle")
        If foundRows(0).Item("RotateGripper") = 1 Then
            chkGripRotate.Checked = True
        Else
            chkGripRotate.Checked = False
        End If

        'Return State
        Return bOK

    End Function

#End Region
#Region " Function - FillGrid() "

    Function FillGrid() As Boolean

        Dim bs As New BindingSource()
        bs.DataSource = tblStdOps
        bs.Filter = "ProfileCode = '" + txtProfileCode.Text + "'"

        With Me.dgvOperations
            .DataSource = bs
            .ReadOnly = False
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .AllowUserToOrderColumns = False
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = False
            .Font = New Font("Arial", 11, FontStyle.Regular)
        End With

        'Index
        With Me.dgvOperations.Columns(0)
            .HeaderText = "Index"
            .Width = 50
            .Visible = False
        End With

        'Profile
        With Me.dgvOperations.Columns(1)
            .HeaderText = "Profile"
            .Width = 100
            .Visible = False
        End With

        'Tool Code
        With Me.dgvOperations.Columns(2)
            .HeaderText = "Tool Code"
            .Width = 100
            .Visible = True
        End With

        'Std Op
        With Me.dgvOperations.Columns(3)
            .HeaderText = "Std Op"
            .Width = 150
            '.EditType = Janus.Windows.GridEX.EditType.DropDownList
            '.HasValueList = True
            'Dim vl As Janus.Windows.GridEX.GridEXValueListItemCollection = .ValueList
            'For Each rVLRow In tblStdOpDef.Rows
            'Dim vli As Janus.Windows.GridEX.GridEXValueListItem = New Janus.Windows.GridEX.GridEXValueListItem(rVLRow.Item("StdOpCode"), rVLRow.Item("StdOpCode") + " / " + rVLRow.Item("StdOpDescription"))
            'vl.Add(vli)
            'Next
            '.ValueList.Sort()
            .Visible = True
        End With

        'Used
        With Me.dgvOperations.Columns(4)
            .HeaderText = "Use"
            .Width = 50
            .Visible = True
        End With

        'Description
        With Me.dgvOperations.Columns(5)
            .HeaderText = "Description"
            .Width = 300
            .Visible = True
        End With

        'Variable 1
        With Me.dgvOperations.Columns(6)
            .HeaderText = "V1"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 2
        With Me.dgvOperations.Columns(7)
            .HeaderText = "V2"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 3
        With Me.dgvOperations.Columns(8)
            .HeaderText = "V3"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 4
        With Me.dgvOperations.Columns(9)
            .HeaderText = "V4"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 5
        With Me.dgvOperations.Columns(10)
            .HeaderText = "V5"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 6
        With Me.dgvOperations.Columns(11)
            .HeaderText = "V6"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 7
        With Me.dgvOperations.Columns(12)
            .HeaderText = "V7"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Variable 8
        With Me.dgvOperations.Columns(13)
            .HeaderText = "V8"
            .Width = 50
            .Visible = True
            .DefaultCellStyle.Format = "0.0"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With

        'Envelope L
        'With Me.dgvOperations.Columns.Item(14)
        '.HeaderText = "EnvelopeL"
        '.Width = 70
        '.Visible = False
        'End With

        'Envelope R
        'With Me.dgvOperations.Columns.Item(15)
        '.HeaderText = "EnvelopeR"
        '.Width = 70
        '.Visible = False
        'End With

        Return True

    End Function

#End Region

#Region " Sub - InitialisePosCalc() "

    Sub InitialisePosCalc()

        With PosCalc

            'Link Controls
            .pbPosCalc = pbPosCalc

            'Set Scale
            .Scale = iScale

            'Set Centre Point
            .XOffset = pbPosCalc.Width / 2
            .YOffset = pbPosCalc.Height / 2

        End With

    End Sub

#End Region
#Region " Sub - UpdateProfiles() "

    Sub UpdateProfiles()

        'Update the profiles in the table with the new values from this form
        Dim rRow As DataRow()
        rRow = tblProfile.Select("CodeName = '" + txtProfileCode.Text + "'")

        rRow(0).Item("System") = chNull(cmbSystems.SelectedValue)
        rRow(0).Item("ProfileTypeID") = chNullIS(cmbProfileTypes.SelectedValue)
        rRow(0).Item("ReplicateID") = chNull(cmbReplicates.Text)
        rRow(0).Item("StdLength") = chNullIS(txtStdLength.Text)
        rRow(0).Item("ZSection") = chNullIS(txtZSupport.Text)
        rRow(0).Item("Flip") = chNull(cmbFlip.Text)
        rRow(0).Item("ZRebate") = chNull(txtRebate.Text)
        rRow(0).Item("Height") = chNull(txtHeight.Text)
        rRow(0).Item("Width") = chNull(txtWidth.Text)
        rRow(0).Item("EuroHeight") = chNull(txtEuroHeight.Text)
        rRow(0).Item("GripPosition") = chNull(txtGripPos.Value)
        rRow(0).Item("NeuronStr") = chNull(txtNeuron.Text)
        rRow(0).Item("Centralise") = nebCentralise.Value
        rRow(0).Item("OptTol") = nebOptTol.Value
        rRow(0).Item("MinOffcut") = nebMinOffcut.Value
        rRow(0).Item("GripY") = nebGripY.Value
        rRow(0).Item("GripZ") = nebGripZ.Value
        rRow(0).Item("ID") = nebID.Value
        rRow(0).Item("CoreColour") = nebCoreColour.Value
        rRow(0).Item("SawCutStyle") = nebSawCutStyle.Value
        If chkGripRotate.Checked Then
            rRow(0).Item("RotateGripper") = 1
        Else
            rRow(0).Item("RotateGripper") = 0
        End If

        tblProfile.BeginLoadData()
        tblProfile.LoadDataRow(rRow(0).ItemArray, System.Data.LoadOption.OverwriteChanges)
        tblProfile.EndLoadData()

    End Sub

#End Region
#Region " Sub - Redraw() "

    Sub Redraw()

        'Declare Variables
        Dim sString As String = ""
        Dim V1 As Double = 0
        Dim V2 As Double = 0
        Dim V3 As Double = 0
        Dim V4 As Double = 0
        Dim V5 As Double = 0
        Dim V6 As Double = 0
        Dim V7 As Double = 0
        Dim V8 As Double = 0

        If bCreating Then Exit Sub

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvOperations.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvOperations.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvOperations.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvOperations.SelectedCells(0).RowIndex
            sr = dgvOperations.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        'Get MND File and Variables
        Try
            sString = sr.Cells(3).Value
            V1 = sr.Cells(6).Value
            V2 = sr.Cells(7).Value
            V3 = sr.Cells(8).Value
            V4 = sr.Cells(9).Value
            V5 = sr.Cells(10).Value
            V6 = sr.Cells(11).Value
            V7 = sr.Cells(12).Value
            V8 = sr.Cells(13).Value
        Catch ex As Exception
            Exit Sub
        End Try

        'Fill Headers
        'Dim rRow As DataRow() = tblStdOpDef.Select("StdOpCode = '" + sString + "'")   SELECT COMMAND REQUIRES .NET 3.5 (XP and Later Only)
        Dim rRow As DataRow
        For Each temprow As DataRow In tblStdOpDef.Rows
            If temprow.Item(0).ToString = sString Then
                rRow = temprow
                dgvOperations.Columns(6).HeaderText = rRow.Item(3).ToString
                dgvOperations.Columns(7).HeaderText = rRow.Item(4).ToString
                dgvOperations.Columns(8).HeaderText = rRow.Item(5).ToString
                dgvOperations.Columns(9).HeaderText = rRow.Item(6).ToString
                dgvOperations.Columns(10).HeaderText = rRow.Item(7).ToString
                dgvOperations.Columns(11).HeaderText = rRow.Item(8).ToString
                dgvOperations.Columns(12).HeaderText = rRow.Item(9).ToString
                dgvOperations.Columns(13).HeaderText = rRow.Item(10).ToString
            End If
        Next

        'Parse MND File
        RedrawOK = False
        Dim sFile As String = prfOpdesignLoc + sString + ".MND"
        ParseMND.ParseMNDFile(sFile, V1, V2, V3, V4, V5, V6, V7, V8, Me.rb1, Me.nebYP, Me.nebZP, Me.nebANG, Me.nebPL, Me.nebToolDia)
        RedrawOK = True

        'Draw Operation
        With PosCalc
            .YP = nebYP.Value
            .ZP = nebZP.Value
            .Angle = nebANG.Value
            .Plunge = nebPL.Value
            .ToolDiameter = nebToolDia.Value
            .Scale = nebZoom.Value
            If optPushGrip.Checked Then
                .GripperType = 2
                .dblGY = nebGripY.Value
                .dblGZ = nebGripZ.Value
                .PushGripperHeight = prfPushGripperHeight
                .PushGripperWidth = prfPushGripperWidth
                If chkGripRotate.Checked Then
                    .PushGripperRotate = True
                Else
                    .PushGripperRotate = False
                End If
            Else
                .GripperType = 1
                .dblGY = txtGripPos.Value
                .dblGZ = 0
                .PullGripperHeight = prfPullGripperHeight
                .PullGripperZPos = prfPullGripperZPos
            End If
            .bShowGrip = ShowNewGripper
            .PushGripperHeight = prfpushGripperHeight
            .PushGripperWidth = prfPushGripperWidth
            .LoadDXF(modGlobal.prfDXFLoc & txtProfileCode.Text & ".dxf")
            .CreateDrawing()
        End With

    End Sub

#End Region
#Region " Sub - FillNewRow() "

    Sub FillNewRow(ByVal iRowIndex As Integer)

        Try
            If dgvOperations.Rows(iRowIndex).IsNewRow = False Then
                bCreating = False
            Else
                bCreating = True
                'Add Routine to Fill ID and Profile Type
                Dim iMax As Integer = GetMaxSTDOpID()
                iMax += 1
                dgvOperations.Rows(iRowIndex).Cells(0).Value = iMax
                dgvOperations.Rows(iRowIndex).Cells(1).Value = txtProfileCode.Text
                dgvOperations.Rows(iRowIndex).Cells(4).Value = True
                dgvOperations.Rows(iRowIndex).Cells(6).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(7).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(8).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(9).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(10).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(11).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(12).Value = 0
                dgvOperations.Rows(iRowIndex).Cells(13).Value = 0

            End If
        Catch ex As Exception

        End Try

    End Sub

#End Region

    'Grid Context Menu
#Region " Event - ctmOperations_Opening "

    Private Sub ctmOperations_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ctmOperations.Opening

        If dgvOperations.SelectedRows.Count = 0 Then
            ctmOperations.Items("CopyToolStripMenuItem").Enabled = False
            ctmOperations.Items("ExportToolStripMenuItem1").Enabled = False
        Else
            ctmOperations.Items("CopyToolStripMenuItem").Enabled = True
            ctmOperations.Items("ExportToolStripMenuItem1").Enabled = True
        End If

    End Sub

#End Region
#Region " Event - dgvOperations_MouseClick "

    Private Sub dgvOperations_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvOperations.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ctmOperations.Show(e.X + Splitter1.Location.X, e.Y + Splitter1.Location.Y + Splitter1.Size.Height + 24)
        End If
    End Sub

#End Region
#Region " Grid Context Menu - Copy "

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click

        'Check user has selected rows
        If dgvOperations.SelectedRows.Count = 0 Then
            MsgBox("Please Select the Row(s) to be Copied.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Initialise String with header line
        Dim s As String = "OpID	ProfileCode	OpToolCode	StdOpCode	Use	OpDescription	OpVariable1	OpVariable2	OpVariable3	OpVariable4	OpVariable5	OpVariable6	OpVariable7	OpVariable8" + vbCrLf

        'Build Cells into Clipboard String
        For Each dr As DataGridViewRow In dgvOperations.SelectedRows
            For Each cCell As DataGridViewCell In dr.Cells
                s += cCell.Value.ToString + vbTab
            Next
            s = Mid(s, 1, Len(s) - 1) + vbCrLf
        Next

        'Post string to clipboard
        Clipboard.SetText(s)

        'MsgBox(s)

    End Sub

#End Region
#Region " Grid Context Menu - Paste "

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click

        'Get Text from the Clipboard
        Dim sClipboard As String = Clipboard.GetText()

        'Check the Text is in a valid format
        If Mid(sClipboard, 1, 4) <> "OpID" Then
            MsgBox("No valid text exists", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Display Clipboard Text
        'MsgBox(sClipboard)

        'Separate Clipboard Rows
        Dim sLines() As String = sClipboard.Split(vbCrLf)

        For Each sLine As String In sLines
            ImportClipboardLine(sLine, txtProfileCode.Text)
        Next

    End Sub

#End Region
#Region " Grid Context Menu - Export "

    Private Sub ExportToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportToolStripMenuItem1.Click

        'Initialise Variables
        Dim s As String = ""

        'Check user has selected rows
        If dgvOperations.SelectedRows.Count = 0 Then
            MsgBox("Please Select the Row(s) to be Exported.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Get Filename
        Dim sdl As New SaveFileDialog
        sdl.DefaultExt = "exp"
        sdl.Filter = "Export File |*.exp"
        sdl.InitialDirectory = "c:\"
        Dim swfdr As System.Windows.Forms.DialogResult = sdl.ShowDialog()

        'If Cancelled Quit routine
        If swfdr = Windows.Forms.DialogResult.Cancel Then Exit Sub

        'Open output file
        FileOpen(1, sdl.FileName, OpenMode.Output, OpenAccess.Default, OpenShare.Default)

        'Print Rows
        For Each dr As DataGridViewRow In dgvOperations.SelectedRows
            s = ""
            For Each cCell As DataGridViewCell In dr.Cells
                s += cCell.Value.ToString + ","
            Next
            s = Mid(s, 1, Len(s) - 1) + vbCrLf
            Print(1, s)
        Next

        'Close File
        FileClose(1)

    End Sub

#End Region
#Region " Grid Context Menu - Import "

    Private Sub ImportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportToolStripMenuItem.Click

        'Declare Variables
        Dim sLine As String
        Dim iPos As Integer = 0
        Dim iLine As Integer = 0
        Dim sProfile As String = ""
        Dim sToolCode As String = ""
        Dim sStdOp As String = ""
        Dim bUse As Boolean = True
        Dim sDesc As String = ""
        Dim dVar1 As Double = 0
        Dim dVar2 As Double = 0
        Dim dVar3 As Double = 0
        Dim dVar4 As Double = 0
        Dim dVar5 As Double = 0
        Dim dVar6 As Double = 0
        Dim dVar7 As Double = 0
        Dim dVar8 As Double = 0

        'Display Dialog
        Dim ofd As New OpenFileDialog
        ofd.DefaultExt = "exp"
        ofd.Filter = "Export File |*.exp"
        ofd.InitialDirectory = "c:\"
        Dim swfdr As System.Windows.Forms.DialogResult = ofd.ShowDialog()

        'If Cancelled Exit Routine
        If swfdr = Windows.Forms.DialogResult.Cancel Then Exit Sub

        'Check File Exists
        If Not File.Exists(ofd.FileName) Then
            MsgBox("Cannot find the requested file", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Open Import File
        FileOpen(1, ofd.FileName, OpenMode.Input, OpenAccess.Default, OpenShare.Default)

        Do While Not EOF(1)
            sLine = LineInput(1)

            'Generate Insert Row
            Dim rInsertRow As DataRow = tblStdOps.NewRow

            'Get new line number
            Dim iMax As Integer = GetMaxSTDOpID()
            iMax += 1

            'Get Line Number
            iPos = InStr(sLine, ",", CompareMethod.Text)
            iLine = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpID") = iMax

            'Get Profile
            iPos = InStr(sLine, ",", CompareMethod.Text)
            sProfile = Mid(sLine, 1, iPos - 1)
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("ProfileCode") = txtProfileCode.Text

            'Get Tool Code
            iPos = InStr(sLine, ",", CompareMethod.Text)
            sToolCode = Mid(sLine, 1, iPos - 1)
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpToolCode") = sToolCode

            'Get StdOp
            iPos = InStr(sLine, ",", CompareMethod.Text)
            sStdOp = Mid(sLine, 1, iPos - 1)
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("StdOpCode") = sStdOp

            'Get Use
            iPos = InStr(sLine, ",", CompareMethod.Text)
            bUse = CBool(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("Use") = bUse

            'Get Description
            iPos = InStr(sLine, ",", CompareMethod.Text)
            sDesc = Mid(sLine, 1, iPos - 1)
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpDescription") = sDesc

            'Get Var1
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar1 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable1") = dVar1

            'Get Var2
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar2 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable2") = dVar2

            'Get Var3
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar3 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable3") = dVar3

            'Get Var4
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar4 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable4") = dVar4

            'Get Var5
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar5 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable5") = dVar5

            'Get Var6
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar6 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable6") = dVar6

            'Get Var7
            iPos = InStr(sLine, ",", CompareMethod.Text)
            dVar7 = CInt(Mid(sLine, 1, iPos - 1))
            sLine = Mid(sLine, iPos + 1)
            rInsertRow.Item("OpVariable7") = dVar7

            'Get Var8
            dVar8 = CInt(sLine)
            rInsertRow.Item("OpVariable8") = dVar8

            tblStdOps.Rows.Add(rInsertRow)

        Loop

        'Close Import File
        FileClose(1)

        FillOperationsGrid(dgvOperations, txtProfileCode.Text, True, "", False, "", False, True)

    End Sub

#End Region
#Region " Grid Context Menu - Delete "

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click

        'Check Rows are Selected
        If dgvOperations.SelectedRows.Count = 0 Then
            MsgBox("Please Select the Row(s) to be deleted.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Check user is sure
        Dim mbrReply As MsgBoxResult = MsgBox("Are you sure you want to delete these rows?", MsgBoxStyle.YesNo)
        If mbrReply = MsgBoxResult.No Then Exit Sub

        'Remove the Rows
        For Each dr As DataGridViewRow In dgvOperations.SelectedRows
            dgvOperations.Rows.Remove(dr)
        Next

        MsgBox("Rows Deleted", MsgBoxStyle.Information)

    End Sub

#End Region

    'Version Change
#Region " Event - cmbVersion_TextChanged() "

    Private Sub cmbVersion_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVersion.TextChanged

        SelectVersion(cmbVersion.Text)

    End Sub

#End Region
#Region " Sub - SelectVersion "

    Sub SelectVersion(ByVal sVersion As String)

        Select Case sVersion
            Case "V1"
                SelectV1()
            Case "V2"
                SelectV2()
            Case "V3"
                SelectV3()
            Case "V4"
                SelectV4()
        End Select

    End Sub

#End Region
#Region " Sub - SelectV1() "

    Sub SelectV1()

        ProfileVersion = cmbVersion.Text

        lblNeuron.Visible = False
        txtNeuron.Visible = False

        lblCentralise.Visible = False
        nebCentralise.Visible = False
        lblOptTol.Visible = False
        nebOptTol.Visible = False
        lblMinOffcut.Visible = False
        nebMinOffcut.Visible = False
        lblGripY.Visible = False
        nebGripY.Visible = False
        lblGripZ.Visible = False
        nebGripZ.Visible = False
        lblID.Visible = False
        nebID.Visible = False
        lblCoreColour.Visible = False
        nebCoreColour.Visible = False
        lblSawCutStyle.Visible = False
        nebSawCutStyle.Visible = False
        lblGripRotate.Visible = False
        chkGripRotate.Visible = False

    End Sub

#End Region
#Region " Sub - SelectV2() "

    Sub SelectV2()

        ProfileVersion = cmbVersion.Text

        lblNeuron.Visible = True
        txtNeuron.Visible = True

        lblCentralise.Visible = False
        nebCentralise.Visible = False
        lblOptTol.Visible = False
        nebOptTol.Visible = False
        lblMinOffcut.Visible = False
        nebMinOffcut.Visible = False
        lblGripY.Visible = False
        nebGripY.Visible = False
        lblGripZ.Visible = False
        nebGripZ.Visible = False
        lblID.Visible = False
        nebID.Visible = False
        lblCoreColour.Visible = False
        nebCoreColour.Visible = False
        lblSawCutStyle.Visible = False
        nebSawCutStyle.Visible = False
        lblGripRotate.Visible = False
        chkGripRotate.Visible = False

    End Sub

#End Region
#Region " Sub - SelectV3() "

    Sub SelectV3()

        ProfileVersion = cmbVersion.Text

        lblNeuron.Visible = False
        txtNeuron.Visible = False

        lblCentralise.Visible = True
        nebCentralise.Visible = True
        lblOptTol.Visible = True
        nebOptTol.Visible = True
        lblMinOffcut.Visible = True
        nebMinOffcut.Visible = True
        lblGripY.Visible = True
        nebGripY.Visible = True
        lblGripZ.Visible = True
        nebGripZ.Visible = True
        lblID.Visible = True
        nebID.Visible = True
        lblCoreColour.Visible = True
        nebCoreColour.Visible = True
        lblSawCutStyle.Visible = True
        nebSawCutStyle.Visible = True
        lblGripRotate.Visible = False
        chkGripRotate.Visible = False

    End Sub

#End Region
#Region " Sub - SelectV4() "

    Sub SelectV4()

        ProfileVersion = cmbVersion.Text

        lblNeuron.Visible = False
        txtNeuron.Visible = False
        lblCentralise.Visible = True
        nebCentralise.Visible = True
        lblOptTol.Visible = True
        nebOptTol.Visible = True
        lblMinOffcut.Visible = True
        nebMinOffcut.Visible = True
        lblGripY.Visible = True
        nebGripY.Visible = True
        lblGripZ.Visible = True
        nebGripZ.Visible = True
        lblID.Visible = True
        nebID.Visible = True
        lblCoreColour.Visible = True
        nebCoreColour.Visible = True
        lblSawCutStyle.Visible = True
        nebSawCutStyle.Visible = True
        lblGripRotate.Visible = True
        chkGripRotate.Visible = True

    End Sub

#End Region

    'Gripper
#Region " Event - nebGripY_ValueChanged() "

    Private Sub nebGripY_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebGripY.ValueChanged

        If optPushGrip.Checked Then
            ShowNewGripper = True
            Redraw()
        Else
            optNoGrip.Checked = False
            optPullGrip.Checked = False
            optPushGrip.Checked = True
        End If

    End Sub

#End Region
#Region " Event - nebGripZ_ValueChanged() "

    Private Sub nebGripZ_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nebGripZ.ValueChanged

        If optPushGrip.Checked Then
            ShowNewGripper = True
            Redraw()
        Else
            optNoGrip.Checked = False
            optPullGrip.Checked = False
            optPushGrip.Checked = True
        End If

    End Sub

#End Region
#Region " Event - txtGripPos_ValueChanged() "

    Private Sub txtGripPos_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGripPos.ValueChanged

        If optPullGrip.Checked Then
            ShowNewGripper = True
            Redraw()
        Else
            optNoGrip.Checked = False
            optPushGrip.Checked = False
            optPullGrip.Checked = True
        End If

    End Sub

#End Region
#Region " Event - optNoGrip_CheckedChanged() "

    Private Sub optNoGrip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNoGrip.CheckedChanged

        ShowNewGripper = False
        Redraw()

    End Sub

#End Region
#Region " Event - optPushGrip_CheckedChanged() "

    Private Sub optPushGrip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPushGrip.CheckedChanged

        ShowNewGripper = True
        Redraw()

    End Sub

#End Region
#Region " Event - optPullGrip_CheckedChanged() "

    Private Sub optPullGrip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPullGrip.CheckedChanged

        ShowNewGripper = True
        Redraw()

    End Sub

#End Region
#Region " Event - chkGripRotate_CheckedChanged() "

    Private Sub chkGripRotate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGripRotate.CheckedChanged

        Redraw()

    End Sub

#End Region

    Private Sub dgvOperations_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOperations.DataError

        ImportMissingOp(sender, e, dgvOperations)

    End Sub

End Class