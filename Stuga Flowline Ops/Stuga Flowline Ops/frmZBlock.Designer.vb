﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmZBlock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmZBlock))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnUndoProfiles = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnUpdateReplicates = New System.Windows.Forms.Button()
        Me.dgvProfiles = New System.Windows.Forms.DataGridView()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.grpType = New System.Windows.Forms.GroupBox()
        Me.chkMk6 = New System.Windows.Forms.CheckBox()
        Me.grpBlock = New System.Windows.Forms.GroupBox()
        Me.rbBlock4 = New System.Windows.Forms.RadioButton()
        Me.rbBlock3 = New System.Windows.Forms.RadioButton()
        Me.rbBlock2 = New System.Windows.Forms.RadioButton()
        Me.rbBlock1 = New System.Windows.Forms.RadioButton()
        Me.rbBlock0 = New System.Windows.Forms.RadioButton()
        Me.grpSteps = New System.Windows.Forms.GroupBox()
        Me.nebSteps = New System.Windows.Forms.NumericUpDown()
        Me.grpJog = New System.Windows.Forms.GroupBox()
        Me.btnLeft = New System.Windows.Forms.Button()
        Me.btnRight = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lstReplicates = New System.Windows.Forms.ListBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblReplicates = New System.Windows.Forms.Label()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.pbPosCalc = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvProfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.grpType.SuspendLayout()
        Me.grpBlock.SuspendLayout()
        Me.grpSteps.SuspendLayout()
        CType(Me.nebSteps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpJog.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Navy
        Me.Panel1.Controls.Add(Me.lblMessage)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.btnUpdateReplicates)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 509)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(973, 87)
        Me.Panel1.TabIndex = 2
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.ForeColor = System.Drawing.Color.White
        Me.lblMessage.Location = New System.Drawing.Point(149, 16)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(13, 13)
        Me.lblMessage.TabIndex = 5
        Me.lblMessage.Text = "?"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.btnUndoProfiles)
        Me.Panel5.Controls.Add(Me.btnClose)
        Me.Panel5.Controls.Add(Me.btnSave)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(533, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(440, 87)
        Me.Panel5.TabIndex = 4
        '
        'btnUndoProfiles
        '
        Me.btnUndoProfiles.BackColor = System.Drawing.Color.White
        Me.btnUndoProfiles.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndoProfiles.Image = CType(resources.GetObject("btnUndoProfiles.Image"), System.Drawing.Image)
        Me.btnUndoProfiles.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUndoProfiles.Location = New System.Drawing.Point(149, 7)
        Me.btnUndoProfiles.Name = "btnUndoProfiles"
        Me.btnUndoProfiles.Size = New System.Drawing.Size(140, 77)
        Me.btnUndoProfiles.TabIndex = 6
        Me.btnUndoProfiles.Text = "Cancel Change"
        Me.btnUndoProfiles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUndoProfiles.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(295, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(140, 77)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Save and Exit"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(3, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 77)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save Profile"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnUpdateReplicates
        '
        Me.btnUpdateReplicates.BackColor = System.Drawing.Color.White
        Me.btnUpdateReplicates.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateReplicates.Image = CType(resources.GetObject("btnUpdateReplicates.Image"), System.Drawing.Image)
        Me.btnUpdateReplicates.Location = New System.Drawing.Point(3, 6)
        Me.btnUpdateReplicates.Name = "btnUpdateReplicates"
        Me.btnUpdateReplicates.Size = New System.Drawing.Size(127, 78)
        Me.btnUpdateReplicates.TabIndex = 1
        Me.btnUpdateReplicates.Text = "Update Replicates"
        Me.btnUpdateReplicates.UseVisualStyleBackColor = False
        '
        'dgvProfiles
        '
        Me.dgvProfiles.AllowUserToAddRows = False
        Me.dgvProfiles.AllowUserToDeleteRows = False
        Me.dgvProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProfiles.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvProfiles.Location = New System.Drawing.Point(0, 0)
        Me.dgvProfiles.Name = "dgvProfiles"
        Me.dgvProfiles.ReadOnly = True
        Me.dgvProfiles.Size = New System.Drawing.Size(439, 509)
        Me.dgvProfiles.TabIndex = 3
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(439, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(8, 509)
        Me.Splitter1.TabIndex = 4
        Me.Splitter1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Navy
        Me.Panel2.Controls.Add(Me.grpType)
        Me.Panel2.Controls.Add(Me.grpBlock)
        Me.Panel2.Controls.Add(Me.grpSteps)
        Me.Panel2.Controls.Add(Me.grpJog)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(447, 353)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(526, 156)
        Me.Panel2.TabIndex = 5
        '
        'grpType
        '
        Me.grpType.Controls.Add(Me.chkMk6)
        Me.grpType.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpType.ForeColor = System.Drawing.Color.White
        Me.grpType.Location = New System.Drawing.Point(252, 84)
        Me.grpType.Name = "grpType"
        Me.grpType.Size = New System.Drawing.Size(92, 69)
        Me.grpType.TabIndex = 19
        Me.grpType.TabStop = False
        Me.grpType.Text = "Type"
        '
        'chkMk6
        '
        Me.chkMk6.AutoSize = True
        Me.chkMk6.Location = New System.Drawing.Point(19, 32)
        Me.chkMk6.Name = "chkMk6"
        Me.chkMk6.Size = New System.Drawing.Size(52, 20)
        Me.chkMk6.TabIndex = 0
        Me.chkMk6.Text = "Mk6"
        Me.chkMk6.UseVisualStyleBackColor = True
        '
        'grpBlock
        '
        Me.grpBlock.Controls.Add(Me.rbBlock4)
        Me.grpBlock.Controls.Add(Me.rbBlock3)
        Me.grpBlock.Controls.Add(Me.rbBlock2)
        Me.grpBlock.Controls.Add(Me.rbBlock1)
        Me.grpBlock.Controls.Add(Me.rbBlock0)
        Me.grpBlock.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpBlock.ForeColor = System.Drawing.Color.White
        Me.grpBlock.Location = New System.Drawing.Point(350, 6)
        Me.grpBlock.Name = "grpBlock"
        Me.grpBlock.Size = New System.Drawing.Size(164, 147)
        Me.grpBlock.TabIndex = 18
        Me.grpBlock.TabStop = False
        Me.grpBlock.Text = "Z Block"
        '
        'rbBlock4
        '
        Me.rbBlock4.AutoSize = True
        Me.rbBlock4.Location = New System.Drawing.Point(15, 125)
        Me.rbBlock4.Name = "rbBlock4"
        Me.rbBlock4.Size = New System.Drawing.Size(72, 20)
        Me.rbBlock4.TabIndex = 4
        Me.rbBlock4.Text = "Block 4"
        Me.rbBlock4.UseVisualStyleBackColor = True
        '
        'rbBlock3
        '
        Me.rbBlock3.AutoSize = True
        Me.rbBlock3.Location = New System.Drawing.Point(15, 99)
        Me.rbBlock3.Name = "rbBlock3"
        Me.rbBlock3.Size = New System.Drawing.Size(72, 20)
        Me.rbBlock3.TabIndex = 3
        Me.rbBlock3.Text = "Block 3"
        Me.rbBlock3.UseVisualStyleBackColor = True
        '
        'rbBlock2
        '
        Me.rbBlock2.AutoSize = True
        Me.rbBlock2.Location = New System.Drawing.Point(15, 73)
        Me.rbBlock2.Name = "rbBlock2"
        Me.rbBlock2.Size = New System.Drawing.Size(72, 20)
        Me.rbBlock2.TabIndex = 2
        Me.rbBlock2.Text = "Block 2"
        Me.rbBlock2.UseVisualStyleBackColor = True
        '
        'rbBlock1
        '
        Me.rbBlock1.AutoSize = True
        Me.rbBlock1.Location = New System.Drawing.Point(15, 47)
        Me.rbBlock1.Name = "rbBlock1"
        Me.rbBlock1.Size = New System.Drawing.Size(72, 20)
        Me.rbBlock1.TabIndex = 1
        Me.rbBlock1.Text = "Block 1"
        Me.rbBlock1.UseVisualStyleBackColor = True
        '
        'rbBlock0
        '
        Me.rbBlock0.AutoSize = True
        Me.rbBlock0.Checked = True
        Me.rbBlock0.Location = New System.Drawing.Point(15, 21)
        Me.rbBlock0.Name = "rbBlock0"
        Me.rbBlock0.Size = New System.Drawing.Size(82, 20)
        Me.rbBlock0.TabIndex = 0
        Me.rbBlock0.TabStop = True
        Me.rbBlock0.Text = "No Block"
        Me.rbBlock0.UseVisualStyleBackColor = True
        '
        'grpSteps
        '
        Me.grpSteps.Controls.Add(Me.nebSteps)
        Me.grpSteps.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpSteps.ForeColor = System.Drawing.Color.White
        Me.grpSteps.Location = New System.Drawing.Point(171, 84)
        Me.grpSteps.Name = "grpSteps"
        Me.grpSteps.Size = New System.Drawing.Size(75, 69)
        Me.grpSteps.TabIndex = 17
        Me.grpSteps.TabStop = False
        Me.grpSteps.Text = "Steps"
        '
        'nebSteps
        '
        Me.nebSteps.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nebSteps.Location = New System.Drawing.Point(6, 19)
        Me.nebSteps.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nebSteps.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nebSteps.Name = "nebSteps"
        Me.nebSteps.Size = New System.Drawing.Size(60, 39)
        Me.nebSteps.TabIndex = 0
        Me.nebSteps.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'grpJog
        '
        Me.grpJog.Controls.Add(Me.btnLeft)
        Me.grpJog.Controls.Add(Me.btnRight)
        Me.grpJog.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpJog.ForeColor = System.Drawing.Color.White
        Me.grpJog.Location = New System.Drawing.Point(171, 6)
        Me.grpJog.Name = "grpJog"
        Me.grpJog.Size = New System.Drawing.Size(173, 70)
        Me.grpJog.TabIndex = 15
        Me.grpJog.TabStop = False
        Me.grpJog.Text = "Jog"
        '
        'btnLeft
        '
        Me.btnLeft.BackColor = System.Drawing.Color.LightGreen
        Me.btnLeft.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeft.Image = CType(resources.GetObject("btnLeft.Image"), System.Drawing.Image)
        Me.btnLeft.Location = New System.Drawing.Point(6, 21)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(69, 40)
        Me.btnLeft.TabIndex = 8
        Me.btnLeft.UseVisualStyleBackColor = False
        '
        'btnRight
        '
        Me.btnRight.BackColor = System.Drawing.Color.LightGreen
        Me.btnRight.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRight.Image = CType(resources.GetObject("btnRight.Image"), System.Drawing.Image)
        Me.btnRight.Location = New System.Drawing.Point(99, 21)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(68, 40)
        Me.btnRight.TabIndex = 9
        Me.btnRight.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lstReplicates)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(165, 156)
        Me.Panel3.TabIndex = 2
        '
        'lstReplicates
        '
        Me.lstReplicates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstReplicates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstReplicates.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstReplicates.FormattingEnabled = True
        Me.lstReplicates.ItemHeight = 18
        Me.lstReplicates.Location = New System.Drawing.Point(0, 32)
        Me.lstReplicates.Name = "lstReplicates"
        Me.lstReplicates.Size = New System.Drawing.Size(165, 124)
        Me.lstReplicates.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblReplicates)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(165, 32)
        Me.Panel4.TabIndex = 0
        '
        'lblReplicates
        '
        Me.lblReplicates.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblReplicates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblReplicates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblReplicates.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReplicates.ForeColor = System.Drawing.Color.White
        Me.lblReplicates.Location = New System.Drawing.Point(0, 0)
        Me.lblReplicates.Name = "lblReplicates"
        Me.lblReplicates.Size = New System.Drawing.Size(165, 32)
        Me.lblReplicates.TabIndex = 0
        Me.lblReplicates.Text = "Replicates"
        Me.lblReplicates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter2.Location = New System.Drawing.Point(447, 343)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(526, 10)
        Me.Splitter2.TabIndex = 6
        Me.Splitter2.TabStop = False
        '
        'pbPosCalc
        '
        Me.pbPosCalc.BackColor = System.Drawing.Color.White
        Me.pbPosCalc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPosCalc.Location = New System.Drawing.Point(447, 0)
        Me.pbPosCalc.Name = "pbPosCalc"
        Me.pbPosCalc.Size = New System.Drawing.Size(526, 343)
        Me.pbPosCalc.TabIndex = 7
        Me.pbPosCalc.TabStop = False
        '
        'frmZBlock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(973, 596)
        Me.Controls.Add(Me.pbPosCalc)
        Me.Controls.Add(Me.Splitter2)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.dgvProfiles)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmZBlock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Z Block Setup"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        CType(Me.dgvProfiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.grpType.ResumeLayout(False)
        Me.grpType.PerformLayout()
        Me.grpBlock.ResumeLayout(False)
        Me.grpBlock.PerformLayout()
        Me.grpSteps.ResumeLayout(False)
        CType(Me.nebSteps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpJog.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.pbPosCalc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnUndoProfiles As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnUpdateReplicates As System.Windows.Forms.Button
    Friend WithEvents dgvProfiles As System.Windows.Forms.DataGridView
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents grpJog As System.Windows.Forms.GroupBox
    Friend WithEvents grpSteps As System.Windows.Forms.GroupBox
    Friend WithEvents nebSteps As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnLeft As System.Windows.Forms.Button
    Friend WithEvents btnRight As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lstReplicates As System.Windows.Forms.ListBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblReplicates As System.Windows.Forms.Label
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents pbPosCalc As System.Windows.Forms.PictureBox
    Friend WithEvents grpBlock As System.Windows.Forms.GroupBox
    Friend WithEvents rbBlock4 As System.Windows.Forms.RadioButton
    Friend WithEvents rbBlock3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbBlock2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbBlock1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbBlock0 As System.Windows.Forms.RadioButton
    Friend WithEvents grpType As System.Windows.Forms.GroupBox
    Friend WithEvents chkMk6 As System.Windows.Forms.CheckBox
End Class
