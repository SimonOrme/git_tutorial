﻿Public Class frmEditProfiles

    Dim bInitialising As Boolean = True

#Region " Form - Load "

    Private Sub frmEditProfiles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        bInitialising = True

        BindProfileGrid(dgvProfile, False, "")

        FillSystems(cmbProfSuite)
        FillProfileTypes(cmbType)

        bInitialising = False

        cmbProfSuite.SelectedValue = prfSelectedProfileSuite
        cmbType.SelectedValue = prfSelectedProfileType
        chkShowReplicates.Checked = prfShowReplicates

    End Sub

#End Region

#Region " Button - Gripper Setup "

    Private Sub btnGripperSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGripperSetup.Click

        Dim frmGripperSetup As New frmGripper
        frmGripperSetup.frmParent = Me

        frmGripperSetup.Show()

    End Sub

#End Region
#Region " Button - Sync Z Blocks "

    Private Sub btnSyncProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSyncProfile.Click

        Dim sLines(1000) As String
        Dim iLinecount As Integer = 0

        'Check OK to Continue
        Dim mbr As MsgBoxResult = MsgBox("This will overwrite the Z Block settings in Profile.mul with the ones set up in profile.saw." + vbCrLf + "Are You Sure?", MsgBoxStyle.YesNo)
        If mbr = MsgBoxResult.No Then Exit Sub

        'Try to Open File
        Try
            FileOpen(1, prfProfileSawLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Can not find Profile.saw" + vbCrLf + ex.Message)
            Exit Sub
        End Try

        'Read in the Profile.saw Lines
        While Not EOF(1)
            sLines(iLinecount) = LineInput(1)
            iLinecount += 1
        End While

        'Close Saw File
        FileClose(1)

        'Update Profile Table with Saw Z Block Settings
        Dim dgvr As DataGridViewRow
        For Each dgvr In Me.dgvProfile.Rows
            Dim sMulProfile As String = dgvr.Cells(0).Value
            Dim sSawProfile As String = ""
            For Each sSawProfile In sLines
                If InStr(sSawProfile, sMulProfile, CompareMethod.Text) <> 0 And sSawProfile <> "#" Then
                    Dim iPos As Integer = InStr(sSawProfile, ",", CompareMethod.Text)
                    Dim sSplitString As String = Mid(sSawProfile, iPos + 1)
                    iPos = InStr(sSplitString, ",", CompareMethod.Text)
                    sSplitString = Mid(sSplitString, iPos + 1)
                    iPos = InStr(sSplitString, ",", CompareMethod.Text)
                    sSplitString = Mid(sSplitString, 1, iPos - 1)
                    dgvr.Cells(1).Value = CInt(sSplitString)
                End If
            Next
        Next

    End Sub

#End Region
#Region " Button - Create Profile.Saw "

    Private Sub btnCreateProfileSaw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateProfileSaw.Click

        CreateProfileSaw()

    End Sub

#End Region
#Region " Button - Save "

    Private Sub btnSaveProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveProfile.Click

        SaveProfileMUL()

        MsgBox("Profiles Saved.", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Button - Undo "

    Private Sub btnUndoProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUndoProfile.Click

        'Get Profile Data from Profile.Mul
        tblProfile = GetProfileTable()
        dsProfile.Tables.Clear()
        dsProfile.Tables.Add(tblProfile)

        BindProfileGrid(dgvProfile, True, "")

    End Sub

#End Region
#Region " Button - Exit "

    Private Sub btnCloseProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCloseProfile.Click

        SaveProfileMUL()

        Me.Close()

    End Sub

#End Region

#Region " Event - cmbSuite_SelectedValueChanged "

    Private Sub cmbSuite_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProfSuite.SelectedValueChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
            If Not binitialising Then prfSelectedProfileSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
        Catch ex As Exception

        End Try
        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region " Event - cmbType_SelectedValueChanged "

    Private Sub cmbType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbType.SelectedValueChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
            If Not binitialising Then prfSelectedProfileType = cmbType.SelectedValue
        Catch ex As Exception

        End Try
        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region " Event - chkShowReplicates_CheckedChanged() "

    Private Sub chkShowReplicates_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowReplicates.CheckedChanged

        Dim sSuite As String = ""
        Try
            sSuite = cmbProfSuite.SelectedValue
        Catch ex As Exception

        End Try

        Dim iType As Integer = -1
        Try
            iType = cmbType.SelectedValue
        Catch ex As Exception

        End Try

        If Not binitialising Then prfShowReplicates = chkShowReplicates.Checked

        FilterProfiles(sSuite, iType)

    End Sub

#End Region
#Region "Sub - dgvProfile_DataError() "

    Private Sub dgvProfile_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvProfile.DataError

        'Find Row an Column Which Caused the error
        Dim iColumn As Integer = e.ColumnIndex
        Dim iRow As Integer = e.RowIndex

        Select Case iColumn

            Case 2          'if Flip Column is at fault
                If dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "0" Then dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "N"
                If dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "1" Then dgvProfile.Rows(iRow).Cells.Item(iColumn).Value = "Y"

            Case 10         'If Systems Column was at Fault

                'Get the Cell Value for the profile suite
                Dim sSuite As String = dgvProfile.Rows(iRow).Cells.Item(iColumn).Value

                'Ask the user for a description
                Dim sDesc As String = ""
                Do While Len(sDesc) = 0
                    sDesc = InputBox("Please Enter the Suite description for " + sSuite + "." + vbCrLf + "This will then be added to SYSTEMS.MUL", sSuite & " does not exist in the Systems table.", "")
                Loop

                'Set the system to use this Suite
                Dim bUse As Boolean = True

                'add new Suite to the Systems Table
                Dim rRow As DataRow
                rRow = tblSystems.NewRow
                rRow.Item("Suite") = sSuite
                rRow.Item("Description") = sDesc
                rRow.Item("Used") = bUse
                tblSystems.Rows.Add(rRow)

                'Update Systems.Mul
                SaveTblSystems()

            Case Else

                'Display the error message
                MsgBox(e.Exception.Message)

        End Select

    End Sub

#End Region

#Region " Sub - FilterProfiles() "

    Sub FilterProfiles(ByVal sSuite As String, ByVal iType As Integer)

        Dim sFilter As String = ""

        If sSuite = "" Or iType = -1 Then
            Exit Sub
        End If

        If sSuite = "ALL" And iType = 0 Then
            sFilter = ""
        ElseIf sSuite = "ALL" Then
            sFilter = "ProfileTypeID = " + iType.ToString
        ElseIf iType = 0 Then
            sFilter = "System = '" + sSuite + "'"
        Else
            sFilter = "ProfileTypeID = " + iType.ToString + " AND System = '" + sSuite + "'"
        End If
        If Not chkShowReplicates.Checked And Len(sFilter) > 0 Then sFilter += " AND"
        If Not chkShowReplicates.Checked Then sFilter += " (ReplicateID = '' OR ReplicateID IS NULL)"

        BindProfileGrid(dgvProfile, True, sFilter)

        Console.WriteLine("*" + sSuite + "*" + iType.ToString + "*" + sFilter + "*")

    End Sub

#End Region

    Private Sub btnZBlock_Click(sender As System.Object, e As System.EventArgs) Handles btnZBlock.Click

        Dim frmzblock As New frmZBlock
        frmzblock.frmParent = Me
        frmzblock.Show()

    End Sub
End Class