﻿Public Class frmTestGen

#Region " Declare Variables "

    Private dsBar1 As DataSet
    Private dsBar2 As DataSet
    Private dsBar3 As DataSet
    Private dsBar4 As DataSet
    Private dsBar5 As DataSet
    Private dsBar6 As DataSet

#End Region

#Region " Form - Load "

    Private Sub frmTestGen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FillProfiles(cmbProfile)
        dsBar1 = InitTable("Bar1")
        dsBar2 = InitTable("Bar2")
        dsBar3 = InitTable("Bar3")
        dsBar4 = InitTable("Bar4")
        dsBar5 = InitTable("Bar5")
        dsBar6 = InitTable("Bar6")

        InitBarGrid(dgvBar1, dsBar1, "Bar1")
        InitBarGrid(dgvBar2, dsBar2, "Bar2")
        InitBarGrid(dgvBar3, dsBar3, "Bar3")
        InitBarGrid(dgvBar4, dsBar4, "Bar4")
        InitBarGrid(dgvBar5, dsBar5, "Bar5")
        InitBarGrid(dgvBar6, dsBar6, "Bar6")

    End Sub

#End Region

#Region " Event - cmbProfile_TextChanged() "

    Private Sub cmbProfile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProfile.TextChanged

        FillOperationsGrid(dgvOperations, cmbProfile.Text, True, "", False, "", False, False)

        dsBar1 = InitTable("Bar1")
        dsBar2 = InitTable("Bar2")
        dsBar3 = InitTable("Bar3")
        dsBar4 = InitTable("Bar4")
        dsBar5 = InitTable("Bar5")
        dsBar6 = InitTable("Bar6")

        InitBarGrid(dgvBar1, dsBar1, "Bar1")
        InitBarGrid(dgvBar2, dsBar2, "Bar2")
        InitBarGrid(dgvBar3, dsBar3, "Bar3")
        InitBarGrid(dgvBar4, dsBar4, "Bar4")
        InitBarGrid(dgvBar5, dsBar5, "Bar5")
        InitBarGrid(dgvBar6, dsBar6, "Bar6")

    End Sub

#End Region
#Region " Event - dgvOperations_SelectionChanged() "

    Private Sub dgvOperations_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOperations.SelectionChanged

        'Declare Variables
        Dim sr As DataGridViewRow

        'Make Sure An Operation Is Selected
        Dim iSelectedRows As Integer = dgvOperations.SelectedRows.Count
        Dim iSelectedCells As Integer = dgvOperations.SelectedCells.Count
        If iSelectedRows = 0 And iSelectedCells = 0 Then
            Exit Sub
        End If

        'Get Profile Code
        If iSelectedRows > 0 Then

            'Row Selected
            sr = dgvOperations.SelectedRows(0)

        Else

            'Cell Selected
            Dim ri As Integer = dgvOperations.SelectedCells(0).RowIndex
            sr = dgvOperations.Rows(ri)
            If sr.Cells.Count < 13 Then Exit Sub

        End If

        lblOpData.Text = sr.Cells(2).Value

    End Sub

#End Region

#Region " Buttons - Bar "

    Private Sub btnBar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar1.Click
        SelectBar(sender)
    End Sub
    Private Sub btnBar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar2.Click
        SelectBar(sender)
    End Sub
    Private Sub btnBar3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar3.Click
        SelectBar(sender)
    End Sub
    Private Sub btnBar4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar4.Click
        SelectBar(sender)
    End Sub
    Private Sub btnBar5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar5.Click
        SelectBar(sender)
    End Sub
    Private Sub btnBar6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBar6.Click
        SelectBar(sender)
    End Sub

#End Region
#Region " Buttons - Position "

    '100
    Private Sub btn100_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn100.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn133_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn133.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn150_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn150.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn166_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn166.Click
        SelectPosition(sender)
    End Sub

    '200
    Private Sub btn200_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn200.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn233_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn233.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn250_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn250.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn266_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn266.Click
        SelectPosition(sender)
    End Sub

    '300
    Private Sub btn300_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn300.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn333_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn333.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn350_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn350.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn366_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn366.Click
        SelectPosition(sender)
    End Sub

    '400
    Private Sub btn400_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn400.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn433_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn433.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn450_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn450.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn466_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn466.Click
        SelectPosition(sender)
    End Sub

    '500
    Private Sub btn500_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn500.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn533_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn533.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn550_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn550.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn566_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn566.Click
        SelectPosition(sender)
    End Sub

    '600
    Private Sub btn600_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn600.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn633_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn633.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn650_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn650.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn666_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn666.Click
        SelectPosition(sender)
    End Sub

    '700
    Private Sub btn700_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn700.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn733_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn733.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn750_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn750.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn766_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn766.Click
        SelectPosition(sender)
    End Sub

    '800
    Private Sub btn800_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn800.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn833_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn833.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn850_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn850.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn866_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn866.Click
        SelectPosition(sender)
    End Sub

    '900
    Private Sub btn900_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn900.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn933_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn933.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn950_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn950.Click
        SelectPosition(sender)
    End Sub
    Private Sub btn966_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn966.Click
        SelectPosition(sender)
    End Sub

#End Region
#Region " Button - Clear All "

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        dsBar1 = InitTable("Bar1")
        dsBar2 = InitTable("Bar2")
        dsBar3 = InitTable("Bar3")
        dsBar4 = InitTable("Bar4")
        dsBar5 = InitTable("Bar5")
        dsBar6 = InitTable("Bar6")

        InitBarGrid(dgvBar1, dsBar1, "Bar1")
        InitBarGrid(dgvBar2, dsBar2, "Bar2")
        InitBarGrid(dgvBar3, dsBar3, "Bar3")
        InitBarGrid(dgvBar4, dsBar4, "Bar4")
        InitBarGrid(dgvBar5, dsBar5, "Bar5")
        InitBarGrid(dgvBar6, dsBar6, "Bar6")

    End Sub

#End Region
#Region " Button - Add Operation "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Dim rRow As DataRow

        Select Case lblBarData.Text
            Case "1"
                rRow = dsBar1.Tables("Bar1").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar1.Tables("Bar1").Rows.Add(rRow)
            Case "2"
                rRow = dsBar2.Tables("Bar2").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar2.Tables("Bar2").Rows.Add(rRow)
            Case "3"
                rRow = dsBar3.Tables("Bar3").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar3.Tables("Bar3").Rows.Add(rRow)
            Case "4"
                rRow = dsBar4.Tables("Bar4").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar4.Tables("Bar4").Rows.Add(rRow)
            Case "5"
                rRow = dsBar5.Tables("Bar5").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar5.Tables("Bar5").Rows.Add(rRow)
            Case "6"
                rRow = dsBar6.Tables("Bar6").NewRow
                rRow.Item("Pos") = CInt(txtPosData.Text)
                rRow.Item("ToolCode") = lblOpData.Text
                dsBar6.Tables("Bar6").Rows.Add(rRow)
            Case Else
                Exit Sub
        End Select

    End Sub

#End Region
#Region " Button - Create 449 "

    Private Sub btn449_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn449.Click

        Dim sFile As String = prfBatchLoc + cmbProfile.Text + ".449"
        Dim sProfile As String = cmbProfile.Text

        'Check Batch Folder Exists
        If Not System.IO.Directory.Exists(prfBatchLoc) Then
            MsgBox("Please Create Batch Folder: " + prfBatchLoc, MsgBoxStyle.Information)
            Exit Sub
        End If

        FileOpen(1, sFile, OpenMode.Output, OpenAccess.Default)
        Print(1, "B," + sProfile + ",S" + vbCrLf)
        Print(1, "L," + sProfile + ",WHITE,1,60000,001" + vbCrLf)

        If dgvBar1.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000001" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar1.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        If dgvBar2.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000002" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar2.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        If dgvBar3.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000003" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar3.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        If dgvBar4.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000004" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar4.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        If dgvBar5.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000005" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar5.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        If dgvBar6.Rows.Count > 1 Then
            Print(1, "P,09800,||,01,01,                                        ,0900,0900,000000000006" + vbCrLf)
            For Each row As DataGridViewRow In dgvBar6.Rows
                If row.Cells(1).Value <> "" Then
                    Print(1, "O," + Int(row.Cells(0).Value * 10).ToString + "," + row.Cells(1).Value + ",000,000,000" + vbCrLf)
                End If
            Next
        End If

        FileClose(1)

    End Sub

#End Region

#Region " Function - InitTable() "

    Function InitTable(ByVal TableID As String) As DataSet

        Dim ds As New DataSet
        Dim tbl As New DataTable(TableID)
        tbl.Locale = System.Globalization.CultureInfo.InvariantCulture

        tbl.Columns.Clear()

        'Pos
        Dim cOpID As New DataColumn("Pos")
        cOpID.DataType = GetType(Integer)
        tbl.Columns.Add(cOpID)

        'ToolCode
        Dim cProfileCode As New DataColumn("ToolCode")
        cProfileCode.DataType = GetType(String)
        tbl.Columns.Add(cProfileCode)

        ds.Tables.Add(tbl)

        Return ds

    End Function

#End Region

#Region " Sub - SelectBar() "

    Sub SelectBar(ByVal sender As System.Object)

        Dim btn As System.Windows.Forms.Button = sender
        Me.lblBarData.Text = btn.Text

    End Sub

#End Region
#Region " Sub - SelectPosition() "

    Sub SelectPosition(ByVal sender As System.Object)

        Dim btn As System.Windows.Forms.Button = sender
        Me.txtPosData.Text = btn.Text

    End Sub

#End Region
#Region " Sub - InitGrid() "

    Sub InitBarGrid(ByVal dgv As DataGridView, ByVal ds As DataSet, ByVal tbl As String)

        Dim bs As New BindingSource(ds, tbl)
        dgv.DataSource = bs

        'Pos
        With dgv.Columns(0)
            .HeaderText = "Position"
            .Width = 75
        End With


        'ToolCode
        With dgv.Columns(1)
            .HeaderText = "Tool Code"
            .Width = 162
        End With

    End Sub

#End Region


End Class