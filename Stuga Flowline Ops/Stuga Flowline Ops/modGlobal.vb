﻿Imports System
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Module modGlobal

#Region " Variable Declaration "

    Public prfSettingsPath As String = Application.StartupPath & "\settings.mul"
    Public prfPasswordPath As String = Application.StartupPath & "\settings.pwl"

    'Taken from Settings.mul
    Public prfProfileMulLoc As String = "c:\multi\profile.mul"
    Public prfStdOpsMulLoc As String = "c:\multi\stdops.mul"
    Public prfZStdOpDefMulLoc As String = "c:\multi\zstdopdef.mul"
    Public prfOpTypesLoc As String = "c:\multi\Optypes.mul"
    Public prfProfileSawLoc As String = "c:\saw\profile.saw"
    Public prfOpdesignLoc As String = "c:\multi\opdesign\"
    Public prfBatchLoc As String = "C:\saw\batches"
    Public prfDXFLoc As String = "c:\multi\DXF\"
    Public prfUseNeuron As Boolean = False
    Public prfDataLoc As String = "C:\StugaCNC\StugaData.mdf"
    Public prfSystemsLoc As String = "C:\multi\SYSTEMS.MUL"
    Public prfHardwareLoc As String = "C:\multi\HARDWARE.MUL"
    Public prfPrepsLoc As String = "C:\multi\PREPS.MUL"
    Public prfDiagnoseLoc As String = "C:\multi\DIAGNOSE\"
    Public prfAHAdjustLoc As String = "C:\multi\AHAdjust.MUL"
    Public prfTipToCollet As Double = 50.0

    'Taken from Settings.PWL
    Public prfPassword As String = "blank"
    Public prfPassEncrypted() As Byte
    Private ReadOnly key() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
    Private ReadOnly iv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}
    Public TripleDES As New clsTripleDES(key, iv)

    'Taken From Danger.Mul
    Public prfOuterRingRad As Short = 253 ' 253 Autoflow, 200 All others
    Public prfDangerY As Double = 30
    Public prfDangerZ As Double = -20
    Public prfDangerRad As Short = 145
    Public prfDatumSize As Single = 10
    Public prfVBladeOffset As Single = 129  '129 For ZX3 / Microline - 110 for Flowline
    Public prfVFrontDatum As Single = 107.5  'For ZX3 / Microline
    Public prfVRearDatum As Single = -7.5  'For ZX3 / Microline
    Public prfVBladeDia As Single = 200 'For ZX3 / Microline
    Public prfVCylinder As Single = 125 'Measured Microline
    Public ProfileWidth As Single = 0
    Public ProfileHeight As Single = 0
    Public ShowDangerRings As Boolean = False
    Public ShowNewGripper As Boolean = False
    Public prfPushGripperMinY As Integer = -2
    Public prfPushGripperMaxY As Integer = 80
    Public prfPushGripperMinZ As Integer = 30
    Public prfPushGripperMaxZ As Integer = 100
    Public prfPushGripperHeight As Double = 7
    Public prfPushGripperWidth As Double = 5
    Public prfPullGripperMinY As Integer = 20
    Public prfPullGripperMaxY As Integer = 60
    Public prfPullGripperMinZ As Integer = 18
    Public prfPullGripperMaxZ As Integer = 30
    Public prfPullGripperHeight As Double = 9
    Public prfPullGripperZPos As Integer = 20
    Public prfSelectedProfileType As Integer = 0
    Public prfSelectedProfileSuite As String = "ALL"
    Public prfShowReplicates As Boolean = True
    Public prfZBlock1 As Single = 20
    Public prfZBlock2 As Single = 20
    Public prfZBlock3 As Single = 20
    Public prfZBlock4 As Single = 20
    Public prfWheelY As Integer = 15


    Public ProfileVersion As String = "V1"

    Public conString As String = "Data Source=.\SQLEXPRESS;AttachDbFilename='" + prfDataLoc + "';Integrated Security=True;Connect Timeout=30;User Instance=True;Asynchronous Processing=True"

    Public sQ As String = Chr(34)

    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

    Public iScale As Integer = 3

    Public prfAccess As String = "User"

    Public Importing As Boolean = False

    'Mul File Tables
    Public dsProfile As New DataSet("tblProfile")
    Public tblProfile As DataTable

    Public dsStdOps As New DataSet("tblStdOps")
    Public tblStdOps As DataTable

    Public dsStdOpDef As New DataSet("tblStdOpDef")
    Public tblStdOpDef As DataTable

    Public dsHardware As New DataSet("tblHardware")
    Public tblHardware As DataTable

    Public dsPreps As New DataSet("tblPreps")
    Public tblPreps As DataTable

    Public dsSystems As New DataSet("tblSystems")
    Public tblSystems As DataTable

    Public dsProfileTypes As New DataSet("tblProfileTypes")
    Public tblProfileTypes As DataTable

    Public dsOpTypes As New DataSet("tblOpTypes")
    Public tblOpTypes As DataTable

    Public stdOpsValid As Boolean = False

#End Region

#Region " Fill Combobox - Replicates "

    Function FillReplicates(ByVal cmbReplicates As ComboBox) As Boolean

        cmbReplicates.DataSource = tblProfile
        cmbReplicates.DisplayMember = "CodeName"
        cmbReplicates.ValueMember = "CodeName"

    End Function

#End Region

#Region " Function - RSAEncrypt() "

    Public Function RSAEncrypt(ByVal DataToEncrypt() As Byte, ByVal ContainerName As String, ByVal DoOAEPPadding As Boolean) As Byte()

        Try
            ' Create a new instance of CspParameters. Pass 13 to specify a DSA container or 1 to specify an RSA container. The default is 1.
            Dim cspParams As New CspParameters

            ' Specify the container name using the passed variable.
            cspParams.KeyContainerName = ContainerName

            'Create a new instance of RSACryptoServiceProvider.
            'Pass the CspParameters class to use the key 
            'from the key in the container.
            Dim RSAalg As New RSACryptoServiceProvider(cspParams)

            'Encrypt the passed byte array and specify OAEP padding.  
            'OAEP padding is only available on Microsoft Windows XP or
            'later.  
            Return RSAalg.Encrypt(DataToEncrypt, DoOAEPPadding)
            'Catch and display a CryptographicException  
            'to the console.

        Catch e As CryptographicException

            Console.WriteLine(e.Message)
            Return Nothing

        End Try

    End Function

#End Region
#Region " Function - RSADecrypt() "

    Public Function RSADecrypt(ByVal DataToDecrypt() As Byte, ByVal ContainerName As String, ByVal DoOAEPPadding As Boolean) As Byte()
        Try
            ' Create a new instance of CspParameters.  Pass
            ' 13 to specify a DSA container or 1 to specify
            ' an RSA container.  The default is 1.
            Dim cspParams As New CspParameters

            ' Specify the container name using the passed variable.
            cspParams.KeyContainerName = ContainerName

            'Create a new instance of RSACryptoServiceProvider.
            'Pass the CspParameters class to use the key 
            'from the key in the container.
            Dim RSAalg As New RSACryptoServiceProvider(cspParams)

            'Decrypt the passed byte array and specify OAEP padding.  
            'OAEP padding is only available on Microsoft Windows XP or
            'later.  
            Return RSAalg.Decrypt(DataToDecrypt, DoOAEPPadding)
            'Catch and display a CryptographicException  
            'to the console.
        Catch e As CryptographicException
            Console.WriteLine(e.ToString())

            Return Nothing
        End Try
    End Function

#End Region

#Region " Sub - WritePWL() "

    Sub WritePWL()

        'Convert Password to Bytes
        Dim ByteConverter As New UTF8Encoding
        Dim dataToEncrypt As Byte() = ByteConverter.GetBytes(prfPassword)

        'Encrypt Data
        Dim encryptedData() As Byte = TripleDES.Encrypt(dataToEncrypt)

        'Store Encrypted Password
        prfPassEncrypted = encryptedData

        'Write to settings.pwl
        Dim writer As New FileStream(prfPasswordPath, FileMode.Create)
        writer.Write(encryptedData, 0, encryptedData.Length)
        writer.Close()

    End Sub

#End Region
#Region " Sub - ReadPWL() "

    Sub ReadPWL()

        Dim bTest As Boolean = False
        Dim ByteConverter As New UTF8Encoding

        'Read the Encrypted Password
        Dim reader As New FileStream(prfPasswordPath, FileMode.Open, FileAccess.Read)
        Dim dataToDecrypt() As Byte = New Byte((reader.Length) - 1) {}
        Dim numBytesToRead As Integer = CType(reader.Length, Integer)
        Dim numBytesRead As Integer = 0
        While (numBytesToRead > 0)
            ' Read may return anything from 0 to numBytesToRead.
            Dim n As Integer = reader.Read(dataToDecrypt, numBytesRead, numBytesToRead)
            ' Break when the end of the file is reached.
            If (n = 0) Then Exit While
            numBytesRead = (numBytesRead + n)
            numBytesToRead = (numBytesToRead - n)
        End While
        numBytesToRead = dataToDecrypt.Length
        reader.Close()

        'Check the Encrypted String is not Corrupt
        If bTest Then
            If ByteConverter.GetString(dataToDecrypt) <> ByteConverter.GetString(prfPassEncrypted) Then
                MsgBox("Error: String not the same" + vbCrLf + ByteConverter.GetString(dataToDecrypt) + vbCrLf + ByteConverter.GetString(prfPassEncrypted) + vbCrLf)
            End If
        End If

        'Decrypt the Password
        Dim DecryptedData As Byte() = TripleDES.Decrypt(dataToDecrypt)

        'Decrypt and Store the Retrieved Password
        prfPassword = ByteConverter.GetString(DecryptedData)

        'Display the Decrypted Password
        If bTest Then MsgBox(ByteConverter.GetString(DecryptedData))

    End Sub

#End Region
#Region " Sub - WriteSettings() "

    Sub WriteSettings()

        FileOpen(1, prfSettingsPath, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, "ProfileMulLoc:    " + modGlobal.prfProfileMulLoc + vbCrLf)
        Print(1, "StdOpsMulLoc:     " + modGlobal.prfStdOpsMulLoc + vbCrLf)
        Print(1, "ZStdOpDefMulLoc:  " + modGlobal.prfZStdOpDefMulLoc + vbCrLf)
        Print(1, "OpTypesMulLoc:    " + modGlobal.prfOpTypesLoc + vbCrLf)
        Print(1, "HardwareMulLoc:   " + modGlobal.prfHardwareLoc + vbCrLf)
        Print(1, "PrepsMulLoc:      " + modGlobal.prfPrepsLoc + vbCrLf)
        Print(1, "SystemsMulLoc:    " + modGlobal.prfSystemsLoc + vbCrLf)
        Print(1, "ProfileSawLoc:    " + modGlobal.prfProfileSawLoc + vbCrLf)
        Print(1, "OpdesignFolderLoc:" + modGlobal.prfOpdesignLoc + vbCrLf)
        Print(1, "BatchFolderLoc:   " + modGlobal.prfBatchLoc + vbCrLf)
        Print(1, "DXFFolderLoc:     " + modGlobal.prfDXFLoc + vbCrLf)
        Print(1, "DiagnoseFolderLoc:" + modGlobal.prfDiagnoseLoc + vbCrLf)
        Print(1, "UseNeuron:        " + modGlobal.prfUseNeuron.ToString + vbCrLf)
        FileClose(1)

    End Sub

#End Region
#Region " Sub - SaveMuls() "

    Sub SaveMuls()

        'Write PROFILE.MUL
        SaveProfileMUL()

        'Write STDOPS.Mul
        SaveStdOpsMUL()

        'Write ZSTDOPDEF.MUL
        SaveZStdOpDefMul()

    End Sub

#End Region

#Region " Function - chNull() "

    Function chNull(ByVal exp As String) As String
        If IsDBNull(exp) Then
            chNull = ""
        Else
            chNull = exp
        End If
    End Function

#End Region
#Region " Function - chNullIS() "

    Function chNullIS(ByVal exp As String) As Integer
        If IsDBNull(exp) Or exp = "" Then
            chNullIS = 0
        Else
            chNullIS = CInt(exp)
        End If
    End Function

#End Region

    'GET Functions
#Region " Function - GetPassword() "

    Function GetPassword() As String
        'Please note that the password generated from here will automatically be converted to Upper Case for testing and is therefore NOT Case Sensitive"
        Return "QWAS"
    End Function

#End Region
#Region " Function - GetReplicate() "

    Function GetReplicate(ByVal sProfile As String) As String

        Dim sReplicate As String = ""
        Dim row() As DataRow = tblProfile.Select("CodeName = '" + sProfile + "'")
        If row.Length > 0 Then
            sReplicate = row(0).Item("ReplicateID").ToString
        Else
            sReplicate = ""
        End If

        Return sReplicate

    End Function

#End Region
#Region " Function - GetStdOp() "

    Function GetStdOp(ByVal sProfile As String, ByVal sReplicate As String, ByVal sOperation As String) As String

        Dim sStdOp As String = ""

        Dim row() As DataRow = tblStdOps.Select("ProfileCode = '" + sProfile + "' AND OpToolCode = '" + sOperation + "'")
        If row.Length > 0 Then
            sStdOp = row(0).Item("StdOpCode").ToString
        Else
            Dim rowReplicate() As DataRow = tblStdOps.Select("ProfileCode = '" + sReplicate + "' AND OpToolCode = '" + sOperation + "'")
            If rowReplicate.Length > 0 Then
                sStdOp = rowReplicate(0).Item("StdOpCode").ToString
            Else
                Dim rowAllPrfs() As DataRow = tblStdOps.Select("ProfileCode = 'ALLPRFS' AND OpToolCode = '" + sOperation + "'")
                If rowAllPrfs.Length > 0 Then
                    sStdOp = rowAllPrfs(0).Item("StdOpCode").ToString
                Else
                    sStdOp = ""
                End If
            End If
        End If

        Return sStdOp

    End Function

#End Region
#Region " Function - GetOpType() "

    Function GetOpType(ByVal sStdOp As String) As Integer

        Dim row() As DataRow = tblStdOpDef.Select("StdOpCode = '" + sStdOp + "'")

        Try
            Return row(0).Item("StdOpType")
        Catch ex As Exception
            Return 0
        End Try

    End Function

#End Region
#Region " Function - CreateTblFlip() "

    Function CreateTblFlip() As DataTable

        Dim tbl As New DataTable("tblFlip")
        tbl.Locale = System.Globalization.CultureInfo.InvariantCulture
        tbl.Columns.Clear()

        'OpType
        Dim cValue As New DataColumn("FlipValue")
        cValue.DataType = GetType(String)
        tbl.Columns.Add(cValue)

        'OpDesc
        Dim cDesc As New DataColumn("FlipDesc")
        cDesc.DataType = GetType(String)
        tbl.Columns.Add(cDesc)

        Dim rRow As DataRow

        'No
        rRow = tbl.NewRow()
        rRow.Item("FlipValue") = "N"
        rRow.Item("FlipDesc") = "No"
        tbl.Rows.Add(rRow)

        'Yes
        rRow = tbl.NewRow()
        rRow.Item("FlipValue") = "Y"
        rRow.Item("FlipDesc") = "Yes"
        tbl.Rows.Add(rRow)

        'Double
        rRow = tbl.NewRow()
        rRow.Item("FlipValue") = "D"
        rRow.Item("FlipDesc") = "Double"
        tbl.Rows.Add(rRow)

        'Check
        rRow = tbl.NewRow()
        rRow.Item("FlipValue") = "C"
        rRow.Item("FlipDesc") = "Check"
        tbl.Rows.Add(rRow)

        Return tbl

    End Function


#End Region
#Region " Function - GetPullGripperY() "

    Function GetPullGripperY(ByVal sProfile As String) As Double

        GetPullGripperY = 0
        For Each dr As DataRow In tblProfile.Rows
            If dr.Item("CodeName") = sProfile Then
                GetPullGripperY = dr.Item("GripPosition")
                Exit For
            End If
        Next

        Return GetPullGripperY

    End Function

#End Region
#Region " Function - GetPushGripperY() "

    Function GetPushGripperY(ByVal sProfile As String) As Double

        GetPushGripperY = 0
        For Each dr As DataRow In tblProfile.Rows
            If dr.Item("CodeName") = sProfile Then
                GetPushGripperY = dr.Item("GripY")
                Exit For
            End If
        Next

        Return GetPushGripperY

    End Function

#End Region
#Region " Function - GetPushGripperZ() "

    Function GetPushGripperZ(ByVal sProfile As String) As Double

        GetPushGripperZ = 0
        For Each dr As DataRow In tblProfile.Rows
            If dr.Item("CodeName") = sProfile Then
                GetPushGripperZ = dr.Item("GripZ")
                Exit For
            End If
        Next

        Return GetPushGripperZ

    End Function

#End Region

    'Profile Types
#Region " Function - CreateTblProfileTypes() "

    Function CreateTblProfileTypes() As DataTable

        Dim tbl As New DataTable("tblProfileTypes")
        tbl.Locale = System.Globalization.CultureInfo.InvariantCulture
        tbl.Columns.Clear()

        'ptValue
        Dim cValue As New DataColumn("ptValue")
        cValue.DataType = GetType(Integer)
        tbl.Columns.Add(cValue)

        'ptDesc
        Dim cDesc As New DataColumn("ptDesc")
        cDesc.DataType = GetType(String)
        tbl.Columns.Add(cDesc)

        Dim rRow As DataRow

        '0, Unknown
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 0
        rRow.Item("ptDesc") = "All Types"
        tbl.Rows.Add(rRow)

        '1, Std Outer
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 1
        rRow.Item("ptDesc") = "Std Outer"
        tbl.Rows.Add(rRow)

        '2, Casement Sash
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 2
        rRow.Item("ptDesc") = "Casement Sash"
        tbl.Rows.Add(rRow)

        '3, T Transom/Mullion
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 3
        rRow.Item("ptDesc") = "T Transom/Mullion"
        tbl.Rows.Add(rRow)

        '4, Tilt/Turn Sash
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 4
        rRow.Item("ptDesc") = "Tilt/Turn Sash"
        tbl.Rows.Add(rRow)

        '5, False Mullion
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 5
        rRow.Item("ptDesc") = "False Mullion"
        tbl.Rows.Add(rRow)

        '6, Door Sash
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 6
        rRow.Item("ptDesc") = "Door Sash"
        tbl.Rows.Add(rRow)

        '7, Patio Transom
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 7
        rRow.Item("ptDesc") = "Patio Transom"
        tbl.Rows.Add(rRow)

        '8, Patio Sash
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 8
        rRow.Item("ptDesc") = "Patio Sash"
        tbl.Rows.Add(rRow)

        '9, Patio Frame
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 9
        rRow.Item("ptDesc") = "Patio Frame"
        tbl.Rows.Add(rRow)

        '10, Add On
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 10
        rRow.Item("ptDesc") = "Add On"
        tbl.Rows.Add(rRow)

        '11, Z Transom/Mullion
        rRow = tbl.NewRow()
        rRow.Item("ptValue") = 11
        rRow.Item("ptDesc") = "Z Transom/Mullion"
        tbl.Rows.Add(rRow)

        Return tbl

    End Function

#End Region
#Region " Fill Combobox - Profile Types "

    Function FillProfileTypes(ByVal cmbProfileTypes As ComboBox) As Boolean

        'Populate Combobox
        cmbProfileTypes.DataSource = tblProfileTypes
        cmbProfileTypes.ValueMember = "ptValue"
        cmbProfileTypes.DisplayMember = "ptDesc"

    End Function

#End Region

    'StdOps.mul
#Region " Function - GetStdOpsTable()  - Initialise tblStdOps "

    Function GetStdOpsTable() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()
        tblTable.TableName = "tblStdOps"

        'OpID
        Dim cOpID As New DataColumn("OpID")
        cOpID.DataType = GetType(Integer)
        tblTable.Columns.Add(cOpID)

        'ProfileCode
        Dim cProfileCode As New DataColumn("ProfileCode")
        cProfileCode.DataType = GetType(String)
        tblTable.Columns.Add(cProfileCode)

        'OpToolCode
        Dim cOpToolCode As New DataColumn("OpToolCode")
        cOpToolCode.DataType = GetType(String)
        tblTable.Columns.Add(cOpToolCode)

        'StdOpCode
        Dim cStdOpCode As New DataColumn("StdOpCode")
        cStdOpCode.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpCode)

        'Use
        Dim cUse As New DataColumn("Use")
        cUse.DataType = GetType(Boolean)
        tblTable.Columns.Add(cUse)

        'OpDescription
        Dim cOpDescription As New DataColumn("OpDescription")
        cOpDescription.DataType = GetType(String)
        tblTable.Columns.Add(cOpDescription)

        'OpVariable1
        Dim cOpVariable1 As New DataColumn("OpVariable1")
        cOpVariable1.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable1)

        'OpVariable2
        Dim cOpVariable2 As New DataColumn("OpVariable2")
        cOpVariable2.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable2)

        'OpVariable3
        Dim cOpVariable3 As New DataColumn("OpVariable3")
        cOpVariable3.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable3)

        'OpVariable4
        Dim cOpVariable4 As New DataColumn("OpVariable4")
        cOpVariable4.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable4)

        'OpVariable5
        Dim cOpVariable5 As New DataColumn("OpVariable5")
        cOpVariable5.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable5)

        'OpVariable6
        Dim cOpVariable6 As New DataColumn("OpVariable6")
        cOpVariable6.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable6)

        'OpVariable7
        Dim cOpVariable7 As New DataColumn("OpVariable7")
        cOpVariable7.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable7)

        'OpVariable8
        Dim cOpVariable8 As New DataColumn("OpVariable8")
        cOpVariable8.DataType = GetType(Double)
        tblTable.Columns.Add(cOpVariable8)

        Dim rRow As DataRow

        If File.Exists(modGlobal.prfStdOpsMulLoc) Then

            FileOpen(1, prfStdOpsMulLoc, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

            Do While Not EOF(1)

                Dim sLine As String = LineInput(1)
                rRow = tblTable.NewRow

                Dim iLoc As Integer = InStr(sLine, ",", CompareMethod.Text)
                'Ignore Non comma Lines
                If iLoc <> 0 Then

                    'OpID
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpID") = CInt(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpID") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'ProfileCode
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ProfileCode") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ProfileCode") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'OpToolCode
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpToolCode") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpToolCode") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpCode
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpCode") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpCode") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'Use
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("Use") = CBool(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("Use") = False
                        sLine = Mid(sLine, 2)
                    End If

                    'OpDescription
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpDescription") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpDescription") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable1
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable1") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable1") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable2
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable2") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable2") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable3
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable3") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable3") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable4
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable4") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable4") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable5
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable5") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable5") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable6
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable6") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable6") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable7
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable7") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable7") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'OpVariable8
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 And iLoc <> 0 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("OpVariable8") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("OpVariable8") = CDbl(sLine)
                    End If

                    'Add New Row to Table
                    tblTable.Rows.Add(rRow)

                End If

            Loop

            FileClose(1)

        End If

        Return tblTable

    End Function

#End Region
#Region " Sub - FillOperationsCombo()  - Fill DataGrid with Tool Code & Desc for a Specific Profile "

    Sub FillOperationsCombo(ByVal dgv As DataGridView, ByVal sProfile As String, ByVal bIncAllprfs As Boolean, ByVal bAddNone As Boolean)

        'Design Holding Table
        Dim dsComboOps As New DataSet
        Dim tblComboOps As New DataTable("ComboOps")

        'Add Columns
        tblComboOps.Columns.Add("ToolCode", GetType(String))
        tblComboOps.Columns.Add("Description", GetType(String))

        'Set PrimaryKey
        tblComboOps.Columns("ToolCode").Unique = True
        tblComboOps.PrimaryKey = New DataColumn() {tblComboOps.Columns("ToolCode")}

        'Add No Ops Option
        Dim drData As DataRow
        drData = tblComboOps.NewRow
        drData("ToolCode") = "<None>"
        drData("Description") = "<None>"
        tblComboOps.Rows.Add(drData)

        'Loop through stdops.mul
        For Each r As DataRow In tblStdOps.Rows

            If bIncAllprfs Then
                'Add both Allprfs preps AND Profile preps and replicated profile preps
                If r.Item(1).ToString = sProfile Or r.Item(1).ToString.ToUpper = "ALLPRFS" Or r.Item(1).ToString.ToUpper = GetReplicate(sProfile) Then
                    Try
                        drData = tblComboOps.NewRow
                        drData("ToolCode") = r.Item(2).ToString
                        drData("Description") = r.Item(5).ToString
                        tblComboOps.Rows.Add(drData)
                    Catch ex As Exception

                    End Try
                End If
            Else
                'Add only Profile Preps
                If r.Item(1).ToString = sProfile Then
                    Try
                        drData = tblComboOps.NewRow
                        drData("ToolCode") = r.Item(2).ToString
                        drData("Description") = r.Item(5).ToString
                        tblComboOps.Rows.Add(drData)
                    Catch ex As Exception

                    End Try
                End If
            End If
        Next

        dsComboOps.Tables.Add(tblComboOps)
        Dim bs As New BindingSource(dsComboOps, "ComboOps")
        dgv.DataSource = bs

        With dgv
            .Font = New Font("Arial", 10)
        End With

        With dgv.Columns(0)
            .Width = 90
        End With

        With dgv.Columns(1)
            .Width = 210
        End With

        'Sort By CodeName
        dgv.Sort(dgv.Columns.Item("Description"), System.ComponentModel.ListSortDirection.Ascending)

    End Sub

#End Region
#Region " Sub - FillOperationsGrid()   - Fill DataGridView with Operations "

    Sub FillOperationsGrid(ByVal dgvMachining As System.Windows.Forms.DataGridView, ByVal sProfile As String, ByVal bProfileEQ As Boolean, ByVal sToolCode As String, ByVal bToolEQ As Boolean, ByVal sStdOp As String, ByVal bStdOpEQ As Boolean, ByVal AdminAccess As Boolean)

        If Not stdOpsValid Then Exit Sub

        'Create Binding Source
        Dim bs As New BindingSource(dsStdOps, "tblStdOps")

        'Add Filter
        Dim sFilter As String = ""
        Dim sProfText As String = ""
        Dim sToolText As String = ""
        Dim sSdOpText As String = ""

        If bProfileEQ Then
            sProfText = "ProfileCode = '" + sProfile + "'"
        Else
            sProfText = "ProfileCode LIKE '" + sProfile + "%'"
        End If

        If bToolEQ Then
            sToolText = "OpToolCode = '" + sToolCode + "'"
        Else
            sToolText = "OpToolCode LIKE '" + sToolCode + "%'"
        End If

        If bStdOpEQ Then
            sSdOpText = "StdOpCode = '" + sStdOp + "'"
        Else
            sSdOpText = "StdOpCode LIKE '" + sStdOp + "%'"
        End If

        If sProfile = "" And sToolCode = "" And sStdOp <> "" Then sFilter = sSdOpText
        If sProfile = "" And sToolCode <> "" And sStdOp = "" Then sFilter = sToolText
        If sProfile <> "" And sToolCode = "" And sStdOp = "" Then sFilter = sProfText

        If sProfile = "" And sToolCode <> "" And sStdOp <> "" Then sFilter = sToolText + " AND " + sSdOpText
        If sProfile <> "" And sToolCode = "" And sStdOp <> "" Then sFilter = sProfText + " AND " + sSdOpText
        If sProfile <> "" And sToolCode <> "" And sStdOp = "" Then sFilter = sProfText + " AND " + sToolText

        If sProfile <> "" And sToolCode <> "" And sStdOp <> "" Then sFilter = sProfText + " AND " + sToolText + " AND " + sSdOpText

        bs.Filter = sFilter

        'Grab Grid Data
        With dgvMachining
            .DataSource = bs
            .AllowUserToAddRows = AdminAccess
            .AllowUserToDeleteRows = AdminAccess
            .AllowUserToOrderColumns = False
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = False
            .Columns.Clear()
            .Font = New Font("Arial", 10, FontStyle.Regular)
        End With

        'OpID
        Dim colID As New DataGridViewTextBoxColumn
        With colID
            .Name = "ID"
            .HeaderText = "ID"
            .Width = 50
            .DataPropertyName = "OpID"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .ReadOnly = True
        End With
        dgvMachining.Columns.Add(colID)
        dgvMachining.Columns("ID").Visible = False 'Was AdminAccess

        'ProfileCode
        Dim colProfileCode As New DataGridViewTextBoxColumn
        With colProfileCode
            .HeaderText = "Profile Code"
            .Width = 70
            .DataPropertyName = "ProfileCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = Not AdminAccess
            .Visible = True
        End With
        dgvMachining.Columns.Add(colProfileCode)

        'OpToolCode
        Dim colOpToolCode As New DataGridViewTextBoxColumn
        With colOpToolCode
            .HeaderText = "Tool Code"
            .Width = 90
            .DataPropertyName = "OpToolCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = Not AdminAccess
            .Visible = True
        End With
        dgvMachining.Columns.Add(colOpToolCode)

        'StdOpCode
        Dim colStdOpCode As New DataGridViewComboBoxColumn
        With colStdOpCode
            .Name = "StdOpCode"
            .HeaderText = "Std Op"
            .Width = 110
            .DataPropertyName = "StdOpCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            Dim dv As New DataView(tblStdOpDef)
            dv.Sort = "StdOpCode"
            .DataSource = dv
            .ValueMember = "StdOpCode"
            .DisplayMember = "StdOpCode"
            .Visible = AdminAccess
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgvMachining.Columns.Add(colStdOpCode)

        'Use
        Dim colUse As New DataGridViewCheckBoxColumn
        With colUse
            .HeaderText = "Use"
            .Width = 40
            .DataPropertyName = "Use"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Visible = True
            .SortMode = DataGridViewColumnSortMode.Automatic
            .ReadOnly = False
        End With
        dgvMachining.Columns.Add(colUse)

        'OpDescription
        Dim colOpDescription As New DataGridViewTextBoxColumn
        With colOpDescription
            .HeaderText = "Description"
            .Width = 220
            .DataPropertyName = "OpDescription"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = True
        End With
        dgvMachining.Columns.Add(colOpDescription)

        'OpVariable1
        Dim colOpVariable1 As New DataGridViewTextBoxColumn
        With colOpVariable1
            .HeaderText = "V1"
            .Width = 50
            .DataPropertyName = "OpVariable1"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable1)

        'OpVariable2
        Dim colOpVariable2 As New DataGridViewTextBoxColumn
        With colOpVariable2
            .HeaderText = "V2"
            .Width = 50
            .DataPropertyName = "OpVariable2"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable2)

        'OpVariable3
        Dim colOpVariable3 As New DataGridViewTextBoxColumn
        With colOpVariable3
            .HeaderText = "V3"
            .Width = 50
            .DataPropertyName = "OpVariable3"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable3)

        'OpVariable4
        Dim colOpVariable4 As New DataGridViewTextBoxColumn
        With colOpVariable4
            .HeaderText = "V4"
            .Width = 50
            .DataPropertyName = "OpVariable4"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable4)

        'OpVariable5
        Dim colOpVariable5 As New DataGridViewTextBoxColumn
        With colOpVariable5
            .HeaderText = "V5"
            .Width = 50
            .DataPropertyName = "OpVariable5"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable5)

        'OpVariable6
        Dim colOpVariable6 As New DataGridViewTextBoxColumn
        With colOpVariable6
            .HeaderText = "V6"
            .Width = 50
            .DataPropertyName = "OpVariable6"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable6)

        'OpVariable7
        Dim colOpVariable7 As New DataGridViewTextBoxColumn
        With colOpVariable7
            .HeaderText = "V7"
            .Width = 50
            .DataPropertyName = "OpVariable7"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable7)

        'OpVariable8
        Dim colOpVariable8 As New DataGridViewTextBoxColumn
        With colOpVariable8
            .HeaderText = "V8"
            .Width = 50
            .DataPropertyName = "OpVariable8"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = AdminAccess
        End With
        dgvMachining.Columns.Add(colOpVariable8)

        UpdateOpCount(dgvMachining)

    End Sub

#End Region
#Region " Sub - SaveStdOpsMUL()        - Save off the STDOPS.MUL "

    Sub SaveStdOpsMUL()
        If Not stdOpsValid Then Exit Sub

        'Initialise Variables
        Dim rRow As DataRow
        Dim sDataRow As String
        Dim oItem As Object

        'Write StdOps.Mul
        FileOpen(1, prfStdOpsMulLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, "StdOps.Mul Created By FlowlineOps.Net" + vbCrLf)
        For Each rRow In tblStdOps.Rows
            sDataRow = ""
            For Each oItem In rRow.ItemArray
                If TypeOf oItem Is Boolean Then
                    If oItem = False Then
                        sDataRow += "0,"
                    Else
                        sDataRow += "-1,"
                    End If
                Else
                    sDataRow += oItem.ToString + ","
                End If
            Next
            sDataRow = Mid(sDataRow, 1, Len(sDataRow) - 1)
            Print(1, sDataRow + vbCrLf)
        Next
        FileClose(1)

        addChecksum()

    End Sub
#End Region
#Region " Function - GetMaxSTDOpID() "

    Function GetMaxSTDOpID() As Integer

        Dim i As Integer = 0
        For Each rRow As DataRow In tblStdOps.Rows
            If rRow.Item(0) > i Then i = rRow.Item(0)
        Next
        Return i

    End Function

#End Region
#Region " Sub - addChecksum()        - Add a 2 character checksum to the beginning of the stdops file "
    Sub addChecksum()
        Dim text As String = ""
        Dim firstLine As String = ""
        Dim checksum As String = getChecksum(firstLine, text)

        ' write the first line with a checksum value
        FileOpen(1, prfStdOpsMulLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, checksum & firstLine & vbCrLf)

        ' write the rest of the file
        Print(1, text)
        FileClose(1)
    End Sub
#End Region
#Region " Sub - UpdateOpCount() "

    Public Sub UpdateOpCount(dgvMachining As System.Windows.Forms.DataGridView)

        Try
            Dim frm As frmEngineer = dgvMachining.FindForm()
            If frm Is Nothing Then Exit Sub
            Dim iRows As Integer = dgvMachining.Rows.Count
            'iRows -= 1
            frm.lblRows.Text = "Operations: " + iRows.ToString
        Catch ex As Exception
            'Dim frm As frmEditMachining = dgvMachining.FindForm()
            'Dim iRows As Integer = dgvMachining.Rows.Count
            'frm.lblRows.Text = "Operations: " + iRows.ToString

        End Try

    End Sub

#End Region

    'Systems
#Region " Function - GetTblSystems()  - Initialise tblSystems "

    Function GetTblSystems() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable("tblSystems")
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture
        Dim rRow As DataRow

        tblTable.Columns.Clear()

        'Suite
        Dim cSuite As New DataColumn("Suite")
        cSuite.DataType = GetType(String)
        tblTable.Columns.Add(cSuite)

        'Description
        Dim cDescription As New DataColumn("Description")
        cDescription.DataType = GetType(String)
        tblTable.Columns.Add(cDescription)

        'Used
        Dim cUsed As New DataColumn("Used")
        cUsed.DataType = GetType(Boolean)
        tblTable.Columns.Add(cUsed)

        If File.Exists(prfSystemsLoc) Then
            FileOpen(1, prfSystemsLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
            Do While Not EOF(1)
                Dim s As String = LineInput(1)
                Dim i As Integer = InStr(s, ",", CompareMethod.Text)
                Dim sSuite As String = Mid(s, 1, i - 1)
                s = Mid(s, i + 1)
                i = InStr(s, ",", CompareMethod.Text)
                Dim sDesc As String = Mid(s, 1, i - 1)
                Dim bUse As Boolean = CBool(Mid(s, i + 1))
                rRow = tblTable.NewRow
                rRow.Item("Suite") = sSuite
                rRow.Item("Description") = sDesc
                rRow.Item("Used") = bUse
                tblTable.Rows.Add(rRow)
            Loop
            FileClose(1)
        Else
            MsgBox("Cannot Find " + prfSystemsLoc, MsgBoxStyle.Critical)
            rRow = tblTable.NewRow
            rRow.Item("Suite") = "ALL"
            rRow.Item("Description") = "All Profiles"
            rRow.Item("Used") = True
            tblTable.Rows.Add(rRow)
        End If

        Return tblTable

    End Function

#End Region
#Region " Sub - FillSystems()         - Fill Combobox With Systems "

    Function FillSystems(ByVal cmbSystems As ComboBox) As Boolean

        'Populate Combobox
        cmbSystems.DataSource = tblSystems.Copy
        cmbSystems.ValueMember = "Suite"
        cmbSystems.DisplayMember = "Description"

    End Function

#End Region
#Region " Sub - BindSystemsGrid()     - Fill DataGridView with Systems "

    Sub BindSystemsGrid(ByVal dgvSuites As DataGridView)

        Importing = True

        'Generate Binding Source
        Dim bs As New BindingSource(dsSystems, "tblSystems")

        'Fill Grid
        With dgvSuites
            .DataSource = bs
            '.RetrieveStructure()
            '.GroupByBoxVisible = False
            .ReadOnly = False
            .AllowUserToDeleteRows = True
            .AllowUserToAddRows = True
            .Font = New Font("Arial", 12, FontStyle.Regular)
        End With

        'Suite Column
        With dgvSuites.Columns(0)
            .HeaderText = "Suite"
            .Width = 100
            .Visible = True
        End With

        'Description Column
        With dgvSuites.Columns(1)
            .HeaderText = "Description"
            .Width = 300
            .Visible = True
        End With

        'Used Column
        With dgvSuites.Columns(2)
            .HeaderText = "Use"
            .Width = 60
            .Visible = False
        End With

        Importing = False

    End Sub

#End Region
#Region " Sub - SaveTblSystems()      - Save off the tblSystems "

    Sub SaveTblSystems()

        FileOpen(1, prfSystemsLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)

        For Each temprow As DataRow In tblSystems.Rows
            Print(1, temprow.Item("Suite") + "," + temprow.Item("Description") + ",TRUE" + vbCrLf)
        Next

        FileClose(1)

    End Sub

#End Region

    'Profile.mul
#Region " Function - GetProfileTable()- Initialise tblProfile "

    Function GetProfileTable() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()
        tblTable.TableName = "tblProfile"

        'CodeName
        Dim cCodeName(1) As DataColumn
        cCodeName(0) = New DataColumn("CodeName")
        cCodeName(0).DataType = GetType(String)
        tblTable.Columns.Add(cCodeName(0))

        tblTable.PrimaryKey = cCodeName

        'ZSection
        Dim cZSection As New DataColumn("ZSection")
        cZSection.DataType = GetType(Integer)
        tblTable.Columns.Add(cZSection)

        'Flip
        Dim cFlip As New DataColumn("Flip")
        cFlip.DataType = GetType(String)
        tblTable.Columns.Add(cFlip)

        'Width
        Dim cWidth As New DataColumn("Width")
        cWidth.DataType = GetType(Double)
        tblTable.Columns.Add(cWidth)

        'GripPosition
        Dim cGripPosition As New DataColumn("GripPosition")
        cGripPosition.DataType = GetType(Double)
        tblTable.Columns.Add(cGripPosition)

        'StdLength
        Dim cStdLength As New DataColumn("StdLength")
        cStdLength.DataType = GetType(Double)
        tblTable.Columns.Add(cStdLength)

        'ReverseLoad
        Dim cReverseLoad As New DataColumn("ReverseLoad")
        cReverseLoad.DataType = GetType(Integer)
        tblTable.Columns.Add(cReverseLoad)

        'ZRebate
        Dim cZRebate As New DataColumn("ZRebate")
        cZRebate.DataType = GetType(Double)
        tblTable.Columns.Add(cZRebate)

        'Height
        Dim cHeight As New DataColumn("Height")
        cHeight.DataType = GetType(Double)
        tblTable.Columns.Add(cHeight)

        'EuroHeight
        Dim cEuroHeight As New DataColumn("EuroHeight")
        cEuroHeight.DataType = GetType(Double)
        tblTable.Columns.Add(cEuroHeight)

        'System
        Dim cSystem As New DataColumn("System")
        cSystem.DataType = GetType(String)
        tblTable.Columns.Add(cSystem)

        'ProfileName
        Dim cProfileName As New DataColumn("ProfileName")
        cProfileName.DataType = GetType(String)
        tblTable.Columns.Add(cProfileName)

        'ReplicateID
        Dim cReplicateID As New DataColumn("ReplicateID")
        cReplicateID.DataType = GetType(String)
        tblTable.Columns.Add(cReplicateID)

        'ProfileTypeID
        Dim cProfileTypeID As New DataColumn("ProfileTypeID")
        cProfileTypeID.DataType = GetType(Integer)
        tblTable.Columns.Add(cProfileTypeID)

        'NeuronStr
        Dim cNeuronStr As New DataColumn("NeuronStr")
        cNeuronStr.DataType = GetType(String)
        tblTable.Columns.Add(cNeuronStr)

        'Centralise (0 or 1, Default 0)
        Dim cCentralise As New DataColumn("Centralise")
        cCentralise.DataType = GetType(Integer)
        cCentralise.DefaultValue = 0
        tblTable.Columns.Add(cCentralise)

        'OptTol (Optimiser Tolerance in mm - Default to 50)
        Dim cOptTol As New DataColumn("OptTol")
        cOptTol.DataType = GetType(Integer)
        cOptTol.DefaultValue = 50
        tblTable.Columns.Add(cOptTol)

        'MinOffcut (Minimum Offcut Length in mm - Default to 700)
        Dim cMinOffcut As New DataColumn("MinOffcut")
        cMinOffcut.DataType = GetType(Integer)
        cMinOffcut.DefaultValue = 700
        tblTable.Columns.Add(cMinOffcut)

        'GripY (Gripper Y Pos - Default to 0)
        Dim cGripY As New DataColumn("GripY")
        cGripY.DataType = GetType(Integer)
        cGripY.DefaultValue = 0
        tblTable.Columns.Add(cGripY)

        'GripZ (Gripper Y Pos - Default to 60)
        Dim cGripZ As New DataColumn("GripZ")
        cGripZ.DataType = GetType(Integer)
        cGripZ.DefaultValue = 60
        tblTable.Columns.Add(cGripZ)

        'ID (Integer starting at 1, profile identifier in recognition - default to -1)
        Dim cID As New DataColumn("ID")
        cID.DataType = GetType(Integer)
        cID.DefaultValue = -1
        tblTable.Columns.Add(cID)

        'CoreColour (Integer representing the core colour as recognised by camera - default to -1)
        Dim cCoreColour As New DataColumn("CoreColour")
        cCoreColour.DataType = GetType(Integer)
        cCoreColour.DefaultValue = -1
        tblTable.Columns.Add(cCoreColour)

        'SawCutStyle (Integer representing a saw cut style - default to 0. This will be used to determine different cut styles for different profile types, where we will be able to adjust the cut speed, height, etc)
        Dim cSawCutStyle As New DataColumn("SawCutStyle")
        cSawCutStyle.DataType = GetType(Integer)
        cSawCutStyle.DefaultValue = 0
        tblTable.Columns.Add(cSawCutStyle)

        'RotateGripper (Integer Representing If Gripper is Rotated - default to 0. This will be set on Push Type Grippers 0 = Standard Rotation, 1 = 90 Degree Rotation as pull gripper)
        Dim cRotateGripper As New DataColumn("RotateGripper")
        cRotateGripper.DataType = GetType(Integer)
        cRotateGripper.DefaultValue = 0
        tblTable.Columns.Add(cRotateGripper)

        Dim rRow As DataRow

        If File.Exists(modGlobal.prfProfileMulLoc) Then
            FileOpen(1, prfProfileMulLoc, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)
            Do While Not EOF(1)
                Dim sLine As String = LineInput(1)
                rRow = tblTable.NewRow

                Dim iLoc As Integer = InStr(sLine, ",", CompareMethod.Text)

                'Ignore Non comma Lines
                If iLoc = 0 Then

                    Select Case Mid(sLine, 1, 1)
                        Case "1"
                            ProfileVersion = "V1"
                        Case "2"
                            ProfileVersion = "V2"
                        Case "3"
                            ProfileVersion = "V3"
                        Case "4"
                            ProfileVersion = "V4"
                        Case Else
                            ProfileVersion = "V1"
                    End Select

                Else

                    'Get CodeName
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("CodeName") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("CodeName") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'Get ZSection
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ZSection") = CInt(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ZSection") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'Get Flip
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("Flip") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("Flip") = "N"
                        sLine = Mid(sLine, 2)
                    End If

                    'Width
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("Width") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("Width") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'GripPosition
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("GripPosition") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("GripPosition") = 25
                        sLine = Mid(sLine, 2)
                    End If

                    'StdLength
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdLength") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdLength") = 6000
                        sLine = Mid(sLine, 2)
                    End If

                    'ReverseLoad
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ReverseLoad") = CInt(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ReverseLoad") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'ZRebate
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ZRebate") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ZRebate") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'Height
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("Height") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("Height") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'EuroHeight
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("EuroHeight") = CDbl(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("EuroHeight") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'System
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("System") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("System") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'ProfileName
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ProfileName") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ProfileName") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'ReplicateID
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ReplicateID") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ReplicateID") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'ProfileTypeID
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("ProfileTypeID") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("ProfileTypeID") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'NeuronStr
                    If ProfileVersion = "V2" Then

                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("NeuronStr") = sString
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("NeuronStr") = ""
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("NeuronStr") = ""
                        End If

                    Else
                        rRow.Item("NeuronStr") = ""
                    End If

                    'Centralise
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then

                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("Centralise") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("Centralise") = 0
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("Centralise") = 0
                        End If

                    Else
                        rRow.Item("Centralise") = 0
                    End If

                    'OptTol
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("OptTol") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("OptTol") = 50
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("OptTol") = 50
                        End If
                    Else
                        rRow.Item("OptTol") = 50
                    End If

                    'MinOffcut
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("MinOffcut") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("MinOffcut") = 700
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("MinOffcut") = 700
                        End If
                    Else
                        rRow.Item("MinOffcut") = 700
                    End If

                    'GripY
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("GripY") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("GripY") = 0
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("GripY") = 0
                        End If
                    Else
                        rRow.Item("GripY") = 0
                    End If

                    'GripZ
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("GripZ") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("GripZ") = 60
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("GripZ") = 60
                        End If
                    Else
                        rRow.Item("GripZ") = 60
                    End If

                    'ID
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("ID") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("ID") = -1
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("ID") = -1
                        End If
                    Else
                        rRow.Item("ID") = -1
                    End If

                    'CoreColour
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("CoreColour") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("CoreColour") = -1
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("CoreColour") = -1
                        End If
                    Else
                        rRow.Item("CoreColour") = -1
                    End If

                    'SawCutStyle
                    If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("SawCutStyle") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("SawCutStyle") = 0
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("SawCutStyle") = 0
                        End If
                    Else
                        rRow.Item("SawCutStyle") = 0
                    End If

                    'RotateGripper
                    If ProfileVersion = "V4" Then
                        If Len(sLine) <> 0 Then
                            iLoc = InStr(sLine, ",", CompareMethod.Text)
                            If iLoc <> 1 Then
                                sString = Mid(sLine, 1, iLoc - 1)
                                rRow.Item("RotateGripper") = CInt(sString)
                                sLine = Mid(sLine, iLoc + 1)
                            Else
                                rRow.Item("RotateGripper") = 0
                                sLine = Mid(sLine, 2)
                            End If
                        Else
                            rRow.Item("RotateGripper") = 0
                        End If
                    Else
                        rRow.Item("RotateGripper") = 0
                    End If

                    'Add New Row to Table
                    tblTable.Rows.Add(rRow)

                End If

            Loop

            FileClose(1)

        End If

        Return tblTable

    End Function

#End Region
#Region " Function - GetProfileWidth()- Return the width of a profile from its codename "

    Function GetProfileWidth(ByVal sProfile) As Short

        GetProfileWidth = 0
        For Each dr As DataRow In tblProfile.Rows
            If dr.Item("CodeName") = sProfile Then
                GetProfileWidth = dr.Item("Width")
                Exit For
            End If
        Next

        Return GetProfileWidth

    End Function

#End Region
#Region " Function - GetProfileHeight()- Return the height of a profile from its codename "

    Function GetProfileHeight(ByVal sProfile) As Short

        GetProfileHeight = 0
        For Each dr As DataRow In tblProfile.Rows
            If dr.Item("CodeName") = sProfile Then
                GetProfileHeight = dr.Item("Height")
                Exit For
            End If
        Next

        Return GetProfileHeight

    End Function

#End Region

#Region " Sub - FillProfiles()        - Fill Combobox with Profiles "

    Sub FillProfiles(ByVal cmbProfile As ComboBox)

        cmbProfile.DataSource = tblProfile
        cmbProfile.DisplayMember = "CodeName"
        cmbProfile.ValueMember = "CodeName"

    End Sub

#End Region
#Region " Sub - BindProfileGrid()     - Fill DataGridView with Profiles "

    Sub BindProfileGrid(ByVal dgvProfile As DataGridView, ByVal bAdminAccess As Boolean, ByVal sFilter As String)

        'Create Binding Source
        Dim bs As New BindingSource(dsProfile, "tblProfile")
        bs.Filter = sFilter

        'Grab Grid Data
        With dgvProfile
            .DataSource = bs
            .Font = New Font("Arial", 12, FontStyle.Regular)
            .Columns.Clear()
            .AllowUserToAddRows = bAdminAccess
            .AllowUserToOrderColumns = False
            .AllowUserToDeleteRows = bAdminAccess
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = True
        End With

        'Dim HeaderStyle As DataGridViewCellStyle = New DataGridViewCellStyle()
        'HeaderStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'HeaderStyle.ForeColor = Color.IndianRed
        'HeaderStyle.BackColor = Color.Ivory

        'CodeName
        Dim colCodeName As New DataGridViewTextBoxColumn
        With colCodeName
            .HeaderText = "Name"
            .Width = 100
            .ReadOnly = Not bAdminAccess
            .DataPropertyName = "CodeName"
            .Name = "CodeName"
        End With
        dgvProfile.Columns.Add(colCodeName)

        'ZSection
        Dim colZSection As New DataGridViewComboBoxColumn
        With colZSection
            .DataPropertyName = "ZSection"
            .HeaderText = "Z ID"
            .DropDownWidth = 50
            .Width = 50
            .MaxDropDownItems = 5
            .FlatStyle = FlatStyle.Standard
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Items.AddRange(0, 1, 2, 3, 4)
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgvProfile.Columns.Add(colZSection)

        'Flip
        Dim colFlip As New DataGridViewComboBoxColumn
        Dim tblFlip As DataTable = CreateTblFlip()
        With colFlip
            .DataPropertyName = "Flip"
            .HeaderText = "Flip"
            .DropDownWidth = 80
            .Width = 80
            .MaxDropDownItems = 4
            .FlatStyle = FlatStyle.Standard
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DataSource = tblFlip
            .ValueMember = "FlipValue"
            .DisplayMember = "FlipDesc"
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgvProfile.Columns.Add(colFlip)

        'Width
        Dim colWidth As New DataGridViewTextBoxColumn
        With colWidth
            .HeaderText = "Width"
            .Width = 60
            .DataPropertyName = "Width"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgvProfile.Columns.Add(colWidth)

        'GripPosition
        Dim colGripPosition As New DataGridViewTextBoxColumn
        With colGripPosition
            .HeaderText = "Grip Pos"
            .Width = 60
            .DataPropertyName = "GripPosition"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgvProfile.Columns.Add(colGripPosition)

        'StdLength
        Dim colStdLength As New DataGridViewTextBoxColumn
        With colStdLength
            .HeaderText = "Std Len"
            .Width = 65
            .DataPropertyName = "StdLength"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        dgvProfile.Columns.Add(colStdLength)

        'ReverseLoad
        Dim colReverseLoad As New DataGridViewTextBoxColumn
        With colReverseLoad
            .HeaderText = "Rev Load"
            .Width = 60
            .DataPropertyName = "ReverseLoad"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        dgvProfile.Columns.Add(colReverseLoad)

        'ZRebate
        Dim colZRebate As New DataGridViewTextBoxColumn
        With colZRebate
            .HeaderText = "Rebate"
            .Width = 70
            .DataPropertyName = "ZRebate"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgvProfile.Columns.Add(colZRebate)

        'Height
        Dim colHeight As New DataGridViewTextBoxColumn
        With colHeight
            .HeaderText = "Height"
            .Width = 60
            .DataPropertyName = "Height"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgvProfile.Columns.Add(colHeight)

        'EuroHeight
        Dim colEuroHeight As New DataGridViewTextBoxColumn
        With colEuroHeight
            .HeaderText = "Euro Ht"
            .Width = 60
            .DataPropertyName = "EuroHeight"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgvProfile.Columns.Add(colEuroHeight)

        'System
        Dim colSystem As New DataGridViewComboBoxColumn
        With colSystem
            .DataPropertyName = "System"
            .HeaderText = "System"
            .Width = 200
            .DropDownWidth = 200
            .MaxDropDownItems = 12
            .FlatStyle = FlatStyle.Standard
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DataSource = tblSystems
            .ValueMember = "Suite"
            .DisplayMember = "Description"
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgvProfile.Columns.Add(colSystem)
        'Add Comboox

        'ProfileName
        Dim colProfileName As New DataGridViewTextBoxColumn
        With colProfileName
            .HeaderText = "Description"
            .Width = 300
            .DataPropertyName = "ProfileName"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgvProfile.Columns.Add(colProfileName)

        'ReplicateID
        Dim colReplicateID As New DataGridViewTextBoxColumn
        With colReplicateID
            .HeaderText = "Replicate"
            .Width = 100
            .DataPropertyName = "ReplicateID"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgvProfile.Columns.Add(colReplicateID)

        'ProfileTypeID
        Dim colProfileTypeID As New DataGridViewComboBoxColumn
        Dim tblProfileTypes As DataTable = CreateTblProfileTypes()
        With colProfileTypeID
            .DataPropertyName = "ProfileTypeID"
            .HeaderText = "Type"
            .Width = 200
            .DropDownWidth = 200
            .MaxDropDownItems = 12
            .FlatStyle = FlatStyle.Standard
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DataSource = tblProfileTypes
            .ValueMember = "ptValue"
            .DisplayMember = "ptDesc"
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgvProfile.Columns.Add(colProfileTypeID)

        'NeuronStr
        Dim colNeuronStr As New DataGridViewTextBoxColumn
        With colNeuronStr
            .HeaderText = "Optim Str"
            .Width = 100
            .DataPropertyName = "NeuronStr"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V2" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colNeuronStr)

        'Centralise
        Dim colCentralise As New DataGridViewTextBoxColumn
        With colCentralise
            .HeaderText = "Centralise"
            .Width = 100
            .DataPropertyName = "Centralise"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colCentralise)

        'OptTol
        Dim colOptTol As New DataGridViewTextBoxColumn
        With colOptTol
            .HeaderText = "OptTol"
            .Width = 100
            .DataPropertyName = "OptTol"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colOptTol)

        'MinOffcut
        Dim colMinOffcut As New DataGridViewTextBoxColumn
        With colMinOffcut
            .HeaderText = "MinOffcut"
            .Width = 100
            .DataPropertyName = "MinOffcut"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colMinOffcut)

        'GripY
        Dim colGripY As New DataGridViewTextBoxColumn
        With colGripY
            .HeaderText = "GripY"
            .Width = 100
            .DataPropertyName = "GripY"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colGripY)

        'GripZ
        Dim colGripZ As New DataGridViewTextBoxColumn
        With colGripZ
            .HeaderText = "GripZ"
            .Width = 100
            .DataPropertyName = "GripZ"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colGripZ)

        'ID
        Dim colID As New DataGridViewTextBoxColumn
        With colID
            .HeaderText = "ID"
            .Width = 100
            .DataPropertyName = "ID"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colID)

        'CoreColour
        Dim colCoreColour As New DataGridViewTextBoxColumn
        With colCoreColour
            .HeaderText = "CoreColour"
            .Width = 100
            .DataPropertyName = "CoreColour"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colCoreColour)

        'SawCutStyle
        Dim colSawCutStyle As New DataGridViewTextBoxColumn
        With colSawCutStyle
            .HeaderText = "SawCutStyle"
            .Width = 100
            .DataPropertyName = "SawCutStyle"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colSawCutStyle)

        'RotateGripper
        Dim colRotateGripper As New DataGridViewTextBoxColumn
        With colRotateGripper
            .HeaderText = "Rotate Gripper"
            .Width = 100
            .DataPropertyName = "RotateGripper"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            If ProfileVersion = "V4" Then .Visible = True Else .Visible = False
        End With
        dgvProfile.Columns.Add(colRotateGripper)

        'Sort By CodeName
        dgvProfile.Sort(dgvProfile.Columns.Item("CodeName"), System.ComponentModel.ListSortDirection.Ascending)

        'Dim grp As New Janus.Windows.GridEX.GridEXGroup(gxProfile.RootTable.Columns(10))
        'dgvProfile.Groups.Add(grp)

    End Sub

#End Region
#Region " Sub - SaveProfileMUL() "

    Sub SaveProfileMUL()

        'Initialise Variables
        Dim rRow As DataRow
        Dim sDataRow As String

        'Write Profile.Mul
        FileOpen(1, prfProfileMulLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, Mid(ProfileVersion, 2, 1) + vbCrLf)
        For Each rRow In tblProfile.Rows
            sDataRow = ""
            sDataRow += rRow.Item("CodeName").ToString + ","
            sDataRow += rRow.Item("ZSection").ToString + ","
            sDataRow += rRow.Item("Flip").ToString + ","
            sDataRow += rRow.Item("Width").ToString + ","
            sDataRow += rRow.Item("GripPosition").ToString + ","
            sDataRow += rRow.Item("StdLength").ToString + ","
            sDataRow += rRow.Item("ReverseLoad").ToString + ","
            sDataRow += rRow.Item("ZRebate").ToString + ","
            sDataRow += rRow.Item("Height").ToString + ","
            sDataRow += rRow.Item("EuroHeight").ToString + ","
            sDataRow += rRow.Item("System").ToString + ","
            sDataRow += rRow.Item("ProfileName").ToString + ","
            sDataRow += rRow.Item("ReplicateID").ToString + ","
            sDataRow += rRow.Item("ProfileTypeID").ToString + ","
            If ProfileVersion = "V2" Then
                sDataRow += rRow.Item("NeuronStr").ToString + ","
            End If
            If ProfileVersion = "V3" Or ProfileVersion = "V4" Then
                sDataRow += rRow.Item("Centralise").ToString + ","
                sDataRow += rRow.Item("OptTol").ToString + ","
                sDataRow += rRow.Item("MinOffcut").ToString + ","
                sDataRow += rRow.Item("GripY").ToString + ","
                sDataRow += rRow.Item("GripZ").ToString + ","
                sDataRow += rRow.Item("ID").ToString + ","
                sDataRow += rRow.Item("CoreColour").ToString + ","
                sDataRow += rRow.Item("SawCutStyle").ToString + ","
            End If
            If ProfileVersion = "V4" Then
                sDataRow += rRow.Item("RotateGripper").ToString + ","
            End If

            'Dim oItem As Object
            'For Each oItem In rRow.ItemArray
            'sDataRow += oItem.ToString + ","
            'Next
            'If prfUseNeuron = False Then sDataRow = Mid(sDataRow, 1, Len(sDataRow) - 1)

            Print(1, sDataRow + vbCrLf)
        Next
        FileClose(1)

    End Sub

#End Region
#Region " Sub - CreateProfileSaw()    - Create Profile.SAW "

    Sub CreateProfileSaw()

        'Check OK to Continue
        Dim mbr As MsgBoxResult = MsgBox("Are You Sure?", MsgBoxStyle.YesNo)
        If mbr = MsgBoxResult.No Then Exit Sub

        'Try to Create File
        Try
            FileOpen(1, prfProfileSawLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Can not create Profile.saw" + vbCrLf + ex.Message)
            Exit Sub
        End Try

        'Print Header
        'Print(1, "File Created by Flowops.Net" + vbCrLf)

        'Print Profiles
        For Each r As DataRow In tblProfile.Rows

            Dim sProfile As String = r.Item("CodeName")
            Dim sWidth As String = CStr(CInt(r.Item("Width") * 10))
            Dim sLength As String = CStr(r.Item("StdLength"))
            Dim sZBlock As String = r.Item("ZSection")
            Dim sFlip As String = r.Item("Flip")
            Dim sLine As String = sProfile + "," + sWidth + "," + sZBlock + ",0,0,0,N,N,N," + sLength + "0," + sFlip + vbCrLf

            Print(1, sLine)

        Next

        'Close File
        FileClose(1)

        'Inform User of Completion
        MsgBox("Saw File Created", MsgBoxStyle.Information)


    End Sub

#End Region


    'ZStdOpDef.Mul
#Region " Function - GetStdOpDefTable() "

    Function GetStdOpDefTable() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable("tblStdOpDef")
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()

        'StdOpCode
        Dim cStdOpCode As New DataColumn("StdOpCode")
        cStdOpCode.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpCode)

        'StdOpDescription
        Dim cStdOpDescription As New DataColumn("StdOpDescription")
        cStdOpDescription.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpDescription)

        'StdOpType
        Dim cStdOpType As New DataColumn("StdOpType")
        cStdOpType.DataType = GetType(Integer)
        tblTable.Columns.Add(cStdOpType)

        'StdOpVar1
        Dim cStdOpVar1 As New DataColumn("StdOpVar1")
        cStdOpVar1.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar1)

        'StdOpVar2
        Dim cStdOpVar2 As New DataColumn("StdOpVar2")
        cStdOpVar2.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar2)

        'StdOpVar3
        Dim cStdOpVar3 As New DataColumn("StdOpVar3")
        cStdOpVar3.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar3)

        'StdOpVar4
        Dim cStdOpVar4 As New DataColumn("StdOpVar4")
        cStdOpVar4.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar4)

        'StdOpVar5
        Dim cStdOpVar5 As New DataColumn("StdOpVar5")
        cStdOpVar5.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar5)

        'StdOpVar6
        Dim cStdOpVar6 As New DataColumn("StdOpVar6")
        cStdOpVar6.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar6)

        'StdOpVar7
        Dim cStdOpVar7 As New DataColumn("StdOpVar7")
        cStdOpVar7.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar7)

        'StdOpVar8
        Dim cStdOpVar8 As New DataColumn("StdOpVar8")
        cStdOpVar8.DataType = GetType(String)
        tblTable.Columns.Add(cStdOpVar8)

        Dim rRow As DataRow

        If File.Exists(modGlobal.prfZStdOpDefMulLoc) Then
            FileOpen(1, prfZStdOpDefMulLoc, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)
            Do While Not EOF(1)
                Dim sLine As String = LineInput(1)
                rRow = tblTable.NewRow

                Dim iLoc As Integer = InStr(sLine, ",", CompareMethod.Text)

                'Ignore Non comma Lines
                If iLoc <> 0 Then

                    'Get StdOpCode
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpCode") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpCode") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpDescription
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpDescription") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpDescription") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpType
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpType") = CInt(sString)
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpType") = 0
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar1
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar1") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar1") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar2
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar2") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar2") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar3
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar3") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar3") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar4
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar4") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar4") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar5
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar5") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar5") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar6
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar6") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar6") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar7
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar7") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar7") = ""
                        sLine = Mid(sLine, 2)
                    End If

                    'StdOpVar8
                    iLoc = InStr(sLine, ",", CompareMethod.Text)
                    If iLoc <> 1 And iLoc <> 0 Then
                        sString = Mid(sLine, 1, iLoc - 1)
                        rRow.Item("StdOpVar8") = sString
                        sLine = Mid(sLine, iLoc + 1)
                    Else
                        rRow.Item("StdOpVar8") = sLine
                        'sLine = Mid(sLine, 2)
                    End If

                    'Add New Row to Table
                    tblTable.Rows.Add(rRow)

                End If

            Loop

            FileClose(1)

        End If

        Return tblTable

    End Function

#End Region
#Region " Function - AddStdOp() "

    Function AddStdOp(ByVal sCode As String, ByVal sDesc As String, ByVal iType As Integer, ByVal sV1 As String, ByVal sV2 As String, ByVal sV3 As String, ByVal sV4 As String, ByVal sV5 As String, ByVal sV6 As String, ByVal sV7 As String, ByVal sV8 As String) As Boolean

        'Initialise variables
        Dim bExists As Boolean = False

        'Check if StdOp already exists
        For Each rRow As DataRow In tblStdOpDef.Rows
            If rRow.Item("StdOpCode") = sCode Then
                bExists = True
                Dim mbrAnswer As MsgBoxResult = MsgBox("This Stdop already exists, do you want to replace it?", MsgBoxStyle.YesNo)
                If mbrAnswer = MsgBoxResult.Yes Then
                    'rRow.SetField("StdOpDescription", sDesc)

                    rRow.Item("StdOpDescription") = sDesc
                    rRow.Item("StdOpType") = iType
                    rRow.Item("StdOpVar1") = sV1
                    rRow.Item("StdOpVar2") = sV2
                    rRow.Item("StdOpVar3") = sV3
                    rRow.Item("StdOpVar4") = sV4
                    rRow.Item("StdOpVar5") = sV5
                    rRow.Item("StdOpVar6") = sV6
                    rRow.Item("StdOpVar7") = sV7
                    rRow.Item("StdOpVar8") = sV8
                End If
                Exit For
            End If
        Next

        If Not bExists Then
            'Add StdOp
            Dim rNewRow As DataRow = tblStdOpDef.NewRow
            rNewRow.Item("StdOpCode") = sCode
            rNewRow.Item("StdOpDescription") = sDesc
            rNewRow.Item("StdOpType") = iType
            rNewRow.Item("StdOpVar1") = sV1
            rNewRow.Item("StdOpVar2") = sV2
            rNewRow.Item("StdOpVar3") = sV3
            rNewRow.Item("StdOpVar4") = sV4
            rNewRow.Item("StdOpVar5") = sV5
            rNewRow.Item("StdOpVar6") = sV6
            rNewRow.Item("StdOpVar7") = sV7
            rNewRow.Item("StdOpVar8") = sV8
            tblStdOpDef.Rows.Add(rNewRow)
        End If

        SaveZStdOpDefMul()

    End Function

#End Region
#Region " Sub - BindZStdOpDefGrid() "

    Sub BindZStdOpDefGrid(ByVal dgv As DataGridView)

        'Create Binding Source
        Dim bs As New BindingSource(dsStdOpDef, "tblStdOpDef")

        'Grab Grid Data
        With dgv
            .DataSource = bs
            .Font = New Font("Arial", 12, FontStyle.Regular)
            .Columns.Clear()
            .AllowUserToAddRows = True
            .AllowUserToOrderColumns = False
            .AllowUserToDeleteRows = True
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = True
        End With

        'StdOpCode
        Dim colStdOpCode As New DataGridViewTextBoxColumn
        With colStdOpCode
            .HeaderText = "Std Op"
            .Width = 100
            .DataPropertyName = "StdOpCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpCode)

        'StdOpDescription
        Dim colStdOpDescription As New DataGridViewTextBoxColumn
        With colStdOpDescription
            .HeaderText = "Description"
            .Width = 350
            .DataPropertyName = "StdOpDescription"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpDescription)

        'StdOpType
        Dim colStdOpType As New DataGridViewComboBoxColumn
        With colStdOpType
            .HeaderText = "Op Type"
            .Width = 250
            .DataPropertyName = "StdOpType"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .MaxDropDownItems = 8
            .DropDownWidth = 250
            Dim dv As New DataView(tblOpTypes)
            dv.Sort = "OpDesc"
            .DataSource = dv
            .ValueMember = "OpType"
            .DisplayMember = "OpDesc"
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgv.Columns.Add(colStdOpType)

        'StdOpVar1
        Dim colStdOpVar1 As New DataGridViewTextBoxColumn
        With colStdOpVar1
            .HeaderText = "Var 1"
            .Width = 70
            .DataPropertyName = "StdOpVar1"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar1)

        'StdOpVar2
        Dim colStdOpVar2 As New DataGridViewTextBoxColumn
        With colStdOpVar2
            .HeaderText = "Var 2"
            .Width = 70
            .DataPropertyName = "StdOpVar2"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar2)

        'StdOpVar3
        Dim colStdOpVar3 As New DataGridViewTextBoxColumn
        With colStdOpVar3
            .HeaderText = "Var 3"
            .Width = 70
            .DataPropertyName = "StdOpVar3"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar3)

        'StdOpVar4
        Dim colStdOpVar4 As New DataGridViewTextBoxColumn
        With colStdOpVar4
            .HeaderText = "Var 4"
            .Width = 70
            .DataPropertyName = "StdOpVar4"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar4)

        'StdOpVar5
        Dim colStdOpVar5 As New DataGridViewTextBoxColumn
        With colStdOpVar5
            .HeaderText = "Var 5"
            .Width = 70
            .DataPropertyName = "StdOpVar5"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar5)

        'StdOpVar6
        Dim colStdOpVar6 As New DataGridViewTextBoxColumn
        With colStdOpVar6
            .HeaderText = "Var 6"
            .Width = 70
            .DataPropertyName = "StdOpVar6"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar6)

        'StdOpVar7
        Dim colStdOpVar7 As New DataGridViewTextBoxColumn
        With colStdOpVar7
            .HeaderText = "Var 7"
            .Width = 70
            .DataPropertyName = "StdOpVar7"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar7)

        'StdOpVar8
        Dim colStdOpVar8 As New DataGridViewTextBoxColumn
        With colStdOpVar8
            .HeaderText = "Var 8"
            .Width = 70
            .DataPropertyName = "StdOpVar8"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colStdOpVar8)

        dgv.Sort(colStdOpCode, System.ComponentModel.ListSortDirection.Ascending)

    End Sub

#End Region
#Region " Sub - SaveZStdOpDefMul() "

    Sub SaveZStdOpDefMul()

        'Initialise Variables
        Dim rRow As DataRow
        Dim sDataRow As String
        Dim oItem As Object

        'Write ZStdOpDef.Mul
        FileOpen(1, prfZStdOpDefMulLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, "ZStdOpDef.Mul Created By FlowlineOps.Net" + vbCrLf)
        For Each rRow In tblStdOpDef.Rows
            sDataRow = ""
            For Each oItem In rRow.ItemArray
                sDataRow += oItem.ToString + ","
            Next
            sDataRow = Mid(sDataRow, 1, Len(sDataRow) - 1)
            Print(1, sDataRow + vbCrLf)
        Next
        FileClose(1)

    End Sub

#End Region

    'Hardware.Mul
#Region " Function - CreateTblHardware() "

    Function CreateTblHardware() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable("tblHardware")
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()

        'HardwareCode
        Dim cHardwareCode As New DataColumn("HardwareCode")
        cHardwareCode.DataType = GetType(String)
        tblTable.Columns.Add(cHardwareCode)

        'HardwareDescription
        Dim cHardwareDescription As New DataColumn("HardwareDescription")
        cHardwareDescription.DataType = GetType(String)
        tblTable.Columns.Add(cHardwareDescription)

        'HardwareType
        Dim cHardwareType As New DataColumn("HardwareType")
        cHardwareType.DataType = GetType(Integer)
        cHardwareType.DefaultValue = 0
        tblTable.Columns.Add(cHardwareType)

        'Try to Open
        Try
            FileOpen(1, prfHardwareLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Open Hardware Table" + vbCrLf + ex.Message, MsgBoxStyle.Exclamation)
            Return tblTable
        End Try

        'read first header line
        LineInput(1)

        'Generate Holding Row Variable
        Dim rRow As DataRow

        'Read Rest of file
        Do While Not EOF(1)

            'Read Line from File
            Dim s As String = LineInput(1)

            'Get Code
            Dim iPos As Integer = InStr(s, ",", CompareMethod.Text)
            Dim sCode As String = Mid(s, 1, iPos - 1)
            s = Mid(s, iPos + 1)

            'Get Desc
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim sDesc As String = Mid(s, 1, iPos - 1)

            'Get Type
            Dim iType As Integer = CInt(Mid(s, iPos + 1))

            'Add Data to Table
            rRow = tblTable.NewRow
            rRow.Item("HardwareCode") = sCode
            rRow.Item("HardwareDescription") = sDesc
            rRow.Item("HardwareType") = iType
            tblTable.Rows.Add(rRow)

        Loop

        'Close Hardware file
        FileClose(1)

        'Return Talbe Contents
        Return tblTable

    End Function

#End Region
#Region " Sub - BindHardwareGrid() "

    Sub BindHardwareGrid(ByVal dgv As DataGridView)

        'Create Binding Source
        Dim bs As New BindingSource(dsHardware, "tblHardware")

        'Grab Grid Data
        With dgv
            .DataSource = bs
            .Font = New Font("Arial", 12, FontStyle.Regular)
            .Columns.Clear()
            .AllowUserToAddRows = True
            .AllowUserToOrderColumns = False
            .AllowUserToDeleteRows = True
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = True
        End With

        'HardwareCode
        Dim colHardwareCode As New DataGridViewTextBoxColumn
        With colHardwareCode
            .HeaderText = "Code"
            .Width = 100
            .DataPropertyName = "HardwareCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colHardwareCode)

        'HardwareDescription
        Dim colHardwareDescription As New DataGridViewTextBoxColumn
        With colHardwareDescription
            .HeaderText = "Description"
            .Width = 400
            .DataPropertyName = "HardwareDescription"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colHardwareDescription)

        'HardwareType
        Dim colHardwareType As New DataGridViewTextBoxColumn
        With colHardwareType
            .HeaderText = "Type"
            .Width = 400
            .DataPropertyName = "HardwareType"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = False
        End With
        dgv.Columns.Add(colHardwareType)

        dgv.Sort(colHardwareCode, System.ComponentModel.ListSortDirection.Ascending)

    End Sub

#End Region
#Region " Sub - SaveHardware() "

    Sub SaveHardware()

        'Initialise Variables
        Dim rRow As DataRow
        Dim sDataRow As String
        Dim oItem As Object

        'Write ZStdOpDef.Mul
        FileOpen(1, prfHardwareLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, "Hardware.Mul Created By FlowlineOps.Net" + vbCrLf)
        For Each rRow In tblHardware.Rows
            sDataRow = ""
            For Each oItem In rRow.ItemArray
                sDataRow += oItem.ToString + ","
            Next
            sDataRow = Mid(sDataRow, 1, Len(sDataRow) - 1)
            Print(1, sDataRow + vbCrLf)
        Next
        FileClose(1)

    End Sub

#End Region

    'Preps.Mul
#Region " Function - GetPrepsTable() "

    Function GetPrepsTable() As DataTable

        Dim sString As String = ""
        Dim tblTable As New DataTable("tblPreps")
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture

        tblTable.Columns.Clear()

        'PrepID
        Dim cPrepID(1) As DataColumn
        cPrepID(0) = New DataColumn("PrepID")
        cPrepID(0).DataType = GetType(Integer)
        cPrepID(0).AutoIncrement = True
        cPrepID(0).AutoIncrementSeed = 1
        cPrepID(0).AutoIncrementStep = 1
        tblTable.Columns.Add(cPrepID(0))

        tblTable.PrimaryKey = cPrepID

        'HardwareCode
        Dim cHardwareCode As New DataColumn("HardwareCode")
        cHardwareCode.DataType = GetType(String)
        tblTable.Columns.Add(cHardwareCode)

        'PrepName
        Dim cPrepName As New DataColumn("PrepName")
        cPrepName.DataType = GetType(String)
        tblTable.Columns.Add(cPrepName)

        'PrepType
        Dim cPrepType As New DataColumn("PrepType")
        cPrepType.DataType = GetType(String)
        tblTable.Columns.Add(cPrepType)

        'PrepPosition
        Dim cPrepPosition As New DataColumn("PrepPosition")
        cPrepPosition.DataType = GetType(Double)
        cPrepPosition.DefaultValue = 0
        tblTable.Columns.Add(cPrepPosition)

        'Backset
        Dim cBackset As New DataColumn("Backset")
        cBackset.DataType = GetType(Double)
        cBackset.DefaultValue = 0
        tblTable.Columns.Add(cBackset)

        'Dim1
        Dim cDim1 As New DataColumn("Dim1")
        cDim1.DataType = GetType(Double)
        cDim1.DefaultValue = 0
        tblTable.Columns.Add(cDim1)

        'Dim2
        Dim cDim2 As New DataColumn("Dim2")
        cDim2.DataType = GetType(Double)
        cDim2.DefaultValue = 0
        tblTable.Columns.Add(cDim2)

        'Yoff
        Dim cYoff As New DataColumn("Yoff")
        cYoff.DataType = GetType(Double)
        cYoff.DefaultValue = 0
        tblTable.Columns.Add(cYoff)

        'Try to Open
        Try
            FileOpen(1, prfPrepsLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Open Preps Table" + vbCrLf + ex.Message, MsgBoxStyle.Exclamation)
            Return tblTable
        End Try

        'read first header line
        LineInput(1)

        'Generate Holding Row Variable
        Dim rRow As DataRow

        'Read Rest of file
        Do While Not EOF(1)

            'Read Line from File
            Dim s As String = LineInput(1)

            'Get HardwareCode
            Dim iPos As Integer = InStr(s, ",", CompareMethod.Text)
            Dim sHardwareCode As String = Mid(s, 1, iPos - 1)
            s = Mid(s, iPos + 1)

            'Get PrepName
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim sPrepName As String = Mid(s, 1, iPos - 1)
            s = Mid(s, iPos + 1)

            'Get PrepType
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim sPrepType As String = Mid(s, 1, iPos - 1)
            s = Mid(s, iPos + 1)

            'Get PrepPosition
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim dPrepPosition As Double = CDbl(Mid(s, 1, iPos - 1))
            s = Mid(s, iPos + 1)

            'Get Backset
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim dBackset As Double = CDbl(Mid(s, 1, iPos - 1))
            s = Mid(s, iPos + 1)

            'Get Dim1
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim dDim1 As Double = CDbl(Mid(s, 1, iPos - 1))
            s = Mid(s, iPos + 1)

            'Get Dim2
            iPos = InStr(s, ",", CompareMethod.Text)
            Dim dDim2 As Double = CDbl(Mid(s, 1, iPos - 1))

            'Get Yoff
            Dim dYoff As Double = CDbl(Mid(s, iPos + 1))

            'Add Data to Table
            rRow = tblTable.NewRow
            rRow.Item("HardwareCode") = sHardwareCode
            rRow.Item("PrepName") = sPrepName
            rRow.Item("PrepType") = sPrepType
            rRow.Item("PrepPosition") = dPrepPosition
            rRow.Item("Backset") = dBackset
            rRow.Item("Dim1") = dDim1
            rRow.Item("Dim2") = dDim2
            rRow.Item("Yoff") = dYoff
            tblTable.Rows.Add(rRow)

        Loop

        'Close Hardware file
        FileClose(1)

        'Return Talbe Contents
        Return tblTable

    End Function

#End Region
#Region " Sub - BindPrepsGrid() "

    Sub BindPrepsGrid(ByVal dgv As DataGridView, ByVal sHardwareCode As String)

        'Create Binding Source
        Dim bs As New BindingSource(dsPreps, "tblPreps")

        If sHardwareCode <> "" Then bs.Filter = "HardwareCode = '" + sHardwareCode + "'"

        'Grab Grid Data
        With dgv
            .DataSource = bs
            .Font = New Font("Arial", 12, FontStyle.Regular)
            .Columns.Clear()
            .AllowUserToAddRows = True
            .AllowUserToOrderColumns = False
            .AllowUserToDeleteRows = True
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = True
        End With

        'PrepID
        Dim colPrepID As New DataGridViewTextBoxColumn
        With colPrepID
            .Name = "PrepID"
            .HeaderText = "ID"
            .Width = 100
            .DataPropertyName = "PrepID"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Visible = False
        End With
        dgv.Columns.Add(colPrepID)
        dgv.Columns("PrepID").Visible = False

        'HardwareCode
        Dim colHardwareCode As New DataGridViewTextBoxColumn
        With colHardwareCode
            .HeaderText = "Code"
            .Width = 100
            .DataPropertyName = "HardwareCode"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = False
        End With
        dgv.Columns.Add(colHardwareCode)

        'PrepName
        Dim colPrepName As New DataGridViewTextBoxColumn
        With colPrepName
            .HeaderText = "Name"
            .Width = 150
            .DataPropertyName = "PrepName"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        End With
        dgv.Columns.Add(colPrepName)

        'PrepType
        Dim colPrepType As New DataGridViewComboBoxColumn
        With colPrepType
            .HeaderText = "Type"
            .Width = 98
            .DropDownWidth = 98
            .MaxDropDownItems = 2
            .FlatStyle = FlatStyle.Standard
            .DataPropertyName = "PrepType"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Items.AddRange("dr_slot", "dr_gbox")
            .SortMode = DataGridViewColumnSortMode.Automatic
        End With
        dgv.Columns.Add(colPrepType)

        'PrepPosition
        Dim colPrepPosition As New DataGridViewTextBoxColumn
        With colPrepPosition
            .HeaderText = "Position"
            .Width = 70
            .DataPropertyName = "PrepPosition"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgv.Columns.Add(colPrepPosition)

        'Backset
        Dim colBackset As New DataGridViewTextBoxColumn
        With colBackset
            .HeaderText = "Backset"
            .Width = 100
            .DataPropertyName = "Backset"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = False
        End With
        dgv.Columns.Add(colBackset)

        'Dim1
        Dim colDim1 As New DataGridViewTextBoxColumn
        With colDim1
            .HeaderText = "Dim1"
            .Width = 70
            .DataPropertyName = "Dim1"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgv.Columns.Add(colDim1)

        'Dim2
        Dim colDim2 As New DataGridViewTextBoxColumn
        With colDim2
            .HeaderText = "Dim2"
            .Width = 70
            .DataPropertyName = "Dim2"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgv.Columns.Add(colDim2)

        'Yoff
        Dim colYoff As New DataGridViewTextBoxColumn
        With colYoff
            .HeaderText = "Yoff"
            .Width = 100
            .DataPropertyName = "Yoff"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
            .Visible = False
        End With
        dgv.Columns.Add(colYoff)

        dgv.Sort(colPrepPosition, System.ComponentModel.ListSortDirection.Ascending)

    End Sub

#End Region
#Region " Sub - SavePreps() "

    Sub SavePreps()

        'Initialise Variables
        Dim rRow As DataRow
        Dim sDataRow As String

        'Write ZStdOpDef.Mul
        FileOpen(1, prfPrepsLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
        Print(1, "Preps.Mul Created By FlowlineOps.Net" + vbCrLf)
        For Each rRow In tblPreps.Rows
            sDataRow = ""
            Dim iItems As Integer = rRow.ItemArray.Length
            For iLoop As Integer = 1 To iItems - 1
                sDataRow += rRow.Item(iLoop).ToString + ","
            Next
            sDataRow = Mid(sDataRow, 1, Len(sDataRow) - 1)
            Print(1, sDataRow + vbCrLf)
        Next
        FileClose(1)

    End Sub

#End Region

    'OpTypes.Mul
#Region " Function - GetOpTypesTable() "

    Function GetOpTypesTable() As DataTable

        Dim tblTable As New DataTable("tblOpTypes")
        tblTable.Locale = System.Globalization.CultureInfo.InvariantCulture
        tblTable.Columns.Clear()

        'OpType
        Dim cOpType As New DataColumn("OpType")
        cOpType.DataType = GetType(Integer)
        tblTable.Columns.Add(cOpType)

        'OpDesc
        Dim cOpDesc As New DataColumn("OpDesc")
        cOpDesc.DataType = GetType(String)
        tblTable.Columns.Add(cOpDesc)

        'OpExpectedTime
        Dim cOpExpectedTime As New DataColumn("OpExpectedTime")
        cOpExpectedTime.DataType = GetType(Double)
        tblTable.Columns.Add(cOpExpectedTime)

        Dim rRow As DataRow

        If File.Exists(prfOpTypesLoc) Then

            FileOpen(1, prfOpTypesLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
            Do While Not EOF(1)
                Dim sString As String = LineInput(1)

                'Get OpType
                Dim iPos As Integer = InStr(sString, ",", CompareMethod.Text)
                Dim iID As Integer = CInt(Mid(sString, 1, iPos - 1))
                sString = Mid(sString, iPos + 1)

                'Get OpDesc
                iPos = InStr(sString, ",", CompareMethod.Text)
                Dim sDesc As String = Mid(sString, 1, iPos - 1)

                'Get OpExpectedTime
                Dim dTime As Double = CDbl(Mid(sString, iPos + 1))

                'Add to Table
                rRow = tblTable.NewRow()
                rRow.Item("OpType") = iID
                rRow.Item("OpDesc") = sDesc
                rRow.Item("OpExpectedTime") = dTime
                tblTable.Rows.Add(rRow)

            Loop
            FileClose(1)

        Else

            FileOpen(1, prfOpTypesLoc, OpenMode.Output, OpenAccess.Write, OpenShare.LockWrite)
            Print(1, "1,Drainage,2" + vbCrLf)
            Print(1, "2,V Notches,10" + vbCrLf)
            Print(1, "3,Trickle Vents,20" + vbCrLf)
            Print(1, "4,Espags,24" + vbCrLf)
            Print(1, "5,Door Handle Preps,60" + vbCrLf)
            Print(1, "7,Door Keeps,45" + vbCrLf)
            Print(1, "10,Spot Preps,4" + vbCrLf)
            Print(1, "18,Letter Boxes,35" + vbCrLf)
            Print(1, "21,Y Notches,5" + vbCrLf)
            Print(1, "22,Slots,45" + vbCrLf)
            Print(1, "50,Square Cuts,8" + vbCrLf)
            Print(1, "51,Door Gearboxes,8" + vbCrLf)
            Print(1, "52,Aly Threshold Prep,15" + vbCrLf)
            Print(1, "53,Door Sash Hinge,4" + vbCrLf)
            Print(1, "54,Door Frame Hinge,4" + vbCrLf)
            Print(1, "55,Laser Hole,8" + vbCrLf)
            Print(1, "56,Drainage - Speedy,2" + vbCrLf)
            Print(1, "57,Drainage - Upstand Removal,4" + vbCrLf)
            Print(1, "58,Drainage - Middle Chamber,10" + vbCrLf)
            Print(1, "59,Four Spot Preps,16" + vbCrLf)
            Print(1, "60,Three Spot Preps,12" + vbCrLf)
            Print(1, "61,Two Spot Preps,8" + vbCrLf)
            FileClose(1)

            '1 - Drainage
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 1
            rRow.Item("OpDesc") = "Drainage"
            rRow.Item("OpExpectedTime") = 2
            tblTable.Rows.Add(rRow)

            '2 - V Notches
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 2
            rRow.Item("OpDesc") = "V Notches"
            rRow.Item("OpExpectedTime") = 10
            tblTable.Rows.Add(rRow)

            '3 - Trickle Vents
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 3
            rRow.Item("OpDesc") = "Trickle Vents"
            rRow.Item("OpExpectedTime") = 20
            tblTable.Rows.Add(rRow)

            '4 - Espags
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 4
            rRow.Item("OpDesc") = "Espags"
            rRow.Item("OpExpectedTime") = 24
            tblTable.Rows.Add(rRow)

            '5 - Door Handle Preps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 5
            rRow.Item("OpDesc") = "Door Handle Preps"
            rRow.Item("OpExpectedTime") = 60
            tblTable.Rows.Add(rRow)

            '7 - Door Keeps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 7
            rRow.Item("OpDesc") = "Door Keeps"
            rRow.Item("OpExpectedTime") = 45
            tblTable.Rows.Add(rRow)

            '10 - Spot Preps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 10
            rRow.Item("OpDesc") = "Spot Preps"
            rRow.Item("OpExpectedTime") = 4
            tblTable.Rows.Add(rRow)

            '18 - Letter Boxes
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 18
            rRow.Item("OpDesc") = "Letter Boxes"
            rRow.Item("OpExpectedTime") = 35
            tblTable.Rows.Add(rRow)

            '21 - Y Notches
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 21
            rRow.Item("OpDesc") = "Y Notches"
            rRow.Item("OpExpectedTime") = 5
            tblTable.Rows.Add(rRow)

            '22 - Slots
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 22
            rRow.Item("OpDesc") = "Slots"
            rRow.Item("OpExpectedTime") = 45
            tblTable.Rows.Add(rRow)

            '50 - Square Cuts
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 50
            rRow.Item("OpDesc") = "Square Cuts"
            rRow.Item("OpExpectedTime") = 8
            tblTable.Rows.Add(rRow)

            '51 - Door Gearboxes
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 51
            rRow.Item("OpDesc") = "Door Gearboxes"
            rRow.Item("OpExpectedTime") = 8
            tblTable.Rows.Add(rRow)

            '52 - Aly Threshold Prep
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 52
            rRow.Item("OpDesc") = "Aly Threshold Prep"
            rRow.Item("OpExpectedTime") = 15
            tblTable.Rows.Add(rRow)

            '53 - Door Sash Hinge
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 53
            rRow.Item("OpDesc") = "Door Sash Hinge"
            rRow.Item("OpExpectedTime") = 4
            tblTable.Rows.Add(rRow)

            '54 - Door Frame Hinge
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 54
            rRow.Item("OpDesc") = "Door Frame Hinge"
            rRow.Item("OpExpectedTime") = 4
            tblTable.Rows.Add(rRow)

            '55 - Laser Hole
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 55
            rRow.Item("OpDesc") = "Laser Hole"
            rRow.Item("OpExpectedTime") = 8
            tblTable.Rows.Add(rRow)

            '56 - Drainage - Speedy
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 56
            rRow.Item("OpDesc") = "Drainage - Speedy"
            rRow.Item("OpExpectedTime") = 2
            tblTable.Rows.Add(rRow)

            '57 - Drainage - Upstand Removal
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 57
            rRow.Item("OpDesc") = "Drainage - Upstand Removal"
            rRow.Item("OpExpectedTime") = 4
            tblTable.Rows.Add(rRow)

            '58 - Drainage - Middle Chamber
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 58
            rRow.Item("OpDesc") = "Drainage - Middle Chamber"
            rRow.Item("OpExpectedTime") = 10
            tblTable.Rows.Add(rRow)

            '59 - Four Spot Preps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 59
            rRow.Item("OpDesc") = "Four Spot Preps"
            rRow.Item("OpExpectedTime") = 16
            tblTable.Rows.Add(rRow)

            '60 - Three Spot Preps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 60
            rRow.Item("OpDesc") = "Three Spot Preps"
            rRow.Item("OpExpectedTime") = 12
            tblTable.Rows.Add(rRow)

            '61 - Two Spot Preps
            rRow = tblTable.NewRow()
            rRow.Item("OpType") = 61
            rRow.Item("OpDesc") = "Two Spot Preps"
            rRow.Item("OpExpectedTime") = 8
            tblTable.Rows.Add(rRow)

        End If

        Return tblTable

    End Function

#End Region
#Region " Sub - BindOpTypesGrid() "

    Sub BindOpTypesGrid(ByVal dgv As DataGridView)

        'Create Binding Source
        Dim bs As New BindingSource(dsOpTypes, "tblOpTypes")

        'Grab Grid Data
        With dgv
            .DataSource = bs
            .Font = New Font("Arial", 12, FontStyle.Regular)
            .Columns.Clear()
            .AllowUserToAddRows = True
            .AllowUserToOrderColumns = False
            .AllowUserToDeleteRows = True
            .AllowUserToResizeColumns = True
            .AllowUserToResizeRows = True
        End With

        'OpType
        Dim colOpType As New DataGridViewTextBoxColumn
        With colOpType
            .Name = "OpType"
            .HeaderText = "ID"
            .Width = 60
            .DataPropertyName = "OpType"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Visible = True
        End With
        dgv.Columns.Add(colOpType)

        'OpDesc
        Dim colOpDesc As New DataGridViewTextBoxColumn
        With colOpDesc
            .HeaderText = "Description"
            .Width = 300
            .DataPropertyName = "OpDesc"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Visible = True
        End With
        dgv.Columns.Add(colOpDesc)

        'OpExpectedTime
        Dim colOpExpectedTime As New DataGridViewTextBoxColumn
        With colOpExpectedTime
            .HeaderText = "Expected Time"
            .Width = 90
            .DataPropertyName = "OpExpectedTime"
            .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "0.0"
        End With
        dgv.Columns.Add(colOpExpectedTime)

        dgv.Sort(colOpType, System.ComponentModel.ListSortDirection.Ascending)

    End Sub

#End Region
#Region " Sub - SaveTblOpTypes()      - Save off the tblOpTypes "

    Sub SaveTblOpTypes()

        FileOpen(1, prfOpTypesLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)

        For Each temprow As DataRow In tblOpTypes.Rows
            Print(1, temprow.Item("OpType").ToString + "," + temprow.Item("OpDesc") + "," + temprow.Item("OpExpectedTime").ToString + vbCrLf)
        Next

        FileClose(1)

    End Sub

#End Region

    'Draw.Mul
#Region " Sub - ReadDrawMul() "

    Sub ReadDrawMul()

        Dim DrawLoc As String = Mid(prfProfileMulLoc, 1, Len(prfProfileMulLoc) - 11) + "draw.mul"
        Try
            FileOpen(1, DrawLoc, OpenMode.Input, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Open " + DrawLoc + vbCrLf + ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub
        End Try
        While Not (EOF(1))
            Dim sDangerString As String = LineInput(1)

            If Mid(sDangerString, 1, 8) = "RingRad=" Then
                prfOuterRingRad = CShort(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 8) = "DangerY=" Then
                prfDangerY = CDbl(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 8) = "DangerZ=" Then
                prfDangerZ = CDbl(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 10) = "DangerRad=" Then
                prfDangerRad = CShort(Mid(sDangerString, 11))
            End If
            If Mid(sDangerString, 1, 10) = "DatumSize=" Then
                prfDatumSize = CSng(Mid(sDangerString, 11))
            End If
            If Mid(sDangerString, 1, 13) = "VBladeOffset=" Then
                prfVBladeOffset = CSng(Mid(sDangerString, 14))
            End If
            If Mid(sDangerString, 1, 12) = "VFrontDatum=" Then
                prfVFrontDatum = CSng(Mid(sDangerString, 13))
            End If
            If Mid(sDangerString, 1, 11) = "VRearDatum=" Then
                prfVRearDatum = CSng(Mid(sDangerString, 12))
            End If
            If Mid(sDangerString, 1, 10) = "VBladeDia=" Then
                prfVBladeDia = CSng(Mid(sDangerString, 11))
            End If
            If Mid(sDangerString, 1, 16) = "PushGripperMinY=" Then
                prfPushGripperMinY = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PushGripperMaxY=" Then
                prfPushGripperMaxY = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PushGripperMinZ=" Then
                prfPushGripperMinZ = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PushGripperMaxZ=" Then
                prfPushGripperMaxZ = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 18) = "PushGripperHeight=" Then
                prfPushGripperHeight = CInt(Mid(sDangerString, 19))
            End If
            If Mid(sDangerString, 1, 17) = "PushGripperWidth=" Then
                prfPushGripperWidth = CInt(Mid(sDangerString, 18))
            End If
            If Mid(sDangerString, 1, 16) = "PullGripperMinY=" Then
                prfPullGripperMinY = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PullGripperMaxY=" Then
                prfPullGripperMaxY = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PullGripperMinZ=" Then
                prfPullGripperMinZ = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 16) = "PullGripperMaxZ=" Then
                prfPullGripperMaxZ = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 18) = "PullGripperHeight=" Then
                prfPullGripperHeight = CInt(Mid(sDangerString, 19))
            End If
            If Mid(sDangerString, 1, 16) = "PullGripperZPos=" Then
                prfPullGripperZPos = CInt(Mid(sDangerString, 17))
            End If
            If Mid(sDangerString, 1, 8) = "ZBlock1=" Then
                prfZBlock1 = CSng(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 8) = "ZBlock2=" Then
                prfZBlock2 = CSng(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 8) = "ZBlock3=" Then
                prfZBlock3 = CSng(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 8) = "ZBlock4=" Then
                prfZBlock4 = CSng(Mid(sDangerString, 9))
            End If
            If Mid(sDangerString, 1, 10) = "VCylinder=" Then
                prfVCylinder = CSng(Mid(sDangerString, 11))
            End If
            If Mid(sDangerString, 1, 12) = "TipToCollet=" Then
                prfTipToCollet = CSng(Mid(sDangerString, 13))
            End If


        End While
        FileClose(1)

    End Sub

#End Region
#Region " Sub - CreateDrawMul() "

    Sub CreateDrawMul()

        'Create Draw.Mul
        Dim DrawLoc As String = Mid(prfProfileMulLoc, 1, Len(prfProfileMulLoc) - 11) + "draw.mul"
        Try
            FileOpen(1, DrawLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Cannot Create " + DrawLoc + vbCrLf + ex.Message)
            Exit Sub
        End Try
        Print(1, "RingRad=" + prfOuterRingRad.ToString + vbCrLf)
        Print(1, "DangerY=" + prfDangerY.ToString + vbCrLf)
        Print(1, "DangerZ=" + prfDangerZ.ToString + vbCrLf)
        Print(1, "DangerRad=" + prfDangerRad.ToString + vbCrLf)
        Print(1, "DatumSize=" + prfDatumSize.ToString + vbCrLf)
        Print(1, "VBladeOffset=" + prfVBladeOffset.ToString + vbCrLf)
        Print(1, "VFrontDatum=" + prfVFrontDatum.ToString + vbCrLf)
        Print(1, "VRearDatum=" + prfVRearDatum.ToString + vbCrLf)
        Print(1, "VBladeDia=" + prfVBladeDia.ToString + vbCrLf)
        Print(1, "PushGripperMinY=" + prfPushGripperMinY.ToString + vbCrLf)
        Print(1, "PushGripperMaxY=" + prfPushGripperMaxY.ToString + vbCrLf)
        Print(1, "PushGripperMinZ=" + prfPushGripperMinZ.ToString + vbCrLf)
        Print(1, "PushGripperMaxZ=" + prfPushGripperMaxZ.ToString + vbCrLf)
        Print(1, "PushGripperHeight=" + prfPushGripperHeight.ToString + vbCrLf)
        Print(1, "PushGripperWidth=" + prfPushGripperWidth.ToString + vbCrLf)
        Print(1, "PullGripperMinY=" + prfPullGripperMinY.ToString + vbCrLf)
        Print(1, "PullGripperMaxY=" + prfPullGripperMaxY.ToString + vbCrLf)
        Print(1, "PullGripperMinZ=" + prfPullGripperMinZ.ToString + vbCrLf)
        Print(1, "PullGripperMaxZ=" + prfPullGripperMaxZ.ToString + vbCrLf)
        Print(1, "PullGripperHeight=" + prfPullGripperHeight.ToString + vbCrLf)
        Print(1, "PullGripperZPos=" + prfPullGripperZPos.ToString + vbCrLf)
        Print(1, "ZBlock1=" + prfZBlock1.ToString + vbCrLf)
        Print(1, "ZBlock2=" + prfZBlock2.ToString + vbCrLf)
        Print(1, "ZBlock3=" + prfZBlock3.ToString + vbCrLf)
        Print(1, "ZBlock4=" + prfZBlock4.ToString + vbCrLf)
        Print(1, "VCylinder=" + prfVCylinder.ToString + vbCrLf)
        Print(1, "TipToCollet=" + prfTipToCollet.ToString + vbCrLf)


        FileClose(1)

    End Sub

#End Region

    'Clipboard
#Region " Sub - ImportClipboardLine() "

    Sub ImportClipboardLine(ByVal sLine As String, ByVal sProfile As String)

        If Mid(sLine, 1, 4) = "OpID" Then Exit Sub

        'MsgBox(sLine)

        'Variable Declaration
        Dim iOpID As Integer = 0
        Dim sProfileCode As String = ""
        Dim sToolCode As String = ""
        Dim sStdOpCode As String = ""
        Dim bUse As Boolean = True
        Dim sDescription As String = ""
        Dim dVar1 As Double = 0
        Dim dVar2 As Double = 0
        Dim dVar3 As Double = 0
        Dim dVar4 As Double = 0
        Dim dVar5 As Double = 0
        Dim dVar6 As Double = 0
        Dim dVar7 As Double = 0
        Dim dVar8 As Double = 0

        'split the clipboard line into individual fields separated by tabs
        Dim sSplitLine() As String = sLine.Split(vbTab)

        'If the line does not have the corrwct number of fields, cancel the routine
        If sSplitLine.Length <> 14 Then Exit Sub

        'Generate Insert Row
        Dim rInsertRow As DataRow = tblStdOps.NewRow

        'Get new line number
        Dim iMax As Integer = GetMaxSTDOpID()
        iMax += 1

        'Get OpID
        iOpID = sSplitLine(0)
        rInsertRow.Item("OpID") = iMax

        'Get ProfileCode
        sProfileCode = sSplitLine(1)
        If sProfile <> "" Then
            rInsertRow.Item("ProfileCode") = sProfile
        Else
            rInsertRow.Item("ProfileCode") = sProfileCode
        End If

        'Get Tool Code
        sToolCode = sSplitLine(2)
        rInsertRow.Item("OpToolCode") = sToolCode

        'Get StdOp Code
        sStdOpCode = sSplitLine(3)
        rInsertRow.Item("StdOpCode") = sStdOpCode

        'Get Use
        bUse = CBool(sSplitLine(4))
        rInsertRow.Item("Use") = bUse

        'Get Description
        sDescription = sSplitLine(5)
        rInsertRow.Item("OpDescription") = sDescription

        'Get Var 1
        dVar1 = CDbl(sSplitLine(6))
        rInsertRow.Item("OpVariable1") = dVar1

        'Get Var 2
        dVar2 = CDbl(sSplitLine(7))
        rInsertRow.Item("OpVariable2") = dVar2

        'Get Var 3
        dVar3 = CDbl(sSplitLine(8))
        rInsertRow.Item("OpVariable3") = dVar3

        'Get Var 4
        dVar4 = CDbl(sSplitLine(9))
        rInsertRow.Item("OpVariable4") = dVar4

        'Get Var 5
        dVar5 = CDbl(sSplitLine(10))
        rInsertRow.Item("OpVariable5") = dVar5

        'Get Var 6
        dVar6 = CDbl(sSplitLine(11))
        rInsertRow.Item("OpVariable6") = dVar6

        'Get Var 7
        dVar7 = CDbl(sSplitLine(12))
        rInsertRow.Item("OpVariable7") = dVar7

        'Get Var 8
        dVar8 = CDbl(sSplitLine(13))
        rInsertRow.Item("OpVariable8") = dVar8

        'Add New Row to Table
        tblStdOps.Rows.Add(rInsertRow)

    End Sub

#End Region
#Region " Sub - ImportProfileClipboardLine() "

    Sub ImportProfileClipboardLine(ByVal sLine As String)

        If Mid(sLine, 1, 8) = "CodeName" Then Exit Sub

        'MsgBox(sLine)

        'Variable Declaration
        Dim sCodeName As String = ""
        Dim iZSection As Integer = 0
        Dim sFlip As String = ""
        Dim dWidth As Double = 0
        Dim dGripPosition As Double = 0
        Dim dStdLength As Double = 6000
        Dim iReverseLoad As Integer = 0
        Dim dZRebate As Double = 0
        Dim dHeight As Double = 0
        Dim dEuroHeight As Double = 0
        Dim sSystem As String = ""
        Dim sProfileName As String = ""
        Dim sReplicateID As String = ""
        Dim iProfileTypeID As Integer = 0
        Dim sNeuronString As String = ""
        Dim iCentralise As Integer = 0
        Dim iOptTol As Integer = 0
        Dim iMinOffcut As Integer = 0
        Dim iGripY As Integer = 0
        Dim iGripZ As Integer = 0
        Dim iID As Integer = 0
        Dim iCoreColour As Integer = 0
        Dim iSawCutStyle As Integer = 0

        'split the clipboard line into individual fields separated by tabs
        Dim sSplitLine() As String = sLine.Split(vbTab)

        'If the line does not have the corrwct number of fields, cancel the routine
        Dim iSplitCount As Integer = sSplitLine.Length
        If iSplitCount <> 14 And iSplitCount <> 15 And iSplitCount <> 23 Then Exit Sub

        'Generate Insert Row
        Dim rInsertRow As DataRow = tblProfile.NewRow

        'Get CodeName
        sCodeName = sSplitLine(0)
        rInsertRow.Item("CodeName") = sCodeName

        'Get ZSection
        iZSection = CInt(sSplitLine(1))
        rInsertRow.Item("ZSection") = iZSection

        'Get Flip
        sFlip = sSplitLine(2)
        rInsertRow.Item("Flip") = sFlip

        'Get Width
        dWidth = CDbl(sSplitLine(3))
        rInsertRow.Item("Width") = dWidth

        'Get GripPosition
        dGripPosition = CDbl(sSplitLine(4))
        rInsertRow.Item("GripPosition") = dGripPosition

        'Get StdLength
        dStdLength = CDbl(sSplitLine(5))
        rInsertRow.Item("StdLength") = dStdLength

        'Get ReverseLoad
        iReverseLoad = CInt(sSplitLine(6))
        rInsertRow.Item("ReverseLoad") = iReverseLoad

        'Get ZRebate
        dZRebate = CDbl(sSplitLine(7))
        rInsertRow.Item("ZRebate") = dZRebate

        'Get Height
        dHeight = CDbl(sSplitLine(8))
        rInsertRow.Item("Height") = dHeight

        'Get EuroHeight
        dEuroHeight = CDbl(sSplitLine(9))
        rInsertRow.Item("EuroHeight") = dEuroHeight

        'Get System
        sSystem = sSplitLine(10)
        rInsertRow.Item("System") = sSystem

        'Get ProfileName
        sProfileName = sSplitLine(11)
        rInsertRow.Item("ProfileName") = sProfileName

        'Get ReplicateID
        sReplicateID = sSplitLine(12)
        rInsertRow.Item("ReplicateID") = sReplicateID

        'Get ProfileTypeID
        iProfileTypeID = CInt(sSplitLine(13))
        rInsertRow.Item("ProfileTypeID") = iProfileTypeID

        'Get NeuronStr
        If iSplitCount = 15 Then
            sNeuronString = sSplitLine(14)
            rInsertRow.Item("NeuronStr") = sNeuronString
        Else
            rInsertRow.Item("NeuronStr") = ""
        End If

        'Get Centralise
        If iSplitCount > 15 Then
            iCentralise = CInt(sSplitLine(15))
            rInsertRow.Item("Centralise") = iCentralise
        Else
            rInsertRow.Item("Centralise") = 0
        End If

        'Get OptTol
        If iSplitCount > 15 Then
            iOptTol = CInt(sSplitLine(16))
            rInsertRow.Item("OptTol") = iOptTol
        Else
            rInsertRow.Item("OptTol") = 500
        End If

        'Get MinOffcut
        If iSplitCount > 15 Then
            iMinOffcut = CInt(sSplitLine(17))
            rInsertRow.Item("MinOffcut") = iMinOffcut
        Else
            rInsertRow.Item("MinOffcut") = 700
        End If

        'Get GripY
        If iSplitCount > 15 Then
            iGripY = CInt(sSplitLine(18))
            rInsertRow.Item("GripY") = iGripY
        Else
            rInsertRow.Item("GripY") = 0
        End If

        'Get GripZ
        If iSplitCount > 15 Then
            iGripZ = CInt(sSplitLine(19))
            rInsertRow.Item("GripZ") = iGripZ
        Else
            rInsertRow.Item("GripZ") = 60
        End If

        'Get ID
        If iSplitCount > 15 Then
            iID = CInt(sSplitLine(20))
            rInsertRow.Item("ID") = iID
        Else
            rInsertRow.Item("ID") = -1
        End If

        'Get CoreColour
        If iSplitCount > 15 Then
            iCoreColour = CInt(sSplitLine(21))
            rInsertRow.Item("CoreColour") = iCoreColour
        Else
            rInsertRow.Item("CoreColour") = -1
        End If

        'Get SawCutStyle
        If iSplitCount > 15 Then
            iSawCutStyle = CInt(sSplitLine(22))
            rInsertRow.Item("SawCutStyle") = iSawCutStyle
        Else
            rInsertRow.Item("SawCutStyle") = 0
        End If

        'Add New Row to Table
        tblProfile.Rows.Add(rInsertRow)

    End Sub

#End Region

#Region " Sub - ImportMissingOp() "

    Sub ImportMissingOp(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs, ByVal dgv As System.Windows.Forms.DataGridView)

        'Find Row an Column Which Caused the error
        Dim iColumn As Integer = e.ColumnIndex
        Dim iRow As Integer = e.RowIndex

        Select Case iColumn

            Case 3          'if Stdop Column is at fault

                'Get the Cell Value for the StdOp
                Dim sStdOpName As String = dgv.Rows(iRow).Cells.Item(iColumn).Value
                Dim sLine As String() = {"", "", 0, 0, 0, 0, 0, 0, 0, 0, 0}

                'Ask the User if whether to import the data
                Dim mbr As MsgBoxResult = MsgBox(sStdOpName + " is not defined " + vbCrLf + "Would you like to import from another machine?", MsgBoxStyle.YesNo)
                If mbr = MsgBoxResult.No Then Exit Sub

                Dim fod As New OpenFileDialog
                fod.Filter = "Stuga OpDef|zstdopdef.mul|All Files (*.*)|*.*"

                Dim dres As DialogResult = fod.ShowDialog
                If dres = DialogResult.Cancel Then Exit Sub

                Dim sPath As String = fod.FileName
                FileOpen(1, sPath, OpenMode.Input, OpenAccess.Read, OpenShare.Default)

                Do While Not EOF(1)
                    Dim s As String = LineInput(1)
                    If Mid(s.ToUpper, 1, Len(sStdOpName) + 1) = sStdOpName.ToUpper + "," Then
                        sLine = s.Split(",")
                        Exit Do
                    End If
                Loop

                FileClose(1)

                Dim sStdOpCode As String = sLine(0)
                Dim sStdOpDescription As String = sLine(1)
                Dim sStdOpType As Integer = sLine(2)
                Dim sStdOpVar1 As String = sLine(3)
                Dim sStdOpVar2 As String = sLine(4)
                Dim sStdOpVar3 As String = sLine(5)
                Dim sStdOpVar4 As String = sLine(6)
                Dim sStdOpVar5 As String = sLine(7)
                Dim sStdOpVar6 As String = sLine(8)
                Dim sStdOpVar7 As String = sLine(9)
                Dim sStdOpVar8 As String = sLine(10)

                'add new Edtry to the Operation Definitions
                Dim rRow As DataRow
                rRow = tblStdOpDef.NewRow
                rRow.Item("StdOpCode") = sStdOpCode
                rRow.Item("StdOpDescription") = sStdOpDescription
                rRow.Item("StdOpType") = sStdOpType
                rRow.Item("StdOpVar1") = sStdOpVar1
                rRow.Item("StdOpVar2") = sStdOpVar2
                rRow.Item("StdOpVar3") = sStdOpVar3
                rRow.Item("StdOpVar4") = sStdOpVar4
                rRow.Item("StdOpVar5") = sStdOpVar5
                rRow.Item("StdOpVar6") = sStdOpVar6
                rRow.Item("StdOpVar7") = sStdOpVar7
                rRow.Item("StdOpVar8") = sStdOpVar8
                tblStdOpDef.Rows.Add(rRow)

                ''Update Systems.Mul
                SaveZStdOpDefMul()

                'Calculate the source and destination of the MND Files
                Dim iLoc As Integer = InStr(sPath.ToUpper, "ZSTDOPDEF.MUL", CompareMethod.Text)
                Dim sSourceOpDesignPath As String = Mid(sPath, 1, iLoc - 1) + "Opdesign\"
                Dim sSource As String = sSourceOpDesignPath + sStdOpCode + ".mnd"
                Dim sDest As String = prfOpdesignLoc + sStdOpCode + ".mnd"

                'Check the Input Opdesign folder Exists
                If Not Directory.Exists(sSourceOpDesignPath) Then
                    MsgBox("Cannot find the Source Opdesign Folder" + vbCrLf + "The MND Files will need to be copied manually")
                    Exit Sub
                End If

                'Copy MND File to the new machine
                If File.Exists(sSource) Then File.Copy(sSource, sDest, True)

                'Copy Tilde File to the new machine
                Dim sTildeName As String = "~" + Mid(sStdOpCode, 2)
                Dim sTildeSource As String = sSourceOpDesignPath + sTildeName + ".mnd"
                Dim sTildeDest As String = prfOpdesignLoc + sTildeName + ".mnd"

                If File.Exists(sTildeSource) Then File.Copy(sTildeSource, sTildeDest)

            Case Else

                'Display the error message
                MsgBox(e.Exception.Message)

        End Select

    End Sub

#End Region
#Region " Sub - CreateAHAdjust "

    Sub CreateAHAdjust()

        Dim ahLoc As String = prfProfileMulLoc
        Dim iLoc As Integer = InStr(ahLoc.ToUpper, "PROFILE.MUL", CompareMethod.Text)
        ahLoc = Mid(ahLoc, 1, iLoc - 1) + "AHAdjust.mul"

        'Check OK to Continue
        Dim mbr As MsgBoxResult = MsgBox("Are You Sure?", MsgBoxStyle.YesNo)
        If mbr = MsgBoxResult.No Then Exit Sub

        'Try to Create File
        Try
            FileOpen(1, ahLoc, OpenMode.Output, OpenAccess.Default, OpenShare.Default)
        Catch ex As Exception
            MsgBox("Can not create AHAdjust.mul" + vbCrLf + ex.Message)
            Exit Sub
        End Try


        'Print Profiles
        For Each r As DataRow In tblProfile.Rows

            Dim sLine As String = ""

            Dim sProfile As String = r.Item("CodeName")
            Dim iType As Integer = r.Item("ProfileTypeID")

            If iType = 3 Or iType = 11 Then
                sLine = sProfile + ",0.0" + vbCrLf
            End If

            Print(1, sLine)

        Next

        'Close File
        FileClose(1)

        'Inform User of Completion
        MsgBox("AHAdjust File Created", MsgBoxStyle.Information)


    End Sub

#End Region

    Sub AddLogRecord(sProfile As String, sToolCode As String, sDesc As String)

        FileOpen(99, Application.StartupPath + "\FlowOpsLog.mul", OpenMode.Append)

        Print(99, Format(Now, "dd/MMM/yyyy HH:mm:ss") + "," + prfAccess + "," + sProfile + "," + sToolCode + "," + sDesc + vbCrLf)

        FileClose(99)

    End Sub

End Module
