﻿Imports System.IO
Public Class FrmMaster

#Region " Form - Load "

    Private Sub FrmMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Text = Me.Text + " " + My.Application.Info.Version.Major.ToString + "." + My.Application.Info.Version.Minor.ToString + "." + My.Application.Info.Version.Build.ToString + "." + My.Application.Info.Version.Revision.ToString

        'Read Settings Table
        If File.Exists(prfSettingsPath) Then
            FileOpen(1, prfSettingsPath, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

            Do While Not EOF(1)

                Dim s As String = LineInput(1)

                'Get Profile.MUL Location
                If Mid(s, 1, 18) = "ProfileMulLoc:    " Then
                    prfProfileMulLoc = Mid(s, 19)
                End If

                'Get StdOps.MUL Location
                If Mid(s, 1, 18) = "StdOpsMulLoc:     " Then
                    prfStdOpsMulLoc = Mid(s, 19)
                End If

                'Get ZStdOpDef.MUL Location
                If Mid(s, 1, 18) = "ZStdOpDefMulLoc:  " Then
                    prfZStdOpDefMulLoc = Mid(s, 19)
                End If

                'Get OpTypes.MUL Location
                If Mid(s, 1, 18) = "OpTypesMulLoc:    " Then
                    prfOpTypesLoc = Mid(s, 19)
                End If

                'Get Hardware.MUL Location
                If Mid(s, 1, 18) = "HardwareMulLoc:   " Then
                    prfHardwareLoc = Mid(s, 19)
                End If

                'Get Preps.MUL Location
                If Mid(s, 1, 18) = "PrepsMulLoc:      " Then
                    prfPrepsLoc = Mid(s, 19)
                End If

                'Get Systems.MUL Location
                If Mid(s, 1, 18) = "SystemsMulLoc:    " Then
                    prfSystemsLoc = Mid(s, 19)
                End If

                'Get Profile.SAW Location
                If Mid(s, 1, 18) = "ProfileSawLoc:    " Then
                    prfProfileSawLoc = Mid(s, 19)
                End If

                'Get OpDesign Folder Location
                If Mid(s, 1, 18) = "OpdesignFolderLoc:" Then
                    prfOpdesignLoc = Mid(s, 19)
                End If

                'Get Batch Folder Location
                If Mid(s, 1, 18) = "BatchFolderLoc:   " Then
                    prfBatchLoc = Mid(s, 19)
                End If

                'Get DXF Folder Location
                If Mid(s, 1, 18) = "DXFFolderLoc:     " Then
                    prfDXFLoc = Mid(s, 19)
                End If

                'Get Diagnose Folder Location
                If Mid(s, 1, 18) = "DiagnoseFolderLoc:" Then
                    prfDiagnoseLoc = Mid(s, 19)
                End If

                'Use Neuron?
                If Mid(s, 1, 18) = "UseNeuron:        " Then
                    prfUseNeuron = Mid(s, 19)
                End If


            Loop

            FileClose(1)
        Else

            WriteSettings()

        End If

        'Load User Password
        If File.Exists(prfPasswordPath) Then
            ReadPWL()
        Else
            WritePWL()
        End If

        'If Password is Blank then get a new one
        If prfPassword.ToUpper = "BLANK" Then

            Dim frmpass As New frmUserPass
            frmpass.Show()

        End If

        If Directory.Exists(prfDiagnoseLoc) Then
            tsTimings.Visible = True
            ToolStripSeparator2.Visible = True
        Else
            tsTimings.Visible = False
            ToolStripSeparator2.Visible = False
        End If


        'Read Draw.Mul
        Dim DrawLoc As String = Mid(prfProfileMulLoc, 1, Len(prfProfileMulLoc) - 11) + "draw.mul"
        If File.Exists(DrawLoc) Then
            ReadDrawMul()
        Else
            CreateDrawMul()
        End If

        conString = "Data Source=.\SQLEXPRESS;AttachDbFilename='" + prfDataLoc + "';Integrated Security=True;Connect Timeout=30;User Instance=True;Asynchronous Processing=True"

        'Get Profile Data from Profile.Mul
        tblProfile = GetProfileTable()
        dsProfile.Tables.Add(tblProfile)

        ' check the validity of the stdOpsTable
        If Not checkChecksum() Then
            MsgBox("Operations file is not valid. To import this data you must import from the Administrator Settings Screen")
            stdOpsValid = False
            Exit Sub
        Else
            'Get Operation Data from StdOps.Mul
            tblStdOps = GetStdOpsTable()
            dsStdOps.Tables.Add(tblStdOps)
            stdOpsValid = True
        End If

        'Get Op Definition Data from ZStdOpDef.Mul
        tblStdOpDef = GetStdOpDefTable()
        dsStdOpDef.Tables.Add(tblStdOpDef)

        'Get System Data from Database
        tblSystems = GetTblSystems()
        dsSystems.Tables.Add(tblSystems)

        'Get Profile Types
        tblProfileTypes = CreateTblProfileTypes()
        dsProfileTypes.Tables.Add(tblProfileTypes)

        'Get Hardware
        tblHardware = CreateTblHardware()
        dsHardware.Tables.Add(tblHardware)

        'Get Preps
        tblPreps = GetPrepsTable()
        dsPreps.Tables.Add(tblPreps)

        'Get Op Types
        tblOpTypes = GetOpTypesTable()
        dsOpTypes.Tables.Add(tblOpTypes)

    End Sub

#End Region

#Region " ToolStrip - Edit Machining "

    Private Sub tsEditMachining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsEditMachining.Click

        EditMachining()

    End Sub

#End Region
#Region " ToolStrip - Edit Profiles "
    Private Sub tsEditProfiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsEditProfiles.Click

        EditProfiles()

    End Sub

#End Region
#Region " ToolStrip - Piece Generator "

    Private Sub tsPieceGen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsPieceGen.Click

        PieceGenerator()

    End Sub

#End Region
#Region " ToolStrip - Test Generator "

    Private Sub tsTestGen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsTestGen.Click

        TestGenerator()

    End Sub

#End Region
#Region " ToolStrip - Unlock "

    Private Sub tsUnLock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsUnLock.Click

        UnlockPassword()

    End Sub

#End Region
#Region " Toolstrip - Lock "

    Private Sub tsLock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsLock.Click

        Lock()

    End Sub

#End Region
#Region " ToolStrip - Administrator Access "

    Private Sub tsEngineer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsEngineer.Click

        AdminAccess()

    End Sub

#End Region
#Region " ToolStrip - Settings "

    Private Sub tsSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsSettings.Click

        Settings()

    End Sub

#End Region
#Region " ToolStrip - Backup "

    Private Sub tsBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsBackup.Click

        Backup()

    End Sub

#End Region
#Region " ToolStrip - Exit to Windows "

    Private Sub tsExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsExit.Click

        ExitToWindows()

    End Sub

#End Region
#Region " ToolStrip - Timings "

    Private Sub tsTimings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsTimings.Click

        Usage()

    End Sub

#End Region

#Region " Menu - Edit Machining "

    Private Sub EditMachiningToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditMachiningToolStripMenuItem.Click

        EditMachining()

    End Sub

#End Region
#Region " Menu - Edit Profiles "

    Private Sub EditProfilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditProfilesToolStripMenuItem.Click

        EditProfiles()

    End Sub

#End Region
#Region " Menu - Piece Generator "

    Private Sub PieceGeneratorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PieceGeneratorToolStripMenuItem.Click

        PieceGenerator()

    End Sub

#End Region
#Region " Menu - Test Generator "

    Private Sub TestGeneratorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestGeneratorToolStripMenuItem.Click

        TestGenerator()

    End Sub

#End Region
#Region " Menu - Unlock "

    Private Sub UnlockToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnlockToolStripMenuItem.Click

        UnlockPassword()

    End Sub

#End Region
#Region " Menu - Lock "

    Private Sub LockToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LockToolStripMenuItem.Click

        Lock()

    End Sub

#End Region
#Region " Menu - Administrator Access "

    Private Sub AdministratorAccessToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdministratorAccessToolStripMenuItem.Click

        AdminAccess()

    End Sub

#End Region
#Region " Menu - Settings "

    Private Sub SettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SettingsToolStripMenuItem.Click

        Settings()

    End Sub

#End Region
#Region " Menu - Backup "

    Private Sub BackupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackupToolStripMenuItem.Click

        Backup()

    End Sub

#End Region
#Region " Menu - Exit to Windows "

    Private Sub ExitToWindowsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToWindowsToolStripMenuItem.Click

        ExitToWindows()

    End Sub

#End Region
#Region " Menu - Timings "

    Private Sub UsageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsageToolStripMenuItem.Click

        Usage()

    End Sub

#End Region

#Region " Sub - EditMachining() "

    Sub EditMachining()

        If Not stdOpsValid Then
            MsgBox("There is no valid machining data loaded. Import data via the Settings Screen (Unlock Required)")
            Exit Sub
        End If

        If prfPassword.ToUpper = "NONE" Then
            Dim frmEditMachining As New frmEditMachining
            frmEditMachining.Show()
        Else
            Dim frmPass As New frmUserAccess
            frmPass.sDestination = "EditMachining"
            frmPass.Show()
        End If


    End Sub

#End Region
#Region " Sub - EditProfiles() "

    Sub EditProfiles()

        If Not stdOpsValid Then
            MsgBox("There is no valid machining data loaded. Import data via the Settings Screen (Unlock Required)")
            Exit Sub
        End If

        If prfPassword.ToUpper = "NONE" Then
            Dim frmEditProfiles As New frmEditProfiles
            frmEditProfiles.Show()
        Else
            Dim frmPass As New frmUserAccess
            frmPass.sDestination = "EditProfiles"
            frmPass.Show()
        End If

    End Sub

#End Region
#Region " Sub - PieceGenerator() "

    Sub PieceGenerator()

        If Not stdOpsValid Then
            MsgBox("There is no valid machining data loaded. Import data via the Settings Screen (Unlock Required)")
            Exit Sub
        End If

        Dim frmPieceGen As New frmPieceGen
        frmPieceGen.Show()

    End Sub

#End Region
#Region " Sub - TestGenerator() "

    Sub TestGenerator()

        If Not stdOpsValid Then
            MsgBox("There is no valid machining data loaded. Import data via the Settings Screen (Unlock Required)")
            Exit Sub
        End If

        Dim frmTestGen As New frmTestGen
        frmTestGen.Show()

    End Sub

#End Region
#Region " Sub - UnlockPassword() "

    Sub UnlockPassword()

        Dim frm As New frmPassword
        frm.Show()

    End Sub

#End Region
#Region " Sub - AdminAccess() "

    Sub AdminAccess()

        If Not stdOpsValid Then
            MsgBox("There is no valid machining data loaded. Import data via the Settings Screen (Unlock Required)")
            Exit Sub
        End If

        Dim frmEngineer As New frmEngineer
        frmEngineer.Show()

    End Sub

#End Region
#Region " Sub - Settings() "

    Sub Settings()

        Dim frmSettings As New frmSettings
        frmSettings.Show()

    End Sub

#End Region
#Region " Sub - Backup() "

    Sub Backup()

        'Display Folder Browser Dialog
        Dim fbd As New FolderBrowserDialog
        fbd.Description = "Please Select the Backup Folder"
        Dim mbr As System.Windows.Forms.DialogResult = fbd.ShowDialog
        If mbr <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        End If

        'Get Backup Path
        Dim sPath As String = fbd.SelectedPath
        If Mid(sPath, Len(sPath), 1) <> "\" Then sPath += "\"

        'Create Backup Folder
        Dim sDay As String = Format(Now(), "dd")
        Dim sMonth As String = Format(Now(), "MM")
        Dim sYear As String = Format(Now(), "yyyy")
        Dim sNewFolder As String = sPath + "Backup" + sDay + sMonth + sYear
        If Not Directory.Exists(sNewFolder) Then
            Try
                MkDir(sNewFolder)
            Catch ex As Exception
                MsgBox("Could not Create Backup Folder", MsgBoxStyle.Exclamation)
                Exit Sub
            End Try
        End If
        sNewFolder += "\"

        'Find Mul File Locations
        Dim iLoc As Integer = InStr(prfProfileMulLoc.ToUpper, "PROFILE.MUL", CompareMethod.Text)
        Dim sMulLoc As String = Mid(prfProfileMulLoc, 1, iLoc - 1)
        Dim di As New DirectoryInfo(sMulLoc)
        Dim fi As FileInfo() = di.GetFiles("*.mul")

        'Copy Mul Files from Mul Loc
        Dim sFrom As String = ""
        Dim sTo As String = ""
        For Each f As FileInfo In fi
            sFrom = sMulLoc + f.Name
            sTo = sNewFolder + f.Name
            File.Copy(sFrom, sTo, True)
        Next

        'Copy Saw Files from MUL Loc
        fi = di.GetFiles("*.saw")
        For Each f As FileInfo In fi
            sFrom = sMulLoc + f.Name
            sTo = sNewFolder + f.Name
            File.Copy(sFrom, sTo, True)
        Next

        'Copy zft Files from MUL Loc
        fi = di.GetFiles("*.zft")
        For Each f As FileInfo In fi
            sFrom = sMulLoc + f.Name
            sTo = sNewFolder + f.Name
            File.Copy(sFrom, sTo, True)
        Next

        'Make Saw Folder
        If Not Directory.Exists(sNewFolder + "Saw") Then
            Try
                MkDir(sNewFolder + "Saw")
            Catch ex As Exception
                MsgBox("Could not Create Saw Folder", MsgBoxStyle.Exclamation)
                Exit Sub
            End Try
        End If

        'Get Saw File Location
        iLoc = InStr(prfProfileSawLoc.ToUpper, "PROFILE.SAW", CompareMethod.Text)
        Dim sSawLoc As String = Mid(prfProfileSawLoc, 1, iLoc - 1)
        di = New DirectoryInfo(sSawLoc)
        fi = di.GetFiles("*.saw")

        'Check if Profile.saw Exists
        Try
            If Directory.Exists(sSawLoc) Then

                'Copy Saw Files
                For Each f As FileInfo In fi
                    sFrom = sSawLoc + f.Name
                    sTo = sNewFolder + "Saw\" + f.Name
                    File.Copy(sFrom, sTo, True)
                Next

                'Copy ZFT Files
                fi = di.GetFiles("*.ZFT")
                For Each f As FileInfo In fi
                    sFrom = sSawLoc + f.Name
                    sTo = sNewFolder + "Saw\" + f.Name
                    File.Copy(sFrom, sTo, True)
                Next
            End If
        Catch ex As Exception

        End Try

        'Make Opdesign Folder
        If Not Directory.Exists(sNewFolder + "Opdesign") Then
            Try
                MkDir(sNewFolder + "Opdesign")
            Catch ex As Exception
                MsgBox("Could not Create Opdesign Folder", MsgBoxStyle.Exclamation)
                Exit Sub
            End Try
        End If

        'Get MND File Location
        Dim sMNDLoc As String = prfOpdesignLoc
        di = New DirectoryInfo(sMNDLoc)
        fi = di.GetFiles("*.mnd")
        If Mid(sMNDLoc, Len(sMNDLoc), 1) <> "\" Then sMNDLoc += "\"

        'Copy MND Files
        For Each f As FileInfo In fi
            sFrom = sMNDLoc + f.Name
            sTo = sNewFolder + "Opdesign\" + f.Name
            File.Copy(sFrom, sTo, True)
        Next

        'Make DXF Folder
        If Not Directory.Exists(sNewFolder + "DXF") Then
            Try
                MkDir(sNewFolder + "DXF")
            Catch ex As Exception
                MsgBox("Could not Create DXF Folder", MsgBoxStyle.Exclamation)
                Exit Sub
            End Try
        End If

        'Get DXF Location
        Dim sDXFLoc As String = prfDXFLoc
        di = New DirectoryInfo(sDXFLoc)
        fi = di.GetFiles("*.dxf")
        If Mid(sDXFLoc, Len(sDXFLoc), 1) <> "\" Then sDXFLoc += "\"

        'Copy DXF Files
        For Each f As FileInfo In fi
            sFrom = sDXFLoc + f.Name
            sTo = sNewFolder + "DXF\" + f.Name
            File.Copy(sFrom, sTo, True)
        Next

        'Copy Additional Files from Backup.MUL
        If File.Exists(sMulLoc + "Backup.mul") Then
            FileOpen(1, sMulLoc + "Backup.mul", OpenMode.Input, OpenAccess.Default, OpenShare.Default)
            Do While Not (EOF(1))

                Dim sBackup As String = LineInput(1)
                Dim sBackupArray As String() = sBackup.Split(",")
                Dim sFromFolder As String = sBackupArray(0)
                Dim sExtension As String = sBackupArray(1)
                Dim sToFolder As String = sBackupArray(2)

                If Mid(sFromFolder, Len(sFromFolder)) <> "\" Then sFromFolder += "\"

                'Make Output Folder
                If Not Directory.Exists(sNewFolder + sToFolder) Then
                    Try
                        MkDir(sNewFolder + sToFolder)
                    Catch ex As Exception
                        MsgBox("Could not Create " + sToFolder + " Folder", MsgBoxStyle.Exclamation)
                        Exit Sub
                    End Try
                End If

                'Copy Files
                di = New DirectoryInfo(sFromFolder)
                fi = di.GetFiles("*." + sExtension)
                For Each f As FileInfo In fi
                    sFrom = sFromFolder + f.Name
                    sTo = sNewFolder + sToFolder + "\" + f.Name
                    File.Copy(sFrom, sTo, True)
                Next

            Loop

            FileClose(1)

        End If

        MsgBox("Files Copied", MsgBoxStyle.Information)

    End Sub

#End Region
#Region " Sub - Usage() "

    Sub Usage()

        Dim frmTimings As New frmTimings
        frmTimings.Show()

    End Sub

#End Region
#Region " Sub - ExitToWindows() "

    Sub ExitToWindows()

        'Write Draw.Mul
        CreateDrawMul()

        Me.Close()

    End Sub

#End Region
#Region " Sub - Unlock() "

    Sub Unlock()

        Me.txtAccess.Text = "Administrator"
        prfAccess = "Administrator"
        tsEngineer.Enabled = True
        tsSettings.Enabled = True
        tsLock.Enabled = True
        tsUnLock.Enabled = False
        AdministratorAccessToolStripMenuItem.Enabled = True
        SettingsToolStripMenuItem.Enabled = True
        UnlockToolStripMenuItem.Enabled = False
        LockToolStripMenuItem.Enabled = True

    End Sub

#End Region
#Region " Sub - Lock() "

    Sub Lock()

        Me.txtAccess.Text = "User"
        prfAccess = "User"
        tsEngineer.Enabled = False
        tsSettings.Enabled = False
        tsUnLock.Enabled = True
        tsLock.Enabled = False
        AdministratorAccessToolStripMenuItem.Enabled = False
        SettingsToolStripMenuItem.Enabled = False
        UnlockToolStripMenuItem.Enabled = True
        LockToolStripMenuItem.Enabled = False

    End Sub

#End Region

End Class