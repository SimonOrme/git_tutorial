﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Stuga Flowline Ops")> 
<Assembly: AssemblyDescription("Stuga Programming Editor")> 
<Assembly: AssemblyCompany("Stuga Machinery")> 
<Assembly: AssemblyProduct("Stuga Flowline Ops")> 
<Assembly: AssemblyCopyright("Copyright ©  2019")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f1706797-e3b9-447f-8146-48e511d94e47")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.21.0.0")> 
<Assembly: AssemblyFileVersion("1.21.0.0")> 
