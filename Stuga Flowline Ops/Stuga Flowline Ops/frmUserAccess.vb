﻿Public Class frmUserAccess

    Public sDestination As String = ""

    Private Sub btnAccess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccess.Click

        If Me.txtPass.Text = prfPassword Then

            Select Case sDestination
                Case "EditMachining"
                    Dim frmEditMachining As New frmEditMachining
                    frmEditMachining.Show()
                Case "EditProfiles"
                    Dim frmEditProfiles As New frmEditProfiles
                    frmEditProfiles.Show()
            End Select

            Me.Close()

        Else

            MsgBox("Incorrect Password", MsgBoxStyle.Exclamation)
            Me.Close()

        End If

    End Sub
End Class