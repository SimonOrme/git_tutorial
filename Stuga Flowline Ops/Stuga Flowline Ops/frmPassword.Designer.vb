﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPassword))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtChallenge = New System.Windows.Forms.Label()
        Me.butOK = New System.Windows.Forms.Button()
        Me.txtPasscode = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(73, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Challenge"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(50, 100)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Enter Unlock Code"
        '
        'txtChallenge
        '
        Me.txtChallenge.AutoSize = True
        Me.txtChallenge.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txtChallenge.Font = New System.Drawing.Font("Courier New", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChallenge.Location = New System.Drawing.Point(45, 45)
        Me.txtChallenge.Name = "txtChallenge"
        Me.txtChallenge.Size = New System.Drawing.Size(111, 32)
        Me.txtChallenge.TabIndex = 2
        Me.txtChallenge.Text = "123456"
        Me.txtChallenge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'butOK
        '
        Me.butOK.Location = New System.Drawing.Point(53, 182)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(95, 30)
        Me.butOK.TabIndex = 4
        Me.butOK.Text = "Ok"
        Me.butOK.UseVisualStyleBackColor = True
        '
        'txtPasscode
        '
        Me.txtPasscode.Font = New System.Drawing.Font("Courier New", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasscode.Location = New System.Drawing.Point(45, 116)
        Me.txtPasscode.Name = "txtPasscode"
        Me.txtPasscode.Size = New System.Drawing.Size(111, 38)
        Me.txtPasscode.TabIndex = 5
        '
        'frmPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(200, 243)
        Me.Controls.Add(Me.txtPasscode)
        Me.Controls.Add(Me.butOK)
        Me.Controls.Add(Me.txtChallenge)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPassword"
        Me.Text = "Unlock Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtChallenge As System.Windows.Forms.Label
    Friend WithEvents butOK As System.Windows.Forms.Button
    Friend WithEvents txtPasscode As System.Windows.Forms.TextBox
End Class
